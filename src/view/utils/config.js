function getServerLocation(){
	return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '') + '/';
}

module.exports = {
    server:"http://api-s.wynvent.com/",
    id:null,
    ip:getServerLocation(),
		s3static:process.env.STATIC_URL
}
