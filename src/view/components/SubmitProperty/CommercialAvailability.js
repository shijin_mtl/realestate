import React from 'react';
import { Radio , Select } from 'antd';
const Option = Select.Option;
var RadioGroup = Radio.Group;
var DatePicker = require("react-bootstrap-date-picker");
import moment from 'moment';


var CommercialAvailability = React.createClass({
   getInitialState:function(){
     return{
     	selectDate:false,
     	immediately:true
     }
   },
  handleChange:function(key,event){
    this.props.changeCommercialAvailability(key,event.target.checked)
  },
  handleChangeSelect:function(key){
    this.props.changeCommercialAvailability(key,false)
  },
  onChange:function(key,value){
  	 value = moment(value).format('YYYY-MM-DD');
     this.props.changeCommercialAvailability(key,value);
  },
  handleChangeRadio:function(key,event){
    this.props.changeCommercialAvailability(key,event.target.value)
  },
  render:function(){
  	return(
  		        
  		        <div className="property-detail-inner-form">
			    <h4>3. Availability</h4>
			    <div className="configuration-details">
			        <div className="row">
			            <div className="col-sm-12">
			                <div className="form-group">
			                    <label  className="col-md-3 control-label">Available from</label>
			                    <div className="col-md-6 col-sm-12">
			                        <div className="row">
			                            <div className="col-md-6 col-sm-12">
			                               
                                          <RadioGroup  onChange={this.handleChangeSelect.bind(this,"available_immediately")} value={this.state.selectDate}>
									        <Radio value={this.props.commercialAvailability.available_immediately}> Select Date</Radio>
                                          </RadioGroup>


			                            </div>
			                            <div className="col-md-6 col-sm-12">
			                                 <DatePicker id="example-datepicker" 
			                                    value={this.props.commercialAvailability.available_from_date} 
			                                    onChange={this.onChange.bind(this,"available_from_date")} />
			                            </div>
			                            <div className="col-md-12 col-sm-12">
			                                
                                            <RadioGroup onChange={this.handleChange.bind(this,"available_immediately")} value={this.state.immediately}>
									          <Radio value={this.props.commercialAvailability.available_immediately}> Immediately</Radio>
                                            </RadioGroup>


			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			        <div className="row">
			            <div className="col-sm-12">
			                <div className="form-group">
			                    <label  className="col-md-3 control-label">Transaction Type</label>
			                    <div className="col-md-6 col-sm-12">

			                        <RadioGroup onChange={this.handleChangeRadio.bind(this,"transaction_type")}>
									     <Radio value="new_property">New Property</Radio>
									     <Radio value="resale">Resale</Radio>
                                    </RadioGroup>



			                    </div>
			                </div>
			            </div>
			        </div>
			     </div>
               </div>
  	)
  }
});
module.exports = CommercialAvailability;