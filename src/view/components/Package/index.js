import React from 'react';



var Package = React.createClass({
  componentWillMount:function(){
     window.scrollTo(0,0);
  },
  render:function(){
  	 return(
            <div className="submit-property-section">
              <div className="container">
              <div className="row">
                <div className="col-sm-12">
                  <ul className="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">More</a></li>
                    <li className="active">Advertise With Us</li>
                  </ul>
                </div>
                </div>

                <div className="row">
                    <div className="col-sm-12">
                        <form className="submit-property-form form-horizontal">
                            <h2 className="text-center">Wynvent Tools.</h2>
                            <h5 className="text-center">Help you win the battle with your mind.</h5>
                        </form>
                    </div>
                </div>
                <hr/>

              <div className="row">
              <div className="col-sm-3">
                <ul className="nav nav-tabs responsive-tabs tabs-left sideways" ref="responsiveTabs">
                    <li className="active"><a data-target="#home1" data-toggle="tab">FOR OWNERS</a>
                    </li>
                    <li><a data-target="#profile1" data-toggle="tab">FOR AGENTS</a>
                    </li>
                    <li><a data-target="#messages1" data-toggle="tab">FOR BUILDERS</a>
                    </li>
                    <li><a data-target="#messages1" data-toggle="tab">FOR BUILDERS</a>
                    </li>
                    <li><a data-target="#messages1" data-toggle="tab">FOR LOAN PROVIDERS</a>
                    </li>
                </ul>

              </div>
                <div className="col-sm-8">
                    <ul className="services-pricing-box">
                        <li className="pricing-box-inner">
                          <div className="head">
                            <h3><span className="blue-ball"></span>Free</h3>
                            <h2>Free</h2>
                            <h5>60 days posting</h5>
                          </div>
                          <ul className="list-group-points">
                            <li className="grey-bg">Up to 10 responses</li>
                            <li>Up to 10 Buyers Details</li>
                            <li className="grey-bg">Normal Visibility</li>
                            <li>-</li>
                            <li className="grey-bg">-</li>
                            <li><a href="#" className="go-free-button">Go Free</a></li>
                          </ul>
                        </li>
                          <li className="pricing-box-inner">
                          <div className="head">
                            <h3><span className="yellow-ball"></span>Premium</h3>

                            <h2><span className="inr-title">INR</span>2000</h2>
                            <h5>120 days posting</h5>
                          </div>
                          <ul className="list-group-points">
                            <li className="grey-bg">Unlimited responses</li>
                            <li>Unlimited Buyers Details</li>
                            <li className="grey-bg">3 times more visibility</li>
                            <li>Verified Tag</li>
                            <li className="grey-bg">Professional Photo Shoot</li>
                            <li><a href="#/address" className="go-premium-button">Go Premium</a></li>
                          </ul>
                        </li>
                    </ul>
                </div>
                <div className="col-sm-1">
                </div>

                </div>
              </div>
            </div>
  	 )
  }
});
module.exports = Package;
