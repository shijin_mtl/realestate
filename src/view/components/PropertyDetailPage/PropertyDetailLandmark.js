import React from 'react';
var PropertyDetailLandmark = React.createClass({
    render:function(){
    	return(
    		    <div className="row">
                {this.props.productLandmark.landmarks_and_neighbourhood && <div className="col-sm-12 landmark">
                    <h3 className="title-type-small">Landmark & Neighbourhood</h3>
                    <p>{this.props.productLandmark.landmarks_and_neighbourhood}</p>
                </div>}
              </div>
    	)
    }
});
module.exports = PropertyDetailLandmark;