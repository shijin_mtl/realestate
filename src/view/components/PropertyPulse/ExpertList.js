import React from 'react';
import Banner from './Banner'
import BackGroundImage from 'public/images/no-photo.jpg';

var ExpertList = React.createClass({
   componentWillMount:function(){
     $(".side-collapse").addClass('in');
      window.scrollTo(0,0);
   },
   render:function(){
      return(
       <div className="col-sm-4 exprcol">
          <div className="expertbox">
             <div className="exprthumb"><img src={this.props.data.image} width="220" alt="" className="img-responsive"/></div>
             <h3>{this.props.data.name}</h3>
             <h4>{this.props.data.area_of_expert}</h4>
          </div>
       </div>
   	 )
   }
});
module.exports = ExpertList;
