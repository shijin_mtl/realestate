import React from 'react';
import moment from 'moment';
import SideHelp from "./SideHelp"
var UserResponse = React.createClass({
   componentDidMount:function(){
     $('.social-dasbd').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots:true,
     });
   },
   hideReponses:function(){
      this.props.hideReponses();
   },
   showResponse:function(){
      var responseList = [];
      var responseArr = this.props.myResponseList;
      for (var i = 0, len = responseArr.length; i < len; i++) {
         if(this.props.responseID == responseArr[i].property ){
       	    responseList.push(
       	    	            <tr key={i}>
                              <td>{responseArr[i].name}</td>
                              <td>{responseArr[i].mobile}</td>
                              <td>{responseArr[i].email}</td>
                              <td>{moment(responseArr[i].timestamp).format("Do MMM YY")}</td>
	                        </tr>
       	    )
            }
       }
       return(responseList)
   },
   render:function(){
   	  return(
   	  	      <div className="profile-response-wrap">
	              <div className="row">
	                  <div className="col-sm-9">
	                      <div className="row respns-tp-info">
	                          <div className="col-sm-12">
	                              <a onClick={this.hideReponses} className="redlnkuppr">Back to listings</a>
	                          </div>
	                          <div className="col-sm-4">Responses for your property</div>
	                          <div className="col-sm-8">
	                              <h3></h3>
	                          </div>
	                      </div>
	                      <div className="table-responsive">
	                          <table className="table response-tbl table-striped table-bordered">
	                              <thead>
	                                  <tr>
	                                      <th>Name</th>
	                                      <th>Mobile</th>
	                                      <th>Email</th>
	                                      <th>Date</th>
	                                  </tr>
	                              </thead>
	                              <tbody>
	                                  {this.showResponse()}
	                              </tbody>
	                          </table>
	                      </div>
	                  </div>
	                  {/*<div className="col-sm-3">
	                      <div className="profl-social">
	                          <h5>WAYS TO IMPROVE YOUR VIEWS</h5>
	                          <SideHelp/>
	                      </div>
	                  </div>*/}
	              </div>
          		</div>
   	  )
   }
});
module.exports = UserResponse;
