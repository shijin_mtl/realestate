import React from 'react';
import BuilderAction from 'components/BuilderList/action/BuilderAction';



var Header = React.createClass({
  getInitialState:function(){
     return{
           searchShow:false,
     }
  },

  getCityFilter:function(){
    var cityVal = $('#location').val()
    BuilderAction.builderList({sort:this.props.sort,page:"1",city:cityVal})
    this.setState({
      searchShow:true
    })

  },
   render:function(){
   	 return(
       <span>
               {/* header */}
                  <ul className="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li className="active">Builders in your city</li>
                  </ul>
                    <div className="agt-top-box">
                       <h2>Builders in your city</h2>
                       <h4>Know the best builders in your city and properties</h4>
                       <hr className="sepline"/> <br/>
                       <div className="selct-ctbox">
                         <div className="row">
                           <div className="col-sm-4 col-xs-12 locn-ctyleft"><span className="locn-ttl-sel"><i className="fa fa-map-marker" aria-hidden="true"></i> Kochi</span></div>
                           <div className="col-sm-4 col-xs-7 locn-txtmid"><input type="text" id="location" className="form-control" placeholder="Enter a city" /></div>
                           <div className="col-sm-4 col-xs-5 locn-btnright"><button className="btn btn-red" onClick={this.getCityFilter}>Find Builders</button></div>
                         </div>
                       </div>
                     </div>
                     {this.state.searchShow && <div className="row text-center"><b><h3>Search Results...</h3></b></div>}
                      <br/>

                {/* end header */}
        </span>
   	 )
   }
});
module.exports = Header;
