import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/BlogDetail/constants/PropertyPulseConstants';
import config from 'utils/config';
import axios from 'axios';

var BlogLatestAction = function(){

}

const blogyApi = axios.create({
  withCredentials: true,
});

BlogLatestAction.prototype = {
	getList:function(params){
		blogyApi.get(config.server + 'api/v1/blog/')
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.BLOG_LATEST_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}

module.exports = new BlogLatestAction();
