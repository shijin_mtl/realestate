import React from 'react';
var Privacy = React.createClass({
  componentWillMount:function(){
     window.scrollTo(0,0);
  },
  render:function(){
  	 return(
  	 	        <section className="common-content">
				  <div className="container">
				    <ul className="breadcrumb">
				      <li><a href="#">Home</a></li>
				      <li className="active">Privacy</li>
				    </ul>
				    <h1>Privacy</h1>
				    <div className="contentbox">


            <div className="row">
						<div className="col-md-1 text-right">
						      <p>1.1.</p>
						</div>
						<div className="col-md-11">
              <p>The Website may require you to create a user account by voluntarily providing your information
                such as your name, email address etc. or by logging in to your account in another service (such
                as Facebook, Twitter and Google, as the Company may in its discretion determine). You may
                further voluntarily provide us with additional information as requested from time to time.</p>
						</div>
            </div>


            <div className="row">
						<div className="col-md-1 text-right">
						      <p>1.2.</p>
						</div>
						<div className="col-md-11">
              <p>We may also track certain information about you based upon your behaviour on the Website.
                We use this information for internal research on our users&#39; demographics, interests, and
                behaviour to better understand, protect and serve our users. This information is compiled and
                analysed on an aggregated basis. This information may include the URL that you just came
                from (whether this URL is on the Website or not), which URL you next go to (whether this
                URL is on the Website or not), your computer browser information, your IP address, and other
                information associated with your interaction with the Website.</p>
						</div>
            </div>

            <div className="row">
						<div className="col-md-1 text-right">
						      <p>1.3.</p>
						</div>
						<div className="col-md-11">
              <p>We also collect and store personal information provided by you from time to time on the
              Website. We only collect and use such information from you that we consider necessary for
              achieving a seamless, efficient and safe experience, customized to your needs including:</p>

              <div className="col-md-1 text-right">
              <p>1.3.1</p>
              </div>
              <div className="col-md-11">
              <p>To communicate necessary account and service related information from time to time;</p>
              </div>

              <div className="col-md-1 text-right">
              <p>1.3.2</p>
              </div>
              <div className="col-md-11">
              <p>To undertake necessary fraud prevention checks, and comply with the security
              standards;</p>
              </div>

              <div className="col-md-1 text-right">
              <p>1.3.3</p>
              </div>
              <div className="col-md-11">
              <p>To comply with applicable laws, rules and regulations</p>
              </div>

              <div className="col-md-1 text-right">
              <p>1.3.4</p>
              </div>
              <div className="col-md-11">
              <p>To enable you to interact with other users of the Website, including pursuant to requests
              by you to contact a third party in relation to a real estate offering; and</p>
              </div>

              <div className="col-md-1 text-right">
              <p>1.3.5</p>
              </div>
              <div className="col-md-11">
              <p>To provide you with information and offers, on updates, on promotions, on related,
              affiliated or associated service providers and partners that we believe would be of
              interest to you.</p>
              </div>

						</div>
            </div>
            <div className="col-md-1 text-right">
                  <p>1.4.</p>
            </div>
            <div className="col-md-11">
              <p>Where any interaction on the Website involves a third party, information pertaining to you as
            collected by the Company and deemed reasonably necessary, may be shared with such third
            party to facilitate such interaction.</p>
            </div>

            <div className="col-md-1 text-right">
                  <p>1.5.</p>
            </div>
            <div className="col-md-11">
              <p>We may also use your contact information to send you offers based on your interests and prior
                activity. The Company may also use contact information internally to direct its efforts for
                Content improvement, to contact you as a survey respondent, to notify you if you win any
                contest; and to send you promotional materials from its contest sponsors or advertisers.</p>
            </div>

            <div className="col-md-1 text-right">
                  <p>1.6.</p>
            </div>
            <div className="col-md-11">
              <p>Notwithstanding anything to the contrary in these Terms of Use, we may disclose your
              information to a third party if we believe that it is reasonably necessary to comply with any law;
              to protect the safety of any person; to address fraud, security or technical issues; or to protect
              our rights and property.</p>
            </div>
            








				    </div>
				  </div>
                </section>
  	 )
  }
});
module.exports = Privacy;
