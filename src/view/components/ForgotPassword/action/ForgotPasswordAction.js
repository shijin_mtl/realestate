import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/ForgotPassword/constants/ForgotPasswordConstant';
import config from 'utils/config';
import axios from 'axios';
var forgotPasswordAction = function(){

}

const forgotApi = axios.create({
  withCredentials: true,
}); 

forgotPasswordAction.prototype = {
	forgotPassword:function(params){
		forgotApi.post(config.server + 'api/v1/reset-password/confirm/',params)
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.FORGOT_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
				
	}
}


module.exports = new forgotPasswordAction();
