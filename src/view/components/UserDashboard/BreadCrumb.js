import React from 'react';
var BreadCrumb = React.createClass({
   render:function(){
   	 return(
   	 	     <ul className="breadcrumb no-print">
            	<li><a href="#">Home</a></li>
             	<li className="active">My Profile</li>
        	 </ul>
   	 )
   }
});
module.exports = BreadCrumb;
