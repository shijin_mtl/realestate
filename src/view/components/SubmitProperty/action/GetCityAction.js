import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/SubmitProperty/constants/SubmitPropertyConstant';
import config from 'utils/config';
import axios from 'axios';

var fetchCities = function(){

}

const cityApi = axios.create({
  withCredentials: true,
}); 


fetchCities.prototype = {
	 city:function(){
		cityApi.get(config.server + 'api/v1/cities/')
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.CITY_REPONSE_RECIEVED,
				        data: response
				});
		    })
		    .catch(function (error) {
		      console.log(error);
            });
				
	 }
}


module.exports = new fetchCities();
