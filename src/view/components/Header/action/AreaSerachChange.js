import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Header/constants/LoginStatusConstant';
import config from 'utils/config';

var onAreaChange = function(){
}

onAreaChange.prototype = {
	AreaStatus:function(value){
		localStorage.area_unit = value?"s_yd":"m2"
	    AppDispatcher.dispatch({
	      actionType: Constants.AREA_CHANGED,
	      data:value
		})
}
}

module.exports = new onAreaChange()
