import React from 'react';
import { Checkbox } from 'antd';
const CheckboxGroup = Checkbox.Group;

var CommercialFacilities  = React.createClass({
		onChange:function(event){
	      this.props.changeCommercialFacility(event);
		},
		componentDidMount:function(){
	    console.log(this.props);
		},


    render:function(){
        var facilityRespArr = this.props.facility?this.props.facility:[];
        var facilityListObj = {Furniture:[],Electronics:[]};
        var furnitureOptions = [] ;
        var electricOptions = [];
        for (var i = 0, len = facilityRespArr.length; i < len; i++) {
            facilityListObj[facilityRespArr[i].name] = facilityRespArr[i].facilities;
        }
        var furnitureFacility = facilityListObj['Furniture'];
        var electronicFacility = facilityListObj['Electronics'];
        for (var j = 0, len = furnitureFacility.length; j < len; j++) {
           furnitureOptions.push({label:furnitureFacility[j].name , value:furnitureFacility[j].id })

        }
        for (var k = 0, len = electronicFacility.length; k < len; k++) {
           electricOptions.push({label:electronicFacility[k].name , value:electronicFacility[k].id })
        }

    	return(
    		     <div className="property-detail-inner-form">
						<h4>4. Facilities</h4>
						<div className="configuration-details">
						  <div className="row">
						      <div className="col-md-6 col-sm-12">
						          <div className="form-group">
						              <label  className="col-md-6 col-sm-6 col-xs-12 control-label">Electronics</label>
						              <div className="col-md-6 col-sm-6 col-xs-12">
						                   <CheckboxGroup options={electricOptions}  onChange={this.onChange} />
						              </div>
						          </div>
						      </div>
						      <div className="col-md-4 col-sm-12">
						        <div className="form-group">
						              <label className="col-md-6 col-sm-6 col-xs-12 control-label">Furniture</label>
						              <div className="col-md-6 col-sm-6 col-xs-12">
						                <CheckboxGroup options={furnitureOptions}  onChange={this.onChange} />

						              </div>
						          </div>
						      </div>
						  </div>



						</div>
                </div>
    	)
    }
});
module.exports = CommercialFacilities;
