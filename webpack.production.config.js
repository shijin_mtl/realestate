var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var public_dir = "src/view";
//var ModernizrWebpackPlugin = require('modernizr-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  devtool: 'source-map',
  entry: [
    path.join(__dirname, public_dir, 'main.js')
  ],
  output: {
    path: path.join(__dirname, '/dist/'),
    filename: '[name].js',
    publicPath: ''
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, public_dir, 'index.html'),
      inject: 'body',
      filename: 'index.html',
      favicon: 'src/view/public/favicon.png'
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery"
    }),
    new ExtractTextPlugin('[name]-[hash].min.css'),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': { 'NODE_ENV': JSON.stringify('production') }
    }),
    //new webpack.optimize.CommonsChunkPlugin('main.js'),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {  warnings: false }
    }),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.EnvironmentPlugin([
           'STATIC_URL',
           'ACCESS_KEY',
           'SECRET_KEY'
    ]),
  ],
  resolve: {
    root: [path.resolve('./src/view/utils'), path.resolve('./src/view')],
    extensions: ['', '.js', '.jsx','.css' , '.json','.txt']
  },
  module: {
   loaders: [
      {
                test: /\.css$/, // Only .css files
                loader: 'style!css' // Run both loaders
            }, {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel'
            },
      {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel'
            },
            {
          test: /\.(ico)$/,
          loader: "static-loader"
      },
      {
          test: /\.(txt)$/,
          loader: "static-loader"
      },
      { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },
      {
          test   : /\.woff/,
          loader : 'url?prefix=font/&limit=10000&mimetype=application/font-woff'
      }, {
          test   : /\.ttf/,
          loader : 'file?prefix=font/'
      }, {
          test   : /\.eot/,
          loader : 'file?prefix=font/'
      }, {
          test   : /\.svg/,
          loader : 'file?prefix=font/'
      },
      {test: /\.(jpe?g|png|gif|svg)$/i, loader: "file-loader?name=dist/public/images/[name].[ext]"}

    ]
  }
};
