var s3 = require('s3');


var client = s3.createClient({
  maxAsyncS3: 20,     // this is the default
  s3RetryCount: 3,    // this is the default
  s3RetryDelay: 1000, // this is the default
  multipartUploadThreshold: 20971520, // this is the default (20 MB)
  multipartUploadSize: 15728640, // this is the default (15 MB)
  s3Options: {
    accessKeyId: process.env.ACCESS_KEY,
    secretAccessKey: process.env.SECRET_KEY,
    "region": "us-standard",
    "s3BucketEndpoint": true,
    "endpoint": process.env.ENDPOINT
  },
});


var params = {
  localDir: "./src/view/public/",
  deleteRemoved: false,

  s3Params: {
    Bucket: process.env.BUCKET,
    Prefix: "react-static/",
  },
};
var uploader = client.uploadDir(params);
uploader.on('error', function(err) {
  console.error("unable to sync:", err.stack);
});
uploader.on('progress', function() {
  console.log(process.env.ACCESS_KEY);
  console.log("progress", uploader.progressAmount, uploader.progressTotal);
});
uploader.on('end', function() {
  console.log("done uploading");
});
