import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/MarketWatch/constants/MarketWatchConstants';
import config from 'utils/config';
import axios from 'axios';
var AgentAction = function(){

}

const agentApi = axios.create({
  withCredentials: true,
});

AgentAction.prototype = {
	agentList:function(params){
        AppDispatcher.dispatch({
	        actionType: Constants.LOADING_RESPONSE_RECEIVED,
	        data: "loading"
		});
		agentApi.get(config.server + 'api/v1/agent/?featured=true')
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.AGENT_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}


module.exports = new AgentAction();
