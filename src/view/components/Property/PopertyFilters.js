import React from 'react';
import { Slider,Checkbox } from 'antd';
import ReactDOM from "react-dom";
import LocalityStore from 'components/Property/store/LocalityStore';
import fetchLocalities from 'components/Property/action/GetLocalityAction';
import LocatoinChangeStore from 'components/Header/store/LocationChangeStore';



var PropertyFilter = React.createClass({

  getInitialState:function(){
    return{
      checkAll:"",
      listRightSide:[]
    }
  },
  componentWillMount:function(){
     LocalityStore.bind(this.locationResponseReceived);
     LocatoinChangeStore.bind(this.LocationChangeData);

  },
  componentWillUnmount:function(){
     LocalityStore.unbind(this.locationResponseReceived);
     LocatoinChangeStore.unbind(this.LocationChangeData);
  },
  LocationChangeData:function(){
    fetchLocalities.locality(localStorage.loacalLocation);
  },
  locationResponseReceived:function(){
    var list = LocalityStore.getResponse()
    var locationResp = list.data
    var locateList = this.props.checkedFilters.locality
    this.state.listRightSide =[]
    for(var i in locationResp)
    {
      if(!Array.isArray(locateList))
      {
        locateList = [locateList]
      }
      if(locateList.indexOf(String(locationResp[i].id))>-1  )
      {
        this.state.listRightSide.push(<div className="col-xs-12"  key={locationResp[i].name}><Checkbox defaultChecked={true} onChange={this.onChange.bind(this,"location",locationResp[i].id)}>{locationResp[i].name}</Checkbox></div>)
      }
      else{
        this.state.listRightSide.push(<div className="col-xs-12"  key={locationResp[i].name}><Checkbox  onChange={this.onChange.bind(this,"location",locationResp[i].id)}>{locationResp[i].name}</Checkbox></div>)
      }
      this.setState({
        listRightSide:this.state.listRightSide
      })
    }



  },
    handleClick:function(value,key){
      this.props.onClickFilter(value,key);
    },
    handleChange:function(key,event){
      this.props.onClickFilter(event.target.value,key)
    },
    onChange:function(key,value){
      this.props.onClickFilter(value,key)
    },
    showLocations:function(){
       var locationList = [];
       var locationResp = this.props.localityList;
       var locateList = this.props.checkedFilters.location
       if(!Array.isArray(locateList))
       {
              if(locateList == undefined){
                locateList = [localStorage.loacalLocation]
              }
              else {
                locateList = []
              }
       }
       for(var i in locationResp)
       {
         if(!Array.isArray(locateList))
         {
           locateList = [locateList]
         }
         if(locateList.indexOf(String(locationResp[i].id))>-1  )
         {
           locationList.push(<div className="col-xs-12"  key={i}><Checkbox defaultChecked={true} onChange={this.onChange.bind(this,"location",locationResp[i].id)}>{locationResp[i].name}</Checkbox></div>)
         }
         else{
           locationList.push(<div className="col-xs-12"  key={i}><Checkbox  onChange={this.onChange.bind(this,"location",locationResp[i].id)}>{locationResp[i].name}</Checkbox></div>)
         }
       }
       return(locationList);
    },
    availableForFamily:function(key){
       this.props.availableForFamily(key);
    },
    componentDidMount:function(){
      $('.filter-btn').click(function(e){
          $('.fltr-lftbox').slideToggle(200);
      });
      $('.filter-btn-cl').click(function(e){
          $('.fltr-lftbox').slideToggle();
      });
          fetchLocalities.locality(this.props.checkedFilters.location?this.props.checkedFilters.location:localStorage.loacalLocation);
          var FilterList = this.props.checkedFilters
          var bedroomList = FilterList.bedroomCount
          try {
            if (bedroomList.indexOf("1") > -1) {
              ReactDOM.findDOMNode(this.refs.bedroomOne).defaultChecked = true
            }
            if (bedroomList.indexOf("2") > -1) {
              ReactDOM.findDOMNode(this.refs.bedroomTwo).defaultChecked = true
            }
            if (bedroomList.indexOf("3") > -1) {
              ReactDOM.findDOMNode(this.refs.bedroomThree).defaultChecked = true
            }
            if (bedroomList.indexOf("4") > -1) {
              ReactDOM.findDOMNode(this.refs.bedroomFour).defaultChecked = true
            }
          } catch (e) {
              ;
          }
          var bathroomCount = FilterList.bathroomCount
          try {
            if (bathroomCount.indexOf("1") > -1) {
              ReactDOM.findDOMNode(this.refs.bathroomOne).defaultChecked = true
            }
            if (bathroomCount.indexOf("2") > -1) {
              ReactDOM.findDOMNode(this.refs.bathroomTwo).defaultChecked = true
            }
            if (bathroomCount.indexOf("3") > -1) {
              ReactDOM.findDOMNode(this.refs.bathroomThree).defaultChecked = true
            }
            if (bathroomCount.indexOf("4") > -1) {
              ReactDOM.findDOMNode(this.refs.bathroomFour).defaultChecked = true
            }
          } catch (e) {
              ;
          }
          var user = this.props.checkedFilters.userType
    },
    componentWillReceiveProps:function(){
       if(this.props.checkedFilters.bedroomCount == undefined)
       {
         ReactDOM.findDOMNode(this.refs.bedroomOne).defaultChecked = false
         ReactDOM.findDOMNode(this.refs.bedroomTwo).defaultChecked = false
         ReactDOM.findDOMNode(this.refs.bedroomThree).defaultChecked = false
         ReactDOM.findDOMNode(this.refs.bedroomFour).defaultChecked = false
       }
       if(this.props.checkedFilters.bedroomCount == undefined)
       {
         ReactDOM.findDOMNode(this.refs.bathroomOne).defaultChecked = false
         ReactDOM.findDOMNode(this.refs.bathroomTwo).defaultChecked = false
         ReactDOM.findDOMNode(this.refs.bathroomThree).defaultChecked = false
         ReactDOM.findDOMNode(this.refs.bathroomFour).defaultChecked = false
       }
       if(this.props.checkedFilters.userType == undefined )
       {
          ReactDOM.findDOMNode(this.refs.owner).defaultChecked = false
          ReactDOM.findDOMNode(this.refs.agent).defaultChecked = false
          ReactDOM.findDOMNode(this.refs.builder).defaultChecked = false

       }
       if(this.props.checkedFilters.availableForFamily == undefined || this.props.checkedFilters.availableForFamily == false)
       {
         ReactDOM.findDOMNode(this.refs.family).defaultChecked = false
       }
    },
    EditDeposit:function(type,e){
      this.props.handleDepositAmount(e.target.value,type)
    },
    handleDepositAmount:function(value,key){

      if(key == 'minDepositAmount')
      {
        if (this.props.filterParams.maxDepositAmount != "" && value >= this.props.filterParams.maxDepositAmount)
        {
          this.props.filterParams.maxDepositAmount = ''
          this.props.filterParams.minDepositAmount = value
        }
        else
        {
        this.props.filterParams.minDepositAmount = value
        }
        this.props.handleDepositAmount(value,key)
      }
      else {

        if (this.props.filterParams.minDepositAmount != "" && value <= this.props.filterParams.minDepositAmount)
        {
          this.props.filterParams.minDepositAmount = ''
          this.props.filterParams.maxDepositAmount = value

        }
        else
        {
          this.props.filterParams.maxDepositAmount = value
        }
        this.props.handleDepositAmount(value,key)


      }
    },
    render:function(){
    	return(
    		   <div className="col-sm-3 fltr-lftbox">
                <div className="filter-content">
                    <span className="filter-btn-cl visible-xs"><span className="lnr lnr-cross"></span></span>
                    <h3>Filters <a href="javascript:;" className="pull-right" onClick={this.props.clearClick}>Clear all</a></h3>
                    <div className="filter-rept">
                        <h4>Budget</h4>
                        <div className="row fltbudgt">
                            <div className="budgsepline"></div>
                            <div className="col-xs-6">
                                <input type="number" className="buginp" placeholder="Min" value = {this.props.checkedFilters.min_budget?this.props.checkedFilters.min_budget:""} onChange={this.handleChange.bind(this,"min_budget")}/>
                            </div>
                            <div className="col-xs-6">
                                <input type="number" className="buginp" placeholder="Max" value = {this.props.checkedFilters.max_budget?this.props.checkedFilters.max_budget:""} onChange={this.handleChange.bind(this,"max_budget")}/>
                            </div>
                        </div>
                    </div>
                    <div className="filter-rept" style={{'display':this.props.filterParams.typeListArr.indexOf("residential_apartment")!= -1 || this.props.filterParams.typeListArr.indexOf("residential_villa")!= -1?"block":'none'}}>
                        <h4>Bedrooms</h4>
                        <div className="row">
                            <div className="col-xs-6">
                                <div className="checkbox checkbox-primary">
                                    <input id="br1" type="checkbox" ref="bedroomOne" />
                                    <label htmlFor="br1" onClick={this.handleClick.bind(this,1,"bedroom")}>1</label>
                                </div>
                            </div>
                            <div className="col-xs-6">
                                <div className="checkbox checkbox-primary">
                                    <input id="br2" type="checkbox" ref="bedroomTwo" />
                                    <label htmlFor="br2" onClick={this.handleClick.bind(this,2,"bedroom")}>2</label>
                                </div>
                            </div>
                            <div className="col-xs-6">
                                <div className="checkbox checkbox-primary">
                                    <input id="br3" type="checkbox" ref="bedroomThree"/>
                                    <label htmlFor="br3" onClick={this.handleClick.bind(this,3,"bedroom")}>3</label>
                                </div>
                            </div>
                            <div className="col-xs-6">
                                <div className="checkbox checkbox-primary">
                                    <input id="br4" type="checkbox" ref="bedroomFour"/>
                                    <label htmlFor="br4" onClick={this.handleClick.bind(this,4,"bedroom")}>4</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="filter-rept"style={{'display':this.props.filterParams.typeListArr.indexOf("residential_apartment")!= -1 || this.props.filterParams.typeListArr.indexOf("residential_villa")!= -1?"block":'none'}}>
                        <h4>Bathrooms</h4>
                        <div className="row">
                            <div className="col-xs-6">
                                <div className="checkbox checkbox-primary">
                                    <input id="btrm1" type="checkbox" ref="bathroomOne"  />
                                    <label htmlFor="btrm1" onClick={this.handleClick.bind(this,1,"bathroom")}>1</label>
                                </div>
                            </div>
                            <div className="col-xs-6">
                                <div className="checkbox checkbox-primary">
                                    <input id="btrm2" type="checkbox" ref="bathroomTwo" />
                                    <label htmlFor="btrm2" onClick={this.handleClick.bind(this,2,"bathroom")}>2</label>
                                </div>
                            </div>
                            <div className="col-xs-6">
                                <div className="checkbox checkbox-primary">
                                    <input id="btrm3" type="checkbox" ref="bathroomThree" />
                                    <label htmlFor="btrm3" onClick={this.handleClick.bind(this,3,"bathroom")}>3</label>
                                </div>
                            </div>
                            <div className="col-xs-6">
                                <div className="checkbox checkbox-primary">
                                    <input id="btrm4" type="checkbox" ref="bathroomFour"/>
                                    <label htmlFor="btrm4"  onClick={this.handleClick.bind(this,4,"bathroom")}>4</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="filter-rept">
                        <h4>Location</h4>
                        <div className="row limit-height">
                          {this.state.listRightSide}
                        </div>
                    </div>
                    <div className="filter-rept">
                        <h4>Listed by</h4>
                        <div className="checkbox single-check checkbox-primary" >
                            <input id="ow1" type="checkbox" ref="owner" />
                            <label htmlFor="ow1"  onClick={this.handleClick.bind(this,"buyer_owner","user_type")} >Owner</label>
                        </div>
                        <div className="checkbox single-check checkbox-primary">
                            <input id="ag1" type="checkbox" ref="agent" />
                            <label htmlFor="ag1" onClick={this.handleClick.bind(this,"agent","user_type")} >Agent</label>
                        </div>
                        <div className="checkbox single-check checkbox-primary" >
                            <input id="ag3" type="checkbox" ref="builder" />
                            <label htmlFor="ag3" onClick={this.handleClick.bind(this,"builder","user_type")} >Builder</label>
                        </div>
                    </div>
                    <div className="filter-rept">
                        <h4>Available for</h4>
                        <div className="checkbox single-check checkbox-primary">
                            <input id="fm1" type="checkbox" ref="family"/>
                            <label htmlFor="fm1" onClick={this.availableForFamily.bind(this,"availableForFamily")} >Family</label>
                        </div>
                    </div>

                    <div className="filter-rept">
                        <h4>Deposit Amount</h4>
                        <div className="row fltbudgt">
                            <div className="budgsepline"></div>
                            <div className="col-xs-6">
                                <input type="number" className="buginp" placeholder="Min" value={this.props.filterParams.minDepositAmount} onChange={this.EditDeposit.bind(this,"minDepositAmount")}/>
                                <ul>
                                    <li onClick={this.handleDepositAmount.bind(this,5000,"minDepositAmount")}><a >5000</a></li>
                                    <li onClick={this.handleDepositAmount.bind(this,6000,"minDepositAmount")}><a >6000</a></li>
                                    <li onClick={this.handleDepositAmount.bind(this,7000,"minDepositAmount")}><a >7000</a></li>
                                    <li onClick={this.handleDepositAmount.bind(this,10000,"minDepositAmount")}><a >10000</a></li>
                                </ul>
                            </div>
                            <div className="col-xs-6">
                                <input type="number" className="buginp" placeholder="Max" value={this.props.filterParams.maxDepositAmount} onChange={this.EditDeposit.bind(this,"maxDepositAmount")}/>
                                <ul>
                                    <li onClick={this.handleDepositAmount.bind(this,5000,"maxDepositAmount")}><a >5000</a>
                                    </li>
                                    <li onClick={this.handleDepositAmount.bind(this,6000,"maxDepositAmount")}><a >6000</a>
                                    </li>
                                    <li onClick={this.handleDepositAmount.bind(this,7000,"maxDepositAmount")}><a >7000</a>
                                    </li>
                                    <li onClick={this.handleDepositAmount.bind(this,10000,"maxDepositAmount")}><a>10000</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                   </div>

                </div>
            </div>

    	)
    }
});
module.exports = PropertyFilter;
