import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/MarketWatch/constants/MarketWatchConstants';
import config from 'utils/config';
import axios from 'axios';
var NewAction = function(){

}

const newApi = axios.create({
  withCredentials: true,
});

NewAction.prototype = {
	newList:function(params){
        AppDispatcher.dispatch({
	        actionType: Constants.LOADING_RESPONSE_RECEIVED,
	        data: "loading"
		});
		newApi.get(config.server + 'api/v1/property/search/?latest=true&city='+String(localStorage['loacalLocation']))
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.PROPERTY_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}

module.exports = new NewAction();
