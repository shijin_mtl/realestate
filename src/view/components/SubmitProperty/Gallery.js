import React from 'react';
import Dropzone from 'react-dropzone'
import {Modal} from 'antd';

const TWO_MiB = 2097152;
var Gallery = React.createClass({
    onDrop:function(file){
    var allowedExtension = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif', 'image/bmp'];
    if(file[0].size > TWO_MiB && ! allowedExtension.indexOf(file[0].type) > -1 )
    {
      Modal.error({
       title: 'File upload error',
       content: 'Please upload image file not more than 2MB in size',
       okText :'Ok'
     });
    }
    else {
      this.props.changePhoto(file[0]);
    }

    },
    handleClick:function(index){
       this.props.deleteImage(index);
    },
    showImageGallery:function(){
      var imgArr = this.props.imgList;
      if(imgArr.length >0){
        var imgList = [];
        for(var i in imgArr){
          imgList.push(<div className="img-gallery" key={i}>
                        <a className="lnr lnr-cross" onClick={this.handleClick.bind(this,i)}></a>
                        <a target="_blank" href="javascript:;"  >

                          <img src={imgArr[i].preview} alt="Fjords" width="300" height="200"/>
                        </a>
                       </div>
                      )
        }
        return(<div className="list_images">{imgList}</div>);
      }else{
         return(<span/>)
      }


    },
  	render:function(){

  		return(
  			    <div className="property-basic-form">
                      <h4 className="form-title-left-border">Gallery</h4>
                      <div className="personal-info-form">
                          <div className="row">
                              <div className="col-md-7 col-sm-12">
                                  <div className="form-group clearfix">
                                      <label className="col-sm-4 control-label hidden-sm hidden-xs">&nbsp;</label>
                                      <div className="col-md-7 col-sm-12">
                                      {this.showImageGallery()}
                                      </div>
                                      <div className="clearfix"></div>
                                      <label className="col-sm-4 control-label hidden-sm hidden-xs">&nbsp;</label>
                                      <div className="col-md-7 col-sm-12 upload-photos">
                                          <div className="clearfix add-photos">
                                               <Dropzone onDrop={this.onDrop}>
                                                  <div className="click_to_upload">
                                                    <span className="lnr lnr-plus-circle"></span>
                                                    Click to Upload <br/><span className="photos">photos</span>
                                                  </div>
                                               </Dropzone>
                                          </div>
                                      </div>
                                  </div>
                                  <div className="form-group">
                                      <label className=" col-md-4 col-sm-12 col-xs-12 control-label text-left">Video Link</label>
                                      <div className="col-md-7 col-sm-12 col-xs-12">
                                          <input type="text" className="form-control" id="" placeholder="" />
                                      </div>
                                  </div>
                              </div>
                              <div className="col-md-5 col-sm-12 pull-right">
                                  <div className="service-box vert-offset-top-0">
                                      <div className="row">
                                          <div className="col-md-2 col-sm-1 col-xs-2"><img src="images/bulb-icon.png" width="32" alt="note" />
                                          </div>
                                          <div className="col-md-10 col-sm-11 col-xs-10">
                                              <p>Did you know that properties with images get 3 times more views?</p>
                                              <p>So, even though it’s optional we recommend you upload lots of property pictures.</p>
                                          </div>
                                      </div>
                                  </div>
                                  <p className="top-buffer">Accepted formats are .jpg, .png & .bmp of maximum size 2MB and minimum resolution of 600 x 400.</p>
                              </div>
                          </div>
                      </div>
                  </div>

  	    )
	}
});
module.exports = Gallery;
