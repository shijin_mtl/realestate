import React from 'react';
import AdListing from '../Advertisement';
import AdAction from 'components/Advertisement/action/AdAction';
import AdStore from 'components/Advertisement/store/AdStore';
var getPriceFormat = require('utils/ChangePrice.js').getPriceFormat;




var PropertyRightSideBar = React.createClass({

    showFeaturedList:function(){
      var featuredarrResp = this.props.propertyContents.slice(0,3);
      var featuredList = [];
      for (var i = 0, len = featuredarrResp.length; i < len; i++) {
          featuredList.push(
                              <div className="featured-list" key={i}>
                                <h4>{featuredarrResp[i].name}</h4>
                                <p>{featuredarrResp[i].locality}</p>
                                <div className="feat-bot">
                                    <span className="fetdprice pull-left">
                                      {featuredarrResp[i].expected_price?<i className="fa fa-inr" aria-hidden="true"></i>:""} {featuredarrResp[i].expected_price?getPriceFormat(featuredarrResp[i].expected_price)+"/-":""}</span>
                                       <a className="pull-right">{featuredarrResp[i].property_for}</a>
                                    <div className="clearfix"></div>
                                </div>
                             </div>
          )
      }
      return(featuredList);
    },
	render:function(){
		return(
			      <div className="col-sm-3 ftred-rgtbox">
              <h3>FEATURED IN YOUR AREA</h3>
                {this.showFeaturedList()}
                <AdListing/>
                {/*<h3>RECENTLY VIEWED</h3>
                 {this.showFeaturedList()}
                 <AdListing multi={true}/>*/}
            </div>
		)
	}
});
module.exports = PropertyRightSideBar;
