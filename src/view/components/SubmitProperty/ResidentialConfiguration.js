import React from 'react';
import { Radio,InputNumber,Select} from 'antd';
const Option = Select.Option;
var RadioGroup = Radio.Group;
var ResidentialConfiguration = React.createClass({
	getInitialState:function(){
      return{
      	     showresidentialVillas:false
      }
	},
	onChange:function(key,event){
      this.props.handleChange(key,event);
	},
	handleClick:function(key,value){
      this.props.changeResidentialConfigurations(key,value);
	},
    componentWillMount:function(){
        if(this.props.propertyType.type == "residential_apartment"){
            this.setState({
                showresidentialVillas:false
            })
        }
        if(this.props.propertyType.type == "residential_villa"){
            this.setState({
                showresidentialVillas:true
            })
        }
    },
	componentWillReceiveProps:function(){
		if(this.props.propertyType.type == "residential_apartment"){
			this.setState({
				showresidentialVillas:false
			})
		}
		if(this.props.propertyType.type == "residential_villa"){
			this.setState({
				showresidentialVillas:true
			})
		}
	},
	handleChange:function(key,event){
			if(key == 'width_of_road_facing_plot' )
			{
				if(event.target.value < 0 )
				{
					event.target.value = 0
				}
			}
      this.props.changeResidentialConfigurations(key,event.target.value);
	},
	changeAreaUnit:function(value){
      this.props.changeResidentialConfigurations('covered_area_unit',value);
	},
	render:function(){
		return(
			   <div className="property-detail-inner-form">
                        <h4>1. Configuration</h4>
                        <div className="configuration-details">
                            <div className="row">
                                <div className="col-md-6 col-sm-12">
                                    <div className="form-group">
                                        <label htmlFor="Floors" className="col-md-4 col-sm-6 col-xs-12 control-label link-text">Bedrooms</label>
                                        <div className="col-md-8 col-sm-6 col-xs-12">
                                                    <Select  style={{ width: 195 }} onChange={this.handleClick.bind(this,"bedroom_count")}  defaultValue={""}>
                                                        <Option value="1">1</Option>
                                                        <Option value="2">2</Option>
                                                        <Option value="3">3</Option>
                                                        <Option value="4">4</Option>
                                                    </Select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-12">
                                    <div className="form-group">
                                        <label  className="col-md-4 col-sm-6 col-xs-12 control-label link-text">Bathrooms</label>
                                        <div className="col-md-8 col-sm-6 col-xs-12">
                                                <Select  style={{ width: 195 }} onChange={this.handleClick.bind(this,"bathroom_count")}>
                                                    <Option value="1">1</Option>
                                                    <Option value="2">2</Option>
                                                    <Option value="3">3</Option>
                                                    <Option value="4">4</Option>
                                                </Select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6 col-sm-12">
                                    <div className="form-group">
                                        <label htmlFor="" className="col-md-4 col-sm-6 col-xs-12 control-label link-text">Balcony</label>
                                        <div className="col-md-8 col-sm-6 col-xs-12">


                                                    <Select  style={{ width: 195 }} onChange={this.handleClick.bind(this,"balcony_count")}>
                                                        <Option value="1">1</Option>
                                                        <Option value="2">2</Option>
                                                        <Option value="3">3</Option>
                                                        <Option value="4">4</Option>
                                                    </Select>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-12">
                                    <div className="form-group">
                                        <label htmlFor="" className="col-md-4 col-sm-6 col-xs-12 control-label link-text">Parking<i className="sub-text">( 4 Wheel)</i>
                                        </label>
                                        <div className="col-md-8 col-sm-9 col-xs-12">

                                                    <Select  style={{ width: 195 }} onChange={this.handleClick.bind(this,"parking_count_four")}>
                                                            <Option value="0">0</Option>
                                                            <Option value="1">1</Option>
                                                            <Option value="2">2</Option>
                                                            <Option value="3">3</Option>
                                                            <Option value="4">4</Option>
                                                    </Select>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-6 col-sm-12 ">
                                    <div className="form-group">
                                        <label  className="col-md-4 col-sm-6 col-xs-12 control-label link-text">Parking<i className="sub-text">( 2 Wheel)</i>
                                        </label>
                                        <div className="col-md-8 col-sm-6 col-xs-12">

                                                    <Select  style={{ width: 195 }} onChange={this.handleClick.bind(this,"parking_count_two")}>
                                                            <Option value="0">0</Option>
                                                            <Option value="1">1</Option>
                                                            <Option value="2">2</Option>
                                                            <Option value="3">3</Option>
                                                            <Option value="4">4</Option>
                                                    </Select>

                                        </div>
                                    </div>
                                </div>

                                <div className="col-md-6 col-sm-12 ">
                                    <div className="form-group">
                                        <label  className="col-md-4 col-sm-6 col-xs-12 control-label link-text">Furnished Status</label>
                                        <div className="col-md-8 col-sm-6 col-xs-12">

                                            <Select  style={{ width: 195 }} onChange={this.handleClick.bind(this,"furnished_status")}>
                                                <Option value="non_furnished">NON FURNISHED</Option>
                                                <Option value="semi_furnished">SEMI FURNISHED</Option>
                                                <Option value="fully_furnished">FULLY FURNISHED</Option>

                                            </Select>

                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div className="row">
                                <div className="col-md-6 col-sm-12">
                                    <div className="form-group">
                                        <label className="col-md-4 col-sm-6 col-xs-12 control-label link-text">Floor No.</label>
                                        <div className="col-md-5 col-sm-6 col-xs-12">
                                            <input type="text" className="form-control" onChange={this.handleChange.bind(this,'floor_number')}
                                            value={this.props.configuration.floor_number}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-12">
                                    <div className="form-group">
                                        <label  className="col-md-4 col-sm-6 col-xs-12 control-label link-text">Total Floors</label>
                                        <div className="col-md-5 col-sm-6 col-xs-12">
                                            <input type="text" className="form-control"  onChange={this.handleChange.bind(this,'total_floor_count')}
                                             value={this.props.configuration.total_floor_count}/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-12 col-sm-12">
                                    <div className="form-group">
                                        <label  className="col-md-2 col-sm-6 col-xs-12 control-label link-text">Covered Area</label>
                                        <div className="col-md-8 col-sm-6 col-xs-12">
                                            <div className="row">
                                                <div className="col-md-6 col-sm-12">
                                                    <input type="text" className="form-control sm-margin" onChange={this.handleChange.bind(this,'covered_area')}
                                                     value={this.props.configuration.covered_area}/>
                                                </div>
                                                <div className="col-md-6 col-sm-12">
                                                    <Select defaultValue="sq_ft" style={{ width: 200 }} onChange={this.changeAreaUnit}>
					                                    <Option value="sq_ft">Sq Ft</Option>
					                                    <Option value="sq_yrd">Yard</Option>

                                                    </Select>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                             {this.state.showresidentialVillas && <div className="row">
							    <div className="col-md-4 col-sm-12">
							        <div className="form-group">
							            <label className="col-md-6 col-sm-6 col-xs-12 control-label">No. of open sides</label>
							            <div className="col-md-6 col-sm-6 col-xs-12">
							                <div className="btn-toolbar mb-3 custom-btn-group" role="toolbar" aria-label="Toolbar with button groups">
							                    <div className="btn-group mr-2" role="group" aria-label="First group">

                                                    <Select  style={{ width: 200 }} onChange={this.handleClick.bind(this,"no_of_open_sides")}>
                                                        <Option value="1">1</Option>
                                                        <Option value="2">2</Option>
                                                        <Option value="3">3</Option>
                                                    </Select>

							                    </div>
							                </div>
							            </div>
							        </div>
							    </div>
                            </div>}

                            {this.state.showresidentialVillas && <div className="row">
                              <div className="col-md-4 col-sm-12">
                                <div className="form-group">
                                <label  className="col-md-6 col-sm-6 col-xs-12 control-label">Width of road <br/>facing the plot</label>
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                 <input type="number" className="form-control sm-margin" min="0" placeholder="In meters" onChange={this.handleChange.bind(this,"width_of_road_facing_plot")} />
                                </div>
                              </div>
                              </div>
                            </div>}
                        </div>
                </div>
		)
	}

});
module.exports = ResidentialConfiguration;
