import React from 'react';
import {Link} from 'react-router';
import { Checkbox } from 'antd';
import { Form, FormGroup,TextArea, Validator, Input,required,minLength,maxLength,pattern,email,custom} from 'romagny13-react-form-validation';
import AgentAction from 'components/AgentSignup/action/AgentDetailsAction';
import EditAction from 'components/AgentSignup/action/getAgentAction';

import AgentDetailStore from 'components/AgentSignup/store/AgentDetailsStore';
import { DatePicker } from 'antd';
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import {Modal} from 'antd';
const TWO_MiB = 2097152;
var allowedExtension = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif', 'image/bmp'];
import moment from 'moment';




const validators = {
    'name': [required('Name is Required'), minLength(3),maxLength(30),pattern(/^[a-zA-Z']+(\s.?[a-zA-Z']+){0,4}$/, '3 to 30 characters no symbols and numbers ')],
};


var MyFormGroup = function (props) {
    var groupclassName = props.hasError ? 'form-group has-error' : 'form-group';
    return (
            <div className={groupclassName}>
                {props.children}
                {props.hasError && <span className="help-block">{props.error}</span>}
            </div>
    );
};

var AgentDetails = React.createClass({
  getInitialState:function(){
    return{model:
          {
            name:this.props.details?this.props.details.title:"",
            contact_person:this.props.details?this.props.details.contact_person:"",
            property_type:this.props.details?this.props.details.property_type:[],
            city:this.props.details?this.props.details.city:"",
            description:this.props.details?this.props.details.description:"",
            founded_date:this.props.details?moment(this.props.details.founded_date).format("YYYY-MM-DD"):"",
            project_handled:this.props.details?this.props.details.project_handled:"",
            logo:"",
          },
          btnStatus:false,
          placeholder:""
    }
  },
   onChange:function(date, dateString) {
    this.state.model.founded_date  = dateString
    this.setState({
      founded_date :this.state.model.founded_date
    })
  },
  componentWillMount:function(){
      AgentDetailStore.bind(this.agentResponseReceived);
  },
    componentDidMount:function(){
      $("#uploadTrigger").click(function(){
      $("#logo").click();
  });
    },
  componentWillUnmount:function(){
      AgentDetailStore.unbind(this.agentResponseReceived);
  },
  agentResponseReceived:function(){
     this.state.btnStatus = false
     var AgentResponse = AgentDetailStore.getResponse();
     this.props.changeContent()
	},
  submit:function(result){


    if(!result.hasError){
       this.setState({
         btnStatus : true
       })
       result.model.city = this.state.model.city
       result.model.description = this.state.model.description
       result.model.founded_date = this.state.model.founded_date
       result.model.project_handled = this.state.model.project_handled
       result.model.logo = this.state.model.logo
       result.property_type = this.state.model.property_type
       result.model.property_type_list = this.state.model.property_type
       result.model.title = this.state.model.name
       if(this.props.details != undefined){
         result.model.user_agent = this.props.details.user_agent
         EditAction.postEditData(result.model,this.props.id)
       }
       else {
         AgentAction.expert(result.model);
       }
    }
  },

  handleChange:function(key,event){
    if ( key == 'name'){
      this.state.model.name  = $('#agent_name').val()
    }
    if ( key == 'contact_person'){
      this.state.model.contact_person  = $('#contact_name').value
    }
    if ( key == 'city'){
      this.state.model.city  = event.target.value
    }
    if ( key == 'description'){
      this.state.model.description  = event.target.value
    }
    if ( key == 'project_handled'){
      this.state.model.project_handled  = event.target.value
    }
    if ( key == 'founded_date'){
      this.state.model.founded_date  = event.target.value
    }
    if ( key == 'logo'){
      if( event.target.files[0].size > TWO_MiB || allowedExtension.indexOf(event.target.files[0].type) <= -1 )
      {
        event.target.files[0]=null
        Modal.error({
         title: 'File upload error',
         content: 'Please upload image file not more than 2MB in size',
         okText :'Ok'
       });
      }
      else {
        this.state.model.logo  = event.target.files[0]
        this.state.placeholder = event.target.files[0].name
        this.setState({
          placeholder:this.state.placeholder
        })
      }
    }
    this.setState({
      model:this.state.model
    })
  },
  changeSelect:function(key,event){
      if(event.target.checked){
        this.state.model.property_type.push(key)
      }
      else {
        var index = this.state.model.property_type.indexOf(key)
         this.state.model.property_type.splice(index, 1);
      }
      this.setState({
        property_type:this.state.model.property_type
      })
  },

  render:function(){
    return(
      <span>
      <div className="submit-property-section">
        <div className="container">
        <div className="row">
              <div className="col-sm-12">
                  <ul className="breadcrumb">
                      <li><Link to="/">Home</Link>
                      </li>
                      <li className="active">AGENT DETAILS</li>
                  </ul>
              </div>
         </div>
           <div className="row">
              <div className="col-sm-12">
                  <Form className="submit-property-form " onSubmit={this.submit} model={this.state.model} ref="form">
                      <h2 className="text-center">TELL US ABOUT YOURSELF</h2>
                      <h5 className="text-center">Help us showcase your profile better</h5>
                      <div className="property-details-section" style = {{'borderBottom': 0 +"px"}}>
                      <div className="col-md-7 col-sm-12">
                          <div className="row">
                              <div className="personal-info-section">
                                  <h4 className="form-title">AGENT  DETAILS</h4>
                                  <div className="property-basic-inner-form">
                                      <div className="form-group make-align">
                                        <label  className="col-sm-3 control-label">Agent Name<span style={{'color':'red'}}>*</span></label>
                                        <div className="col-sm-9">
                                            <Validator validators={validators['name']}>
                                               <MyFormGroup>
                                              <Input type="text" name='name' id='agent_name' className="form-control" value={this.state.model.name} onChange={this.handleChange.bind(this,"name")} placeholder=""/>
                                              </MyFormGroup>
                                            </Validator>
                                        </div>
                                      </div>
                                      <div className="form-group">
                                        <label  className="col-sm-3 control-label">Contact Person<span style={{'color':'red'}}>*</span></label>
                                        <div className="col-sm-9">
                                        <Validator validators={validators['name']}>
                                           <MyFormGroup>
                                            <Input type="text" className="form-control" id="contact_person" name="contact_person" value={this.state.model.contact_person} onChange={this.handleChange.bind(this,"contact_person")} placeholder=""/>
                                          </MyFormGroup>
                                        </Validator>
                                        </div>
                                      </div>
                                      <div className="form-group">
                                        <label  className="col-sm-3 control-label">Agent Logo</label>
                                        <div className="col-sm-9">
                                        <MyFormGroup>
                                          <input type="file" className="form-control hidden" id="logo"  placeholder={this.state.placeholder} onChange={this.handleChange.bind(this,"logo")}/>
                                          <div className="button-browse" id="uploadTrigger" style={{'textAlign': this.state.placeholder!=""?'left':'right','color':this.state.placeholder!=""?'black':'red'}} ><div className="text-left" style={{"width":"50%","display": "inline-block"}}>{this.state.placeholder}</div><div className="text-right" style={{"width":"50%","display": "inline-block"}}>Browse..</div></div>
                                        </MyFormGroup>
                                        </div>
                                      </div>
                                        <div className="col-md-3 col-sm-12">
                                            <label className="control-label">Property Types</label>
                                        </div>
                                         <div className="col-md-9 col-sm-12">
                                         <div className="row">
                                            <div className="col-md-6 col-sm-12">
                                               <Checkbox onChange={this.changeSelect.bind(this,1)} defaultChecked={this.props.details != undefined?this.props.details.property_type.includes(1):false}>Residential/Apartments</Checkbox>
                                               <Checkbox onChange={this.changeSelect.bind(this,2)} defaultChecked={this.props.details != undefined?this.props.details.property_type.includes(2):false}>Residential/Villas </Checkbox>
                                            </div>
                                            <div className="col-md-6 col-sm-12 ">
                                               <Checkbox onChange={this.changeSelect.bind(this,3)} defaultChecked={this.props.details != undefined?this.props.details.property_type.includes(3):false}>Commercial/Showrooms </Checkbox>
                                               <Checkbox onChange={this.changeSelect.bind(this,4)} defaultChecked={this.props.details != undefined?this.props.details.property_type.includes(4):false}>Commercial/Shops</Checkbox>
                                            </div>
                                         </div>
                                         </div>
                                      <div className="form-group">
                                        <label  className="col-sm-3 control-label">Location Presence</label>
                                        <div className="col-sm-9">
                                        <MyFormGroup>
                                          <input type="text" className="form-control" id="city"   value={this.state.model.city} onChange={this.handleChange.bind(this,"city")} placeholder="Type city names separated by comma"/>
                                        </MyFormGroup>
                                        </div>
                                      </div>
                                      <div className="form-group">
                                        <label  className="col-sm-3 control-label">Operating Since</label>
                                        <div className="col-sm-9">
                                        <MyFormGroup>
                                          {/*<input className="form-control"  value={this.state.model.founded_date} onChange={this.handleChange.bind(this,"founded_date")}/ >*/}
                                          <LocaleProvider locale={enUS}  >
                                          <DatePicker onChange={this.onChange} defaultValue={this.props.details!=undefined?moment(this.props.details.founded_date, "YYYY MM DD"):moment()}/>
                                          </LocaleProvider>
                                        </MyFormGroup>
                                        </div>
                                      </div>
                                      <div className="form-group">
                                        <label  className="col-sm-3 control-label">Projects Handled</label>
                                        <div className="col-sm-9">
                                        <MyFormGroup>
                                          <input className="form-control"  value={this.state.model.project_handled} onChange={this.handleChange.bind(this,"project_handled")} placeholder="Type projects names separated by comma"/>
                                        </MyFormGroup>
                                        </div>
                                      </div>
                                      <div className="form-group">
                                        <label  className="col-sm-3 control-label">Description</label>
                                        <div className="col-sm-9">
                                        <MyFormGroup>
                                          <textarea className="form-control" rows="7" value={this.state.model.description} onChange={this.handleChange.bind(this,"description")} ></textarea>
                                        </MyFormGroup>
                                        </div>
                                      </div>

                                      <div className="row">
<div className="col-md-8 col-md-push-3 text-center">  <button type="submit" disabled= {this.state.btnStatus} className="btn btn-red" >SUBMIT</button></div>
                                      </div>

                                  </div>
                              </div>
                          </div>
                        </div>
                      </div>
                  </Form>
              </div>
           </div>
        </div>
      </div>
      <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtnFill' data-toggle="modal" data-target="#myModal3">Open Modal</button>
      <div id="myModal3" className="modal fade pop-show" role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                    <div className="modal-header">
                    <b>FILL ALL  FIELDS</b>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
                    </div>
                    <div className="succes-msgdiv">
                         <img src="images/error-icon.png" width="85" className="icon-succ" alt="Success" />
                         <br/><br/>
                         <h4>ERROR!</h4>
                         <br/>
                         <h5>Please fill all the fields</h5>
                         <br/>
                    </div>
                    <div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" >OK</button></div>
                    <br/>
              </div>
            </div>
      </div>
      </span>
    )
  }
})
module.exports = AgentDetails;
