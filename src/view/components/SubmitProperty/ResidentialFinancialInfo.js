import React from 'react';
import { Checkbox } from 'antd';

var ResidentialFinancialInfo = React.createClass({
   handleChange:function(key,event){
    this.props.changeResidentialFinancialInfo(key,parseInt(event.target.value));
   },
   onChange:function(key,event){
        this.props.changeResidentialFinancialInfo(key,event.target.checked);
   },
   render:function(){
   	 return(
   	 	      <div className="personal-info-section">
                <h4 className="form-title-left-border">Financial Info</h4>
                <div className="personal-info-form">
                    {this.props.showMonthlyRentInfo && <div className="row">
                        <div className="col-md-6 col-sm-12">
                            <div className="form-group">
                                <label htmlFor="" className="col-md-3 control-label link-text">Monthly Rent</label>
                                <div className="col-md-9 col-sm-12">
                                    <input type="text" className="form-control" onChange={this.handleChange.bind(this,"monthly_rent")}/>
                                </div>
                            </div>
                        </div>
                    </div>}
                    {!this.props.showMonthlyRentInfo && <div className="row">
                        <div className="col-md-6 col-sm-12">
                            <div className="form-group">
                                <label htmlFor="" className="col-md-3 control-label link-text">Expected Price</label>
                                <div className="col-md-9 col-sm-12">
                                    <input type="text" className="form-control" onChange={this.handleChange.bind(this,"expected_price")}/>
                                </div>
                            </div>
                        </div>
                    </div>}
                    {!this.props.showMonthlyRentInfo &&  <div className="row">
                        <div className="col-md-6 col-sm-12">
                            <div className="form-group">
                                <label htmlFor="" className="col-md-3 control-label link-text">Other charges</label>
                                <div className="col-md-9 col-sm-12">
                                    <input type="text" className="form-control" onChange={this.handleChange.bind(this,"other_charges")}/>
                                </div>
                            </div>
                        </div>
                    </div>}

                     {!this.props.showMonthlyRentInfo &&  <div className="form-group">
                            <label  className="col-md-2 control-label hidden-sm hidden-xs">&nbsp;</label>
                            <div className="col-md-6 col-sm-12">
                                <Checkbox onChange={this.onChange.bind(this,"exclude_duty_and_reg_charges")} checked={this.props.resedentialFinancialInfo.exclude_duty_and_reg_charges}>Stamp Duty And Registration Charges Excluded</Checkbox>

                            </div>
                     </div>}

                    {this.props.showMonthlyRentInfo  && <div className="row">
                        <div className="col-md-6 col-sm-12">
                            <div className="form-group">
                                <label htmlFor="" className="col-md-3 control-label link-text">Other charges</label>
                                <div className="col-md-4 col-sm-12">

                                        <Checkbox onChange={this.onChange.bind(this,"maintenance_charges")} checked={this.props.resedentialFinancialInfo.maintenance_charges}>Maintenance</Checkbox>

                                        <Checkbox onChange={this.onChange.bind(this,"water_charges")} checked={this.props.resedentialFinancialInfo.water_charges}>Water</Checkbox>

                                        <Checkbox onChange={this.onChange.bind(this,"electricity_charges")} checked={this.props.resedentialFinancialInfo.electricity_charges}>Electricity</Checkbox>

                                </div>
                            </div>
                        </div>
                    </div>}
                    {this.props.showMonthlyRentInfo && <div className="row">
                        <div className="col-md-6 col-sm-12">
                            <div className="form-group">
                                <label htmlFor="" className="col-md-3 control-label link-text">Security Deposit</label>
                                <div className="col-md-9 col-sm-12">
                                    <input type="text" className="form-control" onChange={this.handleChange.bind(this,"security_deposit")} />
                                </div>
                            </div>
                        </div>
                    </div>}
                </div>
              </div>
   	 )
   }
});
module.exports = ResidentialFinancialInfo;
