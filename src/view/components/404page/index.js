import React from 'react';
import config from 'utils/config';


var PageNotFound = React.createClass({
  componentWillMount:function(){
     window.scrollTo(0,0);
  },
  render:function(){
  	 return(
       <section className="wrap-nofound">
        <div className="container">
        <ul className="breadcrumb">
          <li><a href="#">Home</a></li>
          <li className="active">404</li>
        </ul>
        <div className="box-nofound">
          <h1>Oops!</h1>
          <h4>Something went wrong.</h4>
          <img src={config.s3static + "images/errorimg.jpg"} className="img-responsive" alt="404" width="400"/>
          <p><a href="#" className="redlnkuppr">Click here to return to homepage.</a></p>
        </div>
        </div>
        </section>
  	 )
  }
});
module.exports= PageNotFound;
