import React from 'react';
import { Radio , Select } from 'antd';
const Option = Select.Option;
var RadioGroup = Radio.Group;
var CommercialConfiguration = React.createClass({
	handleClick:function(key,value){
       this.props.commercialConfiguration(key,value)
	},
	handleChange:function(key,event){
       this.props.commercialConfiguration(key,event.target.value);
	},
	handleRadioChange:function(key,event){
        this.props.commercialConfiguration(key,event.target.value);
    },
    getInitalState:function(){
     return{
              showFloor:true
     }
    },
    componentWillMount:function(){
       if(this.props.propertyType.type =="commercial_showroom") {
            this.setState({
                  showFloor:true
            })
       }
       if(this.props.propertyType.type == "commercial_shop"){
            this.setState({
                  showFloor:false
            })
       }
    },
    componentWillReceiveProps:function(){
        if(this.props.propertyType.type =="commercial_showroom") {
            this.setState({
                  showFloor:true
            })
       }
       if(this.props.propertyType.type == "commercial_shop"){
            this.setState({
                  showFloor:false
            })
       }
    },
	render:function(){
		return(
			    <div className="property-detail-inner-form">
                            <h4>1. Configuration</h4>
                            <div className="configuration-details">
                                <div className="row">
                                    <div className="col-md-6 col-sm-12">
                                        <div className="form-group">
                                            <label className="col-md-6 col-sm-6 col-xs-12 control-label">Washrooms</label>
                                            <div className="col-md-6 col-sm-6 col-xs-12">


                                                      <Select  style={{ width: 200 }} onChange={this.handleClick.bind(this,"washrooms")}>
                                                        <Option value="1">1</Option>
                                                        <Option value="2">2</Option>
                                                        <Option value="3">3</Option>
                                                        <Option value="4">4</Option>
                                                      </Select>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div className="row">
                                    <div className="col-md-6 col-sm-12">
                                        <div className="form-group">
                                            <label  className="col-md-6 col-sm-6 col-xs-12 control-label">Parking<i className="sub-text">( 4 Wheel)</i>
                                            </label>
                                            <div className="col-md-6 col-sm-6 col-xs-12">

                                              <Select  style={{ width: 200 }} onChange={this.handleClick.bind(this,"parking_count_four")}>
                                                <Option value="0">0</Option>
			                                    <Option value="1">1</Option>
			                                    <Option value="2">2</Option>
			                                    <Option value="3">3</Option>
			                                    <Option value="4">4</Option>
                                              </Select>



                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-sm-12">
                                        <div className="form-group">
                                            <label  className="col-md-6 col-sm-6 col-xs-12 control-label">Parking<i className="sub-text">( 2 Wheel)</i>
                                            </label>
                                            <div className="col-md-6 col-sm-6 col-xs-12">

                                              <Select  style={{ width: 200 }} onChange={this.handleClick.bind(this,"parking_count_two")}>
                                                <Option value="0">0</Option>
			                                    <Option value="1">1</Option>
			                                    <Option value="2">2</Option>
			                                    <Option value="3">3</Option>
			                                    <Option value="4">4</Option>
                                              </Select>



                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-6 col-sm-12 ">
                                        <div className="form-group">
                                            <label  className="col-md-6 col-sm-6 col-xs-12 control-label">Furnished Status</label>
                                            <div className="col-md-6 col-sm-6 col-xs-12">

                                              <Select  style={{ width: 200 }} onChange={this.handleClick.bind(this,"furnished_status")}>
                                                <Option value="non_furnished">NON FURNISHED</Option>
                                                <Option value="semi_furnished">SEMI FURNISHED</Option>
                                                <Option value="fully_furnished">FULLY FURNISHED</Option>

                                            </Select>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {this.state.showFloor && <div className="row">
                                    <div className="col-md-6 col-sm-12">
                                        <div className="form-group">
                                            <label className="col-md-6 col-sm-6 col-xs-12 control-label">Floor No.</label>
                                            <div className="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" className="form-control"  onChange={this.handleChange.bind(this,"floor_number")}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-sm-12">
                                        <div className="form-group">
                                            <label className="col-md-6 col-sm-6 col-xs-12 control-label">Total Floors</label>
                                            <div className="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" className="form-control"  onChange={this.handleChange.bind(this,"total_floor_count")}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>}

                                <div className="row">
                                    <div className="col-md-12 col-sm-12">
                                        <div className="form-group">
                                            <label  className="col-md-3 col-sm-6 col-xs-12 control-label">Covered Area</label>
                                            <div className="col-md-6 col-sm-6 col-xs-12">
                                                <div className="row">
                                                    <div className="col-md-6 col-sm-12">
                                                        <input type="text" className="form-control sm-margin" onChange={this.handleChange.bind(this,"covered_area")} />
                                                    </div>
                                                    <div className="col-md-6 col-sm-12">

                                                        <Select defaultValue='Sq.ft' style={{ width: 200 }} onChange={this.handleClick.bind(this,"covered_area_unit")}>
						                                    <Option value="sq_ft">Sq Ft</Option>
						                                    <Option value="sq_yrd">Yards</Option>

                                                        </Select>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-6 col-sm-12">
                                        <div className="form-group">
                                            <label  className="col-md-6 col-sm-6 col-xs-12 control-label">Corner Shop</label>
                                            <div className="col-md-6 col-sm-6 col-xs-12">

	                                            <RadioGroup onChange={this.handleRadioChange.bind(this,"corner_shop")}>
										        	<Radio value={"yes"}>Yes</Radio>
										        	<Radio value={"no"}>No</Radio>
	                                        	</RadioGroup>


                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-6 col-sm-12">
                                        <div className="form-group">
                                            <label className="col-md-6 col-sm-6 col-xs-12 control-label">Main Road Facing</label>
                                            <div className="col-md-6 col-sm-6 col-xs-12">

                                                <RadioGroup onChange={this.handleRadioChange.bind(this,"main_road_facing")}>
									        	 <Radio value={"yes"}>Yes</Radio>
									        	 <Radio value={"no"}>No</Radio>
                                        	    </RadioGroup>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/*<div className="row">
                                    <div className="col-md-6 col-sm-12">
                                        <div className="form-group">
                                            <label  className="col-md-6 col-sm-6 col-xs-12 control-label">Personal Washroom</label>
                                            <div className="col-md-6 col-sm-6 col-xs-12">

                                                <RadioGroup onChange={this.handleRadioChange.bind(this,"personal_washroom")}>
									        	 <Radio value={"dry"}>Dry</Radio>
									        	 <Radio value={"wet"}>Wet</Radio>
                                        	    </RadioGroup>



                                            </div>
                                        </div>
                                    </div>
                                </div>*/}

                                <div className="row">
                                    <div className="col-md-6 col-sm-12">
                                        <div className="form-group">
                                            <label  className="col-md-5 col-sm-6 col-xs-12 control-label">Pantry</label>
                                            <div className="col-md-7 col-sm-6 col-xs-12">


                                                <RadioGroup onChange={this.handleRadioChange.bind(this,"pantry")}>
									        	 <Radio value={"dry"}>Dry</Radio>
									        	 <Radio value={"wet"}>Wet</Radio>
									        	 <Radio value={"not_available"}>Not Available</Radio>
                                        	    </RadioGroup>



                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                    </div>
		)
	}
})
module.exports = CommercialConfiguration;
