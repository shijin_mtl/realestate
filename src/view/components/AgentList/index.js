import React from 'react';
import Header from './Header'
import SingleAgentList from './SingleAgentList'
import SideBar from './SideBar'
import AgentStore from 'components/AgentList/store/AgentStore';
import AgentAction from 'components/AgentList/action/AgentAction';
import ErrorScenarios from 'components/ErrorScenarios';
import Loading from 'components/Loading';
import { Pagination,Progress,Icon,Popconfirm } from 'antd';






var Scroll  = require('react-scroll');
var scroll     = Scroll.animateScroll;

var AgentList = React.createClass({
  componentDidMount:function(){
    scroll.scrollToTop();
  },
  getInitialState:function(){
     return{
           agentData:"",
           isLoading:true,
           sortParams:'proximity',
           current:"1"
     }
  },
  componentWillMount:function(){
    AgentStore.bind(this.responseReceived);
    AgentAction.AgentList({sort:'proximity',page:"1"})
    window.scrollTo(0,0);
  },
  componentWillUnmount:function(){
    AgentStore.unbind(this.responseReceived);
  },
  responseReceived:function(){
    this.state.agentData = AgentStore.getResponse()
    this.setState({
      agentData:this.state.agentData
    })
    this.setState({
      isLoading:false
    })
  },
  handleChange:function(key1){
    this.state.current  = key1
    this.setState({
      current:key1
    })

    AgentAction.AgentList({sort:this.state.sortParams,page:this.state.current})
   scroll.scrollToTop();
  },


  showAgentList:function(){
     var respList =this.state.agentData.results
     var popularList = []
     if(respList.length > 0){
       for (var i = 0, len = respList.length; i < len; i++) {
         popularList.push(<SingleAgentList
            agentList={respList[i]} key={i}/>)
       }
       return(popularList)
     }
     else{
          return(<ErrorScenarios message="No Agents Registred"/>)
     }
  },
  handleSort:function(key){
    this.state.sortParams = key
    AgentAction.AgentList({sort:key})
    this.setState({
      sortParams:this.state.sortParams
    })
  },



   render:function(){
      var sortObj = {proximity:"Proximity",view_count:"View Count",click_count:"Click Count",contact_count:"Contact Count"};
   	 return(
       <span>
               <section className="agent-listwrap">
                  <div className="container">
                  {/* header */}
                    <Header sort={this.props.sortParams}/>
                  {/* end header */}
                    <div className="row">
                    <div className="row text-center">
                          <label>SORT BY</label> {"  "}
                          <div className="sortdrop dropdown">
                              <button type="button" className="btn dropdown-toggle srt-btndrp" data-toggle="dropdown" aria-expanded="true"><span>{sortObj[this.state.sortParams]}</span>
                              </button>
                              <ul className="dropdown-menu">
                                  <li onClick = {this.handleSort.bind(this,"proximity")}><a>Proximity</a></li>
                                  <li onClick = {this.handleSort.bind(this,"view_count")}><a>View Count</a></li>
                                  <li onClick = {this.handleSort.bind(this,"click_count")}><a>Click Count</a></li>
                                  <li onClick = {this.handleSort.bind(this,"contact_count")}><a>Contact Count</a></li>
                              </ul>
                          </div>
                    </div>
                      {/* List */}
                      <div className="col-sm-9">
                        {!this.state.isLoading?this.showAgentList():<Loading/>}
                        <br/>
                        <div className='row text-center'>
                          {this.state.agentData.count >=10 && <Pagination total={this.state.agentData.count} current={parseInt(this.state.current)}  pageSize={10} onChange={this.handleChange}/>}
                        </div>
                      </div>
                      {/* end list */}

                      {/* side bar */}
                        <SideBar/>
                      {/* end side bar */}
                    </div>
                  </div>
               </section>
        </span>
   	 )
   }
});
module.exports = AgentList;
