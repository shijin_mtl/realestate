import React from 'react';
import { Form, FormGroup, Validator, Input,required,minLength,maxLength,pattern,email,custom} from 'romagny13-react-form-validation';
import { Radio } from 'antd';
var RadioGroup = Radio.Group;
import SignUpAction from 'components/SignUp/action/SignUpAction';
import SignUpStore from 'components/SignUp/store/SignUpStore';
import EmailAlreadyExistsStore from 'components/SignUp/store/EmailAlreadyExistsStore';
import UserAuthSideBar from 'components/UserAuthSideBar';
import SignupSucess from './SignupSucess';
import {Modal} from 'antd';
import {hashHistory,Link} from 'react-router';
import OtpStore from 'components/SignUp/store/OtpStore';
import OtpAction from 'components/SignUp/action/OtpAction';
import config from 'utils/config';

var phone





const validators = {
    'name': [required('Name is Required'), minLength(3),maxLength(30),pattern(/^[a-zA-Z']+(\s[a-zA-Z']+){0,4}$/, '3 to 30 characters special characters are not alowed.')],
    'phone':[required('Phone is Required'), minLength(3),maxLength(15),pattern(/^[0-9 +]{10,15}$/, 'Invalid Phone')],
};

var MyFormGroup = function (props) {
    var groupclassName = props.hasError ? 'form-group has-error' : 'form-group';
    return (
            <div className={groupclassName}>
                {props.children}
                {props.hasError && <span className="help-block">{props.error}</span>}
            </div>
    );
};

var SignUp = React.createClass({
   getInitialState: function () {
        return {
            model: {
                name: '',
                password:'',
                email:'',
                phone:'',
                type:"buyer_owner"
            },
            isShowingModal: false,
            isUserAlreadyExistsModal:false,
            buttonDisabled:false,
            otp:'',
            error:''
        };
    },
    componentWillMount:function(){
      window.scrollTo(0,0);
      SignUpStore.bind(this.signUpResponseReceived);
      EmailAlreadyExistsStore.bind(this.validationResponseReceived)
      OtpStore.bind(this.OtpValidation)
    },
    componentDidMount:function(){
      $('#myModal3').modal({backdrop: 'static', keyboard: false})
    },
    componentWillUnmount:function(){
      SignUpStore.unbind(this.signUpResponseReceived)
      EmailAlreadyExistsStore.unbind(this.validationResponseReceived)
      OtpStore.unbind(this.OtpValidation)
    },
    closeModel:function(){
      if(this.state.model.type == 'builder')
      {
        hashHistory.push("/builder")
      }
      else if(this.state.model.type == 'agent')
      {
        hashHistory.push("/agent")
      }
      else{
          hashHistory.push("/login")
      }
    },
    OtpValidation:function(){
        var otpRespose = OtpStore.getResponse();
        if(otpRespose.data.error)
        {
              this.state.error = otpRespose.data.error
              this.setState({
                error:this.state.error
              })
        }
        if(!otpRespose.data['otp-exists'] && !otpRespose.data['status'] )
        {
              this.state.error = 'OTP ERROR'
              this.setState({
                error:this.state.error
              })
        }

        if(otpRespose.data['status'] )
        {
              this.state.error = 'OTP RESEND'
              this.setState({
                error:this.state.error
              })
        }
        if(otpRespose.data['otp-exists'])
        {
          $('#modelClose').click()
          if(this.state.model.type == 'builder')
          {
            hashHistory.push("/builder")
          }
          else if(this.state.model.type == 'agent')
          {
            hashHistory.push("/agent")
          }
          else{
              hashHistory.push("/login")
          }
        }
    },
    otpSubmit:function(){
      OtpAction.verify({phone:this.state.model.phone,passcode:this.state.otp})
    },
    otpResend:function(){
      this.state.otp =""
      this.setState({
        otp:this.state.otp
      })
      var num
      if(!this.state.model.phone.startsWith("+91"))
       {
         num  = "+91" + this.state.model.phone
       }
      OtpAction.verify({phone:num,passcode:this.state.otp,action:'resend'})
    },
    signUpResponseReceived:function(){
      this.setState({
         buttonDisabled:false
      })

      var signUpResp = SignUpStore.getResponse();
      if(signUpResp.status == 200){
        mixpanel.track(
        "Signup",
        {"browser": "session",
        }
      )
        $('#modelBtnRegister').click()
        this.state.model.phone = phone
        this.setState({
          phone:this.state.model.phone
        })
      }
    },
    validationResponseReceived:function(){
      var emailValdationResponse = EmailAlreadyExistsStore.getResponse();
      if(emailValdationResponse.data['account-exists']){
          Modal.error({
            title: 'This email is already registered',
            okText  :'Ok'
         });
         this.setState({
            buttonDisabled:false
         })
      }
    },
    componentDidMount:function(){
        var pass2 = document.getElementById('password');
          document.getElementById('sh-hd-pass').onclick = function() {
            if( this.innerHTML == 'Show' ) {
              this.innerHTML = 'Hide'
              pass2.type="text";
            } else {
              this.innerHTML = 'Show'
              pass2.type="password";
            }
        }
    },

    onSubmit: function (result) {
        if (!result.hasError) {
           phone = result.model.phone
           if(!result.model.phone.startsWith("+91"))
            {
              result.model.phone = "+91" + result.model.phone
            }
           result.model.type = this.state.model.type;
           SignUpAction.signUp(result.model);
           this.setState({
              buttonDisabled:true
           })
        }
    },
    email:function(value){
        var atpos = value.indexOf("@");
        var dotpos = value.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=value.length || ! /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
            return{
                name: 'email',
                error: 'Enter a Valied Email'
            }
        }
    },
    number:function(value){
      if(value.length!=10 || !/^\d+$/.test(value)){
            return{
                name:'number',
                error:"Invalid number"
            }
      }
    },
    handleChange:function(event,key){
      this.state.model.type = event.target.value;
      this.setState({
         model:this.state.model
      })

    },
    otpChange:function(event){
        this.state.otp = event.target.value;
        this.setState({
           otp:this.state.otp
        })
    },
    handleClose:function(){
        this.setState({
            isShowingModal:false
        })
    },
    emailVaidationClose:function(){
        this.setState({
            isUserAlreadyExistsModal:false
        })
    },
    password:function(value){
      var format = /^((?!.*[\s])(?=.*[!@#$%^&*()_+])(?=.*[0-9]).{8,15})/;
      if(!format.test(value)){
        return{
           name:password,
           error:'Enter a Valied Password, Password must contain a special charecter and a number,Blank spaces are not allowed.'
        }
      }
    },

    render:function(){
      var validation = {
        email:this.email,
        number:this.number,
        password:this.password,
      }
      return(
             <section className="register-wrap">
                <div className="container">
                  <div className="row">
                    <div className="col-sm-6 col-md-7 pull-right regfrmwrap">
                      <div className="reg-bord-box">
                      <div className="reg-box-form">
                        <h3>Signup</h3>
                        <div className="regfrm-inrbx">
                          <Form onSubmit={this.onSubmit}  model={this.state.model} >
                            <div className="form-group">
                              <Validator validators={validators['name']}>
                                <MyFormGroup>
                                    <label htmlFor="name">Name</label>
                                    <Input className="form-control" id="name" name="name" value={this.state.model.name} className="form-control" maxLength={15} />
                                </MyFormGroup>
                              </Validator>
                            </div>
                            <div className="form-group">
                              <Validator validators={[required('Email is required'),validation['email']]}>
                               <MyFormGroup>
                                      <label htmlFor="email">Email</label>
                                      <Input  id="email" name="email" value={this.state.model.email} className="form-control"  />
                                </MyFormGroup>
                              </Validator>
                            </div>
                            <div className="form-group showpassbox">
                              <Validator validators={[required('Password is required'), minLength(8), maxLength(30) ,validation['password']]}>
                                <MyFormGroup>
                                      <label htmlFor="password">Password</label>
                                      <Input type="password" id="password" name="password" value={this.state.model.password} className="form-control"  />
                                </MyFormGroup>
                              </Validator>
                              <button className="ps-shw-lnk" type="button" id="sh-hd-pass" onClick={this.showPassword}>Show</button>
                            </div>
                            <div className="form-group">
                              <Validator validators={validators['phone']}>
                                 <MyFormGroup>
                                        <label htmlFor="phone">Phone Number</label>
                                        <Input type="text" id="phone" name="phone" value={this.state.model.phone} className="form-control"  />
                                  </MyFormGroup>
                              </Validator>
                            </div>
                            <div className="form-group usr-typ-sel">
                                  <p>I am</p>
                                  <RadioGroup onChange={this.handleChange} value={this.state.model.type}>
                                        <Radio value={'buyer_owner'}>Buyer/Owner</Radio>
                                        <Radio value={'agent'}>Agent</Radio>
                                        <Radio value={'builder'}>Builder</Radio>
                                  </RadioGroup>
                            </div>
                            <p className="signup-agr">By signing up I agree to Wynvent’s <Link to="terms-and-conditions">Terms & Conditions</Link></p>
                            <div className="cntr-algn-wrp">
                              <button className="btn btn-red"  disabled={ this.state.buttonDisabled } type="submit">Signup</button>
                            </div>
                          </Form>
                        </div>

                      </div>
                      </div>
                    </div>
                     <UserAuthSideBar />
                  </div>
                </div>
                {this.state.isShowingModal && <SignupSucess />}

                <button type="button"  className="btn btn-info btn-lg"  id='modelBtnRegister'  style={{'display':'none'}} data-toggle="modal" data-backdrop="static" data-target="#myModal3">Open Modal</button>
                <div id="myModal3" className="modal fade pop-show" role="dialog">
                      <div className="modal-dialog">
                        <div className="modal-content">
                              <div className="modal-header">
                              <b>VERIFY PHONE NUMBER</b>
                                <button type="button" className="close" id='modelClose' data-dismiss="modal" onClick={this.closeModel} aria-label="Close">&times;</button>
                              </div>
                              <div className="succes-msgdiv">
                                   <br/>
                                   <h5>We have send a 6 digit code to your number</h5>
                                   <br/>
                                   <h4>+91 {this.state.model.phone}</h4>
                                   <br/>
                                   <h5>Please enter the same to verify number</h5>
                                   <br/>
                                   <h5 style={{'color':'red'}}>{this.state.error}</h5>
                                   <div className="form-group with-pading">
                                    <input type="text" id="otp" value={this.state.otp} onChange={this.otpChange} name="otp" className="form-control"/>
                                   </div>
                                   <h5>Did not get the code ? <a onClick = {this.otpResend}>Resend</a></h5>
                                   <br/>
                              </div>
                              <div className="form-group text-center"><button className="btn btn-red" type="button"  aria-label="Close"  onClick={this.otpSubmit}>VERIFY NUMBER</button></div>
                              <br/>
                        </div>
                      </div>
                </div>

          </section>
      )
    }
});
module.exports = SignUp;
