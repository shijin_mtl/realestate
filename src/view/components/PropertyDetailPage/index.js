import React from 'react';
import BreadCrumb from './BreadCrumb';
import PropertyDetailImage from './PropertyDetailImage';
import PropertyDetailInfo from './PropertyDetailInfo';
import PropertyDetailBrief from './PropertyDetailBrief';
import PropertyDetailDescription from './PropertyDetailDescription';
import PropertyDetailAction from 'components/PropertyDetailPage/action/PropertyDetailAction';
import PropertyDetailStore from 'components/PropertyDetailPage/store/PropertyDetailStore';
import PropertyDetailRightSide from './PropertyDetailRightSide';
import 'public/css/style_bi.css';
import ShortListedAction from 'components/PropertyDetailPage/action/ShortListedAction';
import LoginPopup from 'components/SubmitProperty/LoginPopup';
import ModalWindow from 'react-responsive-modal';
import LoginStore from 'components/Login/store/LoginStore';
import LoginFacebookStore from 'components/Login/store/LoginFacebookStore';
import LoginGoogleStore from 'components/Login/store/LoginGoogleStore';
import PropertyDetailLandmark from './PropertyDetailLandmark';
import MetaTags from 'react-meta-tags';


var PropertyDetailPage = React.createClass({
   getInitialState:function(){
       return{
          propertyDetailList:{},
          showPleaseLogin:false
       }
   },
   componentWillMount:function(){
     window.scrollTo(0,0);
     PropertyDetailStore.bind(this.propertyDetailResponseReceived);
     PropertyDetailAction.propertyDetailAction({id:this.props.location.query.id});
     LoginStore.bind(this.loginResponseReceived);
     LoginFacebookStore.bind(this.facebookLoginResponseReceived);
     LoginGoogleStore.bind(this.googleLoginResponseReceived);

   },
   componentWillUnmount:function(){
      PropertyDetailStore.unbind(this.propertyDetailResponseReceived);
      LoginStore.unbind(this.loginResponseReceived);
      LoginFacebookStore.unbind(this.facebookLoginResponseReceived);
      LoginGoogleStore.unbind(this.googleLoginResponseReceived);
   },
   loginResponseReceived:function(){
       PropertyDetailAction.propertyDetailAction({id:this.props.location.query.id});
   },
   facebookLoginResponseReceived:function(){
       PropertyDetailAction.propertyDetailAction({id:this.props.location.query.id});
   },
   googleLoginResponseReceived:function(){
       PropertyDetailAction.propertyDetailAction({id:this.props.location.query.id});
   },
   propertyDetailResponseReceived:function(){
      var propertyDetailResponse = PropertyDetailStore.getResponse().data;
      this.setState({
         propertyDetailList:propertyDetailResponse
      })
   },
   shortList:function(id){
       if(JSON.parse(localStorage.getItem('isLoggedIn'))){
          if(this.state.propertyDetailList.shortlisted_status){
             ShortListedAction.shortListAction({property_id:id,action:"remove"})
             this.state.propertyDetailList.shortlisted_status = !this.state.propertyDetailList.shortlisted_status ;

           }
        else{
             ShortListedAction.shortListAction({property_id:id,action:"add"})
              this.state.propertyDetailList.shortlisted_status = !this.state.propertyDetailList.shortlisted_status ;
        }

         this.setState({
            propertyDetailList: this.state.propertyDetailList
         })
       }
       else{
              this.setState({
                 showPleaseLogin:true
              })
       }
   },
   handleErrorClose:function(){
       this.setState({
         showPleaseLogin:false
       })
   },
   render:function(){

   	 return(
   	 	        <div className="listing-detail-section">
              <MetaTags>
                  <meta id="meta-description" name="description" content={this.state.propertyDetailList.interesting_details} />
                  <meta id="og-title" property="og:title" content={"Wynvent" + this.state.propertyDetailList.name}/>
              </MetaTags>

   	 	          <div className="container">
                     <BreadCrumb />
                      <div className="row">
                        <div className="col-md-9 col-sm-12">
                          <PropertyDetailImage imageList = {this.state.propertyDetailList}/>
                          <PropertyDetailInfo  propertyDetailInfo={this.state.propertyDetailList}/>
                            <div className="property-brief-section">
                               <PropertyDetailBrief propertyBrief={this.state.propertyDetailList}/>
                               <PropertyDetailDescription  productDescription={this.state.propertyDetailList}/>
                               <PropertyDetailLandmark productLandmark={this.state.propertyDetailList} />
                            </div>
                        </div>
                        <div className='no-print'>
                        <PropertyDetailRightSide  propertyDetailList={this.state.propertyDetailList}
                         pathName={this.props.location.pathname} query={this.props.location.query}
                         imageList = {this.state.propertyDetailList} shortList={this.shortList}/>
                         </div>
                      </div>
   	 	          </div>


                        <ModalWindow open={this.state.showPleaseLogin} onClose={this.handleErrorClose} little>
                              <LoginPopup  onClose={this.handleErrorClose}/>
                        </ModalWindow>

   	 	        </div>
   	 )
   }
});
module.exports  = PropertyDetailPage;
