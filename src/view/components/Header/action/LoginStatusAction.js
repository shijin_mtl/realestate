import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Header/constants/LoginStatusConstant';
import config from 'utils/config';
import axios from 'axios';
var onLoginStatus = function(){

}

const loginStatusApi = axios.create({
  withCredentials: true,
}); 

onLoginStatus.prototype = {
	loginStatus:function(){
		loginStatusApi.get(config.server + 'api/v1/session-status/')
			.then(function (response) {
			        AppDispatcher.dispatch({
				        actionType: Constants.LOGIN_STATUS_RESPONSE_RECIEVED,
				        data: response
					});
			})
			.catch(function (error) {
			    console.log(error);
            });
				
	}
}


module.exports = new onLoginStatus();
