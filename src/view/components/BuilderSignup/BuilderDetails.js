import React from 'react';
import {Link} from 'react-router';
import { Checkbox } from 'antd';
import { Form, FormGroup,TextArea, Validator, Input,required,minLength,maxLength,pattern,email,custom} from 'romagny13-react-form-validation';
import BuilderAction from 'components/BuilderSignup/action/BuilderDetailsAction';
import BuilderDetailStore from 'components/BuilderSignup/store/BuilderDetailsStore';
import BuilderEdit from 'components/BuilderSignup/action/getBuilderAction';


import {Modal} from 'antd';
const TWO_MiB = 2097152;
var allowedExtension = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif', 'image/bmp'];



const validators = {
'name': [required('Name is Required'), minLength(3),maxLength(30),pattern(/^[a-zA-Z']+(\s.?[a-zA-Z']+){0,4}$/, '3 to 30 characters no symbols and numbers ')],
};
var MyFormGroup = function (props) {
var groupclassName = props.hasError ? 'form-group has-error' : 'form-group';
return (
<div className={groupclassName}>
   {props.children}
   {props.hasError && <span className="help-block">{props.error}</span>}
</div>
);
};
var BuilderDetails = React.createClass({
getInitialState:function(){
return{model:
{
name:this.props.details?this.props.details.name:"",
contact_person:this.props.details?this.props.details.contact_person:"",
property_type:this.props.details?this.props.details.property_type:[],
city:this.props.details?this.props.details.city:"",
description:this.props.details?this.props.details.description:"",
featured:false,
verified:false,
logo:""
},
btnStatus:false,
placeholder:""
}
},
componentWillMount:function(){
BuilderDetailStore.bind(this.builderResponseReceived);
},
componentDidMount:function(){
$("#uploadTrigger").click(function(){
$("#logo").click();
});

},
componentWillUnmount:function(){
BuilderDetailStore.unbind(this.builderResponseReceived);
},
builderResponseReceived:function(){
  localStorage.tempId=null
this.setState({
btnStatus:false
})
var BuilderResponse = BuilderDetailStore.getResponse();
this.props.changeContent()
},

submit:function(result){
if(!result.hasError){
this.setState({
btnStatus:true
})
result.model.description = this.state.model.description
result.model.logo = this.state.model.logo
result.model.city = this.state.model.city
result.model.property_type_list = this.state.model.property_type



  if(this.props.details != undefined){
    result.model.user_builder = this.props.details.user_builder
    BuilderEdit.postEditData(result.model,this.props.id)
  }
  else {
    BuilderAction.expert(result.model)
  }


}
},
handleChange:function(key,event){
if ( key == 'name'){
this.state.model.name  = $('#name').value
}
if ( key == 'contact_person'){
this.state.model.contact_person  = $('#contact_name').value
}
if (key == 'city'){
this.state.model.city  = event.target.value
}
if ( key == 'description'){
this.state.model.description  = event.target.value
}
if ( key == 'logo'){

  if(event.target.files[0].size > TWO_MiB || allowedExtension.indexOf(event.target.files[0].type) <= -1)
  {
    event.target.files[0]=null
    Modal.error({
     title: 'File upload error',
     content: 'Please upload image file not more than 2MB in size',
     okText :'Ok'
   });
  }
  else {
    this.state.model.logo  = event.target.files[0]
    this.state.placeholder = event.target.files[0].name
    this.setState({
      placeholder:this.state.placeholder
    })
  }

}
this.setState({
model:this.state.model
})
},
changeSelect:function(key,event){



if(event.target.checked)
{
 this.state.model.property_type.push(key)
}
else
{
    var index = this.state.model.property_type.indexOf(key)
    this.state.model.property_type.splice(index, 1);
}


this.setState({
property_type:this.state.model.property_type
})

},
render:function(){
return(
<span>
   <div className="submit-property-section">
      <div className="container">
         <div className="row">
            <div className="col-sm-12">
               <ul className="breadcrumb">
                  <li>
                     <Link to="/">
                     Home</Link>
                  </li>
                  <li className="active">BUILDER DETAILS</li>
               </ul>
            </div>
         </div>
         <div className="row">
            <div className="col-sm-12">
               <Form className="submit-property-form form-horizontal" onSubmit={this.submit} model={this.state.model} ref="form">
                  <h2 className="text-center">TELL US ABOUT YOURSELF</h2>
                  <h5 className="text-center">Help us showcase your profile better</h5>
                  <div className="property-details-section" style = {{'borderBottom': 0 +"px"}}>
                  <div className='row'>
                     <div className="col-md-7 col-sm-12">
                        {/*<div className="personal-info-section">
                           <h4 className="form-title">BUILDER  DETAILS</h4>
                        </div>
                        <div className="property-basic-inner-form">
                           <div className="row">
                              <div className="col-md-6 col-sm-12">
                                 <div className="form-group make-align">
                                    <label className="col-sm-4 control-label">Company Name</label>
                                    <div className="col-sm-8">
                                       <Validator validators={validators['name']}>
                                          <MyFormGroup>
                                             <Input type="text" name='name' id='name' name='name' className="form-control" value={this.state.model.name} onChange={this.handleChange.bind(this,"name")} placeholder=""/>
                                          </MyFormGroup>
                                       </Validator>
                                    </div>
                                 </div>
                              </div>
                              <div className="col-md-6 col-sm-12 col-sm-hidden"></div>
                           </div>
                           <div className="row">
                              <div className="col-md-6 col-sm-12">
                                 <div className="form-group">
                                    <label className="col-sm-4 control-label">Company Name</label>
                                    <div className="col-sm-8">
                                       <input type="text" className="form-control"/>
                                    </div>
                                 </div>
                              </div>
                              <div className="col-md-6 col-sm-12">
                                 <p>Upload pnhghg</p>
                              </div>
                           </div>
                           <div className="row">
                              <div className="col-md-6 col-sm-12">
                                 <div className="form-group">
                                    <label className="col-sm-4 control-label">Company Name</label>
                                    <div className="col-sm-8">
                                       <div className="">
                                          <input type="file" className="form-control hidden" id="logo" onChange={this.handleChange.bind(this,"logo")}  placeholder={this.state.placeholder} />
                                          <div className="button-browse" id="uploadTrigger">Upload File</div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div className="col-md-6 col-sm-12  col-sm-hidden"></div>
                           </div>
                           <div className="row">
                              <div className="col-md-6 col-sm-12">
                                 <div className="form-group">
                                    <label className="col-sm-4 control-label">Company Name</label>
                                    <div className="col-sm-8">
                                       <div className="row">
                                          <div className="col-md-12 col-sm-12">
                                             <Checkbox onChange={this.changeSelect.bind(this,'residential_apartment')}>Residential House/Villa</Checkbox>
                                             <Checkbox onChange={this.changeSelect.bind(this,'residential_apartment')}>Multistorey Apartment</Checkbox>
                                             <Checkbox onChange={this.changeSelect.bind(this,'residential_apartment')} >Commercial Showroom</Checkbox>
                                             <Checkbox onChange={this.changeSelect.bind(this,'residential_apartment')}>Commercial Office Space</Checkbox>
                                             <Checkbox onChange={this.changeSelect.bind(this,'residential_apartment')}>Commercial Shop</Checkbox>
                                             <Checkbox onChange={this.changeSelect.bind(this,'residential_apartment')}>Industrial Building</Checkbox>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div className="col-md-6 col-sm-12 col-sm-hidden"></div>
                           </div>
                           <div className="row">
                              <div className="col-md-6 col-sm-12">
                                 <div className="form-group">
                                    <label className="col-sm-4 control-label">Company Name</label>
                                    <div className="col-sm-8">
                                       <Validator validators={validators['name']}>
                                          <MyFormGroup>
                                             <Input type="text" name='name' id='name' name='name' className="form-control" value={this.state.model.name} onChange={this.handleChange.bind(this,"name")} placeholder=""/>
                                          </MyFormGroup>
                                       </Validator>
                                    </div>
                                 </div>
                              </div>
                              <div className="col-md-6 col-sm-12 col-sm-hidden"></div>
                           </div>
                           <div className="row">
                              <div className="col-md-6 col-sm-12">
                                 <div className="form-group">
                                    <label className="col-sm-4 control-label">Contact Person</label>
                                    <div className="col-sm-8">
                                       <Validator validators={validators['name']}>
                                          <MyFormGroup>
                                             <Input type="text" className="form-control" id="contact_person" name="contact_person" value={this.state.model.contact_person} onChange={this.handleChange.bind(this,"contact_person")} placeholder=""/>
                                          </MyFormGroup>
                                       </Validator>
                                    </div>
                                 </div>
                              </div>
                              <div className="col-md-6 col-sm-12 col-sm-hidden"></div>
                           </div>
                           <div className="row">
                              <div className="col-md-6 col-sm-12 text-right">
                                 <label className="col-sm-4 control-label col-sm-hidden"></label>
                                 <div className="col-sm-8 text-center">
                                    <button type="submit" className="btn btn-red">SUBMIT</button>
                                 </div>
                              </div>
                              <div className="col-md-6 col-sm-12 col-sm-hidden"></div>
                           </div>
                        </div>*/}
                        <div className="row">
                           <div className="personal-info-section">
                              <h4 className="form-title">BUILDER  DETAILS</h4>
                              <div className="property-basic-inner-form">
                                 <div className="form-group make-align">
                                    <label  className="col-sm-3 control-label">Company Name<span style={{'color':'red'}}>*</span></label>
                                    <div className="col-sm-9">
                                       <Validator validators={validators['name']}>
                                          <MyFormGroup>
                                             <Input type="text" name='name' id='name' name='name' className="form-control" value={this.state.model.name} onChange={this.handleChange.bind(this,"name")} placeholder=""/>
                                          </MyFormGroup>
                                       </Validator>
                                    </div>
                                 </div>
                                { /*<div className="form-group">
                                    <label  className="col-sm-3 control-label">Contact Person<span style={{'color':'red'}}>*</span></label>
                                    <div className="col-sm-9">
                                       <Validator validators={validators['name']}>
                                          <MyFormGroup>
                                             <Input type="text" className="form-control" id="contact_person" name="contact_person" value={this.state.model.contact_person} onChange={this.handleChange.bind(this,"contact_person")} placeholder=""/>
                                          </MyFormGroup>
                                       </Validator>
                                    </div>
                                 </div>*/}
                                 <div className="form-group browse-button">
                                    <label  className="col-sm-3 control-label">Company Logo</label>
                                    <div className="col-sm-9">
                                       <Validator>
                                          <MyFormGroup>
                                          <input type="file" className="form-control hidden" id="logo"  placeholder={this.state.placeholder} onChange={this.handleChange.bind(this,"logo")} style={{'display':'none'}}/>
                                          <div className="button-browse" id="uploadTrigger" style={{'textAlign': this.state.placeholder!=""?'left':'right','color':this.state.placeholder!=""?'black':'red'}} ><div className="text-left" style={{"width":"50%","display": "inline-block"}}>{this.state.placeholder}</div><div className="text-right" style={{"width":"50%","display": "inline-block"}}>Browse..</div></div>
                                          </MyFormGroup>
                                       </Validator>
                                    </div>
                                 </div>
                                 <div className="col-md-3 col-sm-12">
                                    <label className="control-label">Property Types</label>
                                 </div>
                                 <div className="col-md-9 col-sm-12">
                                    <div className="row">
                                       <div className="col-md-6 col-sm-12">
                                          <Checkbox onChange={this.changeSelect.bind(this,1)} defaultChecked={this.props.details != undefined?this.props.details.property_type.includes(1):false}>Residential/Apartments</Checkbox>
                                          <Checkbox onChange={this.changeSelect.bind(this,2)} defaultChecked={this.props.details != undefined?this.props.details.property_type.includes(2):false}>Residential/Villas </Checkbox>
                                       </div>
                                       <div className="col-md-6 col-sm-12 ">
                                          <Checkbox onChange={this.changeSelect.bind(this,3)} defaultChecked={this.props.details != undefined?this.props.details.property_type.includes(3):false}>Commercial/Showrooms </Checkbox>
                                          <Checkbox onChange={this.changeSelect.bind(this,4)} defaultChecked={this.props.details != undefined?this.props.details.property_type.includes(4):false}>Commercial/Shops</Checkbox>
                                       </div>
                                    </div>
                                 </div>
                                 <div className="form-group">
                                    <label  className="col-sm-3 control-label">Location Presence</label>
                                    <div className="col-sm-9">
                                       <Validator>
                                          <MyFormGroup>
                                             <input type="text" className="form-control" id="city"   value={this.state.model.city} onChange={this.handleChange.bind(this,"city")} placeholder="Type city names separated by comma"/>
                                          </MyFormGroup>
                                       </Validator>
                                    </div>
                                 </div>
                                 <div className="form-group">
                                    <label  className="col-sm-3 control-label">Description</label>
                                    <div className="col-sm-9">
                                       <MyFormGroup>
                                          <textarea className="form-control" rows="7" value={this.state.model.description} onChange={this.handleChange.bind(this,"description")} ></textarea>
                                       </MyFormGroup>
                                    </div>
                                 </div>
                                 <div className="form-group">
                                    <label  className="col-sm-3 control-label"></label>
                                    <div className="col-sm-9 text-center">
                                       <button type="submit" disabled = {this.state.btnStatus} className="btn btn-red" >SUBMIT</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="col-md-5 col-sm-12">
                     </div>
                  </div>
            </div>
            </Form>
         </div>
      </div>
   </div>
   </div>
   <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtnFill' data-toggle="modal" data-target="#myModal3">Open Modal</button>
   <div id="myModal3" className="modal fade pop-show" role="dialog">
      <div className="modal-dialog">
         <div className="modal-content">
            <div className="modal-header">
               <b>FILL ALL  FIELDS</b>
               <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
            </div>
            <div className="succes-msgdiv">
               <img src="images/error-icon.png" width="85" className="icon-succ" alt="Success" />
               <br/><br/>
               <h4>ERROR!</h4>
               <br/>
               <h5>Please fill all the fields</h5>
               <br/>
            </div>
            <div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" >OK</button></div>
            <br/>
         </div>
      </div>
   </div>
</span>
)
}
})
module.exports = BuilderDetails;
