import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Home/constants/HomeConstant';
import config from 'utils/config';
import axios from 'axios';
var talkToExpertAction = function(){

}

const expertApi = axios.create({
  withCredentials: true,
}); 

talkToExpertAction.prototype = {
	expert:function(params){
		expertApi.post(config.server + 'api/v1/contact-us/',params)
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.EXPERT_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });			
	}
}


module.exports = new talkToExpertAction();
