import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/AgentList/constants/PropertyPulseConstants';
import config from 'utils/config';
import axios from 'axios';
var AgentAction = function(){

}

const agentApi = axios.create({
  withCredentials: true,
});

const sessionCount = axios.create({
  withCredentials: true,
});

var viewCountApi


AgentAction.prototype = {
	AgentList:function(params){
        AppDispatcher.dispatch({
	        actionType: Constants.LOADING_RESPONSE_RECEIVED,
	        data: "loading"
		});
		agentApi.get(config.server + 'api/v1/agent/?sort='+params.sort +"&page="+params.page+(params.city?"&city="+params.city:"") )
			.then(function (response) {
        makeClick(response.data.results)
		        AppDispatcher.dispatch({
			        actionType: Constants.AGENT_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}


function makeClick(data){
  sessionCount.get(config.server +'api/v1/session-status')
    .then(function (response) {
      viewCountApi = axios.create({
        withCredentials: true,

      });
    }).then(function(){
    for(let i=0;i<data.length;i++)
    {
      viewCountApi.get(config.server +'core/data/process/?type=agent&action=v_ct&slug='+data[i].slug)
        .then(function (response) {
              AppDispatcher.dispatch({
                actionType: Constants.VIEW_RESPONSE_RECEIVED,
                data: response
          });
        })
        .catch(function (error) {
            console.log("error...",error);
        });
    }})
}

module.exports = new AgentAction();
