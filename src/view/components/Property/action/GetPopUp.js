import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Property/constants/PropertyConstant';
import config from 'utils/config'
import axios from 'axios';

var popUpGet = function(){

}

const getPop = axios.create({
  withCredentials: true
});

popUpGet.prototype = {
	 getNow:function(parameters){
	 	getPop.get(config.server  + 'api/v1/activity-based-popup/')
		    .then(function (response) {
		    	AppDispatcher.dispatch({
					actionType: Constants.GET_POPUP_RESPONSE_RECEIVED,
					data: response
				});

            })
			.catch(function (error) {
			     console.log(error);
            });


	 }
}

module.exports = new popUpGet();
