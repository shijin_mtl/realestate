import React from 'react';
import BackGroundImage from 'public/images/no-photo.jpg';
import {hashHistory} from 'react-router';
import {Gmaps,Marker} from 'react-gmaps';
import moment from 'moment';
var getPriceFormat = require('utils/ChangePrice.js').getPriceFormat;



var SinglePropertyList = React.createClass({
	 showImage:function(){				
        	 return(<div className="prop-thumb" style={{backgroundImage: `url(${this.props.propertyList.thumbnail_url?this.props.propertyList.thumbnail_url:"https://d164l10pl7hj29.cloudfront.net/react-static/images/no-photo.jpg"})`}}></div>)

	 },
	 showPropertDetailPage:function(){
        hashHistory.push({
          pathname:"/propertyDetails",
          query:{id:this.props.propertyList.id}
        })
	 },
     render:function(){
     	     var price = getPriceFormat(this.props.propertyList.expected_price);
			 if (this.props.propertyList.coordinates != null)
			 {
				 var  initList = (this.props.propertyList.coordinates).split(" ")
				 initList.splice(0, 1);
				 initList[0] = initList[0].replace('(',"")
				 initList[1] = initList[1].replace(')',"")
				 var lat = initList[1]
				 var long = initList[0]
			 }
	    var createdTime =  moment(this.props.propertyList.created).format("Do MMM YY");
        var objMap = {buyer_owner:"Buyer/Owner",agent:"Agent",builder:"Builder"};
        if(this.props.propertyList.monthly_rent ){
        	price = getPriceFormat(this.props.propertyList.monthly_rent)
        }
     	return(
     		    <div className="col-sm-6 col-md-4 pplrlistsep">
				    <div className="proplistbox ih-item square effect10 bottom_to_top">
				        <a onClick={this.showPropertDetailPage} className="propanchr">
				            <div className="propbandred">{this.props.propertyList.property_for}</div>
				             {this.showImage()}
				            <div className="prop-infobox">
				                <h3>{this.props.propertyList.name}</h3>
				                <h4>{this.props.propertyList.locality}</h4>
				                <h5><span className="pull-left">Posted by {objMap[this.props.propertyList.user_type]}</span> <span className="pull-right">{createdTime}</span><br className="clearfix"/></h5>
				            </div>
				            <div className="prop-price">
				                <h2><i className="fa fa-inr" aria-hidden="true"></i>{price?price:"Not Mentioned"}</h2>
				                {price && <span className="nego-ttl">Slightly Negotiable</span>}
				            </div>
				            <div className="info">
			                {/*<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3928.999851504661!2d76.33791531471172!3d10.016869992839569!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b080c92bbbfefc5%3A0x59376e17faf64b88!2sSayOne+Technologies!5e0!3m2!1sen!2sin!4v1496141440512" width="600" height="450" frameBorder="0" allowFullScreen></iframe>*/}
											<Gmaps onClick={this.showPropertDetailPage}
													width={'350px'}
													height={'450px'}
													lat={ lat }
													lng={ long }
													zoom={12}
													scrollwheel={false}
													viewport={true}>
														<Marker
															lat={ lat }
															lng={ long }
															/>
											</Gmaps>
				                 <div className="hvr-btn-map">
				                    <button className="btn btn-red" onClick={this.showPropertDetailPage}>Read more</button>
				                </div>
				            </div>
				        </a>
				    </div>
				</div>
     	)
     }
});
module.exports = SinglePropertyList;
