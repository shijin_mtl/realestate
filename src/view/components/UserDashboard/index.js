import React from 'react';
import UserDetailsAction from 'components/Header/action/UserDetailsAction';
import UserDetailStore from 'components/Header/store/UserDetailsStore';
import BreadCrumb from './BreadCrumb';
import ProfileList from './ProfileList';
import ShortListedPropertyAction from 'components/UserDashboard/action/ShorlistedPropertyAction';
import ShortListedPropertyStore from 'components/UserDashboard/store/ShortListedStore';
import MyListingAction from 'components/UserDashboard/action/MyListingsAction';
import MyListingStore from 'components/UserDashboard/store/MyListingStore';
import UserResponseStore from 'components/UserDashboard/store/UserResponseStore';
import UserResponseAction from 'components/UserDashboard/action/PopertyResponsesAction';
import DeletePropertyAction from 'components/UserDashboard/action/DeletePropertyAction';
import DeleteProprtyStore from 'components/UserDashboard/store/DeletePropertyStore';
import {hashHistory} from 'react-router';
import config from 'utils/config';



var UserDashboard = React.createClass({
   getInitialState:function(){
      return{
              userDetails:{},
              shortListedList:{results:[]},
              myListings:{results:[]},
              isLoading:true,
              myResponseList:[],
              initialReponsePageNum:1,
              shortListpageNum:1

      }
   },
   onChangeResponsePageNum:function(pageNum){
       this.state.initialReponsePageNum = pageNum;
       this.setState({
           initialReponsePageNum:this.state.initialReponsePageNum
       })
       MyListingAction.myLists({pageNum:this.state.initialReponsePageNum});
   },
   componentWillMount:function(){
     window.scrollTo(0,0);
     UserDetailsAction.userDetails();
     UserDetailStore.bind(this.userResponseReceived);
     ShortListedPropertyStore.bind(this.shortListedResponseReceived);
     MyListingStore.bind(this.myListResponseReceived);
     ShortListedPropertyAction.property({pageNum:this.state.shortListpageNum});
     MyListingAction.myLists({pageNum:this.state.initialReponsePageNum});
     UserResponseAction.userResponse();
     UserResponseStore.bind(this.myResponseReceived);
     $(".side-collapse").addClass('in');
     DeleteProprtyStore.bind(this.deletePropertyResponseReceived);
   },
   componentWillUnmount:function(){
     UserDetailStore.unbind(this.userResponseReceived);
     ShortListedPropertyStore.unbind(this.shortListedResponseReceived);
     UserResponseStore.unbind(this.myResponseReceived);
     MyListingStore.unbind(this.myListResponseReceived);
     DeleteProprtyStore.unbind(this.deletePropertyResponseReceived);

   },
   deletePropertyResponseReceived:function(){
     $('#modelBtn').click()
     MyListingAction.myLists({pageNum:this.state.initialReponsePageNum});
   },
   shortListedResponseReceived:function(){
     var shorListedResp = ShortListedPropertyStore.getResponse();
     this.setState({
        shortListedList:shorListedResp.data
     })
   },
   myResponseReceived:function(){
      var myResponses = UserResponseStore.getResponse();
      this.setState({
         myResponseList:myResponses.data
      })
   },
   userResponseReceived:function(){
     var userResponse = UserDetailStore.getResponse().data;
     this.setState({
     	 userDetails:userResponse
     })
   },
   myListResponseReceived:function(){
      var myListsResp = MyListingStore.getResponse();
        this.setState({
               isLoading:false,
               myListings:myListsResp.data
        })
   },
   changeShortListProperty:function(pageNum){
      this.state.shortListpageNum = pageNum;
      this.setState({
          shortListpageNum:this.state.shortListpageNum
      })
      ShortListedPropertyAction.property({pageNum:this.state.shortListpageNum});

   },
   deleteProperty:function(id,index){
    //  this.state.myListings.results.splice(index,1);
     this.setState({
        myListings: this.state.myListings
     })
     DeletePropertyAction.deleteProperty({id:id})
   },
   packageDetail:function(){

   },
   render:function(){
   	 return(
   	 	     <section className="dashboard-wrap">
   	 	         <div className="container">
                    <BreadCrumb />
                    <ProfileList userDetails = {this.state.userDetails}
                      shortListedList={this.state.shortListedList}
                      myListings={this.state.myListings}
                      isLoading={this.state.isLoading}
                      myResponseList={this.state.myResponseList}
                      onChangeResponsePageNum = {this.onChangeResponsePageNum}
                      changeShortListProperty={this.changeShortListProperty}
                      deleteProperty={this.deleteProperty}
                    />
   	 	         </div>
               <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtn' data-toggle="modal" data-target="#myModal">Open Modal</button>
               <div id="myModal" className="modal fade pop-show" role="dialog">
                     <div className="modal-dialog">
                       <div className="modal-content">
                       <br/>
                             <div className="modal-header">
                             <b>PROPERTY DELETED</b>
                               <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.packageDetail}>&times;</button>
                             </div>
                             <div className="succes-msgdiv">
                                  <img src={config.s3static + "images/done-icon.png"} width="85" className="icon-succ" alt="Success" />
                                  <br/><br/>
                                  <h4>PROPERTY DELETED!</h4>
                                  <br/>
                                  <br/>
                             </div>
                             <div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" onClick={this.packageDetail} >OK</button></div>
                             <br/>
                       </div>
                     </div>
               </div>

   	 	     </section>

   	 )
   }
});
module.exports = UserDashboard;
