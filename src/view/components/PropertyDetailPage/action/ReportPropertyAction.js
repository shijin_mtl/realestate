import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PropertyDetailPage/constants/PropertyDetailConstant';
import config from 'utils/config';
import axios from 'axios';

var reportProperty = function(){

}

const reportPropertyApi = axios.create({
  withCredentials: true,
  headers: {
    'crossDomain': true,
  }
});

const viewCountApi = axios.create({
  withCredentials: true,
});

reportProperty.prototype = {
	contactAction:function(params){
    var userDetail = JSON.parse(localStorage.getItem('userDetails'));
    params.user = userDetail.id?userDetail.id:""
		reportPropertyApi.post(config.server + 'api/v1/property/report/', params)
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.REPORT_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}

module.exports = new reportProperty();
