import React from 'react';
import {Link,hashHistory} from 'react-router';
import ReactDOM from "react-dom";
import config from 'utils/config';
import UIAutocomplete from 'react-ui-autocomplete';
import ListStore from 'components/Home/store/ListStore';
import localAction from 'components/Home/action/fetchLocal';
import LocatoinChangeStore from 'components/Header/store/LocationChangeStore';



var SubHeader2 = React.createClass({
	getInitialState:function(){
	 return{
		tinyList:[],
		suggestList:[]
	 }
 },

	changePropertyFilters:function(key,value){
		ReactDOM.findDOMNode(this.refs.BtnDrop).click()
		if(key == "type"){
			 if (this.state.filterParams.typeListArr.indexOf(value) === -1) {
					this.state.filterParams.typeListArr.push(value);
			 }
			 else{
				 this.state.filterParams.typeListArr.splice(this.state.filterParams.typeListArr.indexOf(value),1)
			 }
		}
		else{
					 this.state.filterParams[key] = value;

		}
			 this.setState({
				 filterParams:this.state.filterParams
				})
	},
	getInitialState:function(){
    return{
			 filterParams:{"property_for":"sale","typeListArr":[]}
		}
	},
	onChange:function(event,newValue,optionValue){
	 this.state.filterParams['locality'] = optionValue.id
 },
 componentWillMount:function(){
	 ListStore.bind(this.autoList2)
	 localAction.getList()
	 LocatoinChangeStore.bind(this.LocationChangeData)
 },
 componentWillUnmount:function(){
	 ListStore.unbind(this.autoList2)
	 LocatoinChangeStore.unbind(this.LocationChangeData)
 },
 LocationChangeData:function(){
	 setTimeout(function(){
			localAction.getList()
		}, 500);
 },
 autoList2:function(){
	 var list = ListStore.getResponse()
	 this.state.suggestList = list
	 this.setState({
		 suggestList:this.state.suggestList
	 })
	 var tinylist = []
	 for(let i =0;i<list.length;i++){
		 tinylist.push(list[i].name)
		 this.setState({
			 tinyList:this.state.tinyList
		 })
	 }
	 this.state.tinyList = tinylist
	 this.setState({
		 tinyList:this.state.tinyList
	 })
	 const getOptions = () => (
			this.state.suggestList
		)
 },

	componentDidMount:function(){
		$('.ui-autocomplete input').attr('placeholder',"LOCATION/AREA");
		localStorage.latitude = ''
		localStorage.longitude = ''

					  var options = {
							types: ['geocode'],
							componentRestrictions: {country: ["in"]}
					 	};

						var autocomplete;
						function initialize() {
						autocomplete = new google.maps.places.Autocomplete(
								/** @type {HTMLInputElement} */(document.getElementById('place-pin2')),
								options);
						google.maps.event.addListener(autocomplete, 'place_changed', function() {

						var place = autocomplete.getPlace()

						var lat = place.geometry.location.lat();
						var lng = place.geometry.location.lng();
						localStorage.setItem('latitude', lat);
						localStorage.setItem('longitude', lng);

						});
					}

				 initialize();


        var sideslider = $('[data-toggle=collapse-side]');
        var sel = sideslider.attr('data-target');
        var sel2 = sideslider.attr('data-target-2');
        sideslider.click(function(event){
           $(sel).toggleClass('in');
           $(sel2).toggleClass('out');
        });
    },
    clickPropertyFor:function(key,value){
    this.state.filterParams[key] = value;
		this.setState({
			filterParams:this.state.filterParams
		})
	},
	onSearch:function(){
		{ /*var loactionValue = ReactDOM.findDOMNode(this.refs.locationId).value
		if(loactionValue == "")
		{
			loactionValue = localStorage.loacalLocation
		}
		this.state.filterParams['location'] = loactionValue;*/ }
		this.state.filterParams['latitude'] = localStorage.latitude
		this.state.filterParams['longitude'] = localStorage.longitude
		this.setState({
			filterParams:this.state.filterParams
		 })
		hashHistory.push({
				 pathname:"/property",
				 query:this.state.filterParams
		 })

	},
	render:function(){
		var propertyForObj = {sale:"Sale",rent:"Rent"}
		return(
			     <div className="bottom-head no-print">
				    <div className="container">
				      <div className="row">
				        <div className="col-xs-5 col-sm-2 col-md-3 logowrap">
				          <Link to="/"  className="toplogo"><img src={config.s3static + "images/logo.png"} alt="Wynvent-logo" className="img-responsive" width="230" /></Link>
				        </div>
				        <div className="col-xs-7 col-sm-10 col-md-9 topsearchwrap">
				          <button data-toggle="collapse-side" data-target=".side-collapse" type="button" className="navbar-toggle pull-right mob-navbar">
				              <span className="icon-bar"></span>
				              <span className="icon-bar"></span>
				              <span className="icon-bar"></span>
				          </button>
				          <button className="btn srchbtn visible-xs" type="button" data-toggle="collapse" data-target="javascript:;searchtop" aria-expanded="false" aria-controls="searchtop"><i className="fa fa-search" aria-hidden="true"></i></button>
				          <div className="collapse in" id="searchtop">
				          <div className="navsearchbox pull-left">
				            <div className="row">
				              <div className="col-xs-6 col-sm-3 srch-sepr purp dropdown">
				                <button type="button" className="btn dropdown-toggle srch-drop-btn"
				                    data-toggle="dropdown" aria-expanded="false" >
				                  <span>{propertyForObj[this.state.filterParams.property_for]}</span>
				                </button>
				                <ul className="dropdown-menu"  >
				                  <li id="1"><a onClick={this.clickPropertyFor.bind(this,"property_for","sale")}>Sale</a></li>
				                  <li id="2"><a onClick={this.clickPropertyFor.bind(this,"property_for","rent")}>Rent</a></li>
				                </ul>
				              </div>
				              <div className="col-xs-6 col-sm-4 srch-sepr prop-typ dropdown">
				                <button type="button" className="btn dropdown-toggle srch-drop-btn" data-toggle="dropdown" ref='BtnDrop'
				                 aria-expanded="false">
				                  <span>Property Type</span>
				                </button>
				                <div className="dropdown-menu" id="proptypedrop" >
				                  <h3>Residential</h3>
				                  <div className="row">
				                    <div className="col-sm-4">
				                      <div className="checkbox checkbox-primary">

				                        <input id="checkbox2" type="checkbox" onClick={this.changePropertyFilters.bind(this,"type","residential_apartment","property")} /><label htmlFor="checkbox2">Apartment</label>
				                      </div>
				                    </div>
				                    <div className="col-sm-4">
				                      <div className="checkbox checkbox-primary">

				                        <input id="checkbox3" type="checkbox" onClick={this.changePropertyFilters.bind(this,"type","residential_villa","property")} /><label htmlFor="checkbox3">House/Villa</label>
				                      </div>
				                    </div>
				                  </div>
				                  <h3>Commercial</h3>
				                  <div className="row">
				                    <div className="col-sm-4">
				                      <div className="checkbox checkbox-primary">
				                        <input id="checkbox5" type="checkbox" onClick={this.changePropertyFilters.bind(this,"type","commercial_showrrom","property")} /><label htmlFor="checkbox5">Showrooms</label>

				                      </div>
				                    </div>
				                    <div className="col-sm-4">
				                      <div className="checkbox checkbox-primary">
				                        <input id="checkbox6" type="checkbox" onClick={this.changePropertyFilters.bind(this,"type","commercial_shop","property")} /><label htmlFor="checkbox6">Shop</label>
				                      </div>
				                    </div>


				                  </div>
				                </div>
				              </div>
				              <div className="col-xs-12 col-sm-5 srch-sepr loc-srch">
											<UIAutocomplete
			                    options={this.state.suggestList?this.state.suggestList:[]}
			                    optionValue="id"
			                    optionFilter={['name']}
			                    optionLabelRender={option => `${option.name}`}
			                    onChange={this.onChange}
													style = {{width:'10%'}}
			                />
											  {/*<input className='auto-class form-control' id="place-pin2" placeholder="LOCATION/AREA" onFocus={this.geolocate} type="text"/>*/}
				                <button type="button" className="btn" onClick={this.onSearch}><i className="fa fa-search" aria-hidden="true"></i></button>
				              </div>
				            </div>
				          </div>
				          </div>
				          <div className="side-collapse in pull-right">
				          <nav className="navbar-collapse">
				                <ul className="nav navbar-nav main-nav main-nav-only">
				                  <li><Link to="/property">buy/rent</Link></li>
				                  <li><Link to="market-watch">market watch</Link></li>
				                  <li><Link to="property-pulse">property pulse</Link></li>
				                  <li className="dropdown">
				                    <a href="javascript:;" className="dropdown-toggle" id="morenavdrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">&amp;more</a>
				                    <span className="bs-caret"><span className="caret"></span></span>
				                    <div className="dropdown-menu moredopbox" aria-labelledby="morenavdrop">
				                      <div className="row">
				                        <div className="col-md-4">
				                          <h4>services</h4>
				                          <ul>
				                            <li><Link to ="/submit-property">Submit a property</Link></li>
				                            <li><a href="javascript:;">Advertise with Us</a></li>
				                            <li><a href="javascript:;">Know your tenant</a></li>
				                            <li><a href="javascript:;">Property Evaluation</a></li>
				                            <li><a href="javascript:;">Legal Services</a></li>
				                            <li><a href="javascript:;">Astrology</a></li>
				                          </ul>
				                        </div>
				                        <div className="col-md-4">
				                          <h4>tools</h4>
				                          <ul>
				                            <li><Link to="loan-calculator">Loan Calculator</Link></li>
				                            <li><a href="javascript:;">Property Worth</a></li>
				                            <li><a href="javascript:;">Compare Localities</a></li>
				                          </ul>
				                        </div>
				                        <div className="col-md-4">
				                          <h4>explore</h4>
				                          <ul>
				                            <li><a href="javascript:;">New Projects</a></li>
				                            <li><a href="javascript:;">Agents</a></li>
				                          </ul>
				                        </div>
				                      </div>
				                    </div>
				                  </li>
				                </ul>
				          </nav>
				          </div>
				        </div>
				      </div>
				    </div>
                </div>
		)
	}
});
module.exports = SubHeader2;
