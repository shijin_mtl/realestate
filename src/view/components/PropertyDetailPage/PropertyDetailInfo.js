import React from 'react';
import moment from 'moment';
import AreaChangeStore from 'components/Header/store/AreaChangeStore';
var unitLink
var getPriceFormat = require('utils/ChangePrice.js').getPriceFormat;


var PropertyDetailInfo = React.createClass({
    getInitialState:function(){
      return {
         area:null,
         unit:null,
         htmlView:null
      }
    },

    componentWillMount:function() {
        AreaChangeStore.bind(this.areaResponse)
    },
    componentWillUnmount:function(){
        AreaChangeStore.unbind(this.areaResponse)
    },
    areaResponse:function() {
      var status = AreaChangeStore.getResponse()
      this.changeArea(status)
    },

    areaInit:function(area,unit) {
        var list
        if(localStorage.area_unit =="s_yd" && this.props.propertyDetailInfo.covered_area_unit== "sq_m"){
            list =  (<h4>{this.m_sq(this.props.propertyDetailInfo.covered_area) +" "+ "SQ ft"} </h4>)
        }
        else if (localStorage.area_unit =="s_yd" && this.props.propertyDetailInfo.covered_area_unit == "sq_ft")
        {
            list =   (<h4>{this.props.propertyDetailInfo.covered_area + " "+"SQ ft"} </h4>)
        }
        else if (localStorage.area_unit == "m2" && this.props.propertyDetailInfo.covered_area_unit == "sq_ft")  {
          list =   (<h4>{this.sq_m(this.props.propertyDetailInfo.covered_area) +" "+ "M sq."} </h4>)
        }
        else {
          list = (<h4>{this.props.propertyDetailInfo.covered_area + " "+" M sq."} </h4>)
        }
        return list
    },


    changeArea:function(status)
    {
      var list
      if(status && this.props.propertyDetailInfo.covered_area_unit== "sq_m"){
          list =  (<h4>{this.m_sq(this.props.propertyDetailInfo.covered_area) +" "+ "SQ ft"} </h4>)
      }
      else if (!status && this.props.propertyDetailInfo.covered_area_unit == "sq_ft")
      {
          list =   (<h4>{this.sq_m(this.props.propertyDetailInfo.covered_area) +" "+ "M sq."} </h4>)
      }
      else if (status && this.props.propertyDetailInfo.covered_area_unit == "sq_ft")  {
        list =   (<h4>{this.props.propertyDetailInfo.covered_area + " "+"SQ ft"} </h4>)
      }
      else {
        list = (<h4>{this.props.propertyDetailInfo.covered_area + " "+" M sq."} </h4>)
      }
      this.state.htmlView = list
      this.setState({
        htmlView:this.state.htmlView
      })

    },
      sq_m:function(value){
        return Math.round((value/10.7639) * 100) / 100
      },
      m_sq:function(value){
        return Math.round((value*10.7639) * 100) / 100
      },




    render:function(){
       var status = AreaChangeStore.getResponse()
        var address ;
        if(this.props.propertyDetailInfo.address){
            address = this.props.propertyDetailInfo.address.slice(0,40);
        }
        var price = getPriceFormat(this.props.propertyDetailInfo.expected_price);
        if(this.props.propertyDetailInfo.monthly_rent){
            price = getPriceFormat(this.props.propertyDetailInfo.monthly_rent);
        }
    	return(
    		    <div className="property-figure">
                    <div className="row">
                        <div className="col-md-7 col-sm-6 col-xs-12">
                            <div className="p-left-side">
                                <h2>{this.props.propertyDetailInfo.name}</h2>
                                <a className="btn small-red-btn" href="javascript:;" role="button">{this.props.propertyDetailInfo.property_for}</a>
                                {/*<a className="btn small-red-btn" href="javascript:;" role="button">VERIFIED</a>*/}
                                <h5>{address}</h5>
                                <ul className="property-posted-detail clearfix">
                                    {this.props.propertyDetailInfo.user_type && <li>Posted by {this.props.propertyDetailInfo.user_type}</li>}
                                    <li>Uploaded : {moment(this.props.propertyDetailInfo.created).format("Do MMM YY")}</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-5 col-sm-6 col-xs-12">
                            <div className="row">
                                <div className="col-sm-6">
                                    <div className="property-size">Area
                                        <br />
                                        {this.state.htmlView?this.state.htmlView:this.areaInit(this.props.propertyDetailInfo.covered_area,this.props.propertyDetailInfo.covered_area_unit)}
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <div className="property-price">
                                        <h3>{ price?price+"/-":"Price"}</h3>{price?"Negotiable":"Not Mentioned"}<i>{price?"+Other charges":""}</i>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
    	)
    }
});
module.exports= PropertyDetailInfo;
