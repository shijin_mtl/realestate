var keyMirror = require('keymirror');
module.exports = keyMirror({
 BUILDER_RESPONSE_RECEIVED:null,
 RESPONSE_CHANGE_EVENT:null,
 EDIT_RESPONSE_RECEIVED:null,
});
