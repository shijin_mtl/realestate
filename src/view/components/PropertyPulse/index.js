import React from 'react';
import Banner from './Banner'
import BackGroundImage from 'public/images/no-photo.jpg';
import BlogList from './BlogList'
import ExpertList from './ExpertList'
import TalktoExpertAction from 'components/PropertyPulse/action/TalktoExpertAction';
import TalktoExpertStore from 'components/PropertyPulse/store/TalktoExpertStore';
import { Form, FormGroup,TextArea, Validator, Input,required,minLength,maxLength,pattern,email,custom} from 'romagny13-react-form-validation';
import Tabs from 'react-responsive-tabs';
import 'react-responsive-tabs/styles.css'
import ExpertAction from 'components/PropertyPulse/action/ExpertAction';
import ExpertStore from 'components/PropertyPulse/store/ExpertStore';
import CategoryAction from 'components/PropertyPulse/action/CategoryAction';
import CategoryStore from 'components/PropertyPulse/store/CategoryStore';
import BlogAction from 'components/PropertyPulse/action/BlogListAction';
import BlogListStore from 'components/PropertyPulse/store/BlogListStore';
import { Pagination } from 'antd';
import config from 'utils/config';
import Loading from 'components/Loading';




const validators = {
    'name': [required('Name is Required'), minLength(3),maxLength(30),pattern(/^[a-zA-Z']+(\s.?[a-zA-Z']+){0,4}$/, '3 to 30 characters no symbols and numbers ')],
    'phone':[required('Phone is Required'), minLength(3),maxLength(15),pattern(/^[0-9+]{10,15}$/, 'Invalid Phone')],
};
var MyFormGroup = function (props) {
    var groupclassName = props.hasError ? 'form-group has-error' : 'form-group';
    return (
            <div className={groupclassName}>
                {props.children}
                {props.hasError && <span className="help-block">{props.error}</span>}
            </div>
    );
};


var PropertyPulse = React.createClass({
  getInitialState:function(){
      return{
              model:{
              	     name:"",
              	     email:"",
              	     mobile:"",
              	     message:""
              },
              btnOnclick:false,
              blogList:"",
              expertList:[],
              categoryList:[],
              pagination:[]
      }
	},

  componentDidMount:function(){
    ExpertAction.expert()
    CategoryAction.getList()
    setTimeout(this.loadList.bind(this,"dd"),1000);
  },

  handleChange:function(key1){
   scroll.scrollToTop();
  },
  componentWillMount:function(){
      TalktoExpertStore.bind(this.expertResponseReceived);
      ExpertStore.bind(this.expertListResponseReceived);
      CategoryStore.bind(this.categoryResponseReceived);
      BlogListStore.bind(this.BlogListReceived)
      $(".side-collapse").addClass('in');
  },
  componentWillUnmount:function(){
      TalktoExpertStore.unbind(this.expertResponseReceived);
      ExpertStore.unbind(this.expertListResponseReceived);
      CategoryStore.unbind(this.categoryResponseReceived);
      BlogListStore.unbind(this.BlogListReceived)
  },
  BlogListReceived:function(){
      var blogList = BlogListStore.getResponse()
      var blog =[];
      this.state.pagination = blogList.count >6 ?<Pagination total={blogList.count} current={parseInt(blogList.current)} onChange={this.handleChange} />:[]
      this.setState({
        pagination:this.state.pagination
      })
      for( var i =0;i<blogList.results.length;i++)
      {
        blog.push(<BlogList data={blogList.results[i]} key={i}/>)
      }

      if(blogList.results.length == 0)
      {
        blog.push(<div key={i} className='row text-center'><br/><h2><b>No Blogs Yet.</b></h2></div>)
      }

      this.state.blogList = blog
      this.setState({
        blogList:this.state.blogList
      })

  },
  expertResponseReceived:function(){
      var expertResponse = TalktoExpertStore.getResponse();
      mixpanel.track(
      "Contact us",
      {"browser": "session",
        "name":document.getElementById('name').value,
        "email":document.getElementById('email').value,
        "mobile":document.getElementById('mobile').value,
        "message":document.getElementById('message').value,
      }
      );
      setTimeout(function(){
        document.getElementById('name').value = "";
        document.getElementById('mobile').value = "";
        document.getElementById('email').value = "";
        document.getElementById('message').value = "";
       }, 100);
      $('#modelBtn').click()
      alert('asdasd')
	},
  categoryResponseReceived:function(){
    var listData = CategoryStore.getResponse()
    this.state.categoryList = listData
    BlogAction.getList(listData[0].id)
    this.setState({
      categoryList:this.state.categoryList
    })


  },
  expertListResponseReceived:function(){
    var result = ExpertStore.getResponse()
    for(let i =0;i<result.length;i++)
    {
      this.state.expertList.push(<ExpertList key={i} data={result[i]}/>)
    }
    this.setState({
      expertList:this.state.expertList
    })
  },
  submit:function(result){
      if(!result.hasError){
        result.model.message = this.state.model.message;
        TalktoExpertAction.expert(result.model);
      }
  },
  getCategoryBar:function(){
    var list = []
    for(let i=0;i<this.state.categoryList.length;i++)
    {
        list.push(<a className={i==0?"item active fcurrent":"item"} key={i} onClick={this.tagClick.bind(this,this.state.categoryList[i].id)}>{this.state.categoryList[i].title}</a>)
    }
    return list
  },
  email:function(value){
        var atpos = value.indexOf("@");
        var dotpos = value.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=value.length || ! /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
            return{
                name: 'email',
                error: 'Invalid email address'
            }
        }
    },
    number:function(value){
      if(value.length!=10 || !/^\d+$/.test(value)){
            return{
                name:'number',
                error:"Invalid number"
            }
      }
    },
    changeMessage:function(event){
  		this.state.model.message = event.target.value;
  		this.setState({
  			 model:this.state.model
  		})
  	},
    tagClick:function(key){
      BlogAction.getList(key)
    },
    loadList:function(){
      var hidWidth;
      var scrollBarWidths = 40;
      var widthOfList = function()
      {
        var itemsWidth = 0;
        $('.item').each(function(){
          var itemWidth = $(this).outerWidth();
          itemsWidth+=itemWidth;
        });
        return itemsWidth;
      };

      var widthOfHidden = function(){
        return (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
      };

      var getLeftPosi = function(){
        return $('.list').position().left;
      };

      var reAdjust = function(){
        if (($('.wrapper').outerWidth()) < widthOfList()) {
          $('.scroller-right').show();
        }
        else {
          $('.scroller-right').hide();
        }

        if (getLeftPosi()<0) {
          $('.scroller-left').show();
        }
        else {
          $('.item').animate({left:"-="+getLeftPosi()+"px"},'slow');
          $('.scroller-left').hide();
        }
      }
      reAdjust();
      $(window).on('resize',function(e){
          reAdjust();
      });
      $('.scroller-right').click(function(e) {
        $('.scroller-left').fadeIn('fast');
        $('.scroller-right').fadeOut('fast');
        $('.list').animate({left:"+="+widthOfHidden()+"px"},'slow',function(){
        });
      });

      $('.scroller-left').click(function() {
        $('.scroller-right').fadeIn('fast');
        $('.scroller-left').fadeOut('fast');

          $('.list').animate({left:"-="+getLeftPosi()+"px"},'slow',function(){

          });

      });

      $(".item").click(function (e) {
      $(this).addClass("fcurrent").siblings().removeClass("fcurrent");
      });
    },
   render:function(){

     var validation = {
         email:this.email,
         number:this.number
       }

   	 return(
            <span>
            <Banner/>
            <section className="blogwrap">
            <div id="topMenu">
            <div id="box">
              <div className="scroller scroller-left"><i className="fa fa-caret-left"></i></div>
              <div className="scroller scroller-right"><i className="fa fa-caret-right"></i></div>
              <div className="wrapper">
                <div className="list">
                {this.getCategoryBar()}
                </div>
              </div>
            </div>
            </div>
            <div className="container">
              <div className="row">
              <div className="col-sm-12">

              </div>
              </div>
              <div className="row">
              {this.state.blogList?this.state.blogList:<Loading/>}
                  </div><br/>
                  <div className="row text-center">{this.state.pagination}</div>

                  <div className="erpertwrap">
                      <div className="row">
                        <div className="col-sm-9 exprtlstwrp">
                          <h2>OUR EXPERTS</h2>
                            <div className="row expr-row">
                            {this.state.expertList!=[]?this.state.expertList:<Loading/>}
                            </div>
                        </div>
                        <div className="col-sm-3">
                          <div className="sidebar-contactbox">
                          <Form onSubmit={this.submit} model={this.state.model} ref="form">
                              <h4 className="sidebarttl">Talk to experts</h4>
                              <p className="cnt-subtext">Leave your query here, and our  experts will get back to you.</p>
                              <div className="form-group">
                              <Validator validators={validators['name']}>
                                 <MyFormGroup>
                                <Input type="text" name='name' id='name' className="form-control" value={this.state.model.name} placeholder="Name"/>
                                </MyFormGroup>
                              </Validator>
                              </div>
                              <div className="form-group">
                              <Validator validators={validators['phone']}>
                               <MyFormGroup>
                                <Input type="tel" name='mobile' id='mobile' className="form-control"  value={this.state.model.mobile} placeholder="Mobile Number"/>
                                </MyFormGroup>
                              </Validator>
                              </div>
                              <div className="form-group">
                              <Validator validators={[required( 'Email is required'),validation['email']]}>
											            <MyFormGroup>
                                <Input type="email" name='email' id='email' className="form-control" placeholder="Email" value={this.state.model.message} />
                                </MyFormGroup>
                              </Validator>
                              </div>
                              <div className="form-group">
                                <textarea className="form-control" name='message' id='message' placeholder="Message" value={this.state.model.message} onChange={this.changeMessage}></textarea>
                              </div>
                              <div className="form-group cntr-algn-wrp">
                                <button className="btn btn-red" type='Submit' >Submit</button>
                              </div>
                              </Form>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
            </section>


            <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtn' data-toggle="modal" data-target="#myModal">Open Modal</button>

            <div id="myModal" className="modal fade pop-show" role="dialog">
                  <div className="modal-dialog">
                    <div className="modal-content">
                          <div className="modal-header">
                          <b>CONTACT</b>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
                          </div>
                          <div className="succes-msgdiv">
                               <img src={config.s3static + "images/done-icon.png"} width="85" className="icon-succ" alt="Success" />
                               <br/><br/>
                               <h4>THANK YOU!</h4>
                               <br/>
                               <h5>Your message has been recieved,We will get back to you soon.</h5>
                               <br/>
                          </div>
                          <div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" >OK</button></div>
                          <br/>
                    </div>
                  </div>
            </div>
            </span>



   	 )
   }
});
module.exports = PropertyPulse;
