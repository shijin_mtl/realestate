import React from 'react';
import {ModalContainer, ModalDialog} from 'react-modal-dialog';
import {Link} from 'react-router';
import config from 'utils/config';

var SignUpSucess = React.createClass({
	handleClose:function(){

	},
	render:function(){
		return(
			    <ModalContainer>
                    <ModalDialog>
                        <div className="modal-header">
     						 {/*<button type="button" className="close" data-dismiss="modal" aria-label="Close">close</button>*/}
						</div>
						<div className="modal-body">
						    <div className="succes-msgdiv">
						        <img src={config.s3static + "images/done-icon.png"} width="85" className="icon-succ" alt="Success" />
						        <h3>THANK YOU!</h3>
						        <h5>For signing in with us</h5>
						        <p className="midtext-suc">You are email has been sucessfully registered</p>
						        <p className="succ-lnkp"><Link to="/">VISIT OUR HOMEPAGE</Link><span className="pipe hidden-xs">|</span><Link to="/property">SEARCH FOR A PROPERTY</Link>
						        </p>
						    </div>
						</div>
                    </ModalDialog>
               </ModalContainer>
		)
	}
})
module.exports = SignUpSucess;
