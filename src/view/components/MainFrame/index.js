import React from 'react';
import 'public/css/bootstrap.min.css';
import 'public/css/font-awesome.min.css';
import 'public/css/linearicon.css';
import 'public/css/common.css';
import 'public/css/toggle-switch.css';
import 'public/css/bootstrap-select.min.css';
import 'public/css/style.css';
import 'public/css/style_bi.css';
import 'public/css/ihover.min.css';
import 'public/css/slick.css';
import 'public/css/slick-theme.css';
import 'antd/dist/antd.css';
import 'public/css/override.css';
import 'public/css/flickity.css';
import 'public/css/bootstrap-responsive-tabs.css';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Home from 'components/Home';
import MetaTags from 'react-meta-tags';
import {hashHistory} from 'react-router';
import config from 'utils/config';
import popUpGet from 'components/MainFrame/action/GetPopUp';
import PopUpStore from 'components/MainFrame/store/PopUpStore';




var intervalID



var MainFrame = React.createClass({
	getInitialState:function(){
      return{
              data:""
      }
  },

	componentDidMount:function()
	{
		 intervalID = setInterval(function()
		 {
			 if(localStorage.isLoggedIn == "false")
			 {
				 $('#modelBtnSync').click()
			 }
			 else {
			 	clearInterval(intervalID);
			 }
		 }
		 , 300000);
	},
	componentWillMount:function(){
			PopUpStore.bind(this.getPopNow)
			popUpGet.getNow()
	},
	componentWillUnmount:function()
	{
		clearInterval(intervalID);
		PopUpStore.unbind(this.getPopNow)
	},
	getPopNow:function(){
		var details = PopUpStore.getResponse()
		this.state.data = details.data
		this.setState({
			data:this.state.data
		})
		if(this.state.data.length==0)
		{
			clearInterval(intervalID);
		}
	},
	routeTo:function(){
		hashHistory.push({
      pathname:"/signup",
    })
	},

	render: function(){
				return (
						<div id="wrapper" className="nav-bd">
							<MetaTags>
							    <meta property="og:locale" content="en_US"/>
									<meta property="og:type" content="website"/>
									<meta property="og:url" content="http://www.wynvent.com/"/>
									<meta property="og:site_name" content="Wynvent"/>
			            <meta id="meta-description" name="description" content="SLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam" />
			            <meta id="og-title" property="og:title" content="Wynvent"/>
	          	</MetaTags>
						 <Header tabKey = {this.props.children.props.route.tabKey}/>
						   {this.props.children}
						 <Footer tabKey = {this.props.children.props.route.tabKey}/>


						 <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtnSync' data-toggle="modal" data-target="#sync-pop">Open Modal</button>
						 <div className="modal fade" id="sync-pop" tabIndex="-1" role="dialog">
						 <div className="vertical-alignment">
							 <div className="modal-dialog modal-lg vertical-align-center" role="document">
										 <div className="modal-content scroll-over">
											 <div className="tab-content">
												 <div className="tab-pane fade active in " id="landing-main0" style={{backgroundImage: `url(${this.state.data[0] ?this.state.data[0].image:""})`}} >
														 <div className="modal-header" style={{'borderBottom': 0}}>
																 <button type="button" className="close" data-dismiss="modal" aria-label="Close" id='closelanding'>close</button>
														 </div>
														 <div className="modal-body">
															 <div className="row">
																 <div className="col-sm-6 col-md-7">
																	 <div className="hm-bannerinfo" style={{"paddingBottom":'50'+'px'}}>
																		 <h2>{this.state.data[0]?this.state.data[0].title:"WHY CREATE AN ACCOUNT WITH US"}? </h2>
																		 <h3>{this.state.data[0]?this.state.data[0].description:"OUR VAST INVENTORY AND RESEARCH OF YOUR AREA HELPS US BRING THE BEST PROPERTIES TO YOU."}</h3>
																	 </div>
																 </div>
																 <div className="col-sm-6 col-md-5">
																	 <div className="hm-bannerinfo" style={{"paddingBottom":'50'+'px'}}>
																	 		<h3>Need a professional help on your property or service? </h3>
																			<h3>Here we are </h3>
																			 <br/>
																	 </div>
																	 <div className='row text-center'>
																	 <button className="btn btn-red" onClick={this.routeTo} type="button" data-dismiss="modal">SignUp</button>
																	 </div>
																 </div>
															 </div>
															 <a href="#landing-success" data-toggle="tab" aria-expanded="false" id="clickOn"></a>
														 </div>
												 </div>
												 <div className="tab-pane fade" id="landing-success">
													 <div className="modal-header">
														 <button type="button" className="close" data-dismiss="modal" aria-label="Close">close</button>
													 </div>
													 <div className="modal-body">
														 <div className="succes-msgdiv">
															 <img src={config.s3static +"images/checkred-icon.png"} width="85" className="icon-succ" alt="Success"/>
															 <h3>THANK YOU!</h3>
															 <h5>For sharing your information with us.</h5>
															 <p className="midtext-suc">Our representatives will get in touch with your shortly.</p>
															 <p>Meanwhile you can:</p>
															 <p className="succ-lnkp"><a href="/#/">VISIT OUR HOMEPAGE</a><span className="pipe hidden-xs">|</span><a href="/#/property">SEARCH FOR A PROPERTY</a></p>
														 </div>
													 </div>
												 </div>
											 </div>
										 </div>
							 </div>
						 </div>
						 </div>

						</div>

                );
		}
});

module.exports = MainFrame;
