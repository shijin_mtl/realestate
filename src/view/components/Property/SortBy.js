import React from 'react';
var SortBy = React.createClass({
  handleSort:function(key){
    this.props.handleSort(key);
  },
  render:function(){
    var sortObj = {proximity:"Proximity",view_count:"View Count",click_count:"Click Count",contact_count:"Contact Count"};
  	return(
  		        <div className="row sortwrap">
                    <div className="col-sm-12 col-md-8">
                        <label>Sort By</label>
                        <div className="sortdrop dropdown">
                            <button type="button" className="btn dropdown-toggle srt-btndrp" data-toggle="dropdown" aria-expanded="true"><span>{sortObj[this.props.filterParams.sort]}</span>
                            </button>
                            <ul className="dropdown-menu">
                                <li onClick={this.handleSort.bind(this,"proximity")}><a>Proximity</a></li>
                                <li onClick={this.handleSort.bind(this,"view_count")}><a>View Count</a></li>
                                <li onClick={this.handleSort.bind(this,"click_count")}><a>Click Count</a></li>
                                <li onClick={this.handleSort.bind(this,"contact_count")}><a>Contact Count</a></li>
                            </ul>
                        </div>
                        <span className="filter-btn visible-xs pull-right">Filter</span>
                    </div>
                    <div className="col-sm-4 col-md-4 hidden-sm hidden-xs">
                        <ul className="nav nav-tabs pull-right" role="tablist">
                            <li role="presentation" className="active">
                              <a href="#prop-grid" aria-controls="prop-grid" role="tab" data-toggle="tab" onClick={()=>this.props.getMyList(1)}><i className="fa fa-th-large" aria-hidden="true"></i></a>
                            </li>
                            <li role="presentation">
                              <a href="#prop-list" aria-controls="prop-list" role="tab" data-toggle="tab" onClick={()=>this.props.getMyList(2)}><i className="fa fa-bars" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
  	)
  }
});
module.exports = SortBy;
