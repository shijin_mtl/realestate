import React from 'react';
import { Form, FormGroup, Validator, Input,required,minLength } from 'romagny13-react-form-validation';
import LoginAction from 'components/Login/action/LoginAction';
import FacebookAuthentication from 'components/Login/FacebookAuthentication';
import GoogleAuthentication from 'components/Login/GoogleAuthentication';
import LoginGoogleAction from 'components/Login/action/LoginGoogleAction';
import UserDetailsAction from 'components/Header/action/UserDetailsAction';
import LoginStore from 'components/Login/store/LoginStore';
import LoginStatusAction from 'components/Header/action/LoginStatusAction';
import LoginFacebookStore from 'components/Login/store/LoginFacebookStore';
import LoginGoogleStore from 'components/Login/store/LoginGoogleStore';
import LoginErrorStore from 'components/Login/store/LoginErrorStore';



var MyFormGroup = function (props) {
    var groupClassName = props.hasError ? 'form-group has-error' : 'form-group';
    return (
            <div className={groupClassName}>
                {props.children}
                {props.hasError && <span className="help-block">{props.error}</span>}
            </div>
    );
};
var LoginPopup = React.createClass({
	 getInitialState:function(){
        return{
        	model: {
                email: '',
                password:'',
                error:""
            },
        }
	 },
	  onSubmit: function (result) {
        if (!result.hasError) {
           LoginAction.login(result.model);
        }

     },
     responseGoogle:function(googleResp){
       var parameters = {'access_token':googleResp.Zi.id_token,'provider':'google'};
       LoginGoogleAction.loginGoogle(parameters);
     },
     componentWillMount:function(){
       LoginStore.bind(this.loginResponseReceived);
       LoginFacebookStore.bind(this.facebookLoginResponseReceived);
       LoginGoogleStore.bind(this.googleLoginResponseReceived);
       LoginErrorStore.bind(this.loginErrorGet)
     },
     componentWillUnmount:function(){
        LoginStore.unbind(this.loginResponseReceived);
        LoginFacebookStore.unbind(this.facebookLoginResponseReceived);
        LoginGoogleStore.unbind(this.googleLoginResponseReceived);
        LoginErrorStore.unbind(this.loginErrorGet)
     },
     loginErrorGet:function(){
        this.state.error = "Invalid username or password "
        this.setState({
          error:this.state.error
        })
     },
     loginResponseReceived:function(){
        var loginResponse = LoginStore.getResponse();
        if(loginResponse.status == 200){
	        LoginStatusAction.loginStatus();
	        UserDetailsAction.userDetails();
	        this.props.onClose();
          this.state.error = ""
          this.setState({
            error:this.state.error
          })
	    }

     },
      googleLoginResponseReceived:function(){
       var googleLoginResponse = LoginGoogleStore.getResponse();
       if(googleLoginResponse.status == 200){
          LoginStatusAction.loginStatus();
          UserDetailsAction.userDetails();
          this.props.onClose();
       }
    },
    facebookLoginResponseReceived:function(){
      var facebookLoginResponse = LoginFacebookStore.getResponse();
      if(facebookLoginResponse.status == 200){
        LoginStatusAction.loginStatus();
        UserDetailsAction.userDetails();
        this.props.onClose();
      }
    },
     render:function(){
     	return(
     		     <div className="modal-header">
				        <h3 className="login-title-text">LOGIN</h3>
				        <div className="regfrm-inrbx">
				              <Form onSubmit={this.onSubmit}  model={this.state.model} >
                          <div className="form-group">
                              <Validator validators={[required('Email or phone number is required'), minLength(3)]}>
                                <MyFormGroup>
                                    <label htmlFor="email">Enter email or mobile to login</label>
                                    <Input className="form-control" id="email" name="email" value={this.state.model.email} className="form-control"  />
                                </MyFormGroup>
                              </Validator>
                          </div>
                          <div className="form-group">
                              <Validator validators={[required('Password is required'), minLength(8)]}>
                                <MyFormGroup>
                                      <label htmlFor="password">Password</label>
                                      <Input type="password" id="password" name="password" value={this.state.model.password} className="form-control"  />

                                </MyFormGroup>
                              </Validator>
                          </div>
                          <span style={{'color':'red'}}>{this.state.error}</span>

                          <div className="form-group cntr-algn-wrp">
                            <button className="btn btn-red" type="submit">Login</button>
                          </div>
                        </Form>
				        </div>
				    <div className="log-bottomwrp">
				            <p>Or login using</p>
				            <div className="log-via-social">
				              <FacebookAuthentication />
				              <GoogleAuthentication   responseHandler={this.responseGoogle}
				                  socialId="496681590196-j1mpm7gbn1m19dtburte2cpe9jgnm7c0.apps.googleusercontent.com"
				                  scope="profile"/>
				            </div>
				          </div>


                  <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtnError' data-toggle="modal" data-target="#myModalError">Open Modal</button>
                  <div id="myModalError" className="modal fade pop-show" role="dialog">
                        <div className="modal-dialog">
                          <div className="modal-content">
                                <div className="modal-header">
                                <b>LOGIN</b>
                                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
                                </div>
                                <div className="succes-msgdiv">
                                     <img src="images/error-icon.png" width="85" className="icon-succ" alt="Success" />
                                     <br/><br/>
                                     <h4>ERROR!</h4>
                                     <br/>
                                     <h5>you entered an invalid username or password</h5>
                                     <br/>
                                </div>
                                <div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" >OK</button></div>
                                <br/>
                          </div>
                        </div>
                  </div>




				      </div>
     	)
     }
});
module.exports = LoginPopup;
