import React from 'react';
import BannerAction from 'components/PropertyPulse/action/BannerAction';
import BannerStore from 'components/PropertyPulse/store/BannerStore';
import BannerSingle from './BannerSingle'


var Banner = React.createClass({
   componentDidMount:function(){

   },

   getInitialState:function(){
       return{
               bannerList:[]
       }
 	},

   componentWillMount:function(){
       BannerStore.bind(this.bannerResponseReceived);
       BannerAction.getList()
   },
   componentWillUnmount:function(){
       BannerStore.unbind(this.bannerResponseReceived);
   },
   bannerResponseReceived:function (){
     var list = BannerStore.getResponse()
     for(let i=0;i<list.length;i++)
     {
       this.state.bannerList.push(<BannerSingle key ={i} data={list[i]}/>)
     }
     this.setState({
       bannerList:this.state.bannerList
     })
       $('.blog-banner').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots:true,
      });

   },


   render:function(){
   	 return(
              <section className="blog-top">
                <div className="blog-banner">
                  {this.state.bannerList}
                </div>
              </section>
   	 )
   }
});
module.exports = Banner;
