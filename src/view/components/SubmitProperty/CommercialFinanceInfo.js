import React from 'react';
import { Select } from 'antd';
const Option = Select.Option;
import { Checkbox } from 'antd';

var CommercialFinancialInfo = React.createClass({
   handleChange:function(key,event){
     this.props.changeCommercialFinancialInfo(key,event.target.value)
   },
   onChange:function(key,event){
     this.props.changeCommercialFinancialInfo(key,event.target.checked)
   },
   render:function(){
   	 return(

   	 	      <div className="property-basic-form">
                        <h4 className="form-title-left-border">Financial Info</h4>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="form-group">
                                    <label  className="col-md-3 control-label">Expected price</label>
                                    <div className="col-md-9 col-sm-12">
                                        <div className="row">
                                            <div className="col-md-3 col-sm-12">
                                               <input type="text" className="form-control" onChange={this.handleChange.bind(this,"expected_price")} />

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="form-group">
                                    <label  className="col-md-3 control-label">Price per sq yard</label>
                                    <div className="col-md-3 col-sm-12">
                                        <input type="text" className="form-control" onChange={this.handleChange.bind(this,"price_per_sq_yard")} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="form-group">
                                    <label  className="col-md-3 control-label">Other charges</label>
                                    <div className="col-md-3 col-sm-12">
                                        <input type="text" className="form-control"  onChange={this.handleChange.bind(this,"other_charges")}/>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                        </div>
                        <div className="form-group">
                            <label  className="col-md-3 control-label hidden-sm hidden-xs">&nbsp;</label>
                            <div className="col-md-6 col-sm-12">
                                <Checkbox onChange={this.onChange.bind(this,"exclude_duty_and_reg_charges")} checked={this.props.commercialFinancialInfo.exclude_duty_and_reg_charges}>Stamp Duty And Registration Charges Excluded</Checkbox>
                            </div>
                        </div>
            </div>
   	 )
   }
});
module.exports = CommercialFinancialInfo;
