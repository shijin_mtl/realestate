import React from 'react';
import { Form, FormGroup,TextArea, Validator, Input,required,minLength,maxLength,pattern,email,custom} from 'romagny13-react-form-validation';
import TalktoExpertAction from 'components/Home/action/TalktoExpertAction';
import TalktoExpertStore from 'components/Home/store/TalktoExpertStore';
import {Modal} from 'antd';
import config from 'utils/config';
import LocationStore from 'components/SubmitProperty/store/LocalityStore';
import {hashHistory,Link} from 'react-router';
import LocationChange from 'components/Header/action/LocationChange';





const validators = {
    'name': [required('Name is Required'), minLength(3),maxLength(30),pattern(/^[a-zA-Z']+(\s.?[a-zA-Z']+){0,4}$/, '3 to 30 characters no symbols and numbers ')],
    'phone':[required('Phone is Required'), minLength(3),maxLength(15),pattern(/^[0-9 +]{10,15}$/, 'Invalid Phone Number')],
};

var MyFormGroup = function (props) {
    var groupclassName = props.hasError ? 'form-group has-error' : 'form-group';
    return (
            <div className={groupclassName}>
                {props.children}
                {props.hasError && <span className="help-block">{props.error}</span>}
            </div>
    );
};
var HomeScreenBanner = React.createClass({
	getInitialState:function(){
      return{
              model:{
              	     name:"",
              	     email:"",
              	     mobile:"",
              	     message:""
              },
              btnOnclick:false,
              city:"",
              cityName:""
      }
	},
  goProperty:function(){
      hashHistory.push("/property")
  },
	componentWillMount:function(){
      TalktoExpertStore.bind(this.expertResponseReceived);
	},
	componentWillUnmount:function(){
      TalktoExpertStore.unbind(this.expertResponseReceived);
	},
  componentDidMount:function(){
    {/*$(document).ready(function(){
    $('#modelBtnLanding').click()
  });*/}
  if(this.props.showPop == 'ok')
  {
    $('#modelBtnLanding').click()
  }
  },

  getCity:function(){
    var locationResponse = LocationStore.getResponse().data;
    var cityList =[]
    if(locationResponse)
    {
    for(let i=0;i<locationResponse.length;i++)
    {
      cityList.push(<li key={i}><a href="javascript:void(0)" data-value="Rent" onClick={this.cityChange.bind(this,locationResponse[i].id,locationResponse[i].name)}>{locationResponse[i].name}</a></li>)
    }
    return cityList
  }
  },
  cityChange:function(id,name){
    localStorage.tempData = name
    LocationChange.loactionStatus(id,name)
    this.state.city = id
    this.state.cityName = name
    this.setState({
      city:this.state.city,
      cityName:this.state.cityName
    })
  },

	expertResponseReceived:function(){
      var expertResponse = TalktoExpertStore.getResponse();
      $('#closelanding').click()
        $('#modelBtn').click()
         setTimeout(function(){
           document.getElementById('Reachname').value = "";
           document.getElementById('Reachmobile').value = "";
           document.getElementById('Reachemail').value = "";
           document.getElementById('Reachmessage').value = "";
           document.getElementById('name').value = "";
           document.getElementById('mobile').value = "";
           document.getElementById('email').value = "";
           document.getElementById('message').value = "";
          }, 100);
         this.state.btnOnclick = false
         this.setState({
           btnOnclick : this.state.btnOnclick
         })
         mixpanel.track(
         "Contact us",
         {"browser": "session",
         "name":document.getElementById('Reachname').value?document.getElementById('Reachname').value:document.getElementById('name').value,
         "mobile":document.getElementById('Reachmobile').value?document.getElementById('Reachmobile').value:document.getElementById('mobile').value,
         "email":document.getElementById('Reachemail').value?document.getElementById('Reachemail').value:document.getElementById('email').value,
         "message":document.getElementById('Reachmessage').value?document.getElementById('Reachmessage'):document.getElementById('message').value,
       }
         );
	},
	changeMessage:function(event){
		{/*this.state.model.message = event.target.value;*/}
		this.setState({
			 model:this.state.model
		})
	},
	onSubmit:function(result){
        this.state.btnOnclick = true
        this.setState({
          btnOnclick : this.state.btnOnclick
        })
        if(!result.hasError){
            result.model.city = this.state.city
            TalktoExpertAction.expert(result.model);
            this.state.btnOnclick = true;
            this.setState({
              btnOnclick : this.state.btnOnclick
            })

        }
        else {
          this.state.btnOnclick = false
          this.setState({
            btnOnclick : this.state.btnOnclick
          })
        }

	},
	changeCity:function(city){
      conosle.log("city....",city)
	},
	showLocationList:function(){
      var locationResp = this.props.locationList;
      var locationArr = [];
      for (var i = 0, len = locationResp.length; i < len; i++) {
      	locationArr.push(<li><a href="javascript:;" onChange={this.changeCity.bind(this,locationResp[i])}> locationResp[i]</a></li>)
      }
	},
	email:function(value){
        var atpos = value.indexOf("@");
        var dotpos = value.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=value.length || ! /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
            return{
                name: 'email',
                error: 'Invalid email address'
            }
        }
    },
    message:function(value){
          if (value.length<9) {
              return{
                  name: 'message',
                  error: 'Invalid message format'
              }
          }
      },
    number:function(value){
      if(value.length!=10 || !/^\d+$/.test(value)){
            return{
                name:'number',
                error:"Invalid number"
            }
      }
    },
    render:function(){
    	var validation = {
        	email:this.email,
        	number:this.number
        }
    	return(
    		        <section className="hm-banner">
					    <div className="container">
					        <div className="row">
					            <div className="col-sm-7 col-md-8">
					                <div className="hm-bannerinfo">
					                    <h3>Welcome to <span className="redspn">Wynvent</span></h3>
					                    <h2>One Step towards your dream house</h2>
					                    <p>Our vast inventory and research of your area helps us bring the best properties to you. </p>
					                </div>
					            </div>
					            <div className="col-sm-5 col-md-4">
					                <div className="hm-bnrform">
					                    <div className="bnrinrbox">
					                        <h3>Get in touch</h3>
					                        <Form onSubmit={this.onSubmit} model={this.state.model} ref="form">
											    <div className="form-group">
											        <Validator validators={validators['name']}>
											            <MyFormGroup>
											                <Input className="form-control" id="name" name="name" value={this.state.model.name} className="form-control"  placeholder="Name" />
											            </MyFormGroup>
											        </Validator>
											    </div>
											    <div className="form-group">
											        <Validator validators={validators['phone']}>
											            <MyFormGroup>
											                <Input type="text" className="form-control" id="mobile" name="mobile" value={this.state.model.mobile} className="form-control"  placeholder="Your 10 digit mobile number" />
											            </MyFormGroup>
											        </Validator>
											    </div>
											    <div className="form-group">
											        <Validator validators={[required( 'Email is required'),validation['email']]}>
											            <MyFormGroup>
											                <Input className="form-control" id="email" name="email" value={this.state.model.email} className="form-control"  placeholder="Email"/>
											            </MyFormGroup>
											        </Validator>
											    </div>
                          <div className="form-group dropdown">
                            <button type="button" className="btn dropdown-toggle form-control" data-toggle="dropdown" aria-expanded="true"><span>{this.state.cityName!=""?this.state.cityName:"Select your city"}</span></button>
                            <ul className="dropdown-menu">
                              {this.getCity()}
                            </ul>
                          </div>
											    <div className="form-group ">
                          <Validator validators={[required( 'Message Required')]}>
                              <MyFormGroup>
											        <TextArea id="message" className="form-control" name="message"  placeholder="Message" value={this.state.model.message} onChange={this.changeMessage}></TextArea>
                              </MyFormGroup>
                          </Validator>
                         </div>
											    <div className="form-group">
											        <button className="btn btn-red" disabled = { this.state.btnOnclick } type="submit">Submit</button>
											    </div>
											</Form>
					                 </div>
					                </div>
					            </div>
					        </div>
					    </div>


              <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtn' data-toggle="modal" data-target="#myModal">Open Modal</button>
              <div id="myModal" className="modal fade pop-show" role="dialog">
                    <div className="modal-dialog modal-lg">
                      <div className="modal-content">
                            <div className="modal-header">
                            <b>CONTACT</b>
                              <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
                            </div>
                            <div className="succes-msgdiv">
                              <img src={config.s3static +"images/checkred-icon.png"} width="85" className="icon-succ" alt="Success"/>
                              <h3>THANK YOU!</h3>
                              <h5>For sharing your information with us.</h5>
                              <p className="midtext-suc">Our representatives will get in touch with your shortly.</p>
                              <p>Meanwhile you can:</p>
                              <p className="succ-lnkp"><a data-dismiss="modal">VISIT OUR HOMEPAGE</a><span className="pipe hidden-xs">|</span><a onClick={this.goProperty} data-dismiss="modal">SEARCH FOR A PROPERTY</a></p>
                            </div>
                            <div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" >OK</button></div>
                            <br/>
                      </div>
                    </div>
              </div>
            <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtnLanding' data-toggle="modal" data-target="#landing-pop">Open Modal</button>
            <div className="modal fade" id="landing-pop" tabIndex="-1" role="dialog">
            <div className="vertical-alignment">
              <div className="modal-dialog modal-lg vertical-align-center" role="document">
                    <div className="modal-content">
                      <div className="tab-content">
                        <div className="tab-pane fade active in" id="landing-main">
                            <div className="modal-header" style={{'borderBottom': 0}}>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close" id='closelanding'>close</button>
                            </div>
                            <div className="modal-body">
                              <div className="row">
                                <div className="col-sm-6 col-md-7">
                                  <div className="hm-bannerinfo">
                                    <h2>Looking for a property? </h2>
                                    <h3>Look no further. We are here to help.</h3>
                                    <p>1k+ Properties. 8k+ Buyers . 500+ Sellers</p>
                                  </div>
                                </div>
                                <div className="col-sm-6 col-md-5">
                                    <div className="hm-bnrform">
                                      <div className="bnrinrbox">
                                        <h3>Get in touch</h3>
                                        <Form onSubmit={this.onSubmit} model={this.state.model} ref="form">
      											    <div className="form-group">
      											        <Validator validators={validators['name']}>
      											            <MyFormGroup>
      											                <Input className="form-control" id="name" name="name" value={this.state.model.name} className="form-control"  placeholder="Name" />
      											            </MyFormGroup>
      											        </Validator>
      											    </div>
      											    <div className="form-group">
      											        <Validator validators={validators['phone']}>
      											            <MyFormGroup>
      											                <Input type="text" className="form-control" id="mobile" name="mobile" value={this.state.model.mobile} className="form-control"  placeholder="Your 10 digit mobile number" />
      											            </MyFormGroup>
      											        </Validator>
      											    </div>
      											    <div className="form-group">
      											        <Validator validators={[required( 'Email is required'),validation['email']]}>
      											            <MyFormGroup>
      											                <Input className="form-control" id="email" name="email" value={this.state.model.email} className="form-control"  placeholder="Email"/>
      											            </MyFormGroup>
      											        </Validator>
      											    </div>
                                <div className="form-group dropdown">
                                  <button type="button" className="btn dropdown-toggle form-control" data-toggle="dropdown" aria-expanded="true"><span>{this.state.cityName!=""?this.state.cityName:"Select your city"}</span></button>
                                  <ul className="dropdown-menu">
                                    {this.getCity(this)}
                                  </ul>
                                </div>

      											    <div className="form-group ">
                                <Validator validators={[required( 'Message Required')]}>
                                    <MyFormGroup>
                                   <TextArea id="message" className="form-control" name="message"  placeholder="Message" value={this.state.model.message} onChange={this.changeMessage}></TextArea>
                                    </MyFormGroup>
                                </Validator>
      											    </div>
      											    <div className="form-group">
      											        <button className="btn btn-red" disabled = { this.state.btnOnclick } type="submit">Submit</button>
      											    </div>
      											</Form>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              <a href="#landing-success" data-toggle="tab" aria-expanded="false" id="clickOn"></a>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="landing-success">
                          <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">close</button>
                          </div>
                          <div className="modal-body">
                            <div className="succes-msgdiv">
                              <img src={config.s3static +"images/checkred-icon.png"} width="85" className="icon-succ" alt="Success"/>
                              <h3>THANK YOU!</h3>
                              <h5>For sharing your information with us.</h5>
                              <p className="midtext-suc">Our representatives will get in touch with your shortly.</p>
                              <p>Meanwhile you can:</p>
                              <p className="succ-lnkp"><a href="/#/">VISIT OUR HOMEPAGE</a><span className="pipe hidden-xs">|</span><a href="/#/property">SEARCH FOR A PROPERTY</a></p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
              </div>
            </div>
            </div>
					</section>
    	)
    }
});
module.exports = HomeScreenBanner;
