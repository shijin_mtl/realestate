import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/UserDashboard/constants/UserConstant';
import config from 'utils/config'
import axios from 'axios';

var shortListedProperty = function(){

}

const shorListedApi = axios.create({
  withCredentials: true
});

shortListedProperty.prototype = {
	 property:function(parameters){
	 	shorListedApi.get(config.server  + 'api/v1/property/shortlisted/?page=' + parameters.pageNum)
		    .then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.SHORTLISTED_RESPONSE_RECEIVED,
			        data: response
				});
            })
			.catch(function (error) {
                console.log(error);
            });


	 }
}


module.exports = new shortListedProperty();
