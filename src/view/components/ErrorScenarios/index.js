import React from 'react';
var ErrorScenarios = React.createClass({
	render:function(){
		return(
			    <section className="wrap-nofound">
				  <div className="container">
				  
				    <div className="no-propfound">
				      <img src="images/notify-yellow-icon.png" width="80" />
				      <p>{this.props.message}</p>
				    </div>

				  </div>
                </section>
		)
	}

});
module.exports = ErrorScenarios;
