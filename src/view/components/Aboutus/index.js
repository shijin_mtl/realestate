import React from 'react';
import MetaTags from 'react-meta-tags';


var Aboutus = React.createClass({
  componentWillMount:function(){
     window.scrollTo(0,0);
  },
  render:function(){
  	 return(
  	 	     <section className="common-content">

           <MetaTags>
               <meta id="meta-description" name="description" content="SLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam" />
           </MetaTags>


				  <div className="container">
				    <ul className="breadcrumb">
				      <li><a href="#">Home</a></li>
				      <li className="active">About Us</li>
				    </ul>
				    <h1>About Us</h1>
				    <div className="contentbox">
				      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec elementum commodo lectus, et commodo urna accumsan nec. Nunc vestibulum ultrices sapien, ut efficitur nunc posuere eu. Cras vitae laoreet neque, a ultrices tortor. Vivamus vitae rutrum mauris. Sed eget dui non augue auctor porta. Sed vel arcu eget orci commodo luctus ac non risus. Donec commodo dui in nisl ultrices vulputate. Mauris nec nibh odio. Praesent condimentum elementum enim vitae semper. Proin blandit sed augue id pulvinar.</p>
				      <p>Donec tincidunt sed tortor ac blandit. Praesent ut neque ac nunc ornare aliquam ac sed eros. Donec eget mollis tellus. Donec laoreet lacus leo. Nulla pellentesque tortor eu massa pulvinar tincidunt sit amet vitae eros. Vivamus libero massa, iaculis vitae varius vel, auctor eget dolor. In lacinia nulla neque, ac rhoncus tortor vestibulum non. Nullam sed ultricies tortor, sit amet dignissim est. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc a justo sed elit lacinia bibendum quis auctor ligula. Pellentesque vitae aliquam orci.</p>
				      <p>Nunc mattis mi ligula, id vulputate arcu viverra nec. Maecenas vitae tellus mauris. Maecenas tortor massa, lacinia eu tortor eu, molestie rhoncus mauris. Nulla leo dui, mollis eu leo non, lacinia efficitur sapien. Aenean scelerisque mauris et diam sodales faucibus. Fusce sed diam nisi. Nunc eu lorem ut ipsum placerat consequat. Quisque porttitor laoreet feugiat. Sed quis consequat turpis. Nam a elementum dui, ut blandit turpis. Aliquam a eros lectus. Morbi sed neque vitae purus consequat dictum. Nullam fermentum commodo odio a tempus. In convallis non felis a sollicitudin.</p>
				    </div>
				  </div>
             </section>
  	 )
  }
});
module.exports= Aboutus;
