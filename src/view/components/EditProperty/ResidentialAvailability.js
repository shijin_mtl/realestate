import React from 'react';
import {Radio,Select} from 'antd';
import moment from 'moment';
var RadioGroup = Radio.Group;
var DatePicker = require("react-bootstrap-date-picker");
const Option = Select.Option;

var ResidentialAvailability = React.createClass({
   getInitialState:function(){
     return{
     	selectDate:false,
     	immediately:true
     }
   },
   onChange:function(value){
   	 value = moment(value).format('YYYY-MM-DD');
   	 this.props.changeResidentialAvailability("available_from_date",value)
   },
   handleChange:function(key,event){
   	this.props.changeResidentialAvailability(key,event.target.checked)
   },
   handleChangeSelect:function(key){
     	this.props.changeResidentialAvailability(key,false);
   },
   handleConstChange:function(key,value){
        this.props.changeResidentialAvailability(key,value);
   },
   render:function(){
   	return(
   		     <div className="property-detail-inner-form">
				    <h4>3. Availability</h4>
				    <div className="configuration-details">
				        <div className="row">
				            <div className="col-sm-12">
				                <div className="form-group">
				                    <label htmlFor="" className="col-md-3 control-label">Available from</label>
				                    <div className="col-md-6 col-sm-12">
				                        <div className="row">
				                            <div className="col-md-6 col-sm-12">
				                                    <RadioGroup onChange={this.handleChangeSelect.bind(this,"available_immediately")} value={this.state.selectDate}>
				                                        <Radio 	value={this.props.residentialAvailability.available_immediately}>Select Date</Radio>
                                                    </RadioGroup>
				                            </div>
				                            <div className="col-md-6 col-sm-12">
				                                  <DatePicker id="example-datepicker" 
				                                    value={this.props.residentialAvailability.available_from_date} 
				                                    onChange={this.onChange} />
				                            </div>
				                            <div className="col-md-12 col-sm-12">
				                                    <RadioGroup onChange={this.handleChange.bind(this,"available_immediately")} value={this.state.immediately}>
				                                        <Radio value={this.props.residentialAvailability.available_immediately}>Immediately</Radio>
                                                    </RadioGroup>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				        <div className="row">
				            <div className="col-sm-12">
				                <div className="form-group">
                                    <label htmlFor="" className="col-md-3 control-label link-text">Age of Construction</label>
                                    <div className="col-md-4 col-sm-12">
                                        <Select  style={{ width: 200 }} onChange={this.handleConstChange.bind(this,"age_of_construction")} value={this.props.residentialAvailability.age_of_construction}>
										    <Option value="new_construction">New Construction</Option>
										    <Option value="less_than_5_years">Less Than 5 Years</Option>
										    <Option value="5_to_10_years">5 to 10 years</Option>
										    <Option value="10_to_15_years">10 to 15 years</Option>
										    <Option value="15_to_20_years">15 to 20 years</Option>
										    <Option value="above_20_years">Above 20 Years</Option>
 										</Select> 
                                    </div>
                                </div>
				            </div>
				        </div>
				    </div>
				</div>

   	)
   }
});
module.exports = ResidentialAvailability;