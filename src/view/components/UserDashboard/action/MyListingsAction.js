import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/UserDashboard/constants/UserConstant';
import config from 'utils/config'
import axios from 'axios';

var myListingProperty = function(){

}

const myListingApi = axios.create({
  withCredentials: true
});

myListingProperty.prototype = {
	 myLists:function(parameters){
 	    
	 	myListingApi.get(config.server  + 'api/v1/property/my-listings/?page=' + parameters.pageNum)
		    .then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.LISTINGS_RESPONSE_RECEIVED,
			        data: response
				});
            })
			.catch(function (error) {
                console.log(error);			     
            });
		
				
	 }
}


module.exports = new myListingProperty();
