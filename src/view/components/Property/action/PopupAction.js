import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PropertyPulse/constants/PropertyPulseConstants';
import config from 'utils/config';
import axios from 'axios';
var PopupAction = function(){

}

const adApi = axios.create({
  withCredentials: true,
});

PopupAction.prototype = {
	getNow:function(params){
    localStorage.viewCount = localStorage.viewCount?parseInt(localStorage.viewCount)+1:1
    AppDispatcher.dispatch({
      actionType: Constants.POP_RESPONSE_RECEIVED,
    })
	}
}


module.exports = new PopupAction();
