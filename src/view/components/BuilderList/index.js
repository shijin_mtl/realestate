import React from 'react';
import Header from './Header'
import BuilderList from './BuilderList'
import SideBar from './SideBar'
import BuilderStore from 'components/BuilderList/store/BuilderStore';
import BuilderAction from 'components/BuilderList/action/BuilderAction';
import ErrorScenarios from 'components/ErrorScenarios';
import Loading from 'components/Loading';
import { Pagination,Progress,Icon,Popconfirm } from 'antd';




var response
var Scroll  = require('react-scroll');
var scroll     = Scroll.animateScroll;


var BuilderAll = React.createClass({
  componentDidMount:function(){
    scroll.scrollToTop();
  },
  getInitialState:function(){
     return{
           builderData:"",
           isLoading:true,
           sortParams:'proximity',
           current:1
     }
  },
  componentWillMount:function(){
    BuilderStore.bind(this.responseReceived);
    BuilderAction.builderList({sort:'proximity',page:"1"})
    window.scrollTo(0,0);
  },
  handleSort:function(key){
    this.state.sortParams = key
    BuilderAction.builderList({sort:key,page:"1"})
    this.setState({
      sortParams:this.state.sortParams
    })
  },

  componentWillUnmount:function(){
    BuilderStore.unbind(this.responseReceived);
  },
  responseReceived:function(){
    this.state.builderData = BuilderStore.getResponse()
    this.setState({
      builderData:this.state.builderData
    })
    this.setState({
      isLoading:false
    })
  },

  showPopularList:function(){
     var respList =this.state.builderData.results
     var popularList = []
     if(respList.length > 0){
       for (var i = 0, len = respList.length; i < len; i++) {
         popularList.push(<BuilderList builderList={respList[i]} key={i}/>)
       }
       return(popularList)
     }
     else{
          return(<ErrorScenarios message="No Builders Registred"/>)
     }
  },
  handleChange:function(key1){
    this.state.current  = key1
    this.setState({
      current:key1
    })

    BuilderAction.builderList({sort:this.state.sortParams,page:this.state.current})
   scroll.scrollToTop();
  },


   render:function(){
     var sortObj = {proximity:"Proximity",view_count:"View Count",click_count:"Click Count",contact_count:"Contact Count"};
   	 return(
       <span>
               <section className="agent-listwrap">
                  <div className="container">
                  {/* header */}
                    <Header sort={this.state.sortParams}/>

                  {/* end header */}
                    <div className="row">

                    <div className="row text-center">
                          <label>SORT BY</label> {"  "}
                          <div className="sortdrop dropdown">
                              <button type="button" className="btn dropdown-toggle srt-btndrp" data-toggle="dropdown" aria-expanded="true"><span>{sortObj[this.state.sortParams]}</span>
                              </button>
                              <ul className="dropdown-menu">
                                  <li onClick = {this.handleSort.bind(this,"proximity")}><a>Proximity</a></li>
                                  <li onClick = {this.handleSort.bind(this,"view_count")}><a>View Count</a></li>
                                  <li onClick = {this.handleSort.bind(this,"click_count")}><a>Click Count</a></li>
                                  <li onClick = {this.handleSort.bind(this,"contact_count")}><a>Contact Count</a></li>
                              </ul>
                          </div>
                    </div>

                      {/* List */}
                      <div className="col-sm-9">
                        {!this.state.isLoading?this.showPopularList():<Loading/>}
                        <br/>
                        <div className='row text-center'>
                            {this.state.builderData.count >=1 &&  <Pagination total={this.state.builderData.count} current={parseInt(this.state.current)}  pageSize={10} onChange={this.handleChange}/>}
                        </div>
                      </div>
                      {/* end list */}

                      {/* side bar */}
                        <SideBar/>
                      {/* end side bar */}

                    </div>
                  </div>
               </section>
        </span>
   	 )
   }
});
module.exports = BuilderAll;
