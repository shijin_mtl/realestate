import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/EditProperty/constants/EditConstant';
import config from 'utils/config';
import axios from 'axios';
import {Link,hashHistory} from 'react-router';

var EditPropertyAction = function(){

}

const editPropertyApi = axios.create({
  withCredentials: true,
  contentType:false,
  processData:false,
});

EditPropertyAction.prototype = {
	editProfile:function(parameters,imageObj,id){
		var data = new FormData();
    parameters.add_facilities=[1,2]
    var locateDetails = JSON.stringify({'latitude':parameters.latitude,'longitude':parameters.longitude});
    data.append('coordinates',locateDetails)
    delete parameters.coordinates
    delete parameters.latitude
    delete parameters.longitude
		for(var i in parameters){
		  data.append(i,parameters[i]!=null?parameters[i]:"");
		}
		if(imageObj.image_ids.length > 0){
			var imgArr = imageObj.image_ids;
			for(var j in imgArr){
				data.append('image_ids',imgArr[j],imgArr[j].name)
			}
		}
    data.append('old_images',imageObj.old_list?imageObj.old_list:[])
		editPropertyApi.post(config.server + 'api/v1/property/'+id.id+'/',data)
		    .then(function (response){
			   hashHistory.push("profile")
		    })
		   .catch(function (error) {
         console.log(error)
      });


	}
}


module.exports = new EditPropertyAction();
