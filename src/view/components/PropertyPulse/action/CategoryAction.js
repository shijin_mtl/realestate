import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PropertyPulse/constants/PropertyPulseConstants';
import config from 'utils/config';
import axios from 'axios';
var CategoryAction = function(){

}

const categoryApi = axios.create({
  withCredentials: true,
});

CategoryAction.prototype = {
	getList:function(params){
		categoryApi.get(config.server + 'api/v1/blog-categories/')
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.CATEGORY_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}


module.exports = new CategoryAction();
