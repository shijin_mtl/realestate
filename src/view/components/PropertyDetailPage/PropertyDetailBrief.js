import React from 'react';
var PropertyDetailBrief = React.createClass({

     render:function(){
      console.log(this.props.propertyBrief)
      function ucfirst(str) {
          var firstLetter = str.substr(0, 1);
          var s =  firstLetter.toUpperCase() + str.substr(1);
          s= s.replace(/_/g, ' ');
          return s
      }
     	return(
     		    <span>
                   <h3 className="title-type-small">brief</h3>
                            <div className="row">
                                <div className="col-sm-6">
                                    <i className="brief-title">Layout</i>
                                    <h5>
                                          {this.props.propertyBrief.bedroom_count && <span>{this.props.propertyBrief.bedroom_count} Bedrooms</span>}
                                          {this.props.propertyBrief.bathroom_count && <span>{this.props.propertyBrief.bathroom_count} Bathrooms</span>}
                                          {this.props.propertyBrief.balcony_count && <span> {this.props.propertyBrief.balcony_count} Balcony </span>}
                                    </h5>
                                </div>
                                <div className="col-sm-6">
                                    <i className="brief-title">Furnishing</i>
                                    <h5>{this.props.propertyBrief.furnished_status} with electronics and kitchenware.</h5>
                                </div>
                            </div>
                            <div className="row">
                                {this.props.propertyBrief.floor_number && <div className="col-sm-6">
                                    <i className="brief-title">Floor</i>
                                    <h5>{this.props.propertyBrief.floor_number} Floor</h5>
                                 </div>}
                                <div className="col-sm-6">
                                    <i className="brief-title">Parking</i>
                                    <h5>4 Wheeler - {this.props.propertyBrief.parking_count_four?this.props.propertyBrief.parking_count_four:"NA"}, 2 Wheeler - {this.props.propertyBrief.parking_count_two?this.props.propertyBrief.parking_count_two:"NA"}</h5>
                                </div>
                            </div>
                            <div className="row">
                                {this.props.propertyBrief.maintenance_charges && <div className="col-sm-6">
                                    <i className="brief-title">Maintenance</i>
                                    <h5>To be paid by the tenant. Approx. 1500 pm.</h5>
                                </div>}
                                {this.props.propertyBrief.security_deposit !=null &&
                                <div className="col-sm-6">
                                    <i className="brief-title">Deposit</i>
                                    <h5>INR {this.props.propertyBrief.security_deposit}/-</h5>
                                </div> }

                            </div>
                            <div className="row">
                                {this.props.propertyBrief.age_of_construction && <div className="col-sm-6">
                                    <i className="brief-title">Building Age</i>
                                    <h5>{this.props.propertyBrief.age_of_construction}</h5>
                                </div>}
                                {this.props.propertyBrief.available_immediately && <div className="col-sm-6">
                                    <i className="brief-title">Availability</i>
                                    <h5>Immediately</h5>
                                 </div>}
                                 { ! this.props.propertyBrief.available_immediately && <div className="col-sm-6">
                                     <i className="brief-title">Availability</i>
                                     <h5>{this.props.propertyBrief.available_from_date} </h5>
                                  </div>}
                            </div>
                            <div className="row">
                                <div className="col-sm-6">
                                    <i className="brief-title">Preferred Tenants</i>
                                    <h5>{this.props.propertyBrief.rent_to_bachelors?"Bachelors:":""}{this.props.propertyBrief.rent_to_bachelors?ucfirst(this.props.propertyBrief.rent_to_bachelors):""}</h5>
                                    <h5>{this.props.propertyBrief.rent_to_family?"Family::":""}{this.props.propertyBrief.rent_to_family?ucfirst(this.props.propertyBrief.rent_to_family):""}</h5>
                                    <h5>{this.props.propertyBrief.rent_to_non_vegetarians?"Non-vegetarians:":""}{this.props.propertyBrief.rent_to_non_vegetarians?ucfirst(this.props.propertyBrief.rent_to_non_vegetarians):""}</h5>
                                    <h5>{this.props.propertyBrief.rent_to_with_pets?"Pets:":""}{this.props.propertyBrief.rent_to_with_pets=="yes"?"Allowed":"Not Allowed" }</h5>
                                </div>

                                <div className="col-sm-6">
                                    <i className="brief-title">Other Charges</i>
                                    <h5>{ucfirst("duty_and_reg_charges")}:{this.props.propertyBrief.electricity_charges?"Included":"Not Included"}</h5>
                                    <h5>{this.props.propertyBrief.other_charges ?ucfirst(this.props.propertyBrief.other_charges):"No Other Charges"}</h5>

                                </div>

                            </div>
     		    </span>

     	)
     }
});
module.exports = PropertyDetailBrief;
