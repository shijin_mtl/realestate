import React from 'react';
import {hashHistory} from 'react-router';



var SingleAgentList = React.createClass({
  goToDetail:function(id){
    hashHistory.push({
      pathname:"/agent-detail",
      query:{id:id}
    })
  },
   render:function(){
   	 return(
           <div className="agtlstbox">
               <div className="row">
               <div className="col-sm-3 builder-logo">
                 <img src={this.props.agentList.logo?this.props.agentList.logo:"images/noimage.png"} className="img-responsive" alt="Agent Logo" width="240" />
               </div>
               <div className="col-sm-9">
                   <a><h3 onClick={this.goToDetail.bind(this,this.props.agentList.id)}>{this.props.agentList.title}</h3></a>
                   <h4>Contact Person:</h4>
                   <p>{this.props.agentList.contact_person}</p>
                   <h4>Projects Handled:</h4>
                   <p>{this.props.agentList.project_handled}</p>
                   <h4>Description:</h4>
                   <p>{this.props.agentList.description}</p>
                    <h4>Operating Since:{this.props.agentList.founded_date}</h4>
                   <div className="row agtlnkbtm">
                     <div className="col-sm-12 col-md-6 feat-verf-disp">
                       {this.props.agentList.featured && <span className="feat-ver-box featboxonly">Featured</span>}
                       {this.props.agentList.verified && <span className="feat-ver-box verbboxonly">Verified</span>}
                     </div>
                     <div className="col-sm-12 col-md-6 lrn-vew-disp">
                       <a href="#" className="btn-line">Contact Agent</a>
                       <a href="#" className="btn-line" onClick={this.goToDetail.bind(this,this.props.agentList.id)}>View Number</a>
                     </div>
                   </div>
                 </div>
               </div>
           </div>
   	 )
   }
});
module.exports = SingleAgentList;
