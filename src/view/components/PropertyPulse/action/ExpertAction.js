import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PropertyPulse/constants/PropertyPulseConstants';
import config from 'utils/config';
import axios from 'axios';
var ExpertAction = function(){

}

const expertApi = axios.create({
  withCredentials: true,
});

ExpertAction.prototype = {
	expert:function(params){
		expertApi.get(config.server + 'api/v1/connect-to-experts/')
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.EXPERT_LIST_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}


module.exports = new ExpertAction();
