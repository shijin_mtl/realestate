import React from 'react';
import {Link} from 'react-router';
var BreadCrumb = React.createClass({
   render:function(){
   	 return(
   	 	     <ul className="breadcrumb">
		      <li><Link to="/">Home</Link></li>
		      <li className="active"><Link to="property">PROPERTIES IN YOUR AREA</Link></li>
		     </ul>
   	 )
   }
});
module.exports = BreadCrumb;
