import React from 'react';
import WhiteFooter from 'components/WhiteFooter';
import FeedbackAction from 'components/Footer/action/FeedBackAction';
import FeedbackStore from 'components/Footer/store/FeedBackStore';
import {Modal} from 'antd';
import {Link} from 'react-router';
import ReachusPopup from './ReachusPopup';
import SubscribeAction from 'components/Footer/action/SubscribeAction';
import SubscribeStore from 'components/Footer/store/SubscribeStore';
import LocationStore from 'components/SubmitProperty/store/LocalityStore';
import {hashHistory} from 'react-router';
import FooterLocationAction from 'components/Footer/action/FooterLocationAction';
import FooterLocationStore from 'components/Footer/store/FooterLocationStore';
import config from 'utils/config';


var MyFormGroup = function (props) {
    var groupClassName = props.hasError ? 'form-group has-error' : 'form-group';
    return (
            <div className={groupClassName}>
                {props.children}
                {props.hasError && <span className="help-block">{props.error}</span>}
            </div>
    );
};

var Footer = React.createClass({
	getInitialState:function(){
      return{
      	      model:{
                      email:"",
                      message:""
      	      },
      	      subscribeEmail:"",
      	      emailSubscribeValidation:false,
      	      feedBackEmailValidation:false,
              buttonState:false,
              locationList:[]
      }
	},
	showFooter:function(){
      if(this.props.tabKey == 3 || this.props.tabKey == 4 || this.props.tabKey == 2 || this.props.tabKey == 1 || this.props.tabKey == 6 || this.props.tabKey == 15 || this.props.tabKey == 16){
         return(<WhiteFooter />)
      }
      else{
      	return(<span/>)
      }
	},
	componentWillMount:function(){
      FeedbackStore.bind(this.feedbackResponseReceived);
      SubscribeStore.bind(this.subscribeResponseReceived);
      LocationStore.bind(this.locationResponseReceived);
      FooterLocationStore.bind(this.locationChangeResponseReceived);
	},
	componentWillUnmount:function(){
      FeedbackStore.unbind(this.feedbackResponseReceived);
      SubscribeStore.unbind(this.subscribeResponseReceived);
      LocationStore.unbind(this.locationResponseReceived);
      FooterLocationStore.unbind(this.locationChangeResponseReceived);

	},
  locationChangeResponseReceived:function(){
     var locationChangeResp = FooterLocationStore.getResponse();
     console.log("locationChangeResp....",locationChangeResp)
  },
	locationResponseReceived:function(){
       var locationResponse = LocationStore.getResponse().data;
       this.setState({
       	  locationList:locationResponse
       })
	},
  onHandleClick:function(id){
    var location =  {'location':[id]}
    hashHistory.push({
        pathname:"/property",
        query:location
    })
    FooterLocationAction.changeLocation(location);
  },
	showLocations:function(){
       var locationList = this.state.locationList.slice(0,9);
       var locationArr = [];
       for(var i in locationList){
       	 locationArr.push(<li key={i}><a onClick={this.onHandleClick.bind(this,locationList[i].id)}>PROPERTIES IN {locationList[i].name}</a></li>)
       }

       return(locationArr)
	},
	subscribeResponseReceived:function(){
       var subscribeResponse = SubscribeStore.getResponse();
        if(subscribeResponse.status == 200){


          mixpanel.track(
          "Newsletter",
          {"browser": "session",
          "email":this.state.subscribeEmail
          }
        )
           $('#modelBtnNews').click()
           this.setState({
           	 subscribeEmail:""
           })
        }
	},
	feedbackResponseReceived:function(){
    this.state.model.email = "";
    this.state.model.message = "";
     var feedbackResponse = FeedbackStore.getResponse();
     if(feedbackResponse.status == 200){
        $('#modelBtn').click()
        this.setState({
      		 buttonState:false
      	})
     }
	},
	subcsirbeNewsLetter:function(){
        var atpos = this.state.subscribeEmail.indexOf("@");
        var dotpos = this.state.subscribeEmail.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=this.state.subscribeEmail.length) {
              this.setState({
              	  emailSubscribeValidation:true
              })
        }
        else{
        	    SubscribeAction.subscribe({email:this.state.subscribeEmail})
        }
	},
	handleChange:function(key,event){
      this.state.model[key] = event.target.value;
      this.setState({
      	 model:this.state.model
      })
      if(key == "email"){
      	this.setState({
      		 feedBackEmailValidation:false
      	})
      }
	},
	submitFeedback:function(){
	  var atpos = this.state.model.email.indexOf("@");
      var dotpos = this.state.model.email.lastIndexOf(".");
      if(this.state.model.email || this.state.model.message){
      	  if ( ! /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(this.state.model.email)) {
      	  	this.setState({
      	  		 feedBackEmailValidation:true
      	  	})
      	  }
      	  else{
            this.setState({
          		 buttonState:true
          	})
      	     FeedbackAction.feedback(this.state.model);
      	     this.setState({
               model:this.state.model
      	     })
      	  }
      }

	},
	changeSubscribeEmail:function(event){
       this.state.subscribeEmail = event.target.value;
       this.setState({
       	  subscribeEmail:this.state.subscribeEmail,
       	  emailSubscribeValidation:false
       })
	},
	showReachUs:function(){
        if(this.props.tabKey ==1){
       	  return( <a href="#" className="rechus-green pull-right" data-toggle="modal" data-target="#reachus-pop"><img src={config.s3static + "images/reachus-icon.png"} width="25" alt="Reach Us" />Reach Us</a>)
        }
        else{
       	  return(<span />)
        }
	},
	render:function(){
     return(

		     	<footer className='no-print'>
		     	    {this.showFooter()}
					<div className="blackfootwrap">
					  <div className="container">
					    {this.showReachUs()}
					    <div className="row">
					      <div className="col-xs-6 col-sm-3">
					        <h2>Useful Links</h2>
					        <ul className="foot-nav">
					          <li><Link to="property">BUY/RENT</Link></li>
					          <li><Link to="submit-property">SELL A PROPERTY</Link></li>
					          <li><Link to="market-watch">MARKET WATCH</Link></li>
					          <li><Link to="about-us">ABOUT US</Link></li>
					          <li><a href="#" data-toggle="modal" data-target="#reachus-pop">CONTACT US</a></li>
					          <li><Link to ="property-pulse">WORK WITH US</Link></li>
					          <li><Link to="terms-and-conditions">TERMS & CONDITIONS</Link></li>
					          <li><Link to="privacy">PRIVACY POLICY</Link></li>
					          <li><Link to="blog">BLOG</Link></li>
					        </ul>
					      </div>
					      <div className="col-xs-6 col-sm-3">
					        <ul className="foot-nav midfootnav">
					          {this.showLocations()}
					        </ul>
					      </div>
					      <div className="col-xs-12 col-sm-3">
					        <h2>feedback</h2>
					        <div className="footfromwrap">
					          <div className="form-group">
					            <input type="email" className="form-control" placeholder="Email" onChange={this.handleChange.bind(this,"email")} value={this.state.model.email}/>
					          </div>
					          {this.state.feedBackEmailValidation && <span className="subscribe-email">Invalid email</span>}
							  <div className="form-group">
							    <textarea className="form-control" placeholder="Message for us" onChange={this.handleChange.bind(this,"message")} value={this.state.model.message}></textarea>
							  </div>
					          <div className="form-group"><button type="button" disabled={this.state.buttonState} className="btn btn-default" onClick={this.submitFeedback}>Send</button></div>
					        </div>
					      </div>
					      <div className="col-xs-12 col-sm-3 ft-lst-clmn">
					        <a href="#" className="logo"><img src={config.s3static + "images/logo-white.png"} alt="" className="img-responsive" width="200" /></a>
					        <p>Some brief about WYnvent can come here. Basically this can be a snippet of about us section.</p>
					        <div className="foot-addrbox">
					          <h5 style={{'color':'white'}}>DELHI OFFICE</h5>
					          <p>Bismi Infotech Building,Plot no. 7, Sector 21, Noida- 52</p>
					        </div>
					        <div className="foot-social">
					          <a href="#"><span className="lnr lnr-phone-handset"></span> +91 9876543210</a>
					          <span className="socialbox">
					              <a href="https://www.facebook.com/Wynvent-1449220591787881/" target="_blank"><i className="fa fa-facebook" aria-hidden="true"></i></a>
					              <a href="https://plus.google.com/communities/110538009573599069288" target="_blank"><i className="fa fa-google-plus" aria-hidden="true"></i></a>
					              <a href="https://twitter.com" target="_blank"><i className="fa fa-twitter" aria-hidden="true"></i></a>
					          </span>
					        </div>
					        <div className="row newsletter-wrap">
					          <div className="col-xs-12"><p>Subscribe to our Newsletter</p></div>
						          <div className="col-xs-10 col-sm-9 col-md-10 nwltr-inp">
						            <input type="email" className="form-control" placeholder="Enter your email address here"  onChange={this.changeSubscribeEmail}
						             value={this.state.subscribeEmail}/>
						          </div>
						          <div className="col-xs-2 col-sm-3 col-md-2 nwltr-btn">
						            <button type="button" className="btn btn-default" onClick={this.subcsirbeNewsLetter}><span className="lnr lnr-chevron-right"></span></button>
						          </div>
						          {this.state.emailSubscribeValidation && <span className="subscribe-email">Invalid email</span>}
					        </div>
					      </div>
					    </div>
					    <p className="cpyrghttxt">Copyright of Wynvent 2016</p>
					  </div>
					</div>
        <ReachusPopup />


        <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtnNews' data-toggle="modal" data-target="#myModalNews">Open Modal</button>
        <div id="myModalNews" className="modal fade pop-show" role="dialog">
              <div className="modal-dialog">
                <div className="modal-content">
                      <div className="modal-header">
                      <b>NEWSLETTER</b>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
                      </div>
                      <div className="succes-msgdiv">
                           <img src={config.s3static + "images/done-icon.png"} width="85" className="icon-succ" alt="Success" />
                           <br/><br/>
                           <h4>SUCCESS!</h4>
                           <br/>
                           <h5>Sucessfully subscribed to our newsletter</h5>
                           <br/>
                      </div>
                      <div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" >OK</button></div>
                      <br/>
                </div>
              </div>
        </div>
		        </footer>

     )
	}
});
module.exports = Footer;
