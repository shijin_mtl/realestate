import React from 'react';
import BuilderDetails from './BuilderDetails'
import Success from './Success'
import BuilderEdit from 'components/BuilderSignup/action/getBuilderAction';
import BuilderEditStore from 'components/BuilderSignup/store/BuilderEditStore';

var BuilderSignup = React.createClass({
  getInitialState:function(){
    window.scroll(0,0)
    return{
      content:this.props.location.query.id?null:<BuilderDetails changeContent = {this.changeContent}/>
    }
  },
  componentWillMount:function(){
    BuilderEditStore.bind(this.builderEditDataReceived);
    if(this.props.location.query.id){
      BuilderEdit.getEditData(this.props.location.query.id)

    }
  },
  componentWillUnmount:function(){
    BuilderEditStore.unbind(this.builderEditDataReceived);
  },
  builderEditDataReceived:function(){
    var details = BuilderEditStore.getResponse()
    this.state.content = <BuilderDetails changeContent = {this.changeContent} details={details} id={this.props.location.query.id}/>
    this.setState({
      content:this.state.content
    })
  },

  changeContent:function(){
    this.state.content = <Success/>
    this.setState({
      content : this.state.content
    })
  },
  render:function(){

    return(
      <span>
        {this.state.content}
      </span>
    )
  }
})
module.exports = BuilderSignup;
