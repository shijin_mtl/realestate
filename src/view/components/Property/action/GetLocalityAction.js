import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Property/constants/PropertyConstant';
import config from 'utils/config';
import axios from 'axios';

var fetchLocalities = function(){

}

const localityApi = axios.create({
  withCredentials: true,
});

fetchLocalities.prototype = {
	 locality:function(id){
		localityApi.get(config.server + 'api/v1/localities/?id='+id)
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.LOCAL_RESPONSE_RECEIVED,
				        data: response
				});
		    })
		    .catch(function (error) {
		      console.log(error);
            });

	 }
}


module.exports = new fetchLocalities();
