import React from 'react';
import { Form, FormGroup, Validator, Input,required,minLength,maxLength,TextArea} from 'romagny13-react-form-validation';
import { Radio } from 'antd';
var RadioGroup = Radio.Group;
import ContactOwnerStore from 'components/PropertyDetailPage/store/ContactOwnerStore';
import ContactOwnerAction from 'components/PropertyDetailPage/action/ContactOwnerAction';
import ReportPropertyStore from 'components/PropertyDetailPage/store/ReportPropertyStore';
import ReportPropertyAction from 'components/PropertyDetailPage/action/ReportPropertyAction';
import {Modal} from 'antd';
import ContactOwnerModal from 'react-responsive-modal';
import ReactDOM from 'react-dom';
import config from 'utils/config';
import {ShareButtons,ShareCounts,generateShareIcon} from 'react-share';
import AdListing from '../Advertisement';


const {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  PinterestShareButton,
  VKShareButton,
} = ShareButtons;

const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');

var MyFormGroup = function (props) {
    var groupclassName = props.hasError ? 'form-group has-error' : 'form-group';
    return (
            <div className={groupclassName}>
                {props.children}
                {props.hasError && <span className="help-block">{props.error}</span>}
            </div>
    );
};

var PropertyDetailRightSide = React.createClass({
     getInitialState:function(){
       return{
               model:{
                       email:"",
                       user_type:"buyer_owner",
                       mobile:"",
                       message:"",
                       name:""
               },
               contactOwnerModal:{
                       email:"",
                       user_type:"buyer_owner",
                       mobile:"",
                       message:"",
                       name:""
               },
               reportModal:{
                       email:"",
                       mobile:"",
                       content:"",
               },
               btnReport:false

           }
     },
     componentWillMount:function(){
       ContactOwnerStore.bind(this.contactResponseReceived);
       ReportPropertyStore.bind(this.reportResponseReceived);
     },
     componentWillUnmount:function(){
      ContactOwnerStore.unbind(this.contactResponseReceived);
      ReportPropertyStore.unbind(this.reportResponseReceived);
     },
     reportResponseReceived:function(){
       $('#reportClose').click()
       this.setState({
         btnReport:false
       })



     },
     contactResponseReceived:function(){
       var contactResponse = ContactOwnerStore.getResponse();
        $('#modelBtn').click()
        setTimeout(function(){
          document.getElementById('name').value = "";
          document.getElementById('mobile').value = "";
          document.getElementById('email').value = "";
          document.getElementById('message').value = "";
          document.getElementById('Cname').value = "";
          document.getElementById('Cmobile').value = "";
          document.getElementById('Cemail').value = "";
          document.getElementById('Cmessage').value = "";

         }, 100);
         $('#modelBtnContact').click()
     },
     email:function(value){
        var atpos = value.indexOf("@");
        var dotpos = value.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=value.length) {
            return{
                name: 'email',
                error: 'Invalid email address'
            }
        }
     },


     onSubmit:function(result){
        if(!result.hasError) {
             result.model.message = this.state.model.message;
             result.model.owner=this.props.propertyDetailList.user;
             result.model.property = this.props.propertyDetailList.id;
             result.model.user = JSON.parse(localStorage.getItem('userDetails')).id;
             result.model.user_type = this.state.model.user_type;
             result.model.slug =this.props.propertyDetailList.slug
             ContactOwnerAction.contactAction(result.model);
             result.model.slug = this.props.propertyDetailList.slug;

        }
     },

     number:function(value){
      if(value.length!=10 || !/^\d+$/.test(value)){
            return{
                name:'number',
                error:"Invalid number"
            }
      }
     },
     changePropertyType:function(event){
        this.state.model['user_type'] = event.target.value;
        this.setState({
             model:this.state.model
        })
     },
     changeOwnerType:function(event){
       this.state.contactOwnerModal['user_type'] = event.target.value
       this.setState({
          contactOwnerModal:this.state.contactOwnerModal
       })
     },
     changeTextAreaOwner:function(event){
       this.state.contactOwnerModal['message'] = event.target.value;
       this.setState({
          contactOwnerModal:this.state.contactOwnerModal
       })
     },
     changeTextArea:function(event){
       this.state.model['message'] = event.target.value;
       this.setState({
          model:this.state.model
       })
     },
     changeTextContent:function(event){
       this.state.reportModal['content'] = event.target.value;
       this.setState({
          model:this.state.reportModal
       })
     },
     contactOwner:function(result){
        if(!result.hasError){
            result.model.message = this.state.contactOwnerModal.message;
            result.model.user_type = this.state.contactOwnerModal['user_type'];
            result.model.owner=this.props.propertyDetailList.user;
            result.model.property = this.props.propertyDetailList.id;
            result.model.slug =this.props.propertyDetailList.slug
            ReactDOM.findDOMNode(this.refs.ownerId).click();
            ContactOwnerAction.contactAction(result.model);
        }
     },
     reportProperty:function(result){
        if(!result.hasError && !/^\s+$/.test(this.state.reportModal.content) && this.state.reportModal.content!="" ){
            this.setState({
              btnReport:true
            })
            result.model.content = this.state.reportModal.content
            result.model.property = this.props.propertyDetailList.id;
            ReportPropertyAction.contactAction(result.model);
        }
        else {
          this.setState({
            btnReport:false
          })
        }
     },
     printProperty:function(){
       window.print();
     },
     showShareFb:function(){
        var ip = config.ip;
        var pathName = this.props.pathName.substr(1);
        var query = this.props.query.id;
        var url = "staging.wynvent.com" +  '#/' + pathName +  '?id='+query;
           return(
                   <FacebookShareButton
                      url={url}
                      className="fa fa-facebook"
                      title={this.props.propertyDetailList.name}/>
                 )

     },
     showShareTwitter:function(){
        var ip = config.ip;
        var pathName = this.props.pathName.substr(1);
        var query = this.props.query.id;
        var url = "staging.wynvent.com" +  '#/' + pathName +  '?id='+query;
            return(
                     <TwitterShareButton
                       url={url}
                       title={this.props.propertyDetailList.name}
                       className="fa fa-twitter"/>
            )

     },
     showShareGoogle:function(){
        var ip = config.ip;
        var pathName = this.props.pathName.substr(1);
        var query = this.props.query.id;
        var url = "staging.wynvent.com" +  '#/' + pathName +  '?id='+query;
            return(
                     <GooglePlusShareButton
                       url={url}
                       title={this.props.propertyDetailList.name}
                       className="fa fa-google-plus"/>
            )

     },
     shortListProperty:function(){
       this.props.shortList(this.props.propertyDetailList.id);
     },
     reportClick:function(){
       $('#reportClick').click()

     },
  	 render:function(){
          var validation = {
              email:this.email,
              number:this.number
          }
      var ShortTest
      var shorlistObj = {true:"fa fa-star",false:"fa fa-star-o"}
      var shortListClassName = 'fa fa-star-o';
      var shortColor ='#333333'
      if(this.props.propertyDetailList.shortlisted_status){
         shortListClassName = 'fa fa-star';
         ShortTest = "SHORTLISTED"
         shortColor = 'red'
      }
      else{
         shortListClassName = 'fa fa-star-o';
         ShortTest = "SHORTLIST"
         shortColor = '#333333'
      }
  	 	return(
              <span>
  	 		        <div className="col-md-3 col-sm-12 no-print">
                          <div className="listing-right-section">
                              <a  className="contact-link"  data-toggle="modal" data-target="#contact-owner-modal"  ref="ownerId">CONTACT OWNER</a>
                              {/*<a  className="contact-link">VIEW PHONE NUMBER</a>*/}
                              <ul className="extra-action">
                                  <li onClick={this.printProperty}><a><i className="lnr lnr-printer"></i>PRINT</a></li>
                                  <li onClick={this.shortListProperty}><a href="javascript:;" style={{'color':shortColor}}><i className={shortListClassName} style={{'color':shortColor}}></i>{ShortTest}</a>
                                  </li>
                                  <li><a onClick={this.reportClick}><i className="lnr lnr-cross-circle"></i>REPORT THIS PROPERTY</a></li>
                                  {/*<li><a href="javascript:;"><i className="lnr lnr-cross-circle"></i>REPORT THIS PROPERTY</a>
                                  </li>*/}
                              </ul>
                              <div className="property-share">
                                  <h3 className="title-type-small">LIKE THIS? SHARE IT</h3>
                                  <ul className="share-widget">
                                      <li>
                                          <a>{this.showShareFb()}</a>
                                      </li>
                                      <li>
                                          <a>{this.showShareTwitter()}</a>
                                      </li>
                                      <li>
                                          <a>{this.showShareGoogle()}</a>
                                      </li>
                                  </ul>
                              </div>
                              <AdListing multi={true}/>
                              <div className="contact-owner">
                                  <h3 className="title-type-small">CONTACT OWNER</h3>
                                  <div className="contact-form-types">
                                      <span className="form-title-head">I am</span>
                                       <RadioGroup onChange={this.changePropertyType} value={this.state.model.user_type}>
                                          <Radio value={'buyer_owner'}>Buyer/Owner</Radio>
                                          <Radio value={'agent'}>Agent</Radio>
                                       </RadioGroup>
                                      <div className="Individual box">

                                          <Form onSubmit={this.onSubmit} model={this.state.model} className="contact-owner-form">

                                              <div className="form-group">
                                                  <Validator validators={[required( 'Name is required'), minLength(3),maxLength(30)]}>
                                                      <MyFormGroup>
                                                          <label htmlFor="name">Name</label>
                                                          <Input className="form-control" id="name" name="name" value={this.state.model.name} className="form-control" />
                                                      </MyFormGroup>
                                                  </Validator>
                                              </div>
                                              <div className="form-group">
                                                  <Validator validators={[required( 'Email is required'),validation[ 'email']]}>
                                                      <MyFormGroup>
                                                          <label htmlFor="email">Email</label>
                                                          <Input id="email" name="email" value={this.state.model.email} className="form-control"  />
                                                      </MyFormGroup>
                                                  </Validator>
                                              </div>
                                              <div className="form-group">
                                                  <Validator validators={[required( 'Number is required'),validation[ 'number']]}>
                                                      <MyFormGroup>
                                                          <label htmlFor="mobile">Number</label>
                                                          <Input type="number" id="mobile" name="mobile" value={this.state.model.mobile} className="form-control"  />
                                                      </MyFormGroup>
                                                  </Validator>
                                              </div>

                                              <div className="form-group ">
                                                  <textarea className="form-control" placeholder="Message" id='message' onChange={this.changeTextArea}></textarea>
                                              </div>

                                              <button className="btn btn-red" type="submit">SEND MESSAGE</button>
                                          </Form>
                                      </div>
                                      <div className="Agent box">
                                          <form className="contact-owner-form">
                                              <div className="form-group ">
                                                  <input type="text" className="form-control" placeholder="Name" />
                                              </div>
                                              <div className="form-group ">
                                                  <input type="email" className="form-control" placeholder="Email" />
                                              </div>
                                              <div className="form-group ">
                                                  <textarea className="form-control" placeholder="Message"></textarea>
                                              </div>
                                              <div className="row">
                                                <button className="btn btn-red">SEND MESSAGE</button>
                                              </div>
                                          </form>
                                      </div>
                                  </div>
                              </div>
                              <AdListing/>
                          </div>


                          <div className="modal fade in" id="contact-owner-modal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" >
                                <div className="modal-dialog" role="document">
                                  <div className="modal-content">
                                    <div className="modal-header">
                                      <button type="button" className="close" data-dismiss="modal" aria-label="Close" id="modelBtn"><span aria-hidden="true">&times;</span></button>
                                      <h4 className="modal-title" id="myModalLabel">Contact Owner</h4>
                                    </div>
                                    <div className="modal-body">

                                      <div className="regfrm-inrbx">
                                         <span className="form-title-head">I am</span>
                                           <RadioGroup onChange={this.changeOwnerType} >
                                              <Radio value={'buyer_owner'}>Buyer/Owner</Radio>
                                              <Radio value={'agent'}>Agent</Radio>
                                              <Radio value={'builder'}>Builder</Radio>
                                           </RadioGroup>
                                        <Form onSubmit={this.contactOwner} model={this.state.contactOwnerModal} className="contact-owner-form">

                                              <div className="form-group">
                                                  <Validator validators={[required( 'Name is required'), minLength(3),maxLength(30)]}>
                                                      <MyFormGroup>
                                                          <label htmlFor="name">Name</label>
                                                          <Input className="form-control" id="Cname" name="name" value={this.state.contactOwnerModal.name} className="form-control" />
                                                      </MyFormGroup>
                                                  </Validator>
                                              </div>
                                              <div className="form-group">
                                                  <Validator validators={[required( 'Email is required'),validation[ 'email']]}>
                                                      <MyFormGroup>
                                                          <label htmlFor="email">Email</label>
                                                          <Input id="Cemail" name="email" value={this.state.contactOwnerModal.email} className="form-control"  />
                                                      </MyFormGroup>
                                                  </Validator>
                                              </div>
                                              <div className="form-group">
                                                  <Validator validators={[required( 'Number is required'),validation[ 'number']]}>
                                                      <MyFormGroup>
                                                          <label htmlFor="mobile">Number</label>
                                                          <Input type="number" id="Cmobile" name="mobile" value={this.state.contactOwnerModal.mobile} className="form-control"  />
                                                      </MyFormGroup>
                                                  </Validator>
                                              </div>

                                              <div className="form-group ">
                                                  <textarea className="form-control" placeholder="Message" id='Cmessage' onChange={this.changeTextAreaOwner}></textarea>
                                              </div>
                                              <div className='row text-center'>
                                                <button className="btn btn-red contact-owner-modal" style={{'marginLeft':0+'px'+'!important'}} type="submit">SEND MESSAGE</button>
                                              </div>
                                          </Form>
                                      </div>


                                    </div>

                                  </div>
                                </div>
                           </div>

                      </div>
                      <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtn' data-toggle="modal" data-target="#myModal">Open Modal</button>
                      <div id="myModal" className="modal fade pop-show" role="dialog">
      											<div className="modal-dialog">
      												<div className="modal-content">
      												<br/>
      															<div className="modal-header">
      															<b>CONTACT</b>
      																<button type="button"  className="close" data-dismiss="modal" aria-label="Close">&times;</button>
      															</div>
      															<div className="succes-msgdiv">
      																	 <img src={config.s3static + "images/done-icon.png"} width="85" className="icon-succ" alt="Success" />
      																	 <br/><br/>
      																	 <h4>CONTACT OWNER SUCCESS!</h4>
      																	 <br/>
      																	 <h5>Your message has been shared with the owner,They shall get back to you soon.</h5>
      																	 <br/>
      															</div>
      															<div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" >OK</button></div>
      															<br/>
      												</div>
      											</div>
      								</div>



                      <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}}  data-toggle="modal" data-target="#myModalReport" id="reportClick">Open Modal</button>
                      <div id="myModalReport" className="modal fade pop-show" role="dialog">
                            <div className="modal-dialog">
                              <div className="modal-content">
                              <br/>
                                    <div className="modal-header">
                                    <b>REPORT PROPERTY</b>
                                      <button type="button" className="close" data-dismiss="modal" aria-label="Close" id="reportClose">&times;</button>
                                    </div>
                                    <div className="model-body">
                                    <Form onSubmit={this.reportProperty} model={this.state.reportModal} className="contact-owner-form add-padding">
                                          <div className="form-group">
                                              <Validator validators={[required( 'Email is required'),validation[ 'email']]}>
                                                  <MyFormGroup>
                                                      <label htmlFor="email">Email<span style={{"color": "red"}}>*</span></label>
                                                      <Input id="reportEmail" name="email" value={this.state.reportModal.email} className="form-control"  />
                                                  </MyFormGroup>
                                              </Validator>
                                          </div>
                                          <div className="form-group">
                                              <Validator validators={[required( 'Number is required'),validation[ 'number']]}>
                                                  <MyFormGroup>
                                                      <label htmlFor="reportMobile">Number<span style={{"color": "red"}}>*</span></label>
                                                      <Input type="number" id="reportMobile" name="mobile" value={this.state.reportModal.mobile} className="form-control"  />
                                                  </MyFormGroup>
                                              </Validator>
                                          </div>
                                          <div className="form-group ">
                                              <textarea className="form-control" placeholder="Message" id='reportContent' onChange={this.changeTextContent}></textarea>
                                          </div>
                                          <div className="row text-center">
                                          <button className="btn btn-red contact-owner-modal" disabled={this.state.btnReport} type="submit">REPORT</button>
                                          </div>
                                      </Form>
                                    </div>
                                    <br/>
                              </div>
                            </div>
                      </div>


                      <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtnContact' data-toggle="modal" data-target="#myModalContact">Open Modal</button>
                      <div id="myModalContact" className="modal fade pop-show" role="dialog">
                            <div className="modal-dialog ">
                              <div className="modal-content">
                                    <div className="modal-header">
                                    <b>CONTACT OWNER</b>
                                      <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
                                    </div>
                                    <div className="succes-msgdiv">
                                      <img src={config.s3static +"images/checkred-icon.png"} width="85" className="icon-succ" alt="Success"/>
                                      <h3>THANK YOU!</h3>
                                      <h5>For sharing your information with us.</h5>
                                      <p className="midtext-suc">Our representatives will get in touch with your shortly.</p>
                                    </div>
                                    <div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" >OK</button></div>
                                    <br/>
                              </div>
                            </div>
                      </div>


                      </span>
  	 	)
	 }
});
module.exports = PropertyDetailRightSide;
