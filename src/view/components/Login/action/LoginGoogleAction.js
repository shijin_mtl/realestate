import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Login/constants/LoginConstant';
import config from 'utils/config';
import axios from 'axios';




var onLoginGoogle = function(){

}


const loginGoogleApi = axios.create({
  timeout: 10000,
  withCredentials: true,
  headers: {
    'crossDomain': true
  },
});

onLoginGoogle.prototype = {
	 loginGoogle:function(parameters){
		axios.post(config.server + 'api/v1/register/', parameters,{withCredentials: true,})
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.GOOGLE_LOGIN_RESPONSE_RECEIVED,
				        data: response
				});
		    })
		   .catch(function (error) {
		      loginGoogleApi.post(config.server + 'api/v1/login/google/', parameters)
			    .then(function (response) {
				    AppDispatcher.dispatch({
					        actionType: Constants.GOOGLE_LOGIN_RESPONSE_RECEIVED,
					        data: response
					});
			    })
            });
				
	 }
}


module.exports = new onLoginGoogle();
