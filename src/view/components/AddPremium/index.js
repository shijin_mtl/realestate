
import React from 'react';
import {Link} from 'react-router';
import ReactDOM from "react-dom";
import {Modal} from 'antd';
import PayStore from 'components/AddPremium/store/AddressStore';
import PayAction from 'components/AddPremium/action/AddressAction';
import {hashHistory} from 'react-router';
import payImage from 'public/images/pay.gif';



var PaySuccess = React.createClass({
  getInitialState:function(){
     return{

     }
  },
  componentWillMount:function(){
      window.scrollTo(0,0);
      PayStore.bind(this.payResponseRecieved)

   },
   componentWillUnmount:function(){
     PayStore.unbind(this.payResponseRecieved)
    },
    componentDidMount:function(){
      PayAction.getList({billing_address:localStorage.address,property_id:localStorage.tempPropId,status:"success"})
    },
    payResponseRecieved:function(){
      console.log("payment success")
      hashHistory.push('/profile');
    },

    render:function(){
         return(
           <div className="container" style={{paddingTop:"100px"}}>
             <div className="row text-center">
           	       <div style={{width:'32%',margin:"auto"}}>
                    <img src={payImage} className="img-responsive hight-adjust" alt="Agent Logo" width="240"/>
                   </div>
             </div>
           </div>

         )
    }
});
module.exports = PaySuccess;
