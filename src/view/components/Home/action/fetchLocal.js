import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Home/constants/HomeConstant';
import config from 'utils/config';
import axios from 'axios';
var localAction = function(){

}

const homeApi = axios.create({
  withCredentials: true,
});

localAction.prototype = {
	getList:function(string){
        AppDispatcher.dispatch({
	        actionType: Constants.LOADING_RESPONSE_RECEIVED,
	        data: "loading"
		});
		homeApi.get(config.server + "/api/v1/localities/?id="+localStorage.loacalLocation)
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.LIST_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}


module.exports = new localAction();
