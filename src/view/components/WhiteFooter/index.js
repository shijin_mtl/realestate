import React from 'react';
import {Link} from 'react-router';
import config from 'utils/config';

var WhiteFooter = React.createClass({
	render:function(){
		return(
			    <div className="whitefootwrap">
					<div className="container">
					  <div className="row">
					    <div className="col-sm-6 col-xs-6 pull-right wh-ft-rgt">
					      <div className="foot-subprop-info">
					        <h2><span className="subttlh2 subttlh2-one">LOOKING TO</span> SELL OR RENT<span className="subttlh2">YOUR PROPERTY</span></h2>
					        <Link to="/submit-property" className="btn btn-red">Submit a property</Link>
					      </div>
					    </div>
					    <div className="col-sm-6 col-xs-6 pull-left wh-ft-lft">
					      <img src={config.s3static + "images/sale-rent-post.jpg"} width="450" className="img-responsive" alt="" />
					    </div>
					  </div>
					</div>
                </div>
            )
	}

})
module.exports = WhiteFooter;
