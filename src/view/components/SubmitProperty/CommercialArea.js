import React from 'react';
import { Radio , Select ,Checkbox} from 'antd';
const Option = Select.Option;
var RadioGroup = Radio.Group;
var CommercialArea = React.createClass({
  changeCornerPlot:function(event){
    this.props.changeCommercialArea("is_corner_plot",event.target.checked)
  },
  handleChange:function(key,event){
    this.props.changeCommercialArea(key,event.target.value)
  },
  selectChange:function(key,value){
    this.props.changeCommercialArea(key,value)
  },
  render:function(){
  	return(
  		     <div className="property-detail-inner-form">
                        <h4>2. Area</h4>
                        <div className="configuration-details">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="form-group">
                                        <label  className="col-md-3 control-label">Plot Area</label>
                                        <div className="col-md-6 col-sm-12">
                                            <div className="row">
                                                <div className="col-md-6 col-sm-12">
                                                    <input type="number" className="form-control sm-margin"  value={this.props.commercialArea.plot_area}
                                                     onChange={this.handleChange.bind(this,"plot_area")}/>
                                                </div>
                                                <div className="col-md-6 col-sm-12">

                                                   <Select  style={{ width: 200 }} defaultValue={"sq_ft"} onChange={this.selectChange.bind(this,"plot_area_unit")}>
    				                                    <Option  value="sq_ft">Sq. Ft</Option>
    				                                    <Option value="sq_yrd">Yard</Option>
                                                   </Select>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="form-group">
                                        <label  className="col-md-3 control-label">Plot Length</label>
                                        <div className="col-md-6 col-sm-12">
                                            <div className="row">
                                                <div className="col-md-6 col-sm-12">
                                                    <input type="text" className="form-control"  value={this.props.commercialArea.plot_length}
                                                     onChange={this.handleChange.bind(this,"plot_length")}/>
                                                </div>
                                                <div className="col-md-6 col-sm-12">
                                                    <p className="area-unit-label">Yd</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="form-group">
                                        <label  className="col-md-3 control-label">Plot Width</label>
                                        <div className="col-md-6 col-sm-12">
                                            <div className="row">
                                                <div className="col-md-6 col-sm-12">
                                                    <input type="text" className="form-control"  value={this.props.commercialArea.plot_width}
                                                    onChange={this.handleChange.bind(this,"plot_width")}/>
                                                </div>
                                                <div className="col-md-6 col-sm-12">
                                                    <p className="area-unit-label">Yd</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="form-group">
                                        <label  className="col-md-3 control-label hidden-sm hidden-xs">&nbsp;</label>
                                        <div className="col-md-6 col-sm-12">
                                            <div className="row">
                                                <div className="col-md-6 col-sm-12">

                                                    <Checkbox onChange={this.changeCornerPlot}>This Is A Corner Plot</Checkbox>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="form-group">
                                        <label className="col-md-3 control-label">Carpet Area</label>
                                        <div className="col-md-6 col-sm-12">
                                            <div className="row">
                                                <div className="col-md-6 col-sm-12">
                                                    <input type="text" className="form-control sm-margin"  onChange={this.handleChange.bind(this,"carpet_area")} />
                                                </div>
                                                <div className="col-md-6 col-sm-12">
                                                   <Select defaultValue={"sq_ft"} style={{ width: 200 }} onChange={this.selectChange.bind(this,"carpet_area_unit")}>
				                                    <Option value="sq_ft">Sq. Ft</Option>
				                                    <Option value="sq_yrd">Yard</Option>
                                                   </Select>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
  	)
  }
});
module.exports = CommercialArea;
