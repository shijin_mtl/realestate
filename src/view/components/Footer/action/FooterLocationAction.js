import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Footer/constants/FooterConstants';
import config from 'utils/config'
import axios from 'axios';

var onFooterLocationChange = function(){

}



onFooterLocationChange.prototype = {
	 changeLocation:function(value){

     AppDispatcher.dispatch({
       actionType: Constants.FOOTER_LOCATION_CHANGE,
       data: value
     });


	 }
}


module.exports = new onFooterLocationChange();
