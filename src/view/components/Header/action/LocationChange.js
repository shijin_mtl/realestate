import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Header/constants/LoginStatusConstant';
import config from 'utils/config';

var onLocationStatus = function(){
}

onLocationStatus.prototype = {
	loactionStatus:function(value){
	    AppDispatcher.dispatch({
	      actionType: Constants.LOCATION_CHANGED,
	      data:value
		})
}
}


module.exports = new onLocationStatus()
