import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PropertyDetailPage/constants/PropertyDetailConstant';
import config from 'utils/config';
import axios from 'axios';
var propertyDetail = function(){

}

const propertyDetailApi = axios.create({
  withCredentials: true,
});

const sessionCount = axios.create({
  withCredentials: true,
});

var viewCountApi


propertyDetail.prototype = {
	propertyDetailAction:function(params){
		propertyDetailApi.get(config.server + 'api/v1/property/' +  params.id)
			.then(function (response) {
        makeClick(response.data.slug)
		        AppDispatcher.dispatch({
			        actionType: Constants.PROPERTY_DETAIL_RESPONSE_RECIEVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}


function makeClick(slug){
  sessionCount.get(config.server +'api/v1/session-status')
    .then(function (response) {
      viewCountApi = axios.create({
        withCredentials: true,
      });
    }).then(
      function(){
      viewCountApi.get(config.server +'core/data/process/?type=property&action=c_ct&slug='+slug)
    }
)
}

module.exports = new propertyDetail();
