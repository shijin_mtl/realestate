import React from 'react';
import BackGroundImage from 'public/images/no-photo.jpg';
import NewInCitySingle from './NewInCitySingle'
import NewStore from 'components/MarketWatch/store/NewStore';
import NewInCityAction from 'components/MarketWatch/action/NewInCityAction';
import LocatoinChangeStore from 'components/Header/store/LocationChangeStore';



var NewInCity = React.createClass({
  getInitialState:function(){
    return{
           list:[],
    }
  },
  componentWillMount:function(){
    NewStore.bind(this.newListRecieved)
    NewInCityAction.newList()
    LocatoinChangeStore.bind(this.LocationChangeData);
  },
  componentWillUnmount:function(){
    NewStore.unbind(this.newListRecieved)
    LocatoinChangeStore.unbind(this.LocationChangeData);
  },
  LocationChangeData:function(){
      setTimeout(function() {
        NewInCityAction.newList();
      }, 0);

  },
  newListRecieved:function(){
    this.setState({
      list:[]
    })
    var data = NewStore.getResponse()
    var newList = data.results
    for(let i=0;i<newList.length;i++){
      this.state.list.push(<NewInCitySingle key={i} data={newList[i]}/>)
    }
    if(newList.length == 0)
    {
      this.state.list=<div className={'text-center'}><h4>No Properties</h4></div>
    }
    this.setState({
      list:this.state.list
    })
  },
    render:function(){
    	return(
        <section className="listnewcity-wrap">
             <div className="container">
               <h2 className="main-ttl">New in your city</h2>
               <div className="row pplrlistrow">
                    {this.state.list}
               </div>
             </div>
        </section>
    	)
    }
});
module.exports = NewInCity;
