import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/UserDashboard/constants/UserConstant';
import config from 'utils/config'
import axios from 'axios';

var onChangePassword = function(){

}

const changePasswordApi = axios.create({
  withCredentials: true
});

onChangePassword.prototype = {
	 changePassword:function(parameters){
	 	var changePasswordParameters = {old_password:parameters.old_password,new_password:parameters.new_password};
	 	changePasswordApi.post(config.server  + 'api/v1/change-password/',changePasswordParameters)
		    .then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.CHANGE_PASSWORD_RESPONSE_RECIEVED,
			        data: response
				});
            })
			.catch(function (error,xhr) {
			      AppDispatcher.dispatch({
			        actionType: Constants.CHANGE_PASSWORD_ERROR_RESPONSE_RECIEVED,
			        data: error.response
				});
            });


	 }
}


module.exports = new onChangePassword();
