import React from 'react';
var PropertyErrorScenarios = React.createClass({
   render:function(){
     return(
     	    <section >
			  <div >
			    <div className="no-resultbox">
			      <div className="warning-box">
			        <img src="images/bulb-icon.png" width="35" className="pull-left" /> Check out ways to improve <br/>responses.
			      </div>
			    </div>
			    <div className="no-propfound">
			      <img src="images/notify-yellow-icon.png" width="80" />
			      <p>Sorry, we couldn’t find any <br/> properties matching your search.</p>
			    </div>

			  </div>
            </section>
     )
   }
});
module.exports = PropertyErrorScenarios;
