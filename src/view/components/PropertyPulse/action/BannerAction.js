import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PropertyPulse/constants/PropertyPulseConstants';
import config from 'utils/config';
import axios from 'axios';
var BannerAction = function(){

}

const bannerApi = axios.create({
  withCredentials: true,
});

BannerAction.prototype = {
	getList:function(params){
		bannerApi.get(config.server + 'api/v1/banners/?'+params+"=true")
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.BANNER_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}


module.exports = new BannerAction();
