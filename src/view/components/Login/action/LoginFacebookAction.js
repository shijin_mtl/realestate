import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Login/constants/LoginConstant';
import config from 'utils/config';
import axios from 'axios';

var onLoginFacebook = function(){

}

const loginFacebookApi = axios.create({
  timeout: 10000,
  withCredentials: true,
  headers: {
    'crossDomain': true
  },
});

onLoginFacebook.prototype = {
	 loginFacebook:function(parameters){
		loginFacebookApi.post(config.server + 'api/v1/register/', parameters)
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.FACEBOOK_LOGIN_RESPONSE_RECEIVED,
				        data: response
				});
		    })
		   .catch(function (error) {
		      loginFacebookApi.post(config.server + 'api/v1/login/facebook/', parameters)
			    .then(function (response) {
				    AppDispatcher.dispatch({
					        actionType: Constants.FACEBOOK_LOGIN_RESPONSE_RECEIVED,
					        data: response
					});
			    })
            });
				
	 }
}


module.exports = new onLoginFacebook();
