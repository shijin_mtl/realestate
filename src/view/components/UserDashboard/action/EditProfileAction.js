import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/UserDashboard/constants/UserConstant';
import config from 'utils/config'
import axios from 'axios';

var onEditprofile = function(){

}

const editProfileApi = axios.create({
  withCredentials: true,
  contentType:false,
  processData:false,
});

onEditprofile.prototype = {
	 editProfile:function(parameters,values){
    parameters.user = JSON.parse(localStorage.getItem('userDetails')).id;
    var data = new FormData();
	var userNames = parameters.name.split(" ");
	var	editParameters = {'fname':userNames[0],'lname':userNames[1]?userNames[1]:"",'email':parameters.email,'phone':parameters.phone,'property_for':parameters.type,'about_me':parameters.about_me};
    data.append('fname',userNames[0])
    data.append('lname',userNames[1]?userNames[1]:"")
    data.append('email',parameters.email)
    data.append('phone',parameters.phone)
    data.append('property_for',parameters.type)
    data.append('about_me',parameters.about_me)
    data.append('profile_image',[])
    editProfileApi.post(config.server  + 'api/v1/account/',data)
		    .then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.EDIT_PROFILE_RESPONSE_RECEIVED,
			        data: response
				});
            })
			.catch(function (error) {
			     console.log(error);
            });
	 }
}


module.exports = new onEditprofile();
