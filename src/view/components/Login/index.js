import React from 'react';
import { Form, FormGroup, Validator, Input,required,minLength } from 'romagny13-react-form-validation';
import {Link,hashHistory} from 'react-router';
import LoginAction from 'components/Login/action/LoginAction';
import LoginStore from 'components/Login/store/LoginStore';
import UserAuthSideBar from 'components/UserAuthSideBar';
import LoginStatusAction from 'components/Header/action/LoginStatusAction';
import UserDetailsAction from 'components/Header/action/UserDetailsAction';
import FacebookAuthentication from './FacebookAuthentication';
import GoogleAuthentication from './GoogleAuthentication';
import LoginFacebookStore from 'components/Login/store/LoginFacebookStore';
import LoginGoogleAction from 'components/Login/action/LoginGoogleAction';
import LoginGoogleStore from 'components/Login/store/LoginGoogleStore';
import LoginErrorStore from 'components/Login/store/LoginErrorStore';
import CheckEmailExistsAction from "components/Login/action/CheckEmailExistsAction"
import CheckEmailExistsStore from 'components/Login/store/CheckEmailExistsStore';
import CheckEmailErrorStore from 'components/Login/store/CheckEmailErrorStore';
import ForgotPasswordAction from 'components/Login/action/ForgotPasswordEmailAction';
import ForgotEmailSendStore from 'components/Login/store/ForgotEmailSendStore';
import {Modal} from 'antd';
import config from 'utils/config';


var Login = React.createClass({
    getInitialState: function () {
        return {
            model: {
                email: '',
                password:''
            },
            showLoginSucessMessage:false,
            showLoginErrorMessage:false,
            loginBtnStatus:false,
            showEamilValidation:false,
            showPasswordValidation:false
        };
    },
    componentWillMount:function(){
      window.scrollTo(0,0);
      LoginStore.bind(this.loginResponseReceived);
      LoginFacebookStore.bind(this.facebookLoginResponseReceived);
      LoginGoogleStore.bind(this.googleLoginResponseReceived);
      LoginErrorStore.bind(this.loginErrorResponseReceived);
      CheckEmailExistsStore.bind(this.checkEmailResponseReceived);
      CheckEmailErrorStore.bind(this.checkEmailErrorResponseReceived);
      ForgotEmailSendStore.bind(this.emailSendResponseReceived);
    },
    componentWillUnmount:function(){
      LoginStore.unbind(this.loginResponseReceived);
      LoginFacebookStore.unbind(this.facebookLoginResponseReceived);
      LoginGoogleStore.unbind(this.googleLoginResponseReceived);
      LoginErrorStore.unbind(this.loginErrorResponseReceived);
      CheckEmailExistsStore.unbind(this.checkEmailResponseReceived);

    },
    checkEmailResponseReceived:function(){
      ForgotPasswordAction.forgotPassword({email:this.state.model.email})
    },
    emailSendResponseReceived:function(){
       $('#modelBtnEmail').click()
    },
    checkEmailErrorResponseReceived:function(){
        $('#modelBtnRegister').click()
    },
    loginErrorResponseReceived:function(){
        $('#modelBtnError').click()
        this.setState({
          loginBtnStatus:false
        })
    },
    googleLoginResponseReceived:function(){
       var googleLoginResponse = LoginGoogleStore.getResponse();
       if(googleLoginResponse.status == 200){
          LoginStatusAction.loginStatus();
          UserDetailsAction.userDetails();
          setTimeout(function(){hashHistory.push("/") }, 500);

       }
    },
    facebookLoginResponseReceived:function(){
      var facebookLoginResponse = LoginFacebookStore.getResponse();
      if(facebookLoginResponse.status == 200){
        LoginStatusAction.loginStatus();
        UserDetailsAction.userDetails();
        setTimeout(function(){hashHistory.push("/") }, 500);
      }
    },

    handleClose:function(){
        this.setState({
           showLoginSucessMessage:false
        })
    },
    handleErrorClose:function(){
        this.setState({
           showLoginErrorMessage:false
        })
    },
    loginResponseReceived:function(){
      var loginResponse = LoginStore.getResponse();
      if(loginResponse.status == 200){
        LoginStatusAction.loginStatus();
        UserDetailsAction.userDetails();
        setTimeout(function(){hashHistory.push("/") }, 500);
        mixpanel.track(
	      "Login",
	      {"browser": "session",
	      }
	      );
      }
    },

    onSubmit: function () {
        if(!this.state.model.email){
           this.setState({
             showEamilValidation:true,
             loginBtnStatus:false
           })
        }
        if(!this.state.model.password){
           this.setState({
             showPasswordValidation:true,
             loginBtnStatus:false
           })

        }
        if(this.state.model.email && this.state.model.password){
             LoginAction.login(this.state.model);
             this.setState({
               loginBtnStatus:true
             })
        }

    },
    handleChange:function(key,event){
      this.state.model[key] = event.target.value;
      if(key == "email"){
         this.setState({
            showEamilValidation:false
         })
      }
      if(key == "password"){
         this.setState({
            showPasswordValidation:false
         })
      }
      this.setState({
           model:this.state.model
      })
    },
    responseGoogle:function(googleResp){
      var parameters = {'access_token':googleResp.Zi.id_token,'provider':'google'};
      LoginGoogleAction.loginGoogle(parameters);
    },
    forgotPassword:function(){
      if(this.state.model.email){
          CheckEmailExistsAction.emailExists({email:this.state.model.email})
      }
      else{
            this.setState({
               showEamilValidation:true
            })
      }
    },
    handleKeyDown:function(event){
       if(event.keyCode == 13){
          if(!this.state.model.email){
           this.setState({
             showEamilValidation:true,
             loginBtnStatus:false
           })
        }
        if(!this.state.model.password){
           this.setState({
             showPasswordValidation:true,
             loginBtnStatus:false
           })

        }
        if(this.state.model.email && this.state.model.password){
             LoginAction.login(this.state.model);
             this.setState({
               loginBtnStatus:true
             })
        }
       }
    },
    render:function(){
     return(
            <section className="register-wrap">
              <div className="container">
                <div className="row">
                  <div className="col-sm-6 col-md-7 pull-right regfrmwrap">
                    <div className="reg-bord-box">
                    <div className="reg-box-form">
                      <h3>Login</h3>
                      <div className="regfrm-inrbx">
                          <div className="form-group">
                              <label htmlFor="email">Enter email or mobile to login</label>
                              <input className="form-control" value={this.state.model.email} className="form-control"  onChange={this.handleChange.bind(this,"email")} onKeyDown={this.handleKeyDown}/>
                              {this.state.showEamilValidation && <span className="error-validation">Email is required</span>}
                          </div>
                          <div className="form-group">
                              <label htmlFor="password">Password</label>
                              <input type="password" value={this.state.model.password} className="form-control"  onChange={this.handleChange.bind(this,"password")} onKeyDown={this.handleKeyDown}/>
                              {this.state.showPasswordValidation && <span className="error-validation">Password is required</span>}
                              <a  className="pull-right forgot-pass" onClick={this.forgotPassword}>Forgot Password?</a>
                              <div className="clearfix"></div>

                          </div>
                          <div className="form-group cntr-algn-wrp">
                            <button className="btn btn-red" disabled = { this.state.loginBtnStatus } type="button" onClick={this.onSubmit}>Login</button>
                          </div>
                      </div>
                      <div className="log-bottomwrp">
                        <p>Don’t have an account? <Link to="/signup" className="green-link">Signup</Link></p>
                        <div className="log-via-social">Or login using<br className="visible-xs visible-sm" />
                           <FacebookAuthentication />
                           <GoogleAuthentication   responseHandler={this.responseGoogle}
                            socialId="1098902080877-5js1rvt4at6in4g6cvltlii2tlnsf21q.apps.googleusercontent.com"
                            scope="profile"/>

                        </div>
                      </div>
                    </div>
                    </div>
                  </div>
                  <UserAuthSideBar />
                </div>
              </div>

              <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtnError' data-toggle="modal" data-target="#myModal">Open Modal</button>
              <div id="myModal" className="modal fade pop-show" role="dialog">
                    <div className="modal-dialog">
                      <div className="modal-content">
                            <div className="modal-header">
                            <b>LOGIN</b>
                              <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
                            </div>
                            <div className="succes-msgdiv">
                                 <img src="images/error-icon.png" width="85" className="icon-succ" alt="Success" />
                                 <br/><br/>
                                 <h4>ERROR!</h4>
                                 <br/>
                                 <h5>you entered an invalid username or password</h5>
                                 <br/>
                            </div>
                            <div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" >OK</button></div>
                            <br/>
                      </div>
                    </div>
              </div>

              <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtnEmail' data-toggle="modal" data-target="#myModal2">Open Modal</button>
              <div id="myModal2" className="modal fade pop-show" role="dialog">
                    <div className="modal-dialog">
                      <div className="modal-content">
                            <div className="modal-header">
                            <b>EMAIL SENT</b>
                              <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
                            </div>
                            <div className="succes-msgdiv">
                                 <img src={config.s3static + "images/done-icon.png"} width="85" className="icon-succ" alt="Success" />
                                 <br/><br/>
                                 <h4>EMAIL SENT!</h4>
                                 <br/>
                                 <h5>Password reset link has been sent to your email</h5>
                                 <br/>
                            </div>
                            <div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" >OK</button></div>
                            <br/>
                      </div>
                    </div>
              </div>

              <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtnRegister' data-toggle="modal" data-target="#myModal3">Open Modal</button>
              <div id="myModal3" className="modal fade pop-show" role="dialog">
                    <div className="modal-dialog">
                      <div className="modal-content">
                            <div className="modal-header">
                            <b>FORGOT PASSWORD</b>
                              <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
                            </div>
                            <div className="succes-msgdiv">
                                 <img src="images/error-icon.png" width="85" className="icon-succ" alt="Success" />
                                 <br/><br/>
                                 <h4>ERROR!</h4>
                                 <br/>
                                 <h5>Email is not registerd yet.</h5>
                                 <br/>
                            </div>
                            <div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" >OK</button></div>
                            <br/>
                      </div>
                    </div>
              </div>
            </section>
          )
   }
})
module.exports = Login;
