import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/SignUp/constant/SignUpConstant';
import config from 'utils/config';
import axios from 'axios';
import AuthService from 'services/AuthService';
var otpVerify = function(){

}

const verifyApi = axios.create({
  withCredentials: true,
  headers:{
  	crossDomain:true
  }
});

otpVerify.prototype = {
	 verify:function(parameters){
		verifyApi.post(config.server + 'api/v1/verify-otp/',parameters)
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.OTP_RESPONSE_RECIEVED,
				        data: response
				});
		    })
		   .catch(function (error) {
         AppDispatcher.dispatch({
               actionType: Constants.OTP_RESPONSE_RECIEVED,
               data: error
       });
            });
	 }
}

module.exports = new otpVerify();
