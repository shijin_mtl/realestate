import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/UserDashboard/constants/UserConstant';
import config from 'utils/config'
import axios from 'axios';

var myPropertyResponse = function(){

}

const propertyResponseApi = axios.create({
  withCredentials: true
});

myPropertyResponse.prototype = {
	 userResponse:function(parameters){
	 	propertyResponseApi.get(config.server  + 'api/v1/property/responses/')
		    .then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.PROPERTY_RESPONSE_RECEIVED,
			        data: response
				});
            })
			.catch(function (error) {
                console.log(error);			     
            });
		
				
	 }
}


module.exports = new myPropertyResponse();
