import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Home/constants/HomeConstant';
import config from 'utils/config';
import axios from 'axios';
var homeAction = function(){

}

const homeApi = axios.create({
  withCredentials: true,
});

homeAction.prototype = {

	homeList:function(params){
        AppDispatcher.dispatch({
	        actionType: Constants.LOADING_RESPONSE_RECEIVED,
	        data: "loading"
		});
		homeApi.get(config.server + 'api/v1/property/list/?type='+params.type +'&city='+localStorage.loacalLocation )
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.HOME_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
      });
	}
}


module.exports = new homeAction();
