var keyMirror = require('keymirror');
module.exports = keyMirror({
 AGENT_RESPONSE_RECEIVED:null,
 RESPONSE_CHANGE_EVENT:null,
 EDIT_RESPONSE_RECEIVED:null,
});
