import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Property/constants/PropertyConstant';
import config from 'utils/config';
import axios from 'axios';
var viewCountApi

var property = function(){

}

const propertyApi = axios.create({
  withCredentials: true,
});


const sessionCount = axios.create({
  withCredentials: true,
});





property.prototype = {
	propertyList:function(params){
		var rentToFamily = {false:"no",true:"yes"};
		AppDispatcher.dispatch({
	        actionType: Constants.LOADING_PROPERTY_RESPONSE_RECEIVED,
	        data: "loading"
		});
		var type = Array.isArray(params.typeListArr)?params.typeListArr.join('&&'):[params.typeListArr];
		var bedroomCount = Array.isArray(params.bedroomCount)?params.bedroomCount.join('&&'):[params.bedroomCount];
		var location = Array.isArray(params.location)?params.location.join(','):[params.location];
    var locality = params.locality
		var bathroomCount =  Array.isArray(params.bathroomCount)?params.bathroomCount.join('&&'):[params.bathroomCount];
		var userType = Array.isArray(params.userType)?params.userType.join('&&list_by='):[params.userType];
    if(params.latitude && params.longitude ){
          var   locate = params.latitude + ',' + params.longitude
          location = ''
    }
    else {
          var  locate = null
    }
		propertyApi.get(config.server + 'api/v1/property/search/?page=' + params.pageNum  + '&property_for=' + params.property_for +'&type='+type +
			            '&bedroom_count=' +bedroomCount + '&bathroom_count=' + bathroomCount + '&list_by=' + userType +'&localities=' + locality
			             + '&min_budget=' + params.min_budget + '&max_budget=' + params.max_budget + '&city=' + location + '&sort_by=' + params.sort
			             + '&rent_to_family=' + rentToFamily[params.availableForFamily] + '&min_security_deposit=' + params.minDepositAmount + '&max_security_deposit=' + params.maxDepositAmount
                         + '&loc='+ locate)
			.then(function (response){
		        AppDispatcher.dispatch({
			        actionType: Constants.PROPERTY_RESPONSE_RECIEVED,
			        data: response
				});
        setTimeout(makeClick(response.data.results), 300);
			}

  )
		.catch(function (error) {
			    console.log("error...",error);
            });
	}
}
function makeClick(data){
  sessionCount.get(config.server +'api/v1/session-status')
    .then(function (response) {
      viewCountApi = axios.create({
        withCredentials: true,
      });
    }).then(function(){
    for(let i=0;i<data.length;i++)
    {
      viewCountApi.get(config.server +'core/data/process/?type=property&action=v_ct&slug='+data[i].slug)
        .then(function (response){
              AppDispatcher.dispatch({
                actionType: Constants.VIEW_RESPONSE_RECEIVED,
                data: response
          });
        })
        .catch(function (error) {
            console.log("error...",error);
        });
    }})
}



module.exports = new property();
