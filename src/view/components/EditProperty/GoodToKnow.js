import React from 'react';
var GoodToKnow = React.createClass({

    handleChange:function(key,event){
       this.props.changeGoodToKnow(key,event.target.value);
    },
	render:function(){
return(
      	        <div className="property-basic-form">
                    <h4 className="form-title-left-border">Good to know</h4>
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="form-group">
                                <label className="col-md-3 control-label">Interesting details</label>
                                <div className="col-md-4 col-sm-12">
                                    <textarea className="form-control" value={this.props.goodToKnow.interesting_details}
                                      onChange={this.handleChange.bind(this,"interesting_details")}></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="form-group">
                                <label className="col-md-3 control-label">Landmarks & Neighbourhood</label>
                                <div className="col-md-4 col-sm-12">
                                    <textarea className="form-control" value={this.props.goodToKnow.landmarks_and_neighbourhood}
                                     onChange={this.handleChange.bind(this,"landmarks_and_neighbourhood")}></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
      )
	}
});
module.exports = GoodToKnow;
