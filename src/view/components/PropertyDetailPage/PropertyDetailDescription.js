import React from 'react';
var PropertyDetailDescription = React.createClass({
   render:function(){
	 return(
	 	     <div className="row">
                {this.props.productDescription.interesting_details && <div className="col-sm-12">
                    <h3 className="title-type-small">DESCRIPTION</h3>
                    <p>{this.props.productDescription.interesting_details}</p>
                </div>}
              </div>

   	 )
   }
});
module.exports = PropertyDetailDescription;
