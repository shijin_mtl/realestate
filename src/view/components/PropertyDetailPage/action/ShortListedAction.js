import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PropertyDetailPage/constants/PropertyDetailConstant';
import config from 'utils/config';
import axios from 'axios';
var propertyDetail = function(){

}

const shortListApi = axios.create({
  withCredentials: true,
  contentType:false,
  processData:false,
});

propertyDetail.prototype = {
	shortListAction:function(params){
    var data = new FormData();
		for(var i in params){
		  data.append(i,params[i]);
		}
		shortListApi.post(config.server + 'api/v1/property/shortlisted/' ,  data)
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.SHORTLISTED_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });

	}
}


module.exports = new propertyDetail();
