import React from 'react';



var BannerSingle = React.createClass({

   render:function(){
     console.log(this.props.data);

   	 return(
       <a href={this.props.data.url}>
       <div>
        <img src={this.props.data.image} alt="" className="img-responsive"/>
        <div className="blgbnrcontainer">
          <div className="blgbnrinfo">
            <h2 style={{'color':'white'}}>{this.props.data.title}</h2>
            <h4 style={{'color':'white'}}>{this.props.data.description}</h4>
          </div>
        </div>
       </div>
       </a>
   	 )
   }
});
module.exports = BannerSingle;
