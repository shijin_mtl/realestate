import React from 'react';
import {Gmaps,Marker} from 'react-gmaps';
import PropertyDetailAction from 'components/PropertyDetailPage/action/PropertyDetailAction';
import PropertyDetailStore from 'components/PropertyDetailPage/store/PropertyDetailStore';
import {hashHistory} from 'react-router';
import config from 'utils/config';




var PropertyList = React.createClass({
  getInitialState:function(){
    window.scroll(0,0)
    return{
    }
  },
  goToDetail:function(){
    hashHistory.push({
      pathname:"/propertyDetails",
      query:{id:this.props.data.id}
    })
  },
  render:function(){
    if (this.props.data.coordinates != null)
    {
      var  initList = (this.props.data.coordinates).split(" ")
      initList.splice(0, 1);
      initList[0] = initList[0].replace('(',"")
      initList[1] = initList[1].replace(')',"")
      var lat = initList[1]
      var long = initList[0]
    }

    return(
      <div className="col-sm-6 pplrlistsep">
        <div className="proplistbox ih-item square effect10 bottom_to_top">
        <a className="propanchr" onClick={this.goToDetail}>
          <div className="propbandred">Sale</div>
          <div className="prop-thumb" style={{"backgroundImage": config.s3static + "images/banner1.jpg"}}>
          </div>
          <div className="prop-infobox">
            <h3>{this.props.data.name}</h3>
            <h4>{this.props.data.locality}</h4>
            <h5><span className="pull-left">{this.props.data.city}</span><br className="clearfix"/></h5>
          </div>
          <div className="prop-price">
            <h2><i className="fa fa-inr" aria-hidden="true"></i>{this.props.data.expected_price?this.props.data.expected_price+"/-":"NOT MENTIONED"}</h2>
            <span className="nego-ttl">Onwards</span>
          </div>
          <div className="info">
          <Gmaps onClick={this.showPropertyDetail}
              width={'300px'}
              height={'450px'}
              lat={lat}
              lng={long}
              zoom={12}
              scrollwheel={false}
              viewport={true}>
                <Marker
                  lat={lat}
                  lng={long}/>
          </Gmaps>
            <div className="hvr-btn-map"><button className="btn btn-red" onClick={this.goToDetail}>Read more</button></div>
          </div>
        </a>
        </div>
      </div>
    )
  }
})
module.exports = PropertyList;
