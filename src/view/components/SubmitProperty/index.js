import React from 'react';
import BreadCrumb from './BreadCrumb';
import PropertyBasic from './PropertyBasic';
import GoodToKnow from './GoodToKnow';
import Gallery from './Gallery';
import 'public/css/loginPopUp.css';
import 'public/css/bootstrap.min.css';
import LoginStatusStore from 'components/Header/store/LoginStatusStore';
import ResidentialConfiguration from './ResidentialConfiguration';
import ResidentialFacilities from './ResidentialFacilities';
import ResidentialAvailability from './ResidetialAvailability';
import SubmitPropertyAction from 'components/SubmitProperty/action/SubmitpropertyAction';
import SubmitPropertyStore from 'components/SubmitProperty/store/SubmitPropertyStore';
import LocalityAction from 'components/SubmitProperty/action/GetLocalityAction';
import LocalityStore from 'components/SubmitProperty/store/LocalityStore';
import CityAction from 'components/SubmitProperty/action/GetCityAction';
import CityStore from 'components/SubmitProperty/store/CityStore';
import { DateField, Calendar } from 'react-date-picker';
import CommericialConfiguration from './CommercialConfiguaration';
import CommercialFacilities from './CommercialFacilities';
import ResidentialFinancialInfo from './ResidentialFinancialInfo';
import ResidentialTenants from './ResidentialTenants';
import CommercialArea from './CommercialArea';
import CommercialAvailability from './CommercialAvailability';
import CommercialFinanceInfo from './CommercialFinanceInfo';
import FacilityAction from 'components/SubmitProperty/action/GetFacilityAction';
import FacilityStore from 'components/SubmitProperty/store/FacilityStore';
import LoginPopup from './LoginPopup';
import ModalWindow from 'react-responsive-modal';
import ResidentialOwners  from './ResidentOwners';
import moment from 'moment';
import {Modal} from 'antd';
import SubmitPropertyErorrStore from 'components/SubmitProperty/store/SubmitpropertyErrorStore';
import {Link} from 'react-router';
import {hashHistory} from 'react-router';
import config from 'utils/config';


var SubmitProperty = React.createClass({
	getInitialState:function(){
      return{
					postBtnDisable:false,
      	  localities:[],
      	  cities:[],
          facilities:[],
          showResidential:true,
          showOwnersResidence:true,
          showCommercial:false,
          showSucess:false,
          showPleaseLogin:false,
          showMonthlyRentInfo:false,
          submitProperty:true,
          property:{
          	 property_for:"sale",
          	 type:"residential_apartment",
          	 locality:"",
          	 zipcode:"",
          	 city:"",
             address:"",
             name:""
          },
          propertyBasicValidation:{
            locality:true,
            zipcode:true,
            address:true,
            city:true,
            name:true
          },
          residentialFacilities:[],
          residentialConfiguration:{
          	bedroom_count:"",
          	bathroom_count:"",
          	balcony_count:"",
          	parking_count_four:"",
          	parking_count_two:"",
          	furnished_status:"",
          	floor_number:"",
          	total_floor_count:"",
          	covered_area:"",
          	covered_area_unit:"sq_ft",
          	no_of_open_sides:1,
          	width_of_road_facing_plot:""

          },
          resedentialFinancialInfo:{
           monthly_rent:"",
           maintenance_charges:false,
           water_charges:false,
           other_charges:"",
           electricity_charges:false,
           security_deposit:"",
           exclude_duty_and_reg_charges:true
          },
          residentialAvailability:{
            available_from_date:moment().format('YYYY-MM-DD'),
            available_immediately:true,
            age_of_construction:""
          },
          residentialOwners:{
            owners_residence:'same_premise'
          },
          commercialAvailability:{
            available_from_date:"2017-04-24",
            available_immediately:true,
            transaction_type:"new_property"
          },

          commercialConfiguration:{
             washrooms:"",
             parking_count_four:"",
             parking_count_two:"",
             furnished_status:"",
             floor_number:"",
             total_floor_count:"",
             corner_shop:"",
             pantry:"",
             personal_washroom:"",
             main_road_facing:"",
             covered_area:"",
             covered_area_unit:"sq_ft"
          },
          commercialFacilities:{
              facility_ids:[]
          },
          goodToKnow:{
             interesting_details:"",
             landmarks_and_neighbourhood:""
          },
          residentialTenants:{
            rent_to_bachelors:"does_not_matter",
            rent_to_family:"does_not_matter",
            rent_to_non_vegetarians:"does_not_matter",
            rent_to_with_pets:"does_not_matter"

          },
          commercialArea:{
            plot_area:"",
            plot_area_unit:"sq_ft",
            plot_length:"",
            plot_length_unit:"",
            plot_width_unit:"",
            plot_width:"",
            carpet_area:"",
            carpet_area_unit:"sq_ft",
            is_corner_plot:false
          },
          commercialFinancialInfo:{
             price_per_sq_yard:"",
             expected_price:"",
             other_charges:"",
             exclude_duty_and_reg_charges:true
          },
          gallery:{
            image_ids:[],
            videoLink:""
          },
          mapCoordinates :{}

       }

	},
	componentWillMount:function(){
		 if(localStorage.getItem('isLoggedIn')=="false")
		 {
			 hashHistory.push({
	 				pathname:"/login",
	 		})
		 }
      window.scrollTo(0, 0);
      LocalityAction.locality();
      CityAction.city();
      FacilityAction.facility();
      LoginStatusStore.bind(this.loginStatusResponseReceived);
      SubmitPropertyStore.bind(this.submitPropertyResponseReceived);
      LocalityStore.bind(this.localityResponseRecieved);
      CityStore.bind(this.cityResponseReceived);
      FacilityStore.bind(this.facilityResponseReceived);
			SubmitPropertyErorrStore.bind(this.submitPropertyErrorReceived);
	},
	componentWillUnmount:function(){
      SubmitPropertyStore.unbind(this.submitPropertyResponseReceived);
      LocalityStore.unbind(this.localityResponseRecieved);
      CityStore.unbind(this.cityResponseReceived);
      FacilityStore.unbind(this.facilityResponseReceived);
			SubmitPropertyErorrStore.unbind(this.submitPropertyErrorReceived);

	},
	submitPropertyErrorReceived:function(){
    var erorrResponse = SubmitPropertyErorrStore.getResponse();
		this.state.postBtnDisable = false
		this.setState ({
			postBtnDisable:this.state.postBtnDisable
		})
		var message = 'please check' + ' '
		if ('covered_area' in erorrResponse){
					message = message + " " + "Coverd Area"
		}
		if ('expected_price' in erorrResponse){
					message = message + " " + "Expected Price"
		}
		if ('total_floor_count' in erorrResponse){
					message = message + " " + "Total  Floor Count"
		}
		if ('floor_number' in erorrResponse){
					message = message + " " + " Floor Number"
		}
			 Modal.error({
				title: 'Data invalid',
				content: message,
				okText :'Ok'
			 });

	},
	submitPropertyResponseReceived:function(){
      var submitPropertyResponse = SubmitPropertyStore.getResponse();
			localStorage.tempPropId = submitPropertyResponse.data
      if(submitPropertyResponse.status == 200){

				mixpanel.track(
	      "Submit Property",
	      {"browser": "session",
	        "name":this.state.property.name,
					"property_for":this.state.property.property_for,
					"city":this.state.property.city,
	      }
	      );


				$('#modelBtn').click()
				this.setState ({
 				 postBtnDisable:false
 			 }),
				this.state.commercialFacilities={
						facility_ids:[]
				},
				this.state.property={
					 property_for:"sale",
					 type:"residential_apartment",
					 locality:"",
					 zipcode:"",
					 city:"",
					 address:"",
					 name:""
				},
				this.state.residentialTenants = {
					rent_to_bachelors:"does_not_matter",
					rent_to_family:"does_not_matter",
					rent_to_non_vegetarians:"does_not_matter",
					rent_to_with_pets:"does_not_matter"
				},
				this.state.goodToKnow = {
					 interesting_details:"",
					 landmarks_and_neighbourhood:""
				},
				this.state.residentialConfiguration = {
					bedroom_count:"",
					bathroom_count:"",
					balcony_count:"",
					parking_count_four:"",
					parking_count_two:"",
					furnished_status:"",
					floor_number:"",
					total_floor_count:"",
					covered_area:"",
					covered_area_unit:"sq_ft",
					no_of_open_sides:1,
					width_of_road_facing_plot:""

				},
				this.state.resedentialFinancialInfo = {
				 monthly_rent:"",
				 maintenance_charges:false,
				 water_charges:false,
				 other_charges:false,
				 electricity_charges:false,
				 security_deposit:""
				},
				this.state.residentialAvailability = {
					available_from_date:moment().format('YYYY-MM-DD'),
					available_immediately:true,
					age_of_construction:""
				},
				this.state.residentialOwners = {
					owners_residence:'same_premise'
				},
				this.state.commercialAvailability = {
					available_from_date:"2017-04-24",
					available_immediately:true,
					transaction_type:"new_property"
				},

				this.state.commercialConfiguration = {
					 washrooms:"",
					 parking_count_four:0,
					 parking_count_two:0,
					 furnished_status:"",
					 floor_number:"",
					 total_floor_count:"",
					 corner_shop:"",
					 pantry:"",
					 personal_washroom:"",
					 main_road_facing:"",
					 covered_area:"",
					 covered_area_unit:"sq_ft"
				},
				this.state.residentialFacilities = {
					 electronicFacility:[],
					 furnitureFacility:[]
				},

	      this.setState({
	      	property:this.state.property,
					residentialConfiguration:this.state.residentialConfiguration,
					resedentialFinancialInfo:this.state.resedentialFinancialInfo,
					residentialAvailability:this.state.residentialAvailability,
					residentialOwners:this.state.residentialOwners,
					commercialAvailability:this.state.commercialAvailability,
					commercialConfiguration:this.state.commercialConfiguration,
					residentialTenants:this.state.residentialTenants,
					residentialFacilities:this.state.residentialFacilities,
					goodToKnow:this.state.goodToKnow,
					commercialFacilities:this.state.commercialFacilities
	      })
      }


	},
	packageDetail:function(){
		hashHistory.push({
				pathname:"/premium",
		})
	},
  facilityResponseReceived:function(){
     var facilityResp = FacilityStore.getResponse().data;
     this.setState({
        facilities:facilityResp
     })
  },
	loginStatusResponseReceived:function(){
		var loginStatusResponse = LoginStatusStore.getResponse();
		if(loginStatusResponse.data['logged-in']){
		}
	},
	cityResponseReceived:function(){
      var cityResponse = CityStore.getResponse();
      if(cityResponse.status == 200){
      	this.setState({
      		cities:cityResponse.data
      	})
      }
	},

	localityResponseRecieved:function(){
      var localityResponseReceived = LocalityStore.getResponse();
      if(localityResponseReceived.status == 200){
      	this.setState({
      		localities:localityResponseReceived.data
      	})
      }
	},
	changePropertyInfo:function(key,value){
      this.state.property[key] = value;
      this.state.propertyBasicValidation[key] = true;
      this.setState({
      	property:this.state.property,
        propertyBasicValidation:this.state.propertyBasicValidation
      })

    if(this.state.property.type == "commercial_showroom" || this.state.property.type == "commercial_shop"){
       this.setState({
         showResidential:false,
         showCommercial:true
       })

    }
    if(this.state.property.type == "residential_apartment" || this.state.property.type == "residential_villa"){
       if(this.state.property.type == "residential_apartment" ){
          this.setState({
             showOwnersResidence:true
          })
       }
       if(this.state.property.type == "residential_villa"){
           this.setState({
             showOwnersResidence:false
          })
       }
       this.setState({
         showResidential:true,
         showCommercial:false
       })
    }

    if(this.state.property.property_for == "rent"){
      this.setState({
          showMonthlyRentInfo:true
      })
    }
    if(this.state.property.property_for == "sale"){
      this.setState({
          showMonthlyRentInfo:false
      })
    }
	},
	changeResidentialConfigurations:function(key,value){
    this.state.residentialConfiguration[key] = value;
    this.setState({
       residentialConfiguration:this.state.residentialConfiguration
    })
  },
  changeCommercialAvailability:function(key,value){
    this.state.commercialAvailability[key] = value;
    this.setState({
        commercialAvailability:this.state.commercialAvailability
    })
  },
	submitProperty:function(){

     var propertyValidation = {}
     if(JSON.parse(localStorage.getItem('isLoggedIn'))){
        propertyValidation = this.state.property;
        for(var i in propertyValidation){
          if(propertyValidation[i]){
            this.state.propertyBasicValidation[i] = true
            this.state.submitProperty = true
          }
          else{
                this.state.propertyBasicValidation[i]  = false
                this.state.submitProperty = false
          }
        }

        this.setState({
          propertyBasicValidation:this.state.propertyBasicValidation
        })

				if(this.state.submitProperty){
					this.setState ({
						postBtnDisable:true
					})
				}
          if(this.state.submitProperty){
             var parameters = {};
             var cordinates = [];
             cordinates.push(this.state.mapCoordinates.lat);
             cordinates.push(this.state.mapCoordinates.lng);
             if(this.state.property.type == "residential_apartment" || this.state.property.type == "residential_villa"){
                     if(this.state.property.property_for == "sale"){
                        delete this.state.resedentialFinancialInfo.monthly_rent;
                     }
                     if(this.state.property.property_for == "rent"){
                         delete this.state.resedentialFinancialInfo.expected_price;
                     }
               parameters =  Object.assign({}, this.state.property, this.state.residentialConfiguration,
                             this.state.residentialAvailability,this.state.resedentialFinancialInfo, this.state.residentialTenants,
                             this.state.goodToKnow,{coordinates:cordinates});
							 parameters.add_facilities =this.state.residentialFacilities
               parameters.user = JSON.parse(localStorage.getItem('userDetails')).id;
               SubmitPropertyAction.submitProperty(parameters,this.state.gallery);
             }

              if(this.state.property.type == "commercial_showroom" || this.state.property.type == "commercial_shop"){
               parameters =  Object.assign({}, this.state.property, this.state.commercialConfiguration,
                             this.state.commercialAvailability,this.state.commercialFinancialInfo, this.state.commercialArea,
                             this.state.goodToKnow,{coordinates:cordinates});
							parameters.add_facilities =this.state.residentialFacilities
               parameters.user = JSON.parse(localStorage.getItem('userDetails')).id;
               SubmitPropertyAction.submitProperty(parameters,this.state.gallery);
             }

          }

     }
     else{
            this.setState({
               showPleaseLogin:true
            })
     }


	},
  changeResidentialFinancialInfo:function(key,value){
    this.state.resedentialFinancialInfo[key] = value;
    this.setState({
        resedentialFinancialInfo:this.state.resedentialFinancialInfo
    })
  },
  changeGoodToKnow:function(key,value){
    this.state.goodToKnow[key] = value;
    this.setState({
       goodToKnow:this.state.goodToKnow
    })
  },
  changeResidentialAvailability:function(key,value){
    this.state.residentialAvailability[key] = value;
    this.setState({
       residentialAvailability:this.state.residentialAvailability
    })

  },
  changeResidentialTenants:function(key,value){
    this.state.residentialTenants[key] = value;
    this.setState({
      residentialTenants:this.state.residentialTenants
    })

  },
  commercialConfiguration:function(key,value){
    this.state.commercialConfiguration[key] = value;
    this.setState({
        commercialConfiguration:this.state.commercialConfiguration
    })
  },
  changeCommercialArea:function(key,value){
    this.state.commercialArea[key] = value;
    this.setState({
       commercialArea:this.state.commercialArea
    })

  },
  changePhoto:function(fileObj){
    this.state.gallery.image_ids.push(fileObj);
    this.setState({
       gallery:this.state.gallery
    })

  },
  changeCommercialFinancialInfo:function(key,value){
    this.state.commercialFinancialInfo[key] = value;
    this.setState({
        commercialFinancialInfo:this.state.commercialFinancialInfo
    })
  },



  changeResidentialFacility:function(value){
		if(this.state.residentialFacilities.indexOf(value) == -1)
		{
			this.state.residentialFacilities.push(value);
		}
		else {
		 this.state.residentialFacilities.splice(this.state.residentialFacilities.indexOf(value),1)
		}
		this.setState({
			residentialFacilities:this.state.residentialFacilities
		})
  },
  changeFurnitureFacility:function(value){
     if(this.state.residentialFacilities.indexOf(value) == -1)
		 {
       this.state.residentialFacilities.push(value);
     }
		 else {
		 	this.state.residentialFacilities.splice(this.state.residentialFacilities.indexOf(value),1)
		 }
		 this.setState({
			 residentialFacilities:this.state.residentialFacilities
		 })
  },

  handleSucessClose:function(){
      this.setState({
         showSucess:false
      })
  },
  handleErrorClose:function(){
     this.setState({
        showPleaseLogin:false
     })
  },
  changeResidentOwner:function(key,value){
    this.state.residentialOwners[key] = value;
    this.setState({
        residentialOwners:this.state.residentialOwners
    })

  },
  pinFromMap:function(cityDetails,mapResponse,postalCode){
     this.state.property.city = cityDetails[0].long_name;
     this.state.property.locality =  mapResponse[0].address_components[1].long_name;
     this.state.property.address = mapResponse[0].formatted_address;
     this.state.property.zipcode = postalCode.address_components[0].long_name;
     this.setState({
          property:this.state.property
     })
  },
  changeMapcordinates:function(coordinates){
     this.state.mapCoordinates  = coordinates;
  },
  deleteImage:function(index){
    this.state.gallery.image_ids.splice(index,1);
    this.setState({
         image_ids:this.state.image_ids
    })
  },
	render:function(){
		return(
			<span>
			   <div className="submit-property-section">
			       <div className="container">
			          <BreadCrumb />
			          <div className="row">
			             <div className="col-sm-12">
			                <form className="submit-property-form form-horizontal">
			                   <h2 className="text-center">Showcase your property to<span className="higlight_text">1 Lac+</span> buyers</h2>
                               <h5 className="text-center">Post now! Post for <span className="higlight_text">Free*..</span></h5>
                               <div className="property-details-section">
	                                <div className="row">
	                                    <div className="col-sm-12">
	                                        <h3>PROPERTY DETAILS</h3>
	                                    </div>
	                                </div>
	                                <PropertyBasic propertyBasic={this.state.property}
	                                   changePropertyInfo={this.changePropertyInfo}
                                     pinFromMap = {this.pinFromMap}
                                     propertyBasicValidation = {this.state.propertyBasicValidation}
                                     changeMapcordinates = {this.changeMapcordinates}
	                                 />
	                                  <div className="property-basic-form">
                                     <h4 className="form-title-left-border">Property Detail</h4>

                                        {this.state.showResidential && <ResidentialConfiguration
                                         configuration= {this.state.residentialConfiguration}
                                         propertyType={this.state.property}
                                         changeResidentialConfigurations={this.changeResidentialConfigurations}/>}

                                        {this.state.showResidential && <ResidentialFacilities
                                         changeResidentialFacility={this.changeResidentialFacility}
                                         facility={this.state.facilities}  changeFurnitureFacility={this.changeFurnitureFacility}/>}

                                        {this.state.showResidential && <ResidentialAvailability
                                         residentialAvailability={this.state.residentialAvailability}
                                         changeResidentialAvailability ={this.changeResidentialAvailability}/>}

                                    </div>
                                        {this.state.showResidential && <ResidentialFinancialInfo
                                         resedentialFinancialInfo = {this.state.resedentialFinancialInfo}
                                         changeResidentialFinancialInfo={this.changeResidentialFinancialInfo}
                                         showMonthlyRentInfo={this.state.showMonthlyRentInfo}/>}


                                        {this.state.showResidential && <div className="personal-info-section">
                                          <h4 className="form-title-left-border">Tenants you would rent out to</h4>
                                          <ResidentialTenants changeResidentialTenants = {this.changeResidentialTenants}
                                            residentialTenants={this.state.residentialTenants}/>
                                          </div>}


                                         {this.state.showResidential && this.state.showOwnersResidence && <ResidentialOwners residentialOwners={this.state.residentialOwners}
                                          changeResidentOwner = {this.changeResidentOwner}/>}

                                        {this.state.showCommercial && <CommericialConfiguration
                                         configuration = {this.state.commercialConfiguration}
                                         commercialConfiguration = {this.commercialConfiguration}
                                         propertyType={this.state.property}/>}

                                        {this.state.showCommercial && <CommercialArea
                                        changeCommercialArea={this.changeCommercialArea} commercialArea={this.state.commercialArea}/>}

                                        {this.state.showCommercial && <CommercialAvailability
                                         commercialAvailability={this.state.commercialAvailability}
                                         changeCommercialAvailability={this.changeCommercialAvailability}/>}

                                        {this.state.showCommercial && <CommercialFacilities
                                         facility={this.state.facilities}
                                         changeCommercialFacility={this.changeCommercialFacility}
                                         changeFurnitureFacility={this.changeFurnitureFacility}/>}


                                        {this.state.showCommercial && <CommercialFinanceInfo
                                          changeCommercialFinancialInfo = {this.changeCommercialFinancialInfo}
                                          commercialFinancialInfo={this.state.commercialFinancialInfo}/>}

                                        <GoodToKnow  goodToKnow={this.state.goodToKnow} changeGoodToKnow={this.changeGoodToKnow}/>

                                        <Gallery  changePhoto={this.changePhoto} imgList = {this.state.gallery.image_ids} deleteImage={this.deleteImage}/>
                                        <div className="row">
    			                                <div className="col-sm-12">
    			                                    <div className="form-group cntr-algn-wrp">
    			                                        <button type="button" className="btn btn-red" disabled = {this.state.postBtnDisable } onClick={this.submitProperty}>POST PROPERTY</button>
    			                                    </div>
    			                                </div>
                                        </div>
                                  <ModalWindow open={this.state.showPleaseLogin} onClose={this.handleErrorClose} little>
                                        <LoginPopup  onClose={this.handleErrorClose}/>
                                  </ModalWindow>
                               </div>
                            </form>
			             </div>
			          </div>
                   </div>
                </div>
								<button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtn' data-toggle="modal" data-target="#myModal">Open Modal</button>
								<div id="myModal" className="modal fade pop-show" role="dialog">
											<div className="modal-dialog">
												<div className="modal-content">
												<br/>
															<div className="modal-header">
															<b>PROPERTY SUBMIT</b>
																<button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.packageDetail}>&times;</button>
															</div>
															<div className="succes-msgdiv">
																	 <img src={config.s3static + "images/done-icon.png"} width="85" className="icon-succ" alt="Success" />
																	 <br/><br/>
																	 <h4>PROPERTY SUBMITTED!</h4>
																	 <br/>
																	 <h5>You can view your Buy/Rent</h5>
																	 <br/>
															</div>
															<div className="form-group text-center"><button className="btn btn-red" type="button" data-dismiss="modal" aria-label="Close" onClick={this.packageDetail} >OK</button></div>
															<br/>
												</div>
											</div>
								</div>

								</span>
		)
	}
})
module.exports = SubmitProperty;
