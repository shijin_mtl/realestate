import React from 'react';
import AdStore from 'components/Advertisement/store/AdStore';
import AdAction from 'components/Advertisement/action/AdAction';



var AdListing = React.createClass({
  getInitialState:function(){
     return{
           adDetail:"",
           adDetailMulti:""
     }
  },
  componentDidMount:function(){
    $(function() {

    $("h7")
        .wrapInner("<span>")

    $("h7 br")
        .before("<span class='spacer'>")
        .after("<span class='spacer'>");
});
  },
  componentWillMount:function(){
    AdStore.bind(this.AdResponseReceived);
    AdAction.GetAdNow()

  },
  componentWillUnmount:function(){
    AdStore.unbind(this.AdResponseReceived);
  },
  componentWillReceiveProps:function() {

  },
  AdResponseReceived:function(){
    var adValue = AdStore.getResponse()
    this.state.adDetail =
    <a href={adValue.data[0].url}>
    <div className="image">
        <img src={adValue.data[0].image} className="img-responsive hight-adjust" alt="Agent Logo" width="240"/>
        <h7 className="test-center"><span>{adValue.data[0].title}</span><br/></h7>
    </div>
    </a>
    this.state.adDetailMulti =
    <a href={adValue.data[0].url}>
    <div className="image">
        <img src={adValue.data[1].image} className="img-responsive hight-adjust" alt="Agent Logo" width="240"/>
        <h7 className="test-center"><span>{adValue.data[1].title}</span><br/></h7>
    </div>
    </a>
    this.setState({
      adDetail:this.state.adDetail,
      adDetailMulti:this.state.adDetailMulti,
    })
  },
  componentDidMount:function() {

  },
	render:function(){
		return(
                <div className="ad-box-md">
                    {this.props.multi?this.state.adDetail:this.state.adDetailMulti}
                </div>
		)
	}
});
module.exports = AdListing;
