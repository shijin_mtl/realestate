import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/BuilderDetails/constants/PropertyPulseConstants';
import config from 'utils/config';
import axios from 'axios';
var BuilderAction = function(){

}

const builderApi = axios.create({
  withCredentials: true,
});
const sessionCount = axios.create({
  withCredentials: true,
});

var viewCountApi

BuilderAction.prototype = {
	builderList:function(params){
        AppDispatcher.dispatch({
	        actionType: Constants.LOADING_RESPONSE_RECEIVED,
	        data: "loading"
		});
		builderApi.get(config.server + 'api/v1/builder/'+params+"/")
			.then(function (response) {
        makeClick(response.data.slug)
		        AppDispatcher.dispatch({
			        actionType: Constants.BUILDER_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}


function makeClick(slug){
  sessionCount.get(config.server +'api/v1/session-status')
    .then(function (response) {
      viewCountApi = axios.create({
        withCredentials: true,
      
      });
    }).then(function(){

      viewCountApi.get(config.server +'core/data/process/?type=builder&action=c_ct&slug='+slug)
})
}

module.exports = new BuilderAction();
