import React from 'react';
import SideBar from './SideBar'
import BlogAction from 'components/BlogDetail/action/BlogDetailAction';
import BlogListStore from 'components/BlogDetail/store/BlogDetailStore';
import Loading from 'components/Loading';
import moment from 'moment';
var ReactDisqusThread = require('react-disqus-thread');
import MetaTags from 'react-meta-tags';


var Scroll  = require('react-scroll');
var scroll     = Scroll.animateScroll;





var BlogDetail = React.createClass({
  getInitialState:function(){
      return{
            blogDetail:"",
            loading:true
      }
	},

  handleNewComment: function(comment) {
        console.log(comment);
    },




  componentDidMount:function(){

      {/*var disqus_config = function () {
          this.page.url = 'a unique URL for each page where Disqus is present';
          this.page.identifier = 'a unique identifier for each page where Disqus is present';
          this.page.title = 'a unique title for each page where Disqus is present';
      };

      (function() {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        var head = document.getElementsByTagName('head')[0];
        var body = document.getElementsByTagName('body')[0];
        console.log('head', head);
        console.log('body', body);
        var d = document, s = d.createElement('script');
        s.src = 'https://staging-wynvent-com-1.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
      })();*/}
  },
  componentWillMount:function(){
      window.scrollTo(0,0);
      BlogListStore.bind(this.BlogListReceived)
      BlogAction.getList(this.props.location.query.id)
  },
  componentWillUnmount:function(){
      BlogListStore.unbind(this.BlogListReceived)
  },
  BlogListReceived:function(){
    window.scrollTo(0,0);
    this.state.blogDetail = BlogListStore.getResponse()
    this.setState({
      blogDetail:this.state.blogDetail,
      loading:false
    })
  },

  render:function(){
    var createdTime =  moment(this.state.blogDetail.creation_date).format("Do MMM YY");
    return(
      this.state.loading?<Loading/>:
          <section className="blogwrap">


          <MetaTags>
              <meta id="meta-description" name="description" content={this.state.blogDetail.content} />
              <meta id="og-title" property="og:title" content={this.state.blogDetail.title}/>
          </MetaTags>


            <div className="container">
              <ul className="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#property-pulse">PROPERTY PULSE</a></li>
                <li className="active">Post</li>
              </ul>
              <div className="row blogdtl-wrap">
                <div className="col-sm-9">
                  <div className="blogthumbdtl">
                    <div className="blg-catgr">{this.state.blogDetail.categories.title}</div>
                    <img src={this.state.blogDetail.image} className="img-responsive" width="850" alt="" />
                  </div>
                  <div className="blodtl-ttltop">
                    <h1>{this.state.blogDetail.title}</h1>
                    <h5 className="row"><span className="pull-left col-sm-8">Posted by {this.state.blogDetail.author.first_name}{' '}{this.state.blogDetail.author.last_name}</span><span className="pull-right col-sm-4 dtblg">{createdTime}</span><br className="clearfix"/></h5>
                  </div>
                  <div className="blogdtl-content">
                    <p>{this.state.blogDetail.lead}</p>
                    <p>{this.state.blogDetail.content}</p>
                    <p>{this.state.blogDetail.content}</p>
                    <p>{this.state.blogDetail.content}</p>
                  </div>
                  {/*<div className="row prev-nxt-bot">
                    <div className="col-xs-6">
                      <a href="#" className="redlnkuppr"> Previous Article</a>
                    </div>
                    <div className="col-xs-6">
                      <a href="#" className="redlnkuppr pull-right">Next Article ></a>
                    </div>
                  </div>*/}
                  {/*<div id="disqus_thread"></div>*/}

                  <ReactDisqusThread
                      shortname="staging-wynvent-com-1"
                      identifier={this.state.blogDetail.title}
                      onNewComment={this.handleNewComment.bind(this)}
                      title={this.state.blogDetail.title}
                      url={"http://localhost:8000/#/blog-detail?id="+this.state.blogDetail.id}
                      />
                </div>
                <SideBar data={this.state.blogDetail.author} id={this.props.location.query.id} changeBlog={this.childLatestClick}/>
              </div>
            </div>
            {/*<a style={{'display':'none'}} href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a>
            <script id="dsq-count-scr" src="//staging-wynvent-com-1.disqus.com/count.js" async></script>*/}

          </section>
    )
  }
})
module.exports = BlogDetail;
