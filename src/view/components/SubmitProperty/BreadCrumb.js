import React from 'react';
import {Link} from 'react-router';
var BreadCrumb =  React.createClass({
   render:function(){
   	 return( 
   	 	        <div className="row">
                    <div className="col-sm-12">
                        <ul className="breadcrumb">
                            <li><Link to="/">Home</Link>
                            </li>
                            <li className="active">submit a property</li>
                        </ul>
                    </div>
               </div>
           )
   }
})
module.exports = BreadCrumb;