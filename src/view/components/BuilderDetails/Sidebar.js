import React from 'react';
import Contact from '../BuilderList/Contact';


var SideBar = React.createClass({
  getInitialState:function(){
    window.scroll(0,0)
    return{
    }
  },
  render:function(){
    return(
      <div className="col-sm-3">
        <div className="sidebar-plan-check">
            <h4 className="sidebarttl">ARE YOU A Builder</h4>
            <h3>Get <b>7x</b> more response for your properties.</h3>
            <p>Register with us.</p>
            <div className="cntr-algn-wrp"><a href="#" className="btn btn-red">CHECK PLANS</a></div>
        </div>

        <div className="sidebar-projects">
          <h4 className="sidebarttl">Top Projects</h4>
          <div className="projlist">
            <h3><a href="#">Project Name</a></h3>
            <p>18 Properties</p>
          </div>
          <div className="projlist">
            <h3><a href="#">Project Name</a></h3>
            <p>18 Properties</p>
          </div>
          <div className="projlist">
            <h3><a href="#">Project Name</a></h3>
            <p>18 Properties</p>
          </div>
          <div className="projlist">
            <h3><a href="#">Project Name</a></h3>
            <p>18 Properties</p>
          </div>
        </div>
        <div className="sidebar-contactbox">
           <Contact/>
        </div>
      </div>
    )
  }
})
module.exports = SideBar;
