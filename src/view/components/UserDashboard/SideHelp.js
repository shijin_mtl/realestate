import React from 'react';
import {ShareButtons,ShareCounts,generateShareIcon} from 'react-share';
import config from 'utils/config';


var SideHelp = React.createClass({

   render:function(){
   	 return(
       <div className="social-boxprfl">
           <div className="social-dasbd">
               <div>
                   <h6>Tell us why your property is worth it!</h6>
                   <span className="redlnkuppr">Share on facebook</span>
                   <div className="dashbrd-social">
                      <a href="#"><i className="fa fa-facebook" aria-hidden="true"></i></a>
                       <a href="#"><i className="fa fa-twitter" aria-hidden="true"></i></a>
                       <a href="#"><i className="fa fa-google-plus" aria-hidden="true"></i></a>
                   </div>
               </div>
               <div>
                   <h6>Add more images of your property.</h6>
                   <div className="add-prop-img">
                       <img src="images/no-photo.jpg" width="70" height="70" className="addprothb" />
                       <div className="add-nwimg">
                           <label className="btn-bs-file btn redlnkuppr">Add now
                               <input type="file" />
                           </label>
                       </div>
                   </div>
               </div>
               <div>
                   <h6>Tell us why your property is worth it!</h6>
                   <p><a href="#" className="redlnkuppr">ADD FACILITIES NEARBY</a>
                   </p>
                   <p><a href="#" className="redlnkuppr">ADD AMENITIES</a>
                   </p>
               </div>
           </div>
       </div>
   	 )
   }
});
module.exports = SideHelp;
