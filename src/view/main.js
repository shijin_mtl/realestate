import React from 'react';
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var MainFrame = require("./components/MainFrame");
var Property = require("./components/Property");
var Home = require("./components/Home");
var Login = require("./components/Login");
var SignUp = require("./components/SignUp");
var SubmitProperty = require("./components/SubmitProperty");
var PropertyDetailPage = require("./components/PropertyDetailPage");
var UserDashboard = require("./components/UserDashboard");
var Privacy = require("./components/Privacy");
var LoanCalculator = require("./components/LoanCalculator");
var Aboutus = require("./components/Aboutus");
var TermsAndConditions = require("./components/TermsAndCondtions");
var PropertyPulse = require("./components/PropertyPulse");
var MarketWatch = require("./components/MarketWatch");
var ForgotPassword = require("./components/ForgotPassword");
var EditProperty = require("./components/EditProperty");
var BuilderAll = require("./components/BuilderList")
var AgentList = require("./components/AgentList")
var BuilderSignup = require("./components/BuilderSignup")
var AgentSignup = require("./components/AgentSignup")
var BuilderDetails = require("./components/BuilderDetails")
var AgentDetails = require("./components/AgentDetails")
var PemiumProperty = require("./components/PremiumProperty")
var BlogDetail = require("./components/BlogDetail")
var Package = require("./components/Package")
var AddressList = require("./components/AddressList")
var PageNotFound = require("./components/404page")
var PaySuccess = require("./components/AddPremium")
var PayTest = require("./components/PayTest")
var CommingSoon = require("./components/CommingSoon")


import  LoginStatusAction from 'components/Header/action/LoginStatusAction';
import { hashHistory,Redirect,IndexRoute,browserHistory} from 'react-router'

function checkSessionStatus(){
	LoginStatusAction.loginStatus();
}
ReactDOM.render(
				<Router history={hashHistory} >
			      <Route path="/" component={MainFrame}  onEnter = {checkSessionStatus}>
			      <IndexRoute component={Home} tabKey={1}/>
						<Route path = "property" component={Property}  tabKey={2}/>
						<Route path = "login" component={Login}  tabKey={3}/>
						<Route path = "signup" component={SignUp}  tabKey={4}/>
						<Route path = "submit-property" component={SubmitProperty}  tabKey={5}/>
						<Route path = "edit-property/:id" component={EditProperty}  tabKey={5}/>
						<Route path = "propertyDetails" component={PropertyDetailPage}  tabKey={6}/>
						<Route path = "profile" component={UserDashboard}  tabKey={7}/>
						<Route path = "privacy" component={Privacy}  tabKey={8}/>
						<Route path = "loan-calculator" component={LoanCalculator}  tabKey={9}/>
						<Route path = "about-us" component={Aboutus}  tabKey={10}/>
						<Route path = "terms-and-conditions" component={TermsAndConditions}  tabKey={11}/>
						<Route path = "market-watch" component={MarketWatch} tabKey={15}/>
						<Route path = "property-pulse" component={PropertyPulse} tabKey={16} />
						<Route path = "password-reset/confirm/:uid/:token" component={ForgotPassword} />
						<Route path = "blog" component={PropertyPulse} />
						<Route path = "builder-list" component={BuilderAll}  tabKey={5}/>
						<Route path = "agent-list" component={AgentList}  tabKey={5}/>
						<Route path = "builder" component={BuilderSignup} />
						<Route path = "agent" component={AgentSignup} />
						<Route path = "builder-detail" component={BuilderDetails} tabKey={12} />
						<Route path = "agent-detail" component={AgentDetails} tabKey={12} />
						<Route path = "premium" component={PemiumProperty} tabKey={5} />
						<Route path = "blog-detail" component={BlogDetail}  tabKey={12}/>
						<Route path = "package" component={Package}  tabKey={5}/>
						<Route path = "address" component={AddressList}  tabKey={5}/>
						<Route path = "add" component={PaySuccess}  tabKey={5}/>
						<Route path = "paytest" component={PayTest}  tabKey={5}/>
						<Route path = "comming-soon" component={CommingSoon}  tabKey={5}/>
						<Route path = "*" component={PageNotFound}  tabKey={5}/>
					</Route >
		        </Router>
					,document.getElementById('renderData')
				);
