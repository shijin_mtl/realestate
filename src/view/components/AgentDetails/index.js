import React from 'react';
import DetailHeader from './Header'
import SideBar from './Sidebar'
import BasicDetails from './BasicDetails'
import AgentStore from 'components/AgentDetails/store/AgentStore';
import AgentAction from 'components/AgentDetails/action/AgentAction';
import PropertyList from './PropertyList'
import MetaTags from 'react-meta-tags';



var AgentSignup = React.createClass({
  getInitialState:function(){
    window.scroll(0,0)
    return{
      agentData:"",
      projectList:[]
    }
  },
  componentWillMount:function(){
    AgentStore.bind(this.responseReceived);
    AgentAction.agentList(this.props.location.query.id)
    window.scrollTo(0,0);
  },
  componentWillUnmount:function(){
    AgentStore.unbind(this.responseReceived);
  },
  responseReceived:function(){
    this.state.agentData = AgentStore.getResponse()
    this.setState({
      agentData:this.state.agentData
    })
    var key = [];
    for(let i=0; i<this.state.agentData.property.length;i++){
      key.push(<PropertyList data={this.state.agentData.property[i]} key={i} />)
    }
    this.state.projectList = key
    this.setState({
      projectList:key
    })
  },
  render:function(){
    return(
      <span>
      <MetaTags>
          <meta id="meta-description" name="description" content={this.state.agentData.description} />
          <meta id="og-title" property="og:title" content={"Wynvent" + this.state.agentData.name}/>
      </MetaTags>

      <section className="agent-dtlwrap">
        <div className="container">
        <DetailHeader data={this.state.agentData.title}/>
        <div className="row">
          <div className="col-sm-9">
            <div className="agtlstbox agt-blddtlwrap">
              <div className="row">
                <BasicDetails data={this.state.agentData} list={this.state.projectList}/>
              </div>
            </div>
          </div>
          <SideBar/>
        </div>
        </div>
      </section>
      </span>
    )
  }
})
module.exports = AgentSignup;
