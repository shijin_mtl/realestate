import React from 'react';
import {hashHistory} from 'react-router';


var Builder = React.createClass({
  goToDetail:function(id){
    hashHistory.push({
      pathname:"/builder-detail",
      query:{id:id}
    })
  },
   render:function(){
   	 return(

           <div className="agtlstbox" >
               <div className="row">
               <div className="col-sm-3 builder-logo">
                 <img src={this.props.builderList.logo?this.props.builderList.logo:"images/noimage.png"} className="img-responsive" alt="Agent Logo" width="240" />
               </div>
               <div className="col-sm-9">
                   <a><h3 onClick={this.goToDetail.bind(this,this.props.builderList.id)}>{this.props.builderList.name}</h3></a>
                   <h4>Property Developed:</h4>
                   <p>{this.props.builderList.property_type.includes(1)?"Residential/Apartments"+" ":""}{this.props.builderList.property_type.includes(2)?"Residential/Villas"+" ":""}
                   {this.props.builderList.property_type.includes(3)?"Commercial/Showrooms"+" ":""}
                   {this.props.builderList.property_type.includes(4)?"Commercial/Shops"+" ":""}
                   </p>
                   <h4>Locations handled:</h4>
                   <p>{this.props.builderList.city}</p>
                   <h4>Builder Info:</h4>
                   <p>{this.props.builderList.description}</p>
                   <div className="row agtlnkbtm">
                     <div className="col-sm-12 col-md-6 feat-verf-disp">
                       {this.props.builderList.featured && <span className="feat-ver-box featboxonly">Featured</span>}
                       {this.props.builderList.verified && <span className="feat-ver-box verbboxonly">Verified</span>}
                     </div>
                     <div className="col-sm-12 col-md-6 lrn-vew-disp">
                       <a onClick={this.goToDetail.bind(this,this.props.builderList.id)} className="btn-line">Learn more</a>
                       {/*<a onClick={this.goToDetail.bind(this,this.props.builderList.id)} className="btn-line">View Properties</a>*/}
                     </div>
                   </div>
                 </div>
               </div>
           </div>
   	 )
   }
});
module.exports = Builder;
