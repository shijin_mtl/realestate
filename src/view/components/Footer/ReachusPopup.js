import React from 'react';
import { Form, FormGroup,TextArea, Validator,pattern, Input,required,minLength,maxLength} from 'romagny13-react-form-validation';
import TalktoExpertAction from 'components/Home/action/TalktoExpertAction';
import TalktoExpertStore from 'components/Home/store/TalktoExpertStore';
import ReactDOM from "react-dom";



const validators = {
    'name': [required('Name is Required'), minLength(3),maxLength(30),pattern(/^[a-zA-Z']+(\s[a-zA-Z']+){0,4}$/, '3 to 30 characters special characters are not alowed.')],
};


var MyFormGroup = function (props) {
    var groupclassName = props.hasError ? 'form-group has-error' : 'form-group';
    return (
            <div className={groupclassName}>
                {props.children}
                {props.hasError && <span className="help-block">{props.error}</span>}
            </div>
    );
};

var ReachusPopup = React.createClass({
    getInitialState:function(){
       return{
       	       model:{
       	       	     name:"",
              	     email:"",
              	     mobile:"",
              	     message:""
       	       }
       }
    },
    componentWillMount:function(){
      TalktoExpertStore.bind(this.expertResponseReceived);
	},
	componentWillUnmount:function(){
      TalktoExpertStore.unbind(this.expertResponseReceived);
	},
	expertResponseReceived:function(){
      var expertResponse = TalktoExpertStore.getResponse();

	},

    email:function(value){
        var atpos = value.indexOf("@");
        var dotpos = value.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=value.length || ! /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
            return{
                name: 'email',
                error: 'Invalid email address'
            }
        }
    },
    onSubmit:function(result){
        if(!result.hasError){
            TalktoExpertAction.expert(result.model)
            setTimeout(function(){
              document.getElementById('name').value = "";
              document.getElementById('mobile').value = "";
              document.getElementById('email').value = "";
              document.getElementById('message').value = "";

             }, 100);
            ReactDOM.findDOMNode(this.refs.popupClose).click()
        }

        ReactDOM.findDOMNode(this.refs.reachUs).reset()
	},
    number:function(value){
      if(value.length!=10 || !/^\d+$/.test(value)){
            return{
                name:'number',
                error:"Invalid number"
            }
      }
    },
    render:function(){
   	 var validation = {
        email:this.email,
        number:this.number
     }
   	 return(
   	 	        <div className="modal fade" id="reachus-pop" tabIndex="-1" role="dialog">
				    <div className="vertical-alignment">
				        <div className="modal-dialog vertical-align-center" role="document">
				            <div className="modal-content">
				                <div className="modal-body">
				                    <div className="hm-bnrform">
				                        <button type="button" className="close pull-right" data-dismiss="modal" ref='popupClose' aria-label="Close">close</button>
				                        <div className="bnrinrbox">
				                            <h3>REACH US</h3>
				                            <h5>Tell us about you and we will help you find what you need.</h5>
				                            <div className="reachformbox">

                                <Form onSubmit={this.onSubmit} model={this.state.model} ref='reachUs'>
      												    <div className="form-group">
      												        <Validator validators={validators['name']}>
      												            <MyFormGroup>
      												                <Input className="form-control" id="Reachname" name="name" value={this.state.model.name} className="form-control"  placeholder="Name" />
      												            </MyFormGroup>
      												        </Validator>
      												    </div>
      												    <div className="form-group">
      												        <Validator validators={[required( 'Number is required '),validation['number']]}>
      												            <MyFormGroup>
      												                <Input type="number" className="form-control" id="Reachmobile" name="mobile" value={this.state.model.mobile} className="form-control"  placeholder="Your 10 digit mobile number" />
      												            </MyFormGroup>
      												        </Validator>
      												    </div>
      												    <div className="form-group">
      												        <Validator validators={[required( 'Email is required'),validation['email']]}>
      												            <MyFormGroup>
      												                <Input className="form-control" id="Reachemail" name="email" value={this.state.model.email} className="form-control"  placeholder="Email"/>
      												            </MyFormGroup>
      												        </Validator>
      												    </div>
      												    <div className="form-group ">
                                  <Validator validators={[required( 'Message is required')]}>
                                   <MyFormGroup>
      												        <TextArea className="form-control" placeholder="Message"  value={this.state.model.message} id='Reachmessage' name='message'/>
                                  </MyFormGroup>
                                  </Validator>
                                  </div>
      												    <div className="form-group">
      												        <button className="btn btn-red" type="submit">Submit</button>
      												    </div>
											           </Form>
				                            </div>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
   	 )
   }
});
module.exports = ReachusPopup;
