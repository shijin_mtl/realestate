import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/MainFrame/constants/PopUpConstants';
import config from 'utils/config'
import axios from 'axios';

var popUpGet = function(){

}

const getPop = axios.create({
  withCredentials: true
});

popUpGet.prototype = {
	 getNow:function(parameters){
	 	getPop.get(config.server  + 'api/v1/user-timer-popup/')
		    .then(function (response) {
		    	AppDispatcher.dispatch({
					actionType: Constants.POPUP_RESPONSE_RECIEVED,
					data: response
				});

            })
			.catch(function (error) {
			     console.log(error);
            });


	 }
}

module.exports = new popUpGet();
