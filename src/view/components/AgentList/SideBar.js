import React from 'react';
import Contact from '../BuilderList/Contact'



var SideBar = React.createClass({
   render:function(){
   	 return(
       <div className="col-sm-3">
           <div className="sidebar-plan-check">
             <h4 className="sidebarttl">ARE YOU A AGENT?</h4>
             <h3>Get <b>7x</b> more response for your properties.</h3>
             <p>Register with us.</p>
             <div className="cntr-algn-wrp"><a href="#package" className="btn btn-red">CHECK PLANS</a></div>
           </div>
           <div className="sidebar-contactbox">
              <Contact/>
            </div>
            <div className="ad-box-md">
              <p style={{'lineHeight': 220+'px', 'textAlign': 'center'}}>Ad Space</p>
            </div>
       </div>
   	 )
   }
});
module.exports = SideBar;
