import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PropertyPulse/constants/PropertyPulseConstants';
import config from 'utils/config';
import axios from 'axios';
var AdAction = function(){

}

const adApi = axios.create({
  withCredentials: true,
});

AdAction.prototype = {
	GetAdNow:function(params){
		adApi.get(config.server + 'api/v1/advertisement/')
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.AD_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
      });
	}
}


module.exports = new AdAction();
