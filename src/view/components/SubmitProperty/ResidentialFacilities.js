import React from 'react';
import { Checkbox,Row } from 'antd';
const CheckboxGroup = Checkbox.Group;

var ResidentialFacilities  = React.createClass({
	onChange:function(key,id,event){
	 if(key == "electronics"){
	 	this.props.changeResidentialFacility(id);
	 }
	 if(key == "furniture")	{
	 	this.props.changeFurnitureFacility(id)
	 }
	},
    render:function(){
        var facilityRespArr = this.props.facility?this.props.facility:[];
        var facilityListObj = {Furniture:[],Electronics:[]};
        var furnitureOptions = [] ;
        var electricOptions = [];
        for (var i = 0, len = facilityRespArr.length; i < len; i++) {
            facilityListObj[facilityRespArr[i].name] = facilityRespArr[i].facilities;
        }
        var furnitureFacility = facilityListObj['Furniture'];
        var electronicFacility = facilityListObj['Electronics'];
        for (var j = 0, len = furnitureFacility.length; j < len; j++) {
           furnitureOptions.push(<div className="checkbox checkbox-primary" key={j}>
                                   <input id={furnitureFacility[j].name} type="checkbox" onChange={this.onChange.bind(this,"furniture",furnitureFacility[j].id)} />
                                     <label htmlFor={furnitureFacility[j].name} >{furnitureFacility[j].name} </label>
                                 </div>
                                )

        }
        for (var k = 0, len = electronicFacility.length; k < len; k++) {
           electricOptions.push(<div className="checkbox checkbox-primary" key={k}>
                                   <input id={electronicFacility[k].name} type="checkbox" onChange={this.onChange.bind(this,"electronics",electronicFacility[k].id)}/>
                                     <label htmlFor={electronicFacility[k].name} >{electronicFacility[k].name} </label>
                                 </div>)
        }

    	return(
    		     <div className="property-detail-inner-form">
						<h4>2. Facilities</h4>
						<div className="configuration-details">
						  <div className="row">
						      <div className="col-md-6 col-sm-12">
						          <div className="form-group">
						              <label  className="col-md-6 col-sm-6 col-xs-12 control-label">Electronics</label>
						              <div className="col-md-6 col-sm-6 col-xs-12">
						                   {electricOptions}
						              </div>
						          </div>
						      </div>
						      <div className="col-md-4 col-sm-12">
						        <div className="form-group">
						              <label  className="col-md-6 col-sm-6 col-xs-12 control-label">Furniture</label>
						              <div className="col-md-6 col-sm-6 col-xs-12">
						                {furnitureOptions}
						              </div>
						          </div>
						      </div>
						  </div>
						</div>
                </div>
    	)
    }
});
module.exports = ResidentialFacilities;
