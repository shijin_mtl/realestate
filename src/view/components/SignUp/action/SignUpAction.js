import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/SignUp/constant/SignUpConstant';
import config from 'utils/config'
import axios from 'axios';

var onSignUp = function(){

}

const signUpApi = axios.create({
  withCredentials: true,
  headers: {
    'crossDomain': true,
  },
});
onSignUp.prototype = {
	 signUp:function(parameters){
	 	var userNames = parameters.name.split(/(\s+)/);
	 	var signUpParameters = {'fname':userNames[0],'lname':userNames[2]?userNames[2]:"",'email':parameters.email,'phone':parameters.phone,'type':parameters.type,'password':parameters.password};
	 	axios.get(config.server  + 'api/v1/check-if-email-exists/?email='+parameters.email)
		    .then(function (res) {
		      if(!res.data['account-exists']){
                 signUpApi.post(config.server + 'api/v1/register/', signUpParameters)
				    .then(function (response) {
					    AppDispatcher.dispatch({
					        actionType: Constants.SIGNUP_RESPONSE_RECIEVED,
					        data: response
						});
				    })
				   .catch(function (error) {
				      console.log(error);
		            });
		      }else{
		      	    AppDispatcher.dispatch({
		   		        actionType: Constants.EMAIL_ALREADY_EXISTS,
				          data: res
					});
		      }
            })
			.catch(function (error) {
			     console.log(error);
      });
	 }
}

module.exports = new onSignUp();
