var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;

import Constants from 'components/Header/constants/LoginStatusConstant';

var response = {};


var assign = require('object-assign');

function parseResponse(resp){
  response = resp;
  localStorage.loacalLocation = resp
}


var LocationChnageStore = assign({},EventEmitter.prototype,{
   emitChangeEvent: function(event) {
       this.emit(event);
   },
   bind: function(callback) {
       this.on(Constants.LOCATION_CHANGED, callback);
   },
   unbind: function(callback) {
       this.removeListener(Constants.LOCATION_CHANGED, callback);
   },
   getResponse:function(){
        return response;
   }
});


Dispatcher.register(function(action){
   switch (action.actionType) {
       case Constants.LOCATION_CHANGED:
           var resp = action.data;
           parseResponse(resp)
           LocationChnageStore.emitChangeEvent(Constants.LOCATION_CHANGED)
       default:
   }
});

module.exports = LocationChnageStore;
