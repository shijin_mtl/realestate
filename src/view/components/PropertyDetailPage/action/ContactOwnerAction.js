import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PropertyDetailPage/constants/PropertyDetailConstant';
import config from 'utils/config';
import axios from 'axios';

var contactOwner = function(){

}

const contactOwnerApi = axios.create({
  withCredentials: true,
});

const sessionCount = axios.create({
  withCredentials: true,
});

var viewCountApi
contactOwner.prototype = {
	contactAction:function(params){
		contactOwnerApi.post(config.server + 'api/v1/property/responses/', params)
			.then(function (response) {
            makeClick(params.slug)
		        AppDispatcher.dispatch({
			        actionType: Constants.CONTACT_OWNER_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}

function makeClick(data){
  sessionCount.get(config.server +'api/v1/session-status')
    .then(function (response) {
      viewCountApi = axios.create({
        withCredentials: true,
      });
    }).then(function(){
      viewCountApi.get(config.server +'core/data/process/?type=property&action=r_ct&slug='+data)
        .then(function (response) {

        })
        .catch(function (error) {
            console.log("error...",error);
        });
      })
}

module.exports = new contactOwner();
