import React from 'react';
import AgentDetails from './AgentDetails'
import Success from './Success'
import AgentEdit from 'components/AgentSignup/action/getAgentAction';
import AgentEditStore from 'components/AgentSignup/store/AgentEditStore';

var AgentSignup = React.createClass({
  getInitialState:function(){
    window.scroll(0,0)
    return{
      content:this.props.location.query.id?null:<AgentDetails changeContent = {this.changeContent}/>
    }
  },

  componentWillMount:function(){
    AgentEditStore.bind(this.agentEditDataReceived);
    if(this.props.location.query.id){
      AgentEdit.getEditData(this.props.location.query.id)
    }
  },
  componentWillUnmount:function(){
    AgentEditStore.unbind(this.agentEditDataReceived);
  },
  agentEditDataReceived:function(){
    var details = AgentEditStore.getResponse()
    this.state.content = <AgentDetails changeContent = {this.changeContent} details={details} id={this.props.location.query.id}/>
    this.setState({
      content:this.state.content
    })
  },


  changeContent:function(){
    this.state.content = <Success/>
    this.setState({
      content : this.state.content
    })
  },
  render:function(){
    return(
      <span>
        {this.state.content}
      </span>
    )
  }
})
module.exports = AgentSignup;
