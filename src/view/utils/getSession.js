
import config from 'utils/config';
import axios from 'axios';

var viewCountApi


const sessionCount = axios.create({
  withCredentials: true,
});


sessionCount.get(config.server +'api/v1/session-status')
  .then(function (response) {
    viewCountApi = axios.create({
      withCredentials: true,
      headers:{
        SESSION_ID:response.data["session-id"],
        WYNVENT_ELK:true,
        TIMEZONE:'UTC',
      }
    });
  })

module.exports = new viewCountApi;
