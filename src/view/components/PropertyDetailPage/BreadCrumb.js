import React from 'react';
import {Link} from 'react-router';
var BreadCrumb = React.createClass({
	render:function(){
		return(
			   <div className="row no-print">
                    <div className="col-sm-12">
                        <ul className="breadcrumb">
                            <li><a href="#">Home</a>
                            </li>
                            <li><Link to="property">PROPERTIES IN YOUR AREA</Link>
                            </li>
                            <li className="active">PROPERTY DETAIL</li>
                        </ul>
                    </div>
                </div>
		)
	}
});
module.exports = BreadCrumb;
