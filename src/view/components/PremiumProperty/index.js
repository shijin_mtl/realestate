import React from 'react';



var PemiumProperty = React.createClass({
	handleClose:function(){

	},
	render:function(){
		return(
      <span>
      <section className="packg-banner">
      <div className="container">
      <div className="bnrinfo">
        <h2>Sell your property quickly.  </h2>
        <h4>With our Premium Package</h4>
        <h5>At a minimal cost of </h5>
        <span className="price-inr">INR XXXX/-</span>
        <a href="#/address" className="btn btn-red">Buy Now</a>
      </div>
      </div>
      </section>

      <section className="packg-wrap">
      <div className="container">
      <ul className="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Submit a property</a></li>
        <li className="active">Premium Package</li>
      </ul>
      <div className="row packg-row">
        <div className="col-sm-7">
          <div className="pack-thumb">
            <img src="images/package.jpg" className="img-responsive" alt="Package" width="700"/>
          </div>
        </div>
        <div className="col-sm-5">
          <h2>What do you get?</h2>
          <ul>
            <li><span className="lnr lnr-checkmark-circle"></span> Maximum Visibility- Guaranteed top position in search for 2 weeks**</li>
            <li><span className="lnr lnr-checkmark-circle"></span> Detailed property description written by experts</li>
            <li><span className="lnr lnr-checkmark-circle"></span> Highlight your ad with 'Premium' tag</li>
            <li><span className="lnr lnr-checkmark-circle"></span> Preferential Visibility in Search results</li>
            <li><span className="lnr lnr-checkmark-circle"></span> Support 50 Photos , up to 5 MB</li>
            <li><span className="lnr lnr-checkmark-circle"></span> Support for Property Video</li>
            <li><span className="lnr lnr-checkmark-circle"></span> Increased total duration on site</li>
            <li><span className="lnr lnr-checkmark-circle"></span> Validity - 3 Months</li>
          </ul>
          <div className="cntr-algn-wrp"><a href="#/address" className="btn btn-red">Go Premium</a></div>
        </div>
      </div>
      </div>
      </section>
      </span>
		)
	}
})
module.exports = PemiumProperty;
