import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Footer/constants/FooterConstants';
import config from 'utils/config'
import axios from 'axios';

var onSubscription = function(){

}

const subscribeApi = axios.create({
  withCredentials: true
});

onSubscription.prototype = {
	 subscribe:function(parameters){
	 	subscribeApi.post(config.server  + 'api/v1/subscribe-newsletter/',parameters)
		    .then(function (response) {
		    	AppDispatcher.dispatch({
					actionType: Constants.SUBSCRIBE_RESPONSE_RECEIVED,
					data: response
				});
		   
            })
			.catch(function (error) {
			     console.log(error);
            });
		
				
	 }
}


module.exports = new onSubscription();
