import React from 'react';
import BuilderList from './BuilderList'
import {Link,hashHistory} from 'react-router';
import BuilderStore from 'components/MarketWatch/store/BuilderStore';
import BuilderAction from 'components/MarketWatch/action/BuilderAction';
import TextTruncate from 'react-text-truncate';



var FeaturedBuilders = React.createClass({
  getInitialState:function(){
    return{
           list:[],
    }
  },
  componentWillMount:function(){
    BuilderStore.bind(this.builderListRecieved)
    BuilderAction.builderList()
  },
  componentWillUnmount:function(){
    BuilderStore.unbind(this.builderListRecieved)
  },
  builderListRecieved:function(){
    var response = BuilderStore.getResponse().results
    for(let i=0;i<response.length;i++){
      this.state.list.push(<BuilderList key={i} data={response[i]}/>)
    }
    this.setState({
      list:this.state.list
    })
  },
  showMore: function(){
    hashHistory.push('/builder-list');
  },
    render:function(){
    	return(
        <section className="featured-proplst-wrap">
          <div className="container">
            <h2 className="main-ttl">Featured Builders</h2>
            <div className="featured-boxwrp">
                <div className="row">
                {this.state.list}
                </div>
            </div>
            <div className="feat-btm-wrp"><h4 className="backline"><a onClick = {this.showMore}>Show All</a></h4></div>
          </div>
        </section>
    	)
    }
});
module.exports = FeaturedBuilders;
