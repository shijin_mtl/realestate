import React from 'react';
import Contact from '../BuilderList/Contact';
import {ShareButtons,ShareCounts,generateShareIcon} from 'react-share';
import config from 'utils/config';
import BlogLatestAction from 'components/BlogDetail/action/BlogLatest';
import BlogLatestStore from 'components/BlogDetail/store/BlogLatestStore';
import BlogAction from 'components/BlogDetail/action/BlogDetailAction';




const {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  PinterestShareButton,
  VKShareButton,
} = ShareButtons;


const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');





var SideBar = React.createClass({
  getInitialState:function(){
      return{
            latestList:[],
      }
	},

  childLatestClick:function(id){
    BlogAction.getList(id)
  },

  componentWillMount:function(){
    BlogLatestStore.bind(this.latestResponseReceived);
    BlogLatestAction.getList()

  },
  componentWillUnmount:function(){
    BlogLatestStore.unbind(this.latestResponseReceived);
  },
  latestResponseReceived:function(){
    var list = BlogLatestStore.getResponse()
    for(let i=0;i<list.length;i++)
    {
      this.state.latestList.push(<li key ={i}><a onClick={this.childLatestClick.bind(this,list[i].id)}>{list[i].title}</a></li>)
    }
    this.setState({
      latestList:this.state.latestList
    })
  },



  showShareFb:function(){
     var ip = config.ip;
     var url = "staging.wynvent.com" +  '#/' + "blog-detail" +  '?id='+this.props.id;
        return(
                <FacebookShareButton
                   url={url}
                   className="fa fa-facebook"
                   />
              )

  },
  showShareTwitter:function(){
     var ip = config.ip;
     var url = "staging.wynvent.com" +  '#/' + "blog-detail" +  '?id='+this.props.id;
         return(
                  <TwitterShareButton
                    url={url}
                    title={"http://"+url}
                    className="fa fa-twitter"/>
         )

  },
  showShareGoogle:function(){
     var ip = config.ip;
     var url = "staging.wynvent.com" +  '#/' +  "blog-detail" +  '?id='+this.props.id;
         return(
                  <GooglePlusShareButton
                    url={url}
                    className="fa fa-google-plus"/>
         )

  },
  fbClick:function(){
    $('.SocialMediaShareButton--facebook fa fa-facebook').click()
  },

  render:function(){
    return(
          <section className="blogwrap">
          <div className="col-sm-3">
            <div className="sidebar-blgauthr">
              <h4 className="sidebarttl">About the author</h4>
              <div className="auth-ttltop">
                <div className="authr-thumb pull-left">
                  <img className="img-responsive img-circle" src={this.props.data.avatar} alt="" width="75" height="75"/>
                </div>
                <h3>{this.props.data.first_name} {' '} {this.props.data.last_name}</h3>
                <h5>{this.props.data.title}</h5>
                <div className="clearfix"></div>
              </div>
              <p>{this.props.data.description}</p>
            </div>
            <br/>
            <div className="property-share">
                <h3 className="title-type-small">LIKE THIS? SHARE IT</h3>
                <ul className="share-widget">
                    <li>
                        <a >{this.showShareFb()}</a>
                    </li>
                    <li>
                        <a>{this.showShareTwitter()}</a>
                    </li>
                    <li>
                        <a>{this.showShareGoogle()}</a>
                    </li>
                </ul>
            </div>
            <div className="sidebar-contactbox">
                  <Contact/>
            </div>
            <div className="sidebar-article">
              <h4 className="sidebarttl">Most Recent Articles</h4>
              <ul>
                {this.state.latestList}
              </ul>
            </div>
          </div>
          </section>
    )
  }
})
module.exports = SideBar;
