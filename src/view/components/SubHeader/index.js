import React from 'react';
import {Link} from 'react-router';
import config from 'utils/config';
var SubHeader1 = React.createClass({
	componentDidMount:function(){
        var sideslider = $('[data-toggle=collapse-side]');
        var sel = sideslider.attr('data-target');
        var sel2 = sideslider.attr('data-target-2');
        sideslider.click(function(event){
           $(sel).toggleClass('in');
           $(sel2).toggleClass('out');
        });
    },
	render:function(){
		return(
			    <div className="bottom-head">
				    <div className="container">
				      <div className="row">
				        <div className="col-xs-5 col-sm-2 col-md-3 logowrap">
				          <Link to="/"  className="toplogo"><img src={config.s3static + "images/logo.png"} alt="Wynvent-logo" className="img-responsive" width="230" /></Link>
				        </div>
				        <div className="col-xs-7 col-sm-10 col-md-9 topsearchwrap">
				          <button data-toggle="collapse-side" data-target=".side-collapse" type="button" className="navbar-toggle pull-right mob-navbar">
				              <span className="icon-bar"></span>
				              <span className="icon-bar"></span>
				              <span className="icon-bar"></span>
				          </button>
				          <div className="side-collapse in pull-right">
				          <nav className="navbar-collapse">
				                <ul className="nav navbar-nav main-nav main-nav-only">
				                  <li><Link to = "property" >buy/rent</Link></li>
				                  <li><Link to="market-watch">market watch</Link></li>
				                  <li><Link to="property-pulse">property pulse</Link></li>
				                  <li className="dropdown">
				                    <a href="/#/" className="dropdown-toggle" id="morenavdrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">&more</a>
				                    <span className="bs-caret"><span className="caret"></span></span>
				                    <div className="dropdown-menu moredopbox" aria-labelledby="morenavdrop">
				                      <div className="row">
				                        <div className="col-md-4">
				                          <h4>services</h4>
				                          <ul>
				                            <li><Link to ="/submit-property">Submit a property</Link></li>
				                            <li><Link to="/package">Advertise with Us</Link></li>
				                            <li><Link to="comming-soon">Know your tenant</Link></li>
				                            <li><Link to="comming-soon">Property Evaluation</Link></li>
				                            <li><Link to="comming-soon">Legal Services</Link></li>
				                            <li><Link to="comming-soon">Astrology</Link></li>
				                          </ul>
				                        </div>
				                        <div className="col-md-4">
				                          <h4>tools</h4>
				                          <ul>
				                            <li><Link to="loan-calculator" href="javascript:;">Loan Calculator</Link></li>
				                            <li><a href="/#/comming-soon">Property Worth</a></li>
				                            <li><a href="/#/comming-soon">Compare Localities</a></li>
				                          </ul>
				                        </div>
				                        <div className="col-md-4">
				                          <h4>explore</h4>
				                          <ul>
				                            <li><Link to="comming-soon">New Projects</Link></li>
				                            <li><Link to="property-pulse">Agents</Link></li>
				                          </ul>
				                        </div>
				                      </div>
				                    </div>
				                  </li>
				                  <li><Link to="/submit-property" className="sbt-prop-btn">Submit a property</Link></li>
				                </ul>
				          </nav>
				          </div>
				        </div>
				      </div>
				    </div>
				   </div>
				 )
	}
});
module.exports = SubHeader1;
