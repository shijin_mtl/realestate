import React from 'react';
import LoginFacebookAction from 'components/Login/action/LoginFacebookAction';
var FacebookAuth  = React.createClass({
	componentWillMount:function(){

	},
	loginWithFacebook:function(){
	 FB.login(function(response) {
      if (response.authResponse) {

      	var parameters = {'access_token':response.authResponse.accessToken,'provider':'facebook'}
      	LoginFacebookAction.loginFacebook(parameters);

      	
      } else {

      }
    });
    return false;

	},
	componentDidMount:function(){
       window.fbAsyncInit = function() {
		    FB.init({
			      appId: '1351681441588759',
			      cookie: true, // This is important, it's not enabled by default
			      version: 'v2.2'
		    });
        };


      (function(d, s, id){
	    var js, fjs = d.getElementsByTagName(s)[0];
	    if (d.getElementById(id)) {return;}
	    js = d.createElement(s); js.id = id;
	    js.src = "//connect.facebook.net/en_US/sdk.js";
	    fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));
	},
	render:function(){
		return(
			     <a href ="javascript:;" onClick={this.loginWithFacebook}>
			      <span className="log-soc"> 
			       <i className="fa fa-facebook" aria-hidden="true"></i>
			      </span>Facebook
			     </a>

		)
	}
})
module.exports = FacebookAuth;