import React from 'react';
import {Gmaps,Marker} from 'react-gmaps';
import BreadCrumb from './BreadCrumb';
import BackGroundImage from 'public/images/no-photo.jpg';
import {hashHistory} from 'react-router';
import moment from 'moment';
var getPriceFormat = require('utils/ChangePrice.js').getPriceFormat;


var SinglePropertyList = React.createClass({
   showImage:function(){
        if(this.props.propertyContents.thumbnail_url){
            return( <div className="prop-thumb" style={{backgroundImage: `url(${this.props.propertyContents.thumbnail_url})`}} /> )
        }
        else{
            return( <div className="prop-thumb" style={{backgroundImage: `url(${BackGroundImage})`}} />)
        }

    },

    openInNewTab:function(url) {
      var win = window.open(url, '_blank');
      win.focus();
    },

    showPropertyDetail:function(){
      this.openInNewTab("/#/propertyDetails?id="+this.props.propertyContents.id)
      PopupAction.getNow()

        {/*hashHistory.push({
          pathname:"/propertyDetails",
          query:{id:this.props.propertyContents.id}
        })*/}
    },

   render:function(){
     var lat = 10.0159;
     var long = 76.3419;
     var createdTime =  moment(this.props.propertyContents.created).format("Do MMM YY") ;
    if (Object.keys(this.props.propertyContents.coordinates).length >= 0)
             {
                  lat = this.props.propertyContents.coordinates.lat
                  long = this.props.propertyContents.coordinates.lng

             }
   	return(
                    <div className="proplistbox ih-item square effect10 bottom_to_top">
                        <a href="javascript:;" className="propanchr">
                            <div className="row lst-grd-wrp">
                                <div className="col-sm-5 lst-grd-thmb">
                                    <div className="propbandred">{this.props.propertyContents.property_for}</div>
                                     {this.showImage()}
                                </div>
                                <div className="col-sm-7 lst-grd-info">
                                    <div className="prop-infobox">
                                        <h3>{this.props.propertyContents.name?this.props.propertyContents.name:"New Builders"}</h3>
                                        <h4>{this.props.propertyContents.locality?this.props.propertyContents.locality:"Not Mentioned"}</h4>
                                        <h5><span className="pull-left">Posted by {this.props.propertyContents.owner_name}</span> <span className="pull-right">{createdTime}</span><br className="clearfix"/></h5>
                                    </div>
                                    <div className="prop-price">
                                        <h2><i className="fa fa-inr" aria-hidden="true"></i>{getPriceFormat(this.props.propertyContents.expected_price)}/-</h2>
                                        <span className="nego-ttl">Slightly Negotiable</span>
                                    </div>

                                </div>
                            </div>
                            <div className="info">
                            <Gmaps
                                width={'100%'}
                                height={'100%'}
                                lat={lat}
                                lng={long}
                                zoom={12}
                                scrollwheel={false}
                                >
                                  <Marker
                                    lat={lat}
                                    lng={long}
                                    />
                            </Gmaps>
                                <div className="hvr-btn-map">
                                    <button className="btn btn-red" onClick={this.showPropertyDetail}>Read more</button>
                                </div>
                            </div>
                        </a>
                    </div>


   	)
   }
});
module.exports= SinglePropertyList;
