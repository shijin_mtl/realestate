import React                from 'react';
import Flickity             from 'flickity';

export default React.createClass({

    getInitialState() {
        return {
            selectedIndex: 0
        }
    },

    componentDidMount() {
        const carousel = this.refs.carousel.getDOMNode();
        const options = {
            cellSelector: '.course-item',
            contain: true,
            initialIndex: 0,
            accessibility: true
        }

        this.flkty = new Flickity(carousel, options);
        this.flkty.on('cellSelect', this.updateSelected);
    },

    updateSelected() {
        var index = this.flkty.selectedIndex;
        this.setState({
            selectedIndex: index
        });
        this.props.updateSelected(index)
    },

    componentWillUnmount() {
        if (this.flkty) {
            this.flkty.off('cellSelect', this.updateSelected);
            this.flkty.destroy();
        }
    },

    render() {
        return (
            <div ref='carousel' className='carousel'>
                <div className="course-item ">1</div>
                <div className="course-item ">2</div>
                <div className="course-item ">3</div>
                <div className="course-item ">3</div>
                <div className="course-item ">3</div>
                <div className="course-item ">3</div>
                <div className="course-item ">3</div>
                <div className="course-item ">3</div>
                <div className="course-item ">3</div>
                <div className="course-item ">3</div>
                <div className="course-item ">3</div>
                
            </div>
        )
    }
})