import React from 'react';
import config from 'utils/config';


var ThingsThatMakesAwesome = React.createClass({
    render:function(){
    	return(
    		     <section className="diagonalwrap">
						<div className="diag-outwrap">
						  <div className="container">
						    <div className="row">
						      <div className="col-sm-3 hidden-sm hidden-xs"></div>
						      <div className="col-sm-12 col-md-9 diagonalbox">
						        <div className="diagnal-content">
						          <h2><span className="sbttl">THINGS</span>THAT MAKE US AWESOME</h2>
						          <div className="row diagnlistwrap">
						            <div className="col-sm-4">
						              <img src={config.s3static + "images/icon-01.png"} width="60" alt="Icon" />
						              <h4>ABOUT US</h4>
						              <p>Wynvent is a trusted community marketplace ofr people to list,buy and sell properties across India-online,from a mobile phone or even a tablet.</p>
						            </div>
						            <div className="col-sm-4">
						              <img src={config.s3static + "images/icon-02.png" } width="60" alt="Icon" />
						              <h4>GROWING COMMUNITY</h4>
						              <p>With a growing community of over 30,0000 users,Wynvent connects people to unique buying and selling experiance,at any price point, in more than 10 cities.</p>
						            </div>
						            <div className="col-sm-4">
						              <img src={config.s3static + "images/icon-03.png" } width="60" alt="Icon" />
						              <h4>MORE THAN 5 LAKH PROPERTIES</h4>
						              <p>Finding the property of your dreams becoms an even better experiance with more tha 5 lack options that Wynvent offers, across Indian Cities.</p>
						            </div>
						          </div>
						          <div className="row diagnlistwrap">
						            <div className="col-sm-4">
						              <img src={config.s3static + "images/icon-04.png"} width="60" alt="Icon" />
						              <h4>FIND THE PERFECT PROPERTY</h4>
						              <p>Now it is possible to search for your new home as you sit and compare multiple properties with real pictures and videos.</p>
						            </div>
						            <div className="col-sm-4">
						              <img src={config.s3static + "images/icon-05.png"} width="60" alt="Icon" />
						              <h4>CUSTOMER SERVICE</h4>
						              <p>With a world-class customer service and ever growing listing,Wynvent is the easiest way for people to buy and sell their property,and showcase into as audience of millions.</p>
						            </div>
						            <div className="col-sm-4">
						              <img src={config.s3static + "images/icon-06.png"} width="60" alt="Icon" />
						              <h4>DOWNLOAD THE APP</h4>
						              <p>Want to search for properties from anywhere,talk to agents,schedule visits and see facilities around the property?Download the free Wynvent App</p>
						            </div>
						          </div>
						        </div>
						      </div>
						    </div>
						  </div>
						</div>
                    </section>
    	)
    }
});
module.exports = ThingsThatMakesAwesome;
