
import React from 'react';
import {Link} from 'react-router';
import ReactDOM from "react-dom";
import {Modal} from 'antd';



var LoanCalculator = React.createClass({





  getInitialState:function(){
     return{
             loanCalculator:{
               requiredAmount:"",
               existingLoanCommitment:"",
               interestRate:"",
               loanTenure:"",
               netIncome:""
             },
             emi:"",
             showEligibleForEmi:false,
             showNotEligibleForEmi:false
     }
  },
  handleChange:function(key,event){
    this.state.loanCalculator[key] = event.target.value;
    if (event.target.value<0)
    {
      event.target.value = 0
    }
    this.setState({
         loanCalculator:this.state.loanCalculator
    })
  },
  componentWillMount:function(){
     $(".side-collapse").addClass('in');
      window.scrollTo(0,0);

   },
   componentDidMount:function(){
    $(':input[type=number]').on('mousewheel',function(e){ $(this).blur(); });

    },
  checkLoanEligibility:function(){
    if(ReactDOM.findDOMNode(this.refs.requiredAmount).value == ""|| ReactDOM.findDOMNode(this.refs.netIncome).value == ""|| ReactDOM.findDOMNode(this.refs.existingLoanCommitment).value == ""|| ReactDOM.findDOMNode(this.refs.loanTenure).value == ""|| ReactDOM.findDOMNode(this.refs.interestRate).value == "" )
    {
      Modal.error({
          title: 'Please fill all the fields',
          okText  :'Ok'
      });
    }
     else if(this.state.loanCalculator.existingLoanCommitment <= 500000 && this.state.loanCalculator.netIncome >= 25000){
         var rate = this.state.loanCalculator.interestRate;
         var month = this.state.loanCalculator.loanTenure;
         var pamt = this.state.loanCalculator.requiredAmount;
         var monthlyInterestRatio = (rate/100)/12;
         var monthlyInterest = (monthlyInterestRatio*pamt);
         var top = Math.pow((1+monthlyInterestRatio),month);
         var bottom = top -1;
         var sp = top / bottom;
         var emi = ((pamt * monthlyInterestRatio) * sp);
         var result = emi.toFixed(2);
         var totalAmount = emi*month;
         var yearlyInteret = totalAmount-pamt;
         var downPayment = pamt*(20/100);
         this.setState({
             emi:emi,
             showEligibleForEmi:true,
             showNotEligibleForEmi:false
         })
    }else{
         this.setState({
             showEligibleForEmi:false,
             showNotEligibleForEmi:true
         })
    }
  },
  render:function(){
       return(
       	       <div className="submit-property-section">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <ul className="breadcrumb">
                                <li><a href="#">Home</a>
                                </li>
                                <li className="active">submit a property</li>
                            </ul>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-sm-12">
                            <form className="submit-property-form form-horizontal">
                                <h2 className="text-center">Wynvent Tools.</h2>
                                <h5 className="text-center">Help you win the battle with your mind.</h5>
                            </form>
                        </div>
                    </div>

                    <hr />
                    <div className="row loan-wrapper">


                        <div className="col-sm-3">

                            <ul className="nav nav-tabs responsive-tabs tabs-left sideways" ref="responsiveTabs">
                                <li className="active"><a data-target="#home1" data-toggle="tab">LOAN CALCULATOR</a>
                                </li>
                                <li><a data-target="#profile1" data-toggle="tab">PROPERTY WORTH</a>
                                </li>
                                <li><a data-target="#messages1" data-toggle="tab">COMPARE LOCALITIES</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-sm-9">
                            <div className="tab-content">
                                <div className="tab-pane active" id="home1">

                                    <div className="row">
                                        <div className="col-md-6 col-sm-12">
                                            <h2>LOAN CALCULATOR</h2>
                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1" >Required Amount <span style={{'color': 'red'}}>*</span></label>
                                                    <input type="number" className="form-control" ref='requiredAmount' onChange={this.handleChange.bind(this,"requiredAmount")} placeholder='Rs.'/>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Net Income per month (Exculde LTA and medical allowance)<span style={{'color': 'red'}}>*</span></label>
                                                    <input type="number" className="form-control" ref='netIncome'  onChange={this.handleChange.bind(this,"netIncome")} placeholder='Rs.'/>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Existing loan commitments<span style={{'color': 'red'}}>*</span></label>
                                                    <input type="number" className="form-control" ref='existingLoanCommitment' onChange={this.handleChange.bind(this,"existingLoanCommitment")} placeholder='Rs.'/>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Loan Tenure<span style={{'color': 'red'}}>*</span></label>
                                                    <input type="number" className="form-control" ref='loanTenure' onChange={this.handleChange.bind(this,"loanTenure")} placeholder="Months"/>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Interest Rate<span style={{'color': 'red'}}>*</span></label>
                                                    <input type="number" className="form-control" ref='interestRate'  onChange={this.handleChange.bind(this,"interestRate")}/>
                                                </div>
                                                <div className="form-group">
                                                    <button  type="buttton"  className="btn btn-red btn-block" onClick={this.checkLoanEligibility}>CHECK ELIGIBILITY</button>
                                                </div>
                                        </div>
                                          <div className="col-md-6 col-sm-12 text-center">
                                            {this.state.showNotEligibleForEmi && <div className="calculator-message not-eligible-message">
                                                <div className="lnr lnr-cross cal-error"></div>
                                                <h3 className='head-pading'>You are not eligible for this loan.</h3>
                                                <br/>
                                                <p>You are Eligible for a maximum loan of 15.13 Lac at EMI 20,000</p>
                                            </div>}
                                            {this.state.showEligibleForEmi && <div className="calculator-message eligible-message">
                                                <div className="lnr lnr-checkmark-circle cal-success"></div>{"\n"}
                                                <h3 className='head-pading' >You are eligible for this loan.</h3>
                                                <p> INR {this.state.loanCalculator.requiredAmount} at EMI {this.state.emi}</p>
                                                <br />
                                                <p>You are Eligible for a maximum loan of {this.state.loanCalculator.requiredAmount} at EMI {this.state.emi}</p>
                                                <Link to="property" className="btn btn-default calculater-btn"  role="button">VIEW PROPERTY</Link>
                                            </div>}

                                        </div>
                                    </div>
                                </div>

                                <div className="tab-pane" id="profile1">

                                    <div className="row">
                                        <div className="col-md-6 col-sm-12">
                                            {/*<h2>PROPERTY WORTH</h2>*/}
                                             <span>Coming Soon</span>
                                            {/*<form>
                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Locality of the property</label>
                                                    <input type="text" className="form-control" id="" placeholder="" />
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Area ( in Sq Ft)</label>
                                                    <input type="text" className="form-control" id="" placeholder="" />
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">No. of Rooms</label>
                                                    <div className="row">
                                                        <div className="col-md-8 col-sm-8  form-group">
                                                            <input type="text" className="form-control" id="" placeholder="" />
                                                        </div>
                                                        <div className="col-md-4 col-sm-4">
                                                            <b>BHK</b>
                                                        </div>
                                                    </div>
                                                </div>



                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Furnished Status</label>
                                                    <a className="btn btn-default btn-select">
                                                        <input type="hidden" className="btn-select-input" id="" name="" value="" />
                                                        <span className="btn-select-value">Date</span>
                                                        <span className="btn-select-arrow glyphicon glyphicon-chevron-down"></span>
                                                        <ul style={{display: "none"}}>
                                                            <li>Non- Furnished</li>

                                                            <li>Semi - Furnished</li>
                                                            <li>Fully - Furnished</li>
                                                        </ul>
                                                    </a>
                                                </div>

                                                <div className="form-group">
                                                    <button className="btn btn-red btn-block">CHECK WORTH</button>
                                                </div>
                                            </form>*/}
                                        </div>
                                        {/*<div className="col-md-5 col-sm-12 text-center">
                                            <div className="calculator-message">

                                                <p>Similar properties in your area are worth.</p>
                                                <h3>INR 75 lac - 92.83 Lac</h3>
                                                <br />
                                                <p>Rent to be expected.</p>
                                                <h3>INR 20,000 - 24,000</h3>
                                                <br />
                                                <a className="btn btn-default calculater-btn" href="#" role="button">SUBMIT PROPERTY</a>

                                            </div>
                                        </div>*/}
                                    </div>
                                </div>

                                <div className="tab-pane" id="messages1">

                                    <div className="row">
                                        <div className="col-md-6 col-sm-12">
                                            {/*<h2>COMPARE LOCALITIES</h2>*/}
                                            <span>Coming Soon</span>
                                            {/*<form>
                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Enter Locality</label>
                                                    <input type="text" className="form-control" id="" placeholder="" />
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Enter another Locality</label>
                                                    <input type="text" className="form-control" id="" placeholder="" />
                                                </div>

                                                <div className="form-group">
                                                    <a href="#" className="cal-add-more">+ Add more</a>
                                                </div>


                                                <div className="form-group">
                                                    <button className="btn btn-red btn-block">COMPARE</button>
                                                </div>
                                            </form>*/}
                                        </div>
                                        {/*<div className="col-md-5 col-sm-12 text-center">
                                            <div className="calculator-message">

                                                <ul className="compare-list">
                                                    <li><i className="lnr lnr-checkmark-circle"></i>Find which is better to invest</li>
                                                    <li><i className="lnr lnr-checkmark-circle"></i>Localities with better ROI</li>
                                                    <li><i className="lnr lnr-checkmark-circle"></i>Localities with good rent</li>
                                                    <li><i className="lnr lnr-checkmark-circle"></i>Compare facilities</li>
                                                </ul>


                                            </div>
                                        </div>*/}
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
              </div>

       )
  }
});
module.exports = LoanCalculator;
