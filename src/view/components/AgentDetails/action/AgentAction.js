import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/AgentDetails/constants/PropertyPulseConstants';
import config from 'utils/config';
import axios from 'axios';
var AgentAction = function(){

}

const agentApi = axios.create({
  withCredentials: true,
});
const sessionCount = axios.create({
  withCredentials: true,
});

var viewCountApi



function getJSessionId(){
        var jsId = document.cookie
        if(jsId != null) {
            if (jsId instanceof Array)
                jsId = jsId[0].substring(11);
            else
                jsId = jsId.substring(11);
        }
        return jsId;
    }


AgentAction.prototype = {
	agentList:function(params){
        AppDispatcher.dispatch({
	        actionTsype: Constants.LOADING_RESPONSE_RECEIVED,
	        data: "loading"
		});
		agentApi.get(config.server + 'api/v1/agent/'+params+"/")
			.then(function (response) {
        makeClick(response.data.slug)
		        AppDispatcher.dispatch({
			        actionType: Constants.AGENT_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}
function makeClick(slug){
  sessionCount.get(config.server +'api/v1/session-status')
    .then(function (response) {
      viewCountApi = axios.create({
        withCredentials: true,
      
      });
    }).then(function(){
      viewCountApi.get(config.server +'core/data/process/?type=agent&action=c_ct&slug='+slug)
    })
}




module.exports = new AgentAction();
