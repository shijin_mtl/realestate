import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/BuilderSignup/constants/BuilderConstants';
import config from 'utils/config';
import axios from 'axios';
import SignUpStore from 'components/SignUp/store/SignUpStore';


var BuilderRegister = function(){

}

const builderApi = axios.create({
  withCredentials: true,
});

BuilderRegister.prototype = {

	expert:function(params){
    var data = new FormData();
    params.city = (params.city).split(",")
    for(var i in params){
      data.append(i,params[i]);
    }

    var signUpResp = SignUpStore.getResponse();
    localStorage.tempId=signUpResp.data?signUpResp.data.id:null
    if(localStorage.tempId == "null")
    {
      window.location = "/#/signup";
    }
    else {
      data.append('user_builder',signUpResp.data?signUpResp.data.id:localStorage.tempId)
  		builderApi.post(config.server + 'api/v1/builder/',data)
  			.then(function (response) {
  		        AppDispatcher.dispatch({
  			        actionType: Constants.BUILDER_RESPONSE_RECEIVED,
  			        data: response
  				});
  			})
  			.catch(function (error) {
  			    console.log("error...",error);
              });
    }


	}
}

module.exports = new BuilderRegister();
