import React from 'react';
import { Checkbox } from 'antd';
import UIAutocomplete from 'react-ui-autocomplete';
import ReactDOM from "react-dom";
import ListStore from 'components/Home/store/ListStore';
import localAction from 'components/Home/action/fetchLocal';
import LocatoinChangeStore from 'components/Header/store/LocationChangeStore';



var PropertyBanner = React.createClass({
    getInitialState:function(){
     return{
     	typeObj:{residential_apartment:false,residential_villa:false},
      tinyList:[],
      suggestList:[]
     }
   },
   handleClick:function(value){
     this.props.changeType(value);
   },
   onSearch:function(){
     this.props.onSearch();
   },
   componentDidMount:function(){
      localStorage.latitude = ''
      localStorage.longitude = ''

      $('.ui-autocomplete input').attr('placeholder',"LOCATION/AREA");
      var propsList = this.props.checkedFilters
      var propertyType = propsList['typeListArr']
      if (propertyType instanceof Array)
      {
         ;
      }
      else {
        propertyType = [propertyType]
      }
      if (propertyType.indexOf("residential_apartment") > -1) {
        ReactDOM.findDOMNode(this.refs.residential_apartment).defaultChecked = true
      }
      if (propertyType.indexOf("residential_villa") > -1) {
        ReactDOM.findDOMNode(this.refs.residential_villa).defaultChecked = true
      }
      if (propertyType.indexOf("commercial_showrrom") > -1) {
        ReactDOM.findDOMNode(this.refs.commercial_showrrom).defaultChecked = true
      }
      if (propertyType.indexOf("commercial_shop") > -1) {
        ReactDOM.findDOMNode(this.refs.commercial_shop).defaultChecked = true
      }

   },

   onChange:function(event,newValue,optionValue){
    this.props.changeBudget('locality',optionValue.id);
  },
   changeType:function(value,type){
   this.props.onChange(value);
          ReactDOM.findDOMNode(this.refs.BtnProperty).click()
   },

   componentWillMount:function(){
     ListStore.bind(this.autoList)
     localAction.getList()
     LocatoinChangeStore.bind(this.LocationChangeData)
   },
   componentWillUnmount:function(){
     ListStore.unbind(this.autoList)
     LocatoinChangeStore.unbind(this.LocationChangeData)
   },
   LocationChangeData:function(){
  	 setTimeout(function(){
  			localAction.getList()
  		}, 500);
   },

   autoList:function(){
     var list = ListStore.getResponse()
     this.state.suggestList = list
     this.setState({
       suggestList:this.state.suggestList
     })
     var tinylist = []
     for(let i =0;i<list.length;i++){
       tinylist.push(list[i].name)
     }
     this.state.tinyList = tinylist
     this.setState({
       tinyList:this.state.tinyList
     })
     const getOptions = () => (
        this.state.suggestList
      )
   },

   handleChange:function(key,event)
   {

     try {
      var  value = event.target.value
     } catch (e) {

     } finally {

     }
     if(key == 'min_budget')
     {
       if (this.props.checkedFilters.max_budget != "" && value >= this.props.checkedFilters.max_budget)
       {
         this.props.checkedFilters.max_budget = ''
         this.props.checkedFilters.min_budget = value
       }
       else
       {
         this.props.checkedFilters.min_budget = value
       }
     }
     else {

       if (this.props.checkedFilters.min_budget != "" && value <= this.props.checkedFilters.min_budget)
       {
         this.props.checkedFilters.min_budget = ''
         this.props.checkedFilters.max_budget = value

       }
       else
       {
         this.props.checkedFilters.max_budget = value
       }
     }

     this.props.changeBudget(key,event.target.value);
   },
   handleBudget:function(key,value,tag){

     this.props.changeBudget(key,value)
     ReactDOM.findDOMNode(this.refs.BtnBudget).click()
     if(key == 'min_budget')
     {
       if (this.props.checkedFilters.max_budget != "" && value >= this.props.checkedFilters.max_budget)
       {
         this.props.checkedFilters.max_budget = ''
         this.props.checkedFilters.min_budget = value
       }
       else
       {
         this.props.checkedFilters.min_budget = value
       }
       this.props.changeBudget(key,value)
     }
     else {
       if (this.props.checkedFilters.min_budget != "" && value <= this.props.checkedFilters.min_budget)
       {
         this.props.checkedFilters.min_budget = ''
         this.props.checkedFilters.max_budget = value
       }
       else
       {
         this.props.checkedFilters.max_budget = value
       }
       this.props.changeBudget(key,value)
     }
   },



   render:function(){
   	var propertForObj = {sale:"Sale",rent:"Rent"};
   	return(
   		    <section className="hm-search listingbnr">
				  <div className="container">
				    <h2>properties in your area</h2>
				    <div className="row hm-srchbox">
				      <div className="col-sm-2 col-xs-6 srch-sepr-hm purp-hm dropdown">
				        <button type="button" className="btn dropdown-toggle srch-drop-btn" data-toggle="dropdown" aria-expanded="true"><span>{propertForObj[this.props.filterParams.property_for]}</span></button>
				          <ul className="dropdown-menu">
				            <li onClick={this.handleClick.bind(this,"sale")}><a href="javascript:void(0)" >Sale</a></li>
				            <li onClick={this.handleClick.bind(this,"rent")}><a href="javascript:void(0)" data-value="Rent">Rent</a></li>
				          </ul>
				      </div>
				      <div className="col-sm-2 col-xs-6 srch-sepr-hm prop-typ-hm dropdown">
				                <button type="button" className="btn dropdown-toggle srch-drop-btn" data-toggle="dropdown" aria-expanded="true" ref='BtnProperty'>
				                  <span>Property Type</span>
				                </button>
				                <div className="dropdown-menu" id="proptypedrop">
				                  <h3>Residential</h3>
				                  <div className="row">
				                    <div className="col-sm-4">
				                        <div className="checkbox checkbox-primary" >
		                                    <input id="rm1" type="checkbox" ref='residential_apartment'/>
		                                    <label htmlFor="rm1" onClick={this.changeType.bind(this,"residential_apartment")}>Apartment</label>
                                        </div>
				                    </div>
				                    <div className="col-sm-4">
				                      <div className="checkbox checkbox-primary" >
		                                    <input id="rm2" type="checkbox" ref='residential_villa'/>
		                                    <label htmlFor="rm2" onClick={this.changeType.bind(this,"residential_villa")}>Villa</label>
                                        </div>
				                    </div>

				                  </div>
				                  <h3>Commercial</h3>
				                  <div className="row">
				                    <div className="col-sm-4">
				                      <div className="checkbox checkbox-primary" >
				                        <input id="rm3"  type="checkbox" onClick={this.changeType.bind(this,"commercial_showrrom")} ref='commercial_showrrom'/>
				                        <label htmlFor="rm3">Showroom</label>
				                      </div>
				                    </div>
				                    <div className="col-sm-4">
				                      <div className="checkbox checkbox-primary" >
				                        <input id="rm4" type="checkbox" onClick={this.changeType.bind(this,"commercial_shop")} ref='commercial_shop'/>
				                        <label  htmlFor="rm4">Shops</label>
				                      </div>
				                    </div>

				                  </div>
				                </div>
				      </div>
				      <div className="col-sm-3 col-xs-12 srch-sepr-hm loc-srch-hm">
				            {/*<UIAutocomplete
							    options={this.props.autoCompeteLocationList}
							    value={this.props.filterParams.location}
							    onChange={this.handleChange.bind(this,"location")} ref='locationPlace'
                            />*/}

                <UIAutocomplete
                    options={this.state.suggestList}
                    optionValue="id"
                    optionFilter={['name']}
                    optionLabelRender={option => `${option.name}`}
                    onChange={this.onChange}
                />



                 {/*<input className='auto-class form-control' id="place-pin" placeholder="LOCATION/AREA" onFocus={this.geolocate} type="text"/>*/}
				      </div>
				      <div className="col-sm-3 col-xs-6 srch-sepr-hm budg-srch-hm dropdown">
				        <button type="button" className="btn dropdown-toggle srch-drop-btn" data-toggle="dropdown" aria-expanded="true" ref='BtnBudget'><span>Budget</span></button>
				        <div className="dropdown-menu" id="budget-hmdrop">
				          <div className="budgsepline"></div>
				          <div className="row">
				            <div className="col-xs-6">
				              <input type="text" className="buginp" placeholder="Min" value={this.props.checkedFilters.min_budget?this.props.checkedFilters.min_budget:""}  onChange={this.handleChange.bind(this,"min_budget")} ref='min_budget' />
				              <ul className='ascoll'>
                        <li><a href="javascript:;">Min</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",50000,"budget")}>50000</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",100000,"budget")}>1 Lac</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",500000,"budget")}>5 lac</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",1000000,"budget")}>10 Lac</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",1500000,"budget")}>15 Lac</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",2000000,"budget")}>20 lac</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",3000000,"budget")}>30 Lac</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",4000000,"budget")}>40 Lac</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",5000000,"budget")}>50 Lac</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",10000000,"budget")}>1 Cr</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",20000000,"budget")}>2 Cr</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",30000000,"budget")}>3 Cr</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",40000000,"budget")}>4 Cr</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",50000000,"budget")}>5 Cr</a>
                        </li>
                        <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"min_budget",100000000,"budget")}>10 Cr</a>
                        </li>
				              </ul>
				            </div>
				            <div className="col-xs-6">
				              <input type="text" className="buginp" placeholder="Max"  value={this.props.checkedFilters.max_budget?this.props.checkedFilters.max_budget:''}   onChange={this.handleChange.bind(this,"max_budget")} ref='max_budget'/>
				              <ul className='ascoll'>
                      <li><a href="javascript:;">Max</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",50000,"budget")}>50000</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",100000,"budget")}>1 Lac</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",500000,"budget")}>5 lac</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",1000000,"budget")}>10 Lac</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",1500000,"budget")}>15 Lac</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",2000000,"budget")}>20 lac</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",3000000,"budget")}>30 Lac</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",4000000,"budget")}>40 Lac</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",5000000,"budget")}>50 Lac</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",10000000,"budget")}>1 Cr</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",20000000,"budget")}>2 Cr</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",30000000,"budget")}>3 Cr</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",40000000,"budget")}>4 Cr</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",50000000,"budget")}>5 Cr</a>
                      </li>
                      <li><a href="javascript:;" onClick={this.handleBudget.bind(this,"max_budget",100000000,"budget")}>10 Cr</a>
                      </li>
				              </ul>
				            </div>
				          </div>
				        </div>
				      </div>
				      <div className="col-sm-2 col-xs-6 srchbtn-hm">
				        <button className="btn btn-red"  onClick={this.onSearch}><i className="fa fa-search" aria-hidden="true"></i> search</button>
				      </div>
				    </div>
				  </div>
            </section>
   	)
   }
});
module.exports = PropertyBanner;
