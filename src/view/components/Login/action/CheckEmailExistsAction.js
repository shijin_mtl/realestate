import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Login/constants/LoginConstant';
import config from 'utils/config';
import axios from 'axios';
import AuthService from 'services/AuthService';
var checkEmailExists = function(){

}

checkEmailExists.prototype = {
    emailExists:function(parameters){
    		axios.get(config.server  + 'api/v1/check-if-email-exists/?email='+parameters.email)
		    .then(function (response) {
		    	if(response.data['account-exists']){
		    	  AppDispatcher.dispatch({
			         actionType: Constants.EMAIL_EXISTS_RESPONSE_RECEIVED,
			         data: response
				  });
		    	}
		    	else{
                    AppDispatcher.dispatch({
				        actionType: Constants.EMAIL_EXISTS_ERROR_RESPONSE_RECEIVED,
				        data: response
				    });
		    	} 
		    	
		    })
		    .catch(function (error){
               
		    })
    }	

}


module.exports = new checkEmailExists();
