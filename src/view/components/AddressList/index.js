
import React from 'react';
import {Link} from 'react-router';
import ReactDOM from "react-dom";
import {Modal} from 'antd';
import AddressStore from 'components/AddressList/store/AddressStore';
import AddressAction from 'components/AddressList/action/AddressAction';
import PaymentStore from 'components/AddressList/store/PaymentStore';


var AddressList = React.createClass({
  getInitialState:function(){
     return{
              addressList:[],
              htmlData:"",
              newAddress:""
     }
  },
  componentWillMount:function(){
      window.scrollTo(0,0);
      AddressStore.bind(this.addressListReceived)
      PaymentStore.bind(this.paymentResponse)
      AddressAction.getList()
   },
   componentWillUnmount:function(){
     AddressStore.unbind(this.addressListReceived)
     PaymentStore.unbind(this.paymentResponse)
    },
    addressListReceived:function(){
      var list = AddressStore.getResponse()
      for(let i=0;i<list.data.length;i++){
        this.state.addressList.push(<span key={i}><p>{list.data[i].billing_address}</p><button onClick={this.useThis.bind(this,list.data[i].billing_address)} className="btn btn-default calculater-btn"  role="button">USE ADDRESS</button></span>)
      }

      this.setState({
        addressList:this.state.addressList
      })
    },
    useThis:function(data){
      this.payAmount(data)
    },
    paymentResponse:function(){
      var response = PaymentStore.getResponse()
      $('#output').html(response.data)
      this.state.htmlData = $(response.data)
      this.setState({
        htmlData:this.state.htmlData
      })
    },
    payAmount:function(address){
      localStorage.address = address
      AddressAction.getListPost(address)
    },
    handleChange: function(event) {
    this.state.newAddress = event.target.value
      this.setState({
        newAddress:this.state.newAddress
      })

     },
     useNew:function() {
       this.payAmount(this.state.newAddress)
     },
  render:function(){
       return(
       	       <div className="submit-property-section">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <ul className="breadcrumb">
                                <li><a href="#">Home</a>
                                </li>
                                <li className="active">submit a property</li>
                            </ul>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12">
                            <form className="submit-property-form form-horizontal">
                                <h2 className="text-center">Wynvent Tools.</h2>
                                <h5 className="text-center">Help you win the battle with your mind.</h5>
                            </form>
                        </div>
                    </div>
                    <hr />
                    <div className="row loan-wrapper">
                        <div className="col-sm-3">
                        <ul className="nav nav-tabs responsive-tabs tabs-left sideways" ref="responsiveTabs">
                            <li className="active"><a data-target="#home1" data-toggle="tab">FOR OWNERS</a>
                            </li>
                        </ul>
                        </div>
                        <div className="col-sm-9">
                            <div className="tab-content">
                                <div className="tab-pane active" id="home1">

                                    <div className="row">
                                        <div className="col-md-6 col-sm-12">
                                            <h2>ADDRESS</h2>
                                            <div className="form-group">
                                                <label htmlFor="exampleInputEmail1" >Provide Your Address Here</label>
                                                <textarea  className="form-control" id="address_new" rows="8" ref='requiredAmount' onChange={this.handleChange}>{this.state.newAddress}</textarea>
                                            </div>
                                            <div className="form-group">
                                                <button  type="buttton"  className="btn btn-red btn-block" onClick={this.useNew}>USE ADDRESS</button>
                                            </div>
                                        </div>
                                          <div className="col-md-6 col-sm-12 text-center">
                                            <div className="calculator-message eligible-message">
                                                {this.state.addressList=![] && <h3 className='head-pading' >Or you can use existing address</h3>}
                                                {this.state.addressList}
                                            </div>
                                            <div id="output"></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-pane" id="profile1">
                                    <div className="row">
                                        <div className="col-md-6 col-sm-12">
                                            {/*<h2>PROPERTY WORTH</h2>*/}
                                             <span>Coming Soon</span>
                                            {/*<form>
                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Locality of the property</label>
                                                    <input type="text" className="form-control" id="" placeholder="" />
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Area ( in Sq Ft)</label>
                                                    <input type="text" className="form-control" id="" placeholder="" />
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">No. of Rooms</label>
                                                    <div className="row">
                                                        <div className="col-md-8 col-sm-8  form-group">
                                                            <input type="text" className="form-control" id="" placeholder="" />
                                                        </div>
                                                        <div className="col-md-4 col-sm-4">
                                                            <b>BHK</b>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Furnished Status</label>
                                                    <a className="btn btn-default btn-select">
                                                        <input type="hidden" className="btn-select-input" id="" name="" value="" />
                                                        <span className="btn-select-value">Date</span>
                                                        <span className="btn-select-arrow glyphicon glyphicon-chevron-down"></span>
                                                        <ul style={{display: "none"}}>
                                                            <li>Non- Furnished</li>

                                                            <li>Semi - Furnished</li>
                                                            <li>Fully - Furnished</li>
                                                        </ul>
                                                    </a>
                                                </div>

                                                <div className="form-group">
                                                    <button className="btn btn-red btn-block">CHECK WORTH</button>
                                                </div>
                                            </form>*/}
                                        </div>
                                        {/*<div className="col-md-5 col-sm-12 text-center">
                                            <div className="calculator-message">

                                                <p>Similar properties in your area are worth.</p>
                                                <h3>INR 75 lac - 92.83 Lac</h3>
                                                <br />
                                                <p>Rent to be expected.</p>
                                                <h3>INR 20,000 - 24,000</h3>
                                                <br />
                                                <a className="btn btn-default calculater-btn" href="#" role="button">SUBMIT PROPERTY</a>

                                            </div>
                                        </div>*/}
                                    </div>
                                </div>

                                <div className="tab-pane" id="messages1">

                                    <div className="row">
                                        <div className="col-md-6 col-sm-12">
                                            {/*<h2>COMPARE LOCALITIES</h2>*/}
                                            <span>Coming Soon</span>
                                            {/*<form>
                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Enter Locality</label>
                                                    <input type="text" className="form-control" id="" placeholder="" />
                                                </div>

                                                <div className="form-group">
                                                    <label htmlFor="exampleInputEmail1">Enter another Locality</label>
                                                    <input type="text" className="form-control" id="" placeholder="" />
                                                </div>

                                                <div className="form-group">
                                                    <a href="#" className="cal-add-more">+ Add more</a>
                                                </div>


                                                <div className="form-group">
                                                    <button className="btn btn-red btn-block">COMPARE</button>
                                                </div>
                                            </form>*/}
                                        </div>
                                        {/*<div className="col-md-5 col-sm-12 text-center">
                                            <div className="calculator-message">

                                                <ul className="compare-list">
                                                    <li><i className="lnr lnr-checkmark-circle"></i>Find which is better to invest</li>
                                                    <li><i className="lnr lnr-checkmark-circle"></i>Localities with better ROI</li>
                                                    <li><i className="lnr lnr-checkmark-circle"></i>Localities with good rent</li>
                                                    <li><i className="lnr lnr-checkmark-circle"></i>Compare facilities</li>
                                                </ul>


                                            </div>
                                        </div>*/}
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
              </div>

       )
  }
});
module.exports = AddressList;
