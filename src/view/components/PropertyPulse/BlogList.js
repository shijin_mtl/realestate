import React from 'react';
import Banner from './Banner'
import BackGroundImage from 'public/images/no-photo.jpg';
import TextTruncate from 'react-text-truncate';
import moment from 'moment';
import {hashHistory} from 'react-router';




var BlogList = React.createClass({
  goToDetail:function(id){
    hashHistory.push({
      pathname:"/blog-detail",
      query:{id:id}
    })
  },
   componentWillMount:function(){
     $(".side-collapse").addClass('in');
   },
   render:function(){
     var createdTime =  moment(this.props.data.creation_date).format("Do MMM YY");
     console.log(this.props);
   	 return(
       <div className="col-sm-4">
           <div className="blogbox-list match-item">
               <div className="blg-catgr">{this.props.data.categories.title}</div>
               <a onClick={this.goToDetail.bind(this,this.props.data.id)}><div className="blog-thumb" style={{backgroundImage: `url(${this.props.data.image?this.props.data.image:BackGroundImage})`}}></div></a>
               <div className="bloginfobox">
                 <h2><a onClick={this.goToDetail.bind(this,this.props.data.id)}>
                 <TextTruncate
                           line={2}
                           truncateText=""
                           text={this.props.data.title}
                           textTruncateChild={<span >...</span>
                           }
                       />
                  </a></h2>
                 <div className="blog-shrtinf">

                 <TextTruncate
                           line={2}
                           truncateText=""
                           text={this.props.data.lead}
                           textTruncateChild={<span onClick={this.goToDetail.bind(this,this.props.data.id)} >...</span>}
                       />
                       </div><br/>
                 <h5 className="row"><span className="pull-left col-sm-8">{this.props.data.author.first_name ?"Posted by":""} {this.props.data.author.first_name} {' '} {this.props.data.author.last_name}</span><span className="pull-right col-sm-4 dtblg">{createdTime}</span><br className="clearfix"/></h5>
               </div>
           </div>
       </div>

   	 )
   }
});
module.exports = BlogList;
