import React from 'react';
var UserAuthSideBar = React.createClass({
	render:function(){
		return(
			    <div className="col-sm-6 col-md-5 pull-left regtabwrap">
				        <div className="regtabbox">
				          <h2>WHY CREATE AN ACCOUNT WITH US?</h2>
				          <ul className="nav nav-tabs" role="tablist">
				            <li className="active"><a href="#buyerowner-tab" aria-controls="buyerowner-tab" role="tab" data-toggle="tab">BUYER/OWNER</a></li>
				            <li><a href="#agent-tab" aria-controls="agent-tab" role="tab" data-toggle="tab">AGENT</a></li>
				            <li><a href="#builder-tab" aria-controls="builder-tab" role="tab" data-toggle="tab">BUILDER</a></li>
				          </ul>
				          <div className="tab-content">
				            <div role="tabpanel" className="tab-pane fade in active" id="buyerowner-tab">
				                <ul>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Lacs of properties to pick from</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">50k+ buyers</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Feature comes here</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Feature comes here</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Feature comes here</span></li>
				                </ul>
				            </div>
				            <div role="tabpanel" className="tab-pane fade" id="agent-tab">
				                <ul>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Lacs of properties to pick from</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">50k+ buyers</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Feature comes here</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Feature comes here</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Feature comes here</span></li>
				                </ul>
				            </div>
				            <div role="tabpanel" className="tab-pane fade" id="builder-tab">
				                <ul>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Lacs of properties to pick from</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">50k+ buyers</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Feature comes here</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Feature comes here</span></li>
				                  <li><span className="lnr lnr-checkmark-circle"></span><span className="specinfo">Feature comes here</span></li>
				                </ul>
				            </div>
				          </div>
				        </div>
      				</div>
		)
	}
});
module.exports = UserAuthSideBar;