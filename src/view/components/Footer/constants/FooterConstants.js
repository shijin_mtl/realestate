var keyMirror = require('keymirror');

module.exports = keyMirror({
 FEEDBACK_RESPONSE_RECIEVED: null,
 SUBSCRIBE_RESPONSE_RECEIVED:null,
 FOOTER_LOCATION_CHANGE:null
});
