import React from 'react';
import 'public/js/slick.min.js';
var Testimonails = React.createClass({
    componentDidMount:function(){
        $('.testislide').slick({
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  arrows: false,
		  autoplaySpeed: 2000,
		  dots:true,
		  responsive: [
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		  ]
        });
    },
	render:function(){
		return(
			    <section className="testihm-wrap">
					<div className="container">
					    <h2>Testimonials <span className="ttl-bdr"></span></h2>
					    <div className="testislide">

					      <div>
					        <div className="testi-item" >
					          <div className="testi-thumb pull-left"><img src="images/testi-icon.jpg" className="img-responsive img-circle"/></div>
					          <h3 className="pull-left">Sudeep Menon</h3>
					          <div className="clearfix"></div>
					          <p>Thank you for helping me find my dream apartment and that too without the hassle of brokerage and agents.</p>
					        </div>
					      </div>
					      <div>
					        <div className="testi-item"  >
					          <div className="testi-thumb pull-left"><img src="images/testi-icon.jpg" className="img-responsive img-circle"/></div>
					          <h3 className="pull-left">Balkrishnan Iyer</h3>
					          <div className="clearfix"></div>
					          <p>Unbelievable no. of options in the area I was looking for and yet so easy to narrow down on the one I wanted.</p>
					        </div>
					      </div>
					      <div>
					        <div className="testi-item" >
					          <div className="testi-thumb pull-left"><img src="images/testi-icon.jpg" className="img-responsive img-circle"/></div>
					          <h3 className="pull-left">Priya Singhania</h3>
					          <div className="clearfix"></div>
					          <p>My wife and I were looking for that perfect quiet place for a retirement home. Wynvent helped.</p>
					        </div>
					      </div>
					      <div>
					        <div className="testi-item" >
					          <div className="testi-thumb pull-left"><img src="images/testi-icon.jpg" className="img-responsive img-circle" /></div>
					          <h3 className="pull-left">Balkrishnan Iyer</h3>
					          <div className="clearfix"></div>
					          <p>Thank you for helping me find my dream apartment and that too without the hassle of brokerage and agents.</p>
					        </div>
					      </div>
					    </div>

		            </div>
                </section>
		)
	}
});
module.exports = Testimonails;
