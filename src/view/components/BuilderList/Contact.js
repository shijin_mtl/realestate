import React from 'react';
import { Form, FormGroup,TextArea, Validator, Input,required,minLength,maxLength,pattern,email,custom} from 'romagny13-react-form-validation';
import TalktoExpertAction from 'components/BuilderList/action/TalktoExpertAction';
import TalktoExpertStore from 'components/BuilderList/store/TalktoExpertStore';
import config from 'utils/config';

const validators = {
    'name': [required('Name is Required'), minLength(3),maxLength(30),pattern(/^[a-zA-Z']+(\s.?[a-zA-Z']+){0,4}$/, '3 to 30 characters no symbols and numbers ')],
    'phone':[required('Phone is Required'), minLength(3),maxLength(15),pattern(/^[0-9 +]{10,15}$/, 'Invalid Phone')],
};
var MyFormGroup = function (props) {
    var groupclassName = props.hasError ? 'form-group has-error' : 'form-group';
    return (
            <div className={groupclassName}>
                {props.children}
                {props.hasError && <span className="help-block">{props.error}</span>}
            </div>
    );
};

var Contact = React.createClass({
  getInitialState:function(){
      return{
              model:{
              	     name:"",
              	     email:"",
              	     mobile:"",
              	     message:""
              },
              btnOnclick:false,
      }
	},
  componentWillMount:function(){
      TalktoExpertStore.bind(this.expertResponseReceived);
      $(".side-collapse").addClass('in');
  },
  componentWillUnmount:function(){
      TalktoExpertStore.unbind(this.expertResponseReceived);
  },
  expertResponseReceived:function(){
      var expertResponse = TalktoExpertStore.getResponse();
      setTimeout(function(){
        document.getElementById('name').value = "";
        document.getElementById('mobile').value = "";
        document.getElementById('email').value = "";
        document.getElementById('message').value = "";
       }, 100);
      $('#modelBtn').click()

	},
  submit:function(result){
      if(!result.hasError){
        result.model.message = this.state.model.message;
        TalktoExpertAction.expert(result.model);
      }
  },
  email:function(value){
        var atpos = value.indexOf("@");
        var dotpos = value.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=value.length || ! /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
            return{
                name: 'email',
                error: 'Invalid email address'
            }
        }
    },
    number:function(value){
      if(value.length!=10 || !/^\d+$/.test(value)){
            return{
                name:'number',
                error:"Invalid number"
            }
      }
    },
    changeMessage:function(event){
  		this.state.model.message = event.target.value;
  		this.setState({
  			 model:this.state.model
  		})
  	},

   render:function(){
     var validation = {
         email:this.email,
         number:this.number
       }
   	 return(
       <span>
       <Form onSubmit={this.submit} model={this.state.model} ref="form">
           <h4 className="sidebarttl">Talk to experts</h4>
           <p className="cnt-subtext">Leave your query here, and our  experts will get back to you.</p>
           <div className="form-group">
           <Validator validators={validators['name']}>
              <MyFormGroup>
             <Input type="text" name='name' id='name' className="form-control" value={this.state.model.name} placeholder="Name"/>
             </MyFormGroup>
           </Validator>
           </div>
           <div className="form-group">
           <Validator validators={validators['phone']}>
            <MyFormGroup>
             <Input type="tel" name='mobile' id='mobile' className="form-control"  value={this.state.model.mobile} placeholder="Mobile Number"/>
             </MyFormGroup>
           </Validator>
           </div>
           <div className="form-group">
           <Validator validators={[required( 'Email is required'),validation['email']]}>
               <MyFormGroup>
             <Input type="email" name='email' id='email' className="form-control" placeholder="Email" value={this.state.model.message} />
             </MyFormGroup>
           </Validator>
           </div>
           <div className="form-group">
             <textarea className="form-control" name='message' id='message' placeholder="Message" value={this.state.model.message} onChange={this.changeMessage}></textarea>
           </div>
           <div className="form-group cntr-algn-wrp">
             <button className="btn btn-red" type='Submit' >Submit</button>
           </div>
          </Form>

          <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtn' data-toggle="modal" data-target="#myModal">Open Modal</button>

          <div id="myModal" className="modal fade pop-show" role="dialog">
                <div className="modal-dialog">
                  <div className="modal-content">
                        <div className="modal-header">
                        <b>CONTACT</b>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
                        </div>
                        <div className="succes-msgdiv">
                             <img src={config.s3static + "images/done-icon.png"} width="85" className="icon-succ" alt="Success" />
                             <br/><br/>
                             <h4>THANK YOU!</h4>
                             <br/>
                             <h5>Your message has been recieved,We will get back to you soon.</h5>
                             <br/>
                        </div>
                        <div className="form-group text-center"><button className="btn btn-red" type="button" style={{width:"30%"}} data-dismiss="modal" aria-label="Close" >OK</button></div>
                        <br/>
                  </div>
                </div>
          </div>
          </span>
   	 )
   }
});
module.exports = Contact;
