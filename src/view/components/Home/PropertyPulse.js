import React from 'react';
import {Link} from 'react-router';
import BlogAction from 'components/Home/action/BlogAction';
import BlogStore from 'components/Home/store/BlogStore';
import TextTruncate from 'react-text-truncate';
import {hashHistory} from 'react-router';
import moment from 'moment';







var PropertyPulse = React.createClass({
	getInitialState:function(){
		 return{
						blogList:[]
		 }
	},
	goToDetail:function(id){
    hashHistory.push({
      pathname:"/blog-detail",
      query:{id:id}
    })
  },
	componentWillMount:function(){
    BlogStore.bind(this.blogListRecieved)
		BlogAction.homeList()
  },
  componentWillUnmount:function(){
    BlogStore.unbind(this.blogListRecieved)
  },
	blogListRecieved:function(){
		var blogList =  BlogStore.getResponse()
		for(let i=0;i<blogList.length;i++)
		{
			var createdTime =  moment(blogList.creation_date).format("D");
			var createdMonth =  moment(blogList.creation_date).format("MMM")
			if(blogList.length>0){
			if(blogList[i].image){
				 if (i === 3)
				 { break; }
			this.state.blogList.push(
				<div className="col-sm-4 hmblog-col" key={i}>
				<div className="hmbloglist">
					<a onClick={this.goToDetail.bind(this,blogList[i].id)}><div className="hmblog-thumb" style={{backgroundImage: `url(${blogList[i].image})`}}></div></a>
					<div className="hmblog-info">
						<div className="blginrinf">
							<h3> <TextTruncate
												line={2}
												truncateText=""
												text={blogList[i].title}
												textTruncateChild={<span >...</span>
												}
										/></h3>
							<div className="trunk-div"><TextTruncate
												line={2}
												truncateText=""
												text={blogList[i].lead}
												textTruncateChild={<span >...</span>
												}
										/></div>
						</div>
						<div className="blgreadlnk"><a onClick={this.goToDetail.bind(this,blogList[i].id)}>Read more</a></div>
					</div>
					<div className="hmblog-date"><span className="mnt-ttl">{createdMonth}</span>{createdTime}</div>
				</div>
			</div>
		)}
	}



	}
		this.setState({
			blogList:this.state.blogList
		})
	},

	render:function(){

		return(
			      <section className="hm-blogwrap">
					  <div className="container">
					    <div className="row hm-blogrow">
					      <div className="col-sm-4 col-sm-offset-4 hmblog-col">
					        <h2><span className="sbttl">LATEST ON OUR BLOG</span>PROPERTY PULSE</h2>
					      </div>
					    </div>
					    <div className="row hm-blogrow">
								{this.state.blogList}
					      <div className="col-sm-12 blog-botwrp"><h4 className="backline"><Link to="property-pulse">View All</Link></h4></div>
					    </div>
					  </div>
                    </section>
		)
	}
});
module.exports = PropertyPulse;
