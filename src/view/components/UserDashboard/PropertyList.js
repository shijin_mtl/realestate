import React from 'react';
import BackGroundImage from 'public/images/no-photo.jpg';
import ErrorScenarios from 'components/ErrorScenarios';
import UserResponse from './UserResponse';
import {hashHistory,Link} from 'react-router';
import {ShareButtons,ShareCounts,generateShareIcon} from 'react-share';
import { Pagination,Progress,Icon,Popconfirm } from 'antd';
import config from 'utils/config';
import moment from 'moment';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import {Gmaps,Marker} from 'react-gmaps';

var Scroll  = require('react-scroll');
var scroll     = Scroll.animateScroll;





const {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  PinterestShareButton,
  VKShareButton,
} = ShareButtons;


const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');




var PropertyList = React.createClass({


  showShareFb:function(id){
     var ip = config.ip;
     var pathName = "propertyDetails"
     var query = id;
     var url = "staging.wynvent.com" +  '#/' + pathName +  '?id='+query;
        return(
                <FacebookShareButton
                   url={url}
                   className="fa fa-facebook" size={32} round={true}
                   />
              )

  },
  showShareTwitter:function(id){
    var ip = config.ip;
    var pathName = "propertyDetails"
    var query = id;
    var url = "staging.wynvent.com" +  '#/' + pathName +  '?id='+query;
    var title = 'http://'+url
         return(
                  <TwitterShareButton
                    url={url}
                    title={title}
                    className="fa fa-twitter"/>
         )

  },
  showShareGoogle:function(id){
    var ip = config.ip;
    var pathName = "propertyDetails"
    var query = id;
    var url = "staging.wynvent.com" +  '#/' + pathName +  '?id='+query;
         return(
                  <GooglePlusShareButton
                    url={url}

                    className="fa fa-google-plus"/>
         )

  },

   getInitialState:function(){
      return{
               showResponses:false,
               ResponseId:'',
               tab:[],
      }
   },
   showPropertyDetail:function(id){
       hashHistory.push({
         pathname:"/propertyDetails",
         query:{id:id}
       })
   },
    componentDidMount:function(){
     $('.social-user-dasbd').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots:true,
     });
   },



   showShortListedProperty:function(){
      var shortListedArr = this.props.shortListedList.results;
      var shorListedList = [];

      var objMap = {buyer_owner:"Buyer/Owner",agent:"Agent",builder:"Builder"};
        if(shortListedArr.length>0){
            for(var i in shortListedArr){
              var createdTime =  moment(shortListedArr[i].created).format("Do MMM YY");
              if(shortListedArr[i].coordinates){
                var  initList = shortListedArr[i].coordinates.split(" ")
                initList.splice(0, 1);
                initList[0] = initList[0].replace('(',"")
                initList[1] = initList[1].replace(')',"")
                var long = initList[0];
                var lat = initList[1];
            }
            if (shortListedArr[i].coordinates != null)
            {
              var  initList = (shortListedArr[i].coordinates).split(" ")
              initList.splice(0, 1);
              initList[0] = initList[0].replace('(',"")
              initList[1] = initList[1].replace(')',"")
              var lat = initList[1]
              var long = initList[0]
            }

             shorListedList.push(
                                    <div className="col-sm-4 col-md-4 pplrlistsep" key={i}>
                                        <div className="proplistbox ih-item square effect10 bottom_to_top">
                                            <a  className="propanchr">
                                                <div className="propbandred">{shortListedArr[i].property_for}</div>
                                                 {shortListedArr[i].thumbnail_url && <div className="prop-thumb" style={{backgroundImage: `url(${shortListedArr[i].thumbnail_url})`}}> </div>}
                                                 {!shortListedArr[i].thumbnail_url && <div className="prop-thumb" style={{backgroundImage: `url(${BackGroundImage})`}}> </div>}
                                                <div className="prop-infobox">
                                                    <h3>{shortListedArr[i].name}</h3>
                                                    <h4>{shortListedArr[i].locality}</h4>
                                                    <h5><span className="pull-left">Posted by {objMap[shortListedArr[i].user_type]}</span> <span className="pull-right">{createdTime}</span><br className="clearfix"/></h5>
                                                </div>
                                                <div className="prop-price">
                                                    <h2><i className="fa fa-inr" aria-hidden="true"></i>78,00,000/-</h2>
                                                    <span className="nego-ttl">Slightly Negotiable</span>
                                                </div>
                                                <div className="info">
                                                <div>
                                                <Gmaps onClick={this.showPropertyDetail.bind(this,shortListedArr[i].id)}
                                                    width={'340px'}
                                                    height={'450px'}
                                                    lat={lat}
                                                    lng={long}
                                                    zoom={12}
                                                    scrollwheel={false}
                                                    >
                                                      <Marker
                                                        lat={lat}
                                                        lng={long}/>
                                                </Gmaps>
                                                </div>
                                                    <div className="hvr-btn-map">
                                                        <button onClick={this.showPropertyDetail.bind(this,shortListedArr[i].id)} className="btn btn-red">Read more</button>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                     </div>
            )
           }


           return(shorListedList);
        }
        else{
             return(<ErrorScenarios message="No properties have been shortlisted"/>);
        }
   },
   showResponsesFn:function(id){
     this.state.ResponseId = id
     this.setState({
         ResponseId:this.state.ResponseId,
         showResponses:true
     })
   },
   hideReponses:function(){
     this.setState({
         showResponses:false
     })

   },
   handleChange:function(pageNum){
     scroll.scrollTo(150);
     this.props.onChangeResponsePageNum(pageNum);
   },
   confirm:function(id,index){
     this.props.deleteProperty(id,index);
   },
   editPropertySub:function(id){
     hashHistory.push("edit-property/"+id)
   },
   changeTab:function(data,name,event)
   {
        this.state[event.target.id] =
        <LineChart width={300} height={200} data={data}>
           <XAxis dataKey="date"/>
           <YAxis label={name} />
           <Tooltip/>
           <Line dataKey={name} stroke="#f7a74a"  activeDot={{r: 5}}/>
        </LineChart>
        this.setState({
          [event.target.id]:this.state[event.target.id],
        })
   },
   getInitial:function(data){
     var firstData = <LineChart width={300} height={200} data={data}>
        <XAxis dataKey="date"/>
        <YAxis label={"view"}/>
        <Tooltip/>
        <Line  dataKey="view" stroke="#f7a74a" activeDot={{r: 5}}/>
     </LineChart>

     return firstData
   },
   getAddress:function(id){
     localStorage.tempPropId = id
     hashHistory.push({
       pathname:"/address",
     })

   },
   showMyListings:function(){
     var  data = [
      {date: 'jan 2017', view: 1},
      {date: 'jan 2017', view: 6},
      {date: 'jan 2017', view: 4},
      {date: 'jan 2017', view: 6},

     ];

     var data2 = [
      {date: 'jan 2017', view: 6},
      {date: 'jan 2017', view: 1},
      {date: 'jan 2017', view: 2},
      {date: 'jan 2017', view: 3},

   ];




     var myList = this.props.myListings.results;
     var myListingsList = [];
     if(myList.length>0){
        for (var i = 0, len = myList.length; i < len; i++)
        {
          var date =moment(myList[i].created).add(1, 'M').format("YYYY-MM-DD")
          console.log(date);
          var click_data = myList[i].clickcount_graph_data.length != 0?myList[i].clickcount_graph_data:[{date: 'last month', click: 0}]
          var view_data = myList[i].viewcount_graph_data.length != 0?myList[i].viewcount_graph_data:[{date: 'last month', view: 0}]
          var response_data = myList[i].responsecount_graph_data.length != 0? myList[i].responsecount_graph_data:[{date: 'last month', response: 0}]
            myListingsList.push(
                                <div className="prfl-prp-lstwrp" key={i}>
                                  <a  className="property-dlt">
                                    <Popconfirm title="Are you sure delete this property?" placement="topRight"  onConfirm={this.confirm.bind(this,myList[i].id,i)} onCancel={this.cancel} okText="Yes" cancelText="No">
                                       <Icon type="delete" /> Remove
                                    </Popconfirm>
                                  </a>
                                    <div className="row">
                                        <div className="col-sm-5 profl-list-info match-item">
                                            <h3>{myList[i].name} | FOR {myList[i].property_for}</h3>
                                            <span className="subttlpst">Property Detail Completion</span>
                                            <div className="progr-barwrap">
                                                <Progress percent={myList[i].completeness_data.percentage}/>
                                                Completeness <b>{myList[i].completeness_data.percentage}%</b>
                                                <a href="javascript:;" className="dashboard-link" onClick={this.editPropertySub.bind(this,myList[i].id)}>ADD DETAILS</a>
                                            </div>
                                            <div className="estimtmrent">
                                                Estimated value   <strong>{myList[i].expected_price?"INR"+ myList[i].expected_price:'Not Provided'}</strong>
                                            </div>
                                            <div className="estimtmrent">
                                                Estimated Rent <strong>{myList[i].monthly_rent? "INR"+ myList[i].monthly_rent+"/m":'Not Provided'}</strong>
                                            </div>
                                        </div>
                                        <div className="col-sm-4 profl-view match-item">
                                        <ul className="nav nav-tabs" role="tablist">
                                                <li className="active"><a href="#profl-viewtab" aria-controls="profl-viewtab" role="tab" data-toggle="tab" id={myList[i].name} onClick={this.changeTab.bind(this,view_data,"view")}>Views</a></li>
                                                <li><a href="#profl-cnttab" aria-controls="profl-cnttab" role="tab" data-toggle="tab" id={myList[i].name} onClick={this.changeTab.bind(this,click_data,"click")}>Click</a></li>
                                                <li><a href="#profl-cnttab" aria-controls="profl-cnttab" role="tab" data-toggle="tab" id={myList[i].name} onClick={this.changeTab.bind(this,response_data,"response")}>Contact</a></li>
                                        </ul>
                                            <div className="tab-content">
                                                <div role="tabpanel" className="tab-pane fade in active" id="profl-viewtab">
                                                  {this.state[myList[i].name]?this.state[myList[i].name]:this.getInitial(view_data)}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-3 profl-social match-item">
                                            <h5>WAYS TO IMPROVE YOUR VIEWS</h5>
                                            <div className="social-boxprfl">
                                                <div className="social-user-dasbd">
                                                    <div>
                                                        <h6>Tell us why your property is worth it!</h6>
                                                        <span className="redlnkuppr">Share on </span>
                                                        <ul className="share-widget">
                                                            <li>
                                                                <a>{this.showShareFb(myList[i].id)}</a>
                                                            </li>
                                                            <li>
                                                                <a>{this.showShareTwitter(myList[i].id)}</a>
                                                            </li>
                                                            <li>
                                                                <a>{this.showShareGoogle(myList[i].id)}</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div>
                                                        <h6>Add more images of your property.</h6>
                                                        <div className="add-prop-img">
                                                            <img src="images/no-photo.jpg" width="70" height="70" className="addprothb" />
                                                            <div className="add-nwimg">
                                                                <label className="btn-bs-file btn redlnkuppr" onClick={this.editPropertySub.bind(this,myList[i].id)}>Add now
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <h6>Tell us why your property is worth it!</h6>
                                                        <p><a  className="redlnkuppr" onClick={this.editPropertySub.bind(this,myList[i].id)}>ADD FACILITIES NEARBY</a>
                                                        </p>
                                                        <p><a  className="redlnkuppr" onClick={this.editPropertySub.bind(this,myList[i].id)}>ADD AMENITIES</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr className="sepdotline" />
                                    <div className="row prfl-prp-lstbot">
                                        <div className="col-sm-3">Listing Type : {myList[i].is_premium?"Premium":"Standard"} {!myList[i].is_premium && <a onClick={this.getAddress.bind(this,myList[i].id)}className="redlnkuppr">upgrade</a>}</div>
                                        <div className="col-sm-3">Listing Status: {myList[i].is_premium==true?"Verified":"Unveified"}</div>
                                        <div className="col-sm-3">{myList[i].is_premium?"Expires on:"+date:""}</div>
                                        <div className="col-sm-3" onClick={this.showResponsesFn.bind(this,myList[i].id)}><a className="btn btn-red pull-right">view responses</a></div>
                                    </div>
                                </div>
            )
         }
             return(myListingsList);
     }
     else{
            return(<ErrorScenarios message="You haven't added any property"/>);
     }

   },
   changeShortListProperty:function(pageNum){
      this.props.changeShortListProperty(pageNum)
   },
   render:function(){
   	 return(
            <span>
     	 	    {!this.state.showResponses && <div className="profile-listing-wrap">
              <ul className="nav nav-tabs prfl-mn" role="tablist">
                  <li className="active">
                    <a href="#profl-lst" aria-controls="profl-lst" role="tab" data-toggle="tab">My Listings</a>
                  </li>
                  <li>
                    <a href="#profl-shrtlst" aria-controls="profl-shrtlst" role="tab" data-toggle="tab">Shortlisted Properties</a>
                  </li>
              </ul>
              <div className="tab-content">
                  <div role="tabpanel" className="tab-pane fade in active" id="profl-lst">

                         {this.showMyListings()}
                         <div className='row text-center'>
                         {this.props.myListings.results.length>0 && <Pagination total={this.props.myListings.count} current={parseInt(this.props.myListings.current)} onChange={this.handleChange} pageSize={3}/>}
                  </div></div>
                  <div role="tabpanel" className="tab-pane fade" id="profl-shrtlst">
                      <div className="row pplrlistrow">
                      <div className='row'>
                         {this.showShortListedProperty()}
                         </div>
                         <div className='row text-center'>
                         { this.props.shortListedList.results.length>0 &&  <Pagination pageSize={3} onChange={this.changeShortListProperty} total={this.props.shortListedList.count} current={parseInt(this.props.shortListedList.current)}/>}
                         </div>
                       </div>
                  </div>
              </div>
          </div>}
          {this.state.showResponses && <UserResponse hideReponses={this.hideReponses} myResponseList={this.props.myResponseList} responseID = {this.state.ResponseId}/>}
         </span>
   	 )
   }
});
module.exports = PropertyList;
