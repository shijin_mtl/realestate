import React from 'react';
import UIAutocomplete from 'react-ui-autocomplete';
import ReactDOM from "react-dom";
import ListStore from 'components/Home/store/ListStore';
import localAction from 'components/Home/action/fetchLocal';



var HomeScreenFilter = React.createClass({
  getInitialState:function(){
     return{
            tinyList:[],
            suggestList:[]
     }
  },
   handleClick:function(key,value,type){

     if(key == 'min_budget' && this.props.filterParams.max_budget !='')
     {
        if (value >= this.props.filterParams.max_budget)
        {
          this.props.filterParams.max_budget = ''
        }
     }
     if(key == 'max_budget'  && this.props.filterParams.min_budget !='' )
     {
       if (value <= this.props.filterParams.min_budget)
       {
         this.props.filterParams.min_budget = ''
       }
     }

     this.props.changeFilters(key,value);

     if(type == 'property'){
       ReactDOM.findDOMNode(this.refs.BtnProperty).click()
     }
     else if (type == 'budget') {
       ReactDOM.findDOMNode(this.refs.BtnBudget).click()
     }
     else {
       ;
     }
   },
   handleChange:function (value) {
   	  this.props.changeFilters("location",value);
   },
   onSearch:function(){
      this.props.onSearch();
   },
   componentWillMount:function(){
     ListStore.bind(this.autoList)
     localAction.getList()
   },
   componentWillUnmount:function(){
     ListStore.unbind(this.autoList)
   },
   autoList:function(){
     var list = ListStore.getResponse()
     this.state.suggestList = list
     this.setState({
       suggestList:this.state.suggestList
     })
     var tinylist = []
     for(let i =0;i<list.length;i++){
       tinylist.push(list[i].name)
     }
     this.state.tinyList = tinylist
     this.setState({
       tinyList:this.state.tinyList
     })

     const getOptions = () => (
        this.state.suggestList
      )

   },
   componentDidMount:function(){
     localStorage.latitude = ''
     localStorage.longitude = ''
     $('.ui-autocomplete input').attr('placeholder',"LOCATION/AREA");
   },
   valueEdit:function (type,e) {
     if(type=="min"){
       this.props.changeFilters('min_budget',e.target.value);
     }
     if(type=="max")
     {
       this.props.changeFilters('max_budget',e.target.value);
     }
   },
   changePosition:function(event){
   },

   onChange:function(event,newValue,optionValue){
     this.props.changeFilters('locality',optionValue.id);
    this.setState({
      value: newValue
    });
  },



   render:function(){
   	 var propertForObj = {sale:"Sale",rent:"Rent"};
   	 return(
   	 	    <section className="hm-search">
			    <div className="container">
			        <div className="row hm-srchbox">
			            <div className="col-sm-2 col-xs-6 srch-sepr-hm purp-hm dropdown">
			                <button type="button" className="btn dropdown-toggle srch-drop-btn" data-toggle="dropdown" aria-expanded="true">
			                  <span>{propertForObj[this.props.filterParams.property_for]}</span>
			                </button>
			                <ul className="dropdown-menu">
			                    <li onClick={this.handleClick.bind(this,"property_for","sale")}><a href="javascript:void(0)">Sale</a>
			                    </li>
			                    <li onClick={this.handleClick.bind(this,"property_for","rent")}><a href="javascript:void(0)" >Rent</a>
			                    </li>
			                </ul>
			            </div>
			            <div className="col-sm-2 col-xs-6 srch-sepr-hm prop-typ-hm dropdown">
			                <button type="button" className="btn dropdown-toggle srch-drop-btn" data-toggle="dropdown" ref='BtnProperty' aria-expanded="true">
			                    <span>Property Type</span>
			                </button>
			                <div className="dropdown-menu" id="proptypedrop" >
			                    <h3>Residential</h3>
			                    <div className="row">
			                        <div className="col-sm-4">
			                            <div className="checkbox checkbox-primary">
			                                <input id="checkbox2" type="checkbox" />
			                                <label htmlFor="checkbox2" onClick={this.handleClick.bind(this,"type","residential_apartment","property")}>Apartment</label>
			                            </div>
			                        </div>
			                        <div className="col-sm-4">
			                            <div className="checkbox checkbox-primary">
			                                <input id="checkbox3" type="checkbox" />
			                                <label htmlFor="checkbox3" onClick={this.handleClick.bind(this,"type","residential_villa","property")}>Villa</label>
			                            </div>
			                        </div>
			                    </div>
			                    <h3>Commercial</h3>
			                    <div className="row">
			                        <div className="col-sm-4">
			                            <div className="checkbox checkbox-primary">
			                                <input id="checkbox5" type="checkbox" />
			                                <label htmlFor="checkbox5" onClick={this.handleClick.bind(this,"type","commercial_showrrom","property")}>Showrooms</label>
			                            </div>
			                        </div>
			                        <div className="col-sm-4">
			                            <div className="checkbox checkbox-primary">
			                                <input id="checkbox6" type="checkbox" />
			                                <label htmlFor="checkbox6" onClick={this.handleClick.bind(this,"type","commercial_shop","property")}>Shop</label>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div className="col-sm-3 col-xs-12 srch-sepr-hm loc-srch-hm">
                  <UIAutocomplete
                      options={this.state.suggestList}
                      optionValue="id"
                      optionFilter={['name']}
                      optionLabelRender={option => `${option.name}`}
                      onChange={this.onChange}
                  />
                    {/*<input className='auto-class form-control' id="place-pin" placeholder="LOCATION/AREA" onFocus={this.geolocate} type="text"/>*/}
			            </div>
			            <div className="col-sm-3 col-xs-6 srch-sepr-hm budg-srch-hm dropdown">
			                <button type="button" ref='BtnBudget' className="btn dropdown-toggle srch-drop-btn" data-toggle="dropdown" aria-expanded="true"><span>Budget</span>
			                </button>
			                <div className="dropdown-menu" id="budget-hmdrop">
			                    <div className="budgsepline"></div>
			                    <div className="row">
			                        <div className="col-xs-6">
			                            <input type="number" className="buginp" placeholder="Min" value={this.props.filterParams.min_budget} onChange={this.valueEdit.bind(this,"min")}/>
			                            <ul className='ascoll'>
			                                <li><a href="javascript:;">Min</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",50000,"budget")}>50000</a>
			                                </li>
			                                <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",100000,"budget")}>1 Lac</a>
			                                </li>
			                                <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",500000,"budget")}>5 lac</a>
			                                </li>
			                                <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",1000000,"budget")}>10 Lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",1500000,"budget")}>15 Lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",2000000,"budget")}>20 lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",3000000,"budget")}>30 Lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",4000000,"budget")}>40 Lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",5000000,"budget")}>50 Lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",10000000,"budget")}>1 Cr</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",20000000,"budget")}>2 Cr</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",30000000,"budget")}>3 Cr</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",40000000,"budget")}>4 Cr</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",50000000,"budget")}>5 Cr</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"min_budget",100000000,"budget")}>10 Cr</a>
			                                </li>
			                            </ul>
			                        </div>
			                        <div className="col-xs-6">
			                            <input type="number" className="buginp" placeholder="Max" value={this.props.filterParams.max_budget} onChange={this.valueEdit.bind(this,"max")}/>
                                  <ul className='ascoll'>
			                                <li><a href="javascript:;">Max</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",50000,"budget")}>50000</a>
			                                </li>
			                                <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",100000,"budget")}>1 Lac</a>
			                                </li>
			                                <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",500000,"budget")}>5 lac</a>
			                                </li>
			                                <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",1000000,"budget")}>10 Lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",1500000,"budget")}>15 Lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",2000000,"budget")}>20 lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",3000000,"budget")}>30 Lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",4000000,"budget")}>40 Lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",5000000,"budget")}>50 Lac</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",10000000,"budget")}>1 Cr</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",20000000,"budget")}>2 Cr</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",30000000,"budget")}>3 Cr</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",40000000,"budget")}>4 Cr</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",50000000,"budget")}>5 Cr</a>
			                                </li>
                                      <li><a href="javascript:;" onClick={this.handleClick.bind(this,"max_budget",100000000,"budget")}>10 Cr</a>
			                                </li>
			                            </ul>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div className="col-sm-2 col-xs-6 srchbtn-hm">
			                <button className="btn btn-red" onClick={this.onSearch}><i className="fa fa-search" aria-hidden="true"></i> search</button>
			            </div>
			        </div>
			    </div>
			</section>
		)
   }
});
module.exports = HomeScreenFilter;



// .ui-autocomplete input
