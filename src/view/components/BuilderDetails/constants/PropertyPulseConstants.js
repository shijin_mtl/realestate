var keyMirror = require('keymirror');
module.exports = keyMirror({
 EXPERT_RESPONSE_RECEIVED:null,
 RESPONSE_CHANGE_EVENT:null,
 LOADING_RESPONSE_RECEIVED:null,
 BUILDER_RESPONSE_RECEIVED:null
});
