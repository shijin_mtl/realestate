import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/SubmitProperty/constants/SubmitPropertyConstant';
import config from 'utils/config';
import axios from 'axios';

var onChangeProperty = function(){

}


const submitPropertyApi = axios.create({
   withCredentials:true,
   contentType:false,
   processData:false,
});

onChangeProperty.prototype = {
	 submitImage:function(parameters,imageObj,parms){
     if (parms.about_me == "")
     {
       parms.about_me = 'No data'
     }
	 	var data = new FormData();
		for(var i in parameters){
		  data.append(i,parameters[i]);
		}
    for(var i in parms){
		  data.append(i,parms[i]);
		}
    data.append('property_for','sale')
    console.log(imageObj)
		data.append('profile_image',imageObj)
    console.log(imageObj)
		submitPropertyApi.post(config.server + 'api/v1/account/update-profile-image/', data)
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.SUBMIT_PROPERTY_RESPONSE_RECIEVED,
				        data: response
				  });
		    })
		   .catch(function (error) {
          if(error.response.status == 401){
            AppDispatcher.dispatch({
                 actionType: Constants.SUBMIT_PROPERTY_ERROR_RESPONSE_RECIEVED,
                 data: error.response.data
           });
          }
      });
	 }

}


module.exports = new onChangeProperty();
