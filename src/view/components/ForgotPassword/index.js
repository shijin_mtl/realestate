import React from 'react';
import { Form, FormGroup, Validator, Input,required,minLength,maxLength } from 'romagny13-react-form-validation'; 
import ForgotPasswordAction from 'components/ForgotPassword/action/ForgotPasswordAction';
import ForgotPasswordStore from 'components/ForgotPassword/store/ForgotPasswordStore';
import {Modal} from 'antd';
import {hashHistory} from 'react-router';
var MyFormGroup = function (props) {
    var groupclassName = props.hasError ? 'form-group has-error' : 'form-group';
    return (
            <div className={groupclassName}>
                {props.children}
                {props.hasError && <span className="help-block">{props.error}</span>}
            </div>
    );
};

var ForgotPassword = React.createClass({
  getInitialState:function(){
     return{
     	     model:{
     	     	         password:"",
                     confirmPassword:""
     	     }
     }
  },
  password:function(value){
      var format = /^((?!.*[\s])(?=.*[!@#$%^&*()_+])(?=.*[0-9]).{8,15})/;
      if(!format.test(value)){
        return{
           name:password,
           error:'Enter a Valied Password, Password must contain a special charecter and a number,Blank spaces are not allowed.'
        }
      }
  },
  onSubmit:function(results){
     if(!results.hasError){
        console.log(results.model); 
        if(results.model.password == results.model.confirmPassword){
           ForgotPasswordAction.forgotPassword({password:results.model.password,uid:this.props.params.uid,token:this.props.params.token})
        }
        else{
                Modal.error({
                  title: 'Password and Confirm password donot match',
                  okText  :'Ok'
                });
        }
     }
  },
  componentWillMount:function(){
     console.log(this.props.params);
     ForgotPasswordStore.bind(this.forgotPasswordResponseReceived)
  },
  componentWillUnmount:function(){
      ForgotPasswordStore.unbind(this.forgotPasswordResponseReceived)
  },
  forgotPasswordResponseReceived:function(){
     var response = ForgotPasswordStore.getResponse();
     hashHistory.push("/login")
  },
  render:function(){
  	var validation = {
        password:this.password,
    }
  	return(

  		    <div className="forgot-password">
                <Form onSubmit={this.onSubmit}  model={this.state.model} >
                    <div className="form-group showpassbox">
                      <Validator validators={[required('Password is required'), minLength(8), maxLength(30) ,validation['password']]}>
                        <MyFormGroup>
                              <label htmlFor="password">New Password</label>
                              <Input type="password" id="password" name="password" value={this.state.model.password} className="form-control"  />
                        </MyFormGroup>
                      </Validator>
                    </div>



                    <div className="form-group showpassbox">
                      <Validator validators={[required('Password is required'), minLength(8), maxLength(30) ,validation['password']]}>
                        <MyFormGroup>
                              <label htmlFor="password">Confirm Password</label>
                              <Input type="password" id="confirmPassword" name="confirmPassword" value={this.state.model.confirmPassword} className="form-control"  />
                        </MyFormGroup>
                      </Validator>
                    </div>
                    <div className="cntr-algn-wrp">
                        <button className="btn btn-red"   type="submit">Reset Password</button>
                    </div>
                </Form>
  		    </div>
  	) 
  }
});
module.exports = ForgotPassword;