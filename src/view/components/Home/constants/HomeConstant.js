var keyMirror = require('keymirror');
module.exports = keyMirror({
 HOME_RESPONSE_RECEIVED: null,
 LOADING_RESPONSE_RECEIVED:null,
 EXPERT_RESPONSE_RECEIVED:null,
 BLOG_RESPONSE_RECEIVED:null,
 LIST_RESPONSE_RECEIVED:null,
});
