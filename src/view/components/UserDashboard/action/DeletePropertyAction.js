import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/UserDashboard/constants/UserConstant';
import config from 'utils/config'
import axios from 'axios';

var onDeleteProperty = function(){

}

const deletePropertyApi = axios.create({
  withCredentials: true
});

onDeleteProperty.prototype = {
	 deleteProperty:function(parameters){
	 	deletePropertyApi.delete(config.server  + 'api/v1/property/' + parameters.id + '/')
		    .then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.DELETE_PROPERTY_RESPONSE_RECEIVED,
			        data: response
				});
            })
			.catch(function (error) {
			       console.log("error",error)
            });


	 }
}


module.exports = new onDeleteProperty();
