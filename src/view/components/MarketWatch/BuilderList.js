import React from 'react';
import {hashHistory} from 'react-router';



var BuilderList = React.createClass({
  goToDetail:function(id){
    hashHistory.push({
      pathname:"/builder-detail",
      query:{id:id}
    })
  },
    render:function(){
    	return(
        <div className="col-sm-3 padding-just">
          <div className="featured-boxlist match-item" style={{'minHeight': '302'+'px'}}>
            <div className="build-logobox">
              <div className="build-logo-vertic">
                <img src={this.props.data.logo?this.props.data.logo:"images/noimage.png"} width="140" alt="" className="img-responsive"/>
              </div>
            </div>
            <div className="build-info">
              <a><h3 onClick={this.goToDetail.bind(this,this.props.data.id)}>{this.props.data.name}</h3></a>
              <ul>
                <li>City
                  <ul>
                    <li>{this.props.data.city}</li>
                  </ul>
                </li>
              </ul>
              <ul>
                <li>1 Completed Project
                </li>
              </ul>
            </div>
          </div>
        </div>
    	)
    }
});
module.exports = BuilderList;
