import React from 'react';
import moment from "moment";
import HomeScreenFilter from 'components/Home/HomeScreenFilter';
import MarketBanner from './Banner';
import NewInCity from './NewInCity'
import FeaturedBuilders from './FeaturedBuilders'
import FeaturedAgents from './FeaturedAgents'
import {hashHistory} from 'react-router';
import BackGroundImage from 'public/images/no-photo.jpg';


var MarketWatch = React.createClass({
   getInitialState:function(){
     return{
            type:"residential",
            homeList:[],
            locationList:[],
            isLoading:true,
            filterParams:{property_for:"sale",typeListArr:[],location:"",min_budget:"",max_budget:"",page:1},
            locationTop:localStorage.loacalLocation?localStorage.loacalLocation:8
     }
   },

   componentWillMount:function(){
     $(".side-collapse").addClass('in');
   },
   componentDidMount:function(){

           var options = {
             types: ['geocode'],
             componentRestrictions: {country: ["in"]}
            };

           var autocomplete;
           function initialize() {
             autocomplete = new google.maps.places.Autocomplete(
                 /** @type {HTMLInputElement} */(document.getElementById('place-pin')),
                 options);
             google.maps.event.addListener(autocomplete, 'place_changed', function() {

           var place = autocomplete.getPlace()

           var lat = place.geometry.location.lat();
           var lng = place.geometry.location.lng();
           localStorage.setItem('latitude', lat);
           localStorage.setItem('longitude', lng);

                   });
                 }

                initialize();
   },
   

   onSearch:function(){
     this.state.filterParams.latitude = localStorage.latitude
     this.state.filterParams.longitude = localStorage.longitude

     this.setState({
       filterParams:this.state.filterParams
     })
     if(localStorage.latitude =="")
     {
          this.state.filterParams.location = localStorage.loacalLocation
     }
     else {
       {
            this.state.filterParams.location = ''
            this.setState({
              filterParams:this.state.filterParams
            })
       }
     }
      hashHistory.push({
          pathname:"/property",
          query:this.state.filterParams
      })
   },
   changeFilters:function(key,value){
   	 if(key == "type"){
	      if (this.state.filterParams.typeListArr.indexOf(value) === -1) {
	         this.state.filterParams.typeListArr.push(value);
	      }
	      else{
	        this.state.filterParams.typeListArr.splice(this.state.filterParams.typeListArr.indexOf(value),1)
	      }
   	 }
   	 else{
            this.state.filterParams[key] = value;
   	 }
        this.setState({
		     	filterParams:this.state.filterParams
		     })
   },

   render:function(){
   	 return(
       <span>
               {/* banner */}
                 <MarketBanner/>
               {/* end banner */}

               {/* filter */}
                  <HomeScreenFilter filterParams = {this.state.filterParams} changeFilters={this.changeFilters} onSearch={this.onSearch} locationList={this.state.locationList}/>
               {/* end filter */}

               {/* New in city  */}
                  <NewInCity/>
               {/* end new  in city  */}

               {/* featured builders  */}
                  <FeaturedBuilders/>
               {/* end featured  */}

               {/* featured agents  */}
                  <FeaturedAgents/>
               {/* end featured agents  */}


        </span>
   	 )
   }
});
module.exports = MarketWatch;
