import React from 'react';
import DetailHeader from './Header'
import SideBar from './Sidebar'
import BasicDetails from './BasicDetails'
import BuilderStore from 'components/BuilderDetails/store/BuilderStore';
import BuilderAction from 'components/BuilderDetails/action/BuilderAction';
import PropertyList from './PropertyList'
import MetaTags from 'react-meta-tags';



var BuilderSignup = React.createClass({
  getInitialState:function(){
    window.scroll(0,0)
    return{
      builderData:"",
      projectList:[]
    }
  },
  componentWillMount:function(){
    BuilderStore.bind(this.responseReceived);
    BuilderAction.builderList(this.props.location.query.id)
    window.scrollTo(0,0);
  },
  componentWillUnmount:function(){
    BuilderStore.unbind(this.responseReceived);
  },
  responseReceived:function(){
    this.state.builderData = BuilderStore.getResponse()
    this.setState({
      builderData:this.state.builderData
    })
    var key = [];
    for(let i=0; i<this.state.builderData.property.length;i++){
      key.push(<PropertyList data={this.state.builderData.property[i]} key={i} />)
    }
    this.state.projectList = key
    this.setState({
      projectList:key
    })
  },

  render:function(){
    return(
      <span>

      <MetaTags>
          <meta id="meta-description" name="description" content={this.state.builderData.description} />
          <meta id="og-title" property="og:title" content={"Wynvent" + this.state.builderData.name}/>
      </MetaTags>


      <section className="agent-dtlwrap">
        <div className="container">
        <DetailHeader data={this.state.builderData.name}/>
        <div className="row">
          <div className="col-sm-9">
            <div className="agtlstbox agt-blddtlwrap">
              <div className="row">
                <BasicDetails data={this.state.builderData} list={this.state.projectList}/>
              </div>
            </div>
          </div>
          <SideBar/>
        </div>
        </div>
      </section>
      </span>
    )
  }
})
module.exports = BuilderSignup;
