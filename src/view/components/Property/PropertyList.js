import React from 'react';
import FilterBreadCrumb from './FilterBreadCrumb';
import PropertyFilters from './PopertyFilters';
import BreadCrumb from './BreadCrumb';
import PropertyRightSideBar from './PropertyRightSideBar';
import SortBy from './SortBy';
import SingleProperty from './SingleProperty';
import SinglePropertyList from './SinglePropertyList';
import { Pagination } from 'antd';
import Loading from 'components/Loading';
import PropertyErrorScenario from './PropertyErrorScenario';



var Scroll  = require('react-scroll');

var scroll     = Scroll.animateScroll;

var PropertyList = React.createClass({
  getInitialState: function () {
       return {
           single:[],
           wide:[]
       };
   },

     handleChange:function(key1){
     	this.props.onChangePagination(key1);
      scroll.scrollToTop();
     },
     onClickFilters:function(key,value){
        this.props.onChangeFilters(key,value);
     },
     showPropertyList:function(){
         var propertyList = []
         var data = this.props.propertyListResp;
         if(data.length > 0){
	         for (var i = 0, len = data.length; i < len; i++) {
	            propertyList.push(<SingleProperty key={i} propertyContents = {data[i]} localityList={this.props.localityList}/>);
	         }
	         return(propertyList);
	     }
	     else{
                 return(<PropertyErrorScenario /> )
	     }
     },
     showPropertySingleList:function(){
         var propertySingleList = []
         var data = this.props.propertyListResp;
         if(data.length > 0){
          for (var i = 0, len = data.length; i < len; i++) {
             propertySingleList.push(<SinglePropertyList  key={i} propertyContents = {data[i]} localityList={this.props.localityList} />)
          }
          return(propertySingleList);
         }
         else{
         	   return(<PropertyErrorScenario />)
         }
     },
     handleSort:function(key){
       this.props.handleSort(key);
     },
     availableForFamily:function(key){
       this.props.availableForFamily(key);
     },
     handleDepositAmount:function(value,key){
       this.props.handleDepositAmount(value,key);
     },
     componentWillReceiveProps :function(){
       if(this.props.propertyListResp!=[]){
         this.state.single = this.showPropertyList()
         this.setState({
           single:this.state.single
         })
       }
     },
     getMyList:function(key) {
       this.state.single =<Loading/>
       this.setState({
         single:this.state.single
       })
       this.state.single = key==1?this.showPropertyList():this.showPropertySingleList()
       this.setState({
         single:this.state.single
       })
     },

     render:function(){
         var propertyList = [];
         var propertySingleList = []
         var data = this.props.propertyListResp;
         for (var i = 0, len = data.length; i < len; i++) {
            propertyList.push(<SingleProperty key={i} propertyContents = {data[i]} localityList={this.props.localityList}/>);
            propertySingleList.push(<SinglePropertyList  key={i} propertyContents = {data[i]} localityList={this.props.localityList} />)
         }
     	 return(
     	 	    <section className="listing-midwrap">
				    <div className="container">
				        <BreadCrumb />
				        <div className="row">
				            <PropertyFilters clearClick = { this.props.clearClick }localityList={this.props.localityList} onClickFilter={this.onClickFilters}
                              checkedFilters = {this.props.checkedFilters} availableForFamily={this.availableForFamily}
                              handleDepositAmount={this.handleDepositAmount} filterParams={this.props.filterParams}/>
				            <div className="col-sm-6 list-midbox">
				                <FilterBreadCrumb propertyResp={this.props.propertyResp}/>
				                <SortBy filterParams={this.props.filterParams} handleSort={this.handleSort} getMyList={this.getMyList}/>

				                {!this.props.isLoading && <div className="tab-content">
				                    <div role="tabpanel" className="tab-pane fade in active" id="prop-grid">
				                       {this.state.single}
				                    </div>

				                </div>}
				                {this.props.isLoading && <Loading/ >}
				            </div>
                    <PropertyRightSideBar  propertyContents ={ this.props.propertyListResp}/>
				        </div>
                <div className="row text-center">
				         <Pagination total={this.props.propertyResp.count} current={parseInt(this.props.propertyResp.current)} onChange={this.handleChange} />
                </div>
				    </div>
          </section>


     	 )
     }
})
module.exports = PropertyList;
