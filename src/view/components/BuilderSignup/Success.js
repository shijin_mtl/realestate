import React from 'react';
import {Link} from 'react-router';

var Success = React.createClass({
  getInitialState:function(){
    window.scroll(0,0)
    return{
    }
  },
  render:function(){
    return(
      <div className="submit-property-section">
      <div className="container">
      <div className="row">
            <div className="col-sm-12">
                <ul className="breadcrumb">
                    <li><Link to="/">Home</Link>
                    </li>
                    <li className="active" >BUILDER DETAILS</li>
                    <li className="active">THANK YOU</li>
                </ul>
            </div>
       </div>
        <div className="row">
          <div className="col-sm-12">
          <form className="submit-property-form " ref="form">
              <br/><br/><br/><br/>
              <h2 className="text-center"><b>Thank you for sharing your information.</b></h2>
              <h5 className="text-center">We will showcase your profile on builder page, post verification</h5>
              <br/>
              <div className="property-details-section" style = {{'borderBottom': 0 +"px"}}>
              <h5 className="text-center">Meanwhile you can check our <a style = {{'color': "red"}}>Plans</a> for better visibility.</h5>
              <h5 className="text-center">Or register yourself as <a style = {{'color': "red"}}> Premium builder.</a></h5>
              <h5 className="text-center"> <a href="/#/login" style = {{'color': "red"}}> Login</a></h5>
              </div>
              <br/><br/><br/><br/>
          </form>
          </div>
        </div>
        </div>
      </div>
    )
  }
})
module.exports = Success;
