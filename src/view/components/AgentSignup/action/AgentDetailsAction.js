import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/AgentSignup/constants/AgentConstants';
import config from 'utils/config';
import axios from 'axios';
import SignUpStore from 'components/SignUp/store/SignUpStore';



var AgentRegister = function(){

}

const agentApi = axios.create({
  withCredentials: true,
});


AgentRegister.prototype = {
  expert:function(params){
    params.title = params.name
    var data = new FormData();
    params.city = (params.city).split(",")
    for(var i in params){
      data.append(i,params[i]);
    }

    var signUpResp = SignUpStore.getResponse();
    localStorage.tempId=signUpResp.data?signUpResp.data.id:null
    if(localStorage.tempId == "null")
    {
      window.location = "/#/signup";
    }
    else {
      data.append('user_agent',signUpResp.data?signUpResp.data.id:localStorage.tempId)
  		agentApi.post(config.server + 'api/v1/agent/',data)
  			.then(function (response) {
  		        AppDispatcher.dispatch({
  			        actionType: Constants.AGENT_RESPONSE_RECEIVED,
  			        data: response
  				});
  			})
  			.catch(function (error) {
  			    console.log("error...",error);
              });
    }

	}
}

module.exports = new AgentRegister();
