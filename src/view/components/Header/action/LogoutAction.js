import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Header/constants/LoginStatusConstant';
import config from 'utils/config';
import axios from 'axios';

var onLogOut = function(){

}

const loginOutApi = axios.create({
  timeout: 10000,
  withCredentials: true,

});

onLogOut.prototype = {
	 logout:function(){
		loginOutApi.post(config.server + 'api/v1/logout/')
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.LOGOUT_RESPONSE_RECIEVED,
				        data: response
				});
		    })
		   .catch(function (error) {
		      console.log(error);
            });

	 }
}


module.exports = new onLogOut();
