import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Login/constants/LoginConstant';
import config from 'utils/config';
import axios from 'axios';
import AuthService from 'services/AuthService';
var forgotPasswordVerification = function(){

}

const forgotAPI = axios.create({
  withCredentials: true,
  headers:{
  	crossDomain:true
  }
});

forgotPasswordVerification.prototype = {
	 forgotPassword:function(parameters){
		forgotAPI.post(config.server + 'api/v1/reset-password/', parameters)
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.FORGOT_PASSWORD_EMAIL_SENDING,
				        data: response
				});
		    })
		   .catch(function (error) {
		      console.log("error",error)
            });
				
	 }

}


module.exports = new forgotPasswordVerification();
