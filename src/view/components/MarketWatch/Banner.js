import React from 'react';
import BannerAction from 'components/PropertyPulse/action/BannerAction';
import BannerStore from 'components/PropertyPulse/store/BannerStore';
import { default as Fade } from 'react-fade'




var MarketBanner = React.createClass({


  getInitialState:function(){
      return{
              bannerList:""
      }
  },

  tick: function() {
      BannerAction.getList("property")
  },

  componentDidMount: function() {
    this.interval = setInterval(this.tick, 5000);
  },

  componentWillMount:function(){
      BannerStore.bind(this.bannerResponseReceived);
      BannerAction.getList("property")
  },
  componentWillUnmount:function(){
      BannerStore.unbind(this.bannerResponseReceived);
      clearInterval(this.interval);
  },
  bannerResponseReceived:function (){
    var list = BannerStore.getResponse()
    this.state.bannerList = list[0]
    this.setState({
      bannerList:this.state.bannerList
    })
  },

    render:function(){
    	return(
        <Fade
          in={false}
          duration={1.5}
        >
        <section className="explr-banner" style={{backgroundImage: `url(${this.state.bannerList.image})`}}>
            <div className="container">
            <div className="bnrinfo">
            <h2 style={{'color':'white'}}>{this.state.bannerList.title}</h2>
            <h4 style={{'color':'white'}}>{this.state.bannerList.description}</h4>
            <a href={this.state.bannerList.url} className="btn btn-default" style={{'borderColor':'red'}}>VIEW PROPERTIES</a>
            </div>
            </div>
       </section>
       </Fade>
    	)
    }
});
module.exports = MarketBanner;
