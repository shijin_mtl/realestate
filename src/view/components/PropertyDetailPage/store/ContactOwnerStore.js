var Dispatcher = require('dispatcher/dispatcher');
var EventEmitter = require('events').EventEmitter;
import Constants from 'components/PropertyDetailPage/constants/PropertyDetailConstant';

var response = {};

var assign = require('object-assign');

function parseResponse(resp){
  response = resp;
}

var ContactOwnerStore = assign({},EventEmitter.prototype,{
   emitChangeEvent: function(event) {
       this.emit(event);
   },
   bind: function(callback) {
       this.on(Constants.RESPONSE_CHANGE_EVENT, callback);
   },
   unbind: function(callback) {
       this.removeListener(Constants.RESPONSE_CHANGE_EVENT, callback);
   },
   getResponse:function(){
        return response;
   }
});

Dispatcher.register(function(action){
   switch (action.actionType) {
       case Constants.CONTACT_OWNER_RESPONSE_RECEIVED:
           var resp = action.data;
           parseResponse(resp)
           ContactOwnerStore.emitChangeEvent(Constants.RESPONSE_CHANGE_EVENT)
       default:
   }
});

module.exports = ContactOwnerStore;
