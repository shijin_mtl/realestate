import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/BuilderSignup/constants/BuilderConstants';
import config from 'utils/config';
import axios from 'axios';


var BuilderEdit = function(){

}

const builderApi = axios.create({
  withCredentials: true,
});

BuilderEdit.prototype = {
	getEditData:function(id){
  		builderApi.get(config.server + 'api/v1/builder/'+id+"/")
  			.then(function (response) {
  		        AppDispatcher.dispatch({
  			        actionType: Constants.EDIT_RESPONSE_RECEIVED,
  			        data: response
  				});
  			})
  			.catch(function (error) {
  			    console.log("error...",error);
              });
	},
  postEditData:function(params,id){
    var data = new FormData();
    params.city = (params.city).split(",")
    for(var i in params){
      data.append(i,params[i]);
    }
  		builderApi.post(config.server + 'api/v1/builder/'+id+"/",data)
  			.then(function (response) {
          window.location = "/#/profile";
  			})
  			.catch(function (error) {
  			    console.log("error...",error);
              });
	}
}

module.exports = new BuilderEdit();
