import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/SubmitProperty/constants/SubmitPropertyConstant';
import config from 'utils/config';
import axios from 'axios';

var onSubmitProperty = function(){

}


const submitPropertyApi = axios.create({
   withCredentials:true,
   contentType:false,
   processData:false,
});

onSubmitProperty.prototype = {
	 submitProperty:function(parameters,imageObj){
     if(parameters.add_facilities=[])
     {
      delete parameters.add_facilities
     }
	 	var data = new FormData();
    var locateDetails = JSON.stringify({"latitude":parameters.coordinates[0],"longitude":parameters.coordinates[1]});
    data.append('coordinates',locateDetails)
    delete parameters.coordinates;
		for(var i in parameters){
		  data.append(i,parameters[i]);
		}

		if(imageObj.image_ids.length > 0){
			var imgArr = imageObj.image_ids;
			for(var j in imgArr){
				data.append('image_ids',imgArr[j],imgArr[j].name)
			}

		}
		submitPropertyApi.post(config.server + 'api/v1/property/submit/', data)
		    .then(function (response){
			    AppDispatcher.dispatch({
				        actionType: Constants.SUBMIT_PROPERTY_RESPONSE_RECIEVED,
				        data: response
				  });
		    })
		   .catch(function (error) {
          if(error.response.status == 401 || error.response.status == 500){
            AppDispatcher.dispatch({
                 actionType: Constants.SUBMIT_PROPERTY_ERROR_RESPONSE_RECIEVED,
                 data: error.response.data
           });
          }
      });
	 }

}


module.exports = new onSubmitProperty();
