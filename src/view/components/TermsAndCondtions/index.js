import React from 'react';
var TermsAndConditions = React.createClass({
	componentWillMount:function(){
     window.scrollTo(0,0);
    },
	render:function(){
		return(
			    <section className="common-content">
				  <div className="container">
				    <ul className="breadcrumb">
				      <li><a href="#">Home</a></li>
				      <li className="active">Terms & Conditions</li>
				    </ul>
				    <h1>Terms & Conditions</h1>
				    <div className="contentbox">
				      <p>“Wynvent” is a trademark of Abhisarga Construction Private Limited, a company
								incorporated under the Companies Act, 2013 with its registered office at 8-2-
								630/A/B/1A, Ferry&#39;s Boutique, Office No.11,12 Club, Ground Floor, Mt. Banjara
								Apartment, Road No. 12, Hyderabad - 500034, Telangana (Company). The domain
								name www.wynvent.com is owned by the Company and this website is proprietary to
								the Company.</p>

				      <p>It is strongly recommended that you read and understand these Terms of Use carefully,
							as by accessing this website (hereinafter the “Website”), you agree to be bound by the same and
							agree that it constitutes an agreement between you and the Company (hereinafter the “User Agreement”). If you
							do not agree with this User Agreement, you should not use or access the Website for any purpose whatsoever. </p>


				      <p>This document is published in accordance with the provisions of Rule 3 of the Information Technology
							(Intermediaries Guidelines) Rules, 2011. The User Agreement may be updated from time to time by the Company
							without notice. It is therefore strongly recommended
							that you review the User Agreement, as available on the Website, each time you access and/ or use the Website.</p>

							<p>
							The terms ‘visitor’, ‘user’, ‘you’ hereunder refer to the person visiting, accessing, browsing through and/ or using the Website at any point in time.
							</p>

							<p>Should you have any clarifications regarding the Terms of Use, please do not hesitate to contact us at <a href="mailto:info@wynvent.com">info@wynvent.com.</a></p>

						  <h3> <b>I. SERVICE OVERVIEW</b></h3>
							<br/>
						  <p>The Website is a platform for users to interact with and market/ advertise to third parties, who are granted access to the Website to display and offer real estate services, products and related information. For abundant clarity, the Company does not provide any service to users other than providing the Website as a platform to interact and market/ advertise to third parties by placing Content or viewing the same at a user’s sole risk.
								The Company is not liable for any Content offered by or to you, and does not have any control, involvement or influence over the same. The Company therefore disclaims all warranties and liabilities associated with any Content on the Website.</p>

								<h3> <b>II. ELIGIBILITY</b></h3>
								<br/>
								<p>You are only permitted to access and/ or use the website if you are over 18 years of age. You hereby confirm that you at least 18 years of age and are “competent to contract” within the meaning of Section 11 of the Indian Contract Act, 1872. </p>

								<h3> <b>III. LICENSE & ACCESS</b></h3>
								<br/>
								<p>The Company grants you a limited sub-license to access and make personal use of the Website, but not to download (other than page caching) or modify it, or any portion of it, except with express written consent of the Company. Such limited sub-license does not include/ permit any resale or commercial use of the Website or its contents; any collection and use of any product listings, descriptions, or prices; any derivative use of the Website or its contents; any downloading or copying of information for the benefit of another merchant; or any use of data mining, robots, or similar data gathering and extraction tools. The Website or any portion of the Website may not be reproduced, duplicated, copied, sold, resold, visited, or otherwise exploited for any commercial purpose without express written consent of the Company. You may not frame or utilize framing techniques to enclose any trademark, logo, or other proprietary information (including images, text, page layout, or form) of the Website or of the Company and/ or its affiliates without the express written consent of the Company. You may not use any meta tags or any other “hidden text” utilizing the Company’s name or trademarks without the express written consent of the Company.  You shall not attempt to gain unauthorized access to any portion or feature of the Website, or any other systems or networks connected to the Website or to any server, computer, network, or to any of the services offered on or through the Website, by hacking, ‘password mining’ or any other illegitimate means.</p>
								<p>You hereby agree and undertake not to host, display, upload, modify, publish, transmit, update or share any information that:</p>


								<p>(i) belongs to another person and to which you do not have any right;</p>
								<p>(ii) is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, paedophilic, libellous, invasive of another’s privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever;</p>
								<p>(iii) harms minors in any way;</p>
								<p>(iv) infringes any patent, trademark, copyright or other proprietary/intellectual property rights;</p>
								<p>(v) violates any law for the time being in force;</p>
								<p>(vi) deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;</p>
								<p>(vii) impersonates another person;</p>
								<p>(viii) contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource; </p>
								<p>(ix) threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting any other nation; or</p>
								<p>(x) is misleading or known to be false in any way.</p>

								<p>Further, you agree not to display or provide any information that does not belong to you, other than where you act as an authorised agent or representative of a third party.
								 Any unauthorized use of the Website or display of third party material without authority, whether infringing or not shall automatically terminate the permission granted by the Company to use/access the Website.</p>

								 <h3> <b>IV. ACCOUNT & REGISTRATION OBLIGATIONS</b></h3>
								 <br/>
								 <p>All users may have to register and login for use of certain or all features of the Website. You must keep your account and registration details current and correct for communications related to you, and you have the sole responsibility to protect, and prevent misuse of your use account, and the associated username and password.  Any payment made by you to the Company in respect of use of the Website shall be non-refundable unless specifically agreed in writing otherwise.</p>
								 <p>By agreeing to the Terms of Use, the user agrees to receive promotional communication and newsletters from the Company and its partners. The user can opt out from such communication and/or newsletters either by unsubscribing on the Website itself, or by contacting the customer services team and placing a request for unsubscribing.</p>
								 <p>The Company reserves the right to terminate or refuse your registration, or refuse to permit access to the Website, if: (i) it is discovered or brought to notice that you do not conform to the criteria for use/access of the Website, or (ii) the Company has reason to believe that the eligibility criteria is not met by a user, or the user may breach the terms of this User Agreement.</p>
								 <p>As part of the registration process on the Website, the Company may collect the personally identifiable information about you: Name - including first and last name, email address, mobile phone number and other contact details, demographic profile (like your age, gender, occupation, education, address etc.) and information about the pages on the Website you visit/ access, the links you click on the Website, the number of times you access a particular page/ feature and any such information. </p>
								 <p>Information collected about you is subject to the Privacy Policy of the Company <a href="/#/privacy">Privacy Policy</a>, which is incorporated in these Terms of Use by reference. You hereby confirm that you have read and understood the Privacy Policy.</p>

								 <h3> <b>V. YOU AGREE AND CONFIRM:</b></h3>
 								 <br/>
								 <p>(i) That you will use the Website for lawful purposes only, and comply with all applicable laws and regulations while using any Content available on the Website. You specifically agree that you will not use any Content except for providing services of real estate to other users or for viewing Content for self-consumption.</p>
								 <p>(ii) You will provide authentic and true information in all instances. The Company reserves the right to confirm and validate the information and other details provided by you at any point of time.
								 If at any time, the information provided by you is found to be false or inaccurate (wholly or partly), the Company shall have the right in its sole discretion to reject registration, and/or debar you from using its services and other affiliated services in the future without any prior intimation whatsoever.</p>
								 <p>(iii) That you are accessing the services available on the Website and interacting with third parties at your sole risk and are using prudent judgment in doing so.</p>
								 <p>(iv) If you are a developer, as defined under the Real Estate (Regulation and Development) Act, 2016 (“RERA”), you confirm that any product or service relating to a project is for a project registered with the competent authority and complied with the provision of RERA.</p>
								 <br/>
								 <p>If you are a real estate agent, you confirm that you are a duly registered under RERA. </p>
								 <h3> <b>VI. DISCLAIMERS:</b></h3>
 								 <br/>
								 <p>The Company does not make any representation or warranties in respect of any information, service or product available on the Website or any Content, nor does the Company implicitly or explicitly support or endorse the such Content or information, service or product placed on the Website. The Company accepts no liability for any error, inaccuracy or omission of third parties or for information/inputs received from third parties and provided to you through the Website or otherwise.</p>
								 <p>THE COMPANY SPECIFICALLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, NON-INFRINGEMENT AND ANY WARRANTIES ARISING OUT OF THE COURSE OF DEALING OR USAGE OF TRADE.</p>
								 <p>You acknowledge and agree that the Company does not have any control over the quality, failure to provide or any other aspect whatsoever of the Content and is not responsible for damages on account of use thereof, or reliance thereon. The Company shall not be liable for errors in Content available on the Website, all of which are made available to you on a “as is where is” basis, and you agree to independently verify the same at your own cost and expense.</p>
								 <h3> <b>VII. CONTENT:</b></h3>
 								 <br/>
								 <p>You hereby represent, warrant and covenant to the Company that all information, service offerings, products, prices and any other content submitted or posted by you to the Website (“Content”) shall be true, complete and accurate, and not misleading in any manner. Content must not infringe the rights of any third party, including any intellectual property rights, rights of publicity, rights of privacy, rights to payment of royalties and residuals; or result in any tort, injury, damage or harm to any third party.</p>
								 <p>Any Content submitted or posted by you may undergo a screening process. This process may include review and modifications to the Content by our in-house team. </p>
								 <p>We may modify or adapt your Content in order to transmit, display or distribute it over computer networks and in various media and/or make changes to your Content as are necessary to conform and adapt it to any requirements or limitations of any networks, devices, services or media.</p>
								 <p>Further, any reliance placed on Content shall be at your sole risk and expense.</p>
								 <h3> <b>VIII. COPYRIGHT & TRADEMARK:</b></h3>
 								 <br/>
								 <p>The Company, its suppliers and licensors expressly reserve all intellectual property rights in all text, programs, products, processes, technology, images, content and other materials which appear on the Website. Access to or use of the Website does not confer and should not be considered as conferring upon anyone any license to the Company or any third party’s intellectual property rights. All rights, including copyright, in and to the Website are owned by or licensed to the Company. Any use of the Website or its contents, including copying or storing it or them in whole or part is prohibited without the permission of the Company. </p>
								 <p>You may not modify, distribute or re-post anything on the Website for any purpose. The names and logos and all related product and service names, design marks and slogans are the trademarks/service marks of the Company, its affiliates and/or its partners. All other marks are the property of their respective owners. No trademark or service mark license is granted in connection with the materials contained on the Website. Access to or use of the Website does not authorize anyone to use any name, logo or mark in any manner. References on the Website to any names, marks, products or services of third parties or hypertext links to third party sites or information are provided solely as a convenience to you and do not in any way constitute or imply the Company’s endorsement, sponsorship or recommendation of the third party, the information, its product or services. </p>
								 <p>The Website may contain links to third party websites, offers and advertisements, which are not controlled by the Company nor are they endorsed by the Company in any manner. The Company is not responsible for content on any such third-party links and does not make any representations regarding the content, material products or services on such sites. If you decide to access a link of any third party websites, you do so entirely at your own risk and expense. </p>
								 <h3> <b>IX. INDEMNITY:</b></h3>
								<br/>
								<p>You agree to defend, indemnify and hold harmless the Company, its employees, directors, officers, agents and their successors and assigns from and against any and all claims, liabilities, damages, losses, costs and expenses, including attorneys fees, caused by or arising out of claims based upon a breach of any warranty, representation or undertaking in this User Agreement, or arising out of a violation of any applicable law (including but not limited in relation to intellectual property rights, payment of statutory dues and taxes, claims of libel, defamation, violation of rights of privacy or publicity, loss of service by other subscribers and infringement of intellectual property or other rights). This clause shall survive the expiry or termination of this User Agreement.</p>
								<h3> <b>X. LIMITATION OF LIABILITY:</b></h3>
							 <br/>
							 <p>Neither the Company, nor any of its affiliates, shall have any liability for Content placed on the Website or derived by you from the Website.
							 It is acknowledged and agreed that notwithstanding anything to the contrary, the Company shall not be liable, under any circumstances, whether in contract or in tort, for any indirect, special, consequential or incidental losses or damages, including on grounds of loss of profit, loss of reputation or loss of business opportunities.</p>



							<h3> <b>XI. TERMINATION:</b></h3>
						 <br/>
						 <p>This User Agreement is effective unless and until terminated, either by you or by the Company. You may terminate this User Agreement at any time, provided that you discontinue any further use of the Website. The Company may terminate this User Agreement at any time and may do so immediately without notice, and accordingly deny you access to the Website. </p>
						 <p>The Company’s right to be indemnified pursuant to the terms hereof shall survive any termination of this User Agreement. Any such termination of the User Agreement shall not affect any liability that may have arisen under the User Agreement prior to the date of termination.</p>

						 <h3> <b>XII. GOVERNING LAW AND JURISDICTION:</b></h3>
						<br/>
						<p>The User Agreement shall be governed by and construed in accordance with the laws of India, without giving effect to the principles of conflict of laws thereunder.</p>
						<p>Any dispute or difference, whether on interpretation or otherwise, in respect of any terms hereof shall be referred to an independent arbitrator to be appointed by the Company. Such arbitrator’s decision shall be final and binding on the parties. The arbitration shall be in accordance with the Arbitration and Conciliation Act, 1996, as amended or replaced from time to time. The seat of arbitration shall be New Delhi and the language of the arbitration shall be English.</p>
						<p>Subject to the aforesaid, the Courts at New Delhi shall have exclusive jurisdiction over any proceedings arising in respect of the User Agreement.</p>

						<h3> <b>XIII. GRIEVANCE POLICY:</b></h3>
					  <br/>
					  <p>In accordance with Information Technology Act, 2000 and the Information Technology (Intermediaries Guidelines) Rules, 2011, the name and contact details of the Grievance Officer who can be contacted for any complaints or concerns pertaining to the Website, including those pertaining to breach of the Terms of Use or and other polices are published as under.</p>
						<div className="row">
						<div className="col-md-3">
						<p>Name: </p>
						<p>Postal Address:</p>
						<p>Email Address: </p>
						</div>
						<div className="col-md-9">
						<p>Anjali Dang  </p>
						<p>4/46, W.E.A., Karol Bagh, New Delhi- 110005</p>
						<p>contactus@wynvent.com  </p>
						</div>

						</div>
						</div>
				  </div>
				</section>
		)
	}

});
module.exports= TermsAndConditions;
