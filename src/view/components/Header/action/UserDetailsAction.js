import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Header/constants/LoginStatusConstant';
import config from 'utils/config';
import axios from 'axios';

var fetchUserDetails = function(){

}

const userDetailsApi = axios.create({
  withCredentials: true,
}); 


fetchUserDetails.prototype = {
	 userDetails:function(){
		userDetailsApi.get(config.server + 'api/v1/account/')
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.USER_DETAILS_RESPONSE_RECIEVED,
				        data: response
				});
		    })
		    .catch(function (error) {
		      console.log(error);
            });
				
	 }
}


module.exports = new fetchUserDetails();
