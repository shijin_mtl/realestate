import React from 'react';

var DetailHeader = React.createClass({
  getInitialState:function(){
    window.scroll(0,0)
    return{
    }
  },
  render:function(){
    return(
      <span>
          <ul className="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="/#/builder-list">Builders in your city</a></li>
              <li className="active">{}</li>
          </ul>
          <div className="agt-top-box">
              <h2>{this.props.data}</h2>
              <h4>Know the Builder</h4>
              <hr className="sepline"/>
          </div>
      </span>
    )
  }
})
module.exports = DetailHeader;
