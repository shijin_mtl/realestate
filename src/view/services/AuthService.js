import axios from 'axios';
module.exports= {
	authService:function(){
		const loginApi = axios.create({
		  timeout: 10000,
		  withCredentials: true,
		  headers:{
		  	crossDomain:true
		  }
 
		});

		return loginApi;
	}
}