import React from 'react';
import SubHeader1 from 'components/SubHeader';
import SubHeader2 from 'components/SubHeaderSearchBar';
import LoginStatusStore from 'components/Header/store/LoginStatusStore';
import UserDetailsStore from 'components/Header/store/UserDetailsStore';
import {Link,hashHistory} from 'react-router';
import LogoutAction from 'components/Header/action/LogoutAction';
import LocationChange from 'components/Header/action/LocationChange';
import LogOutStore from 'components/Header/store/LogOutStore';
import FetchLocationAction  from 'components/SubmitProperty/action/GetLocalityAction';
import LocationStore from 'components/SubmitProperty/store/LocalityStore';
import NewInCityAction from 'components/MarketWatch/action/NewInCityAction';
import onAreaChange from 'components/Header/action/AreaSerachChange';
import AreaChangeStore from 'components/Header/store/AreaChangeStore';
import NearChangeStore from 'components/Header/store/NearChangeResponse';
import nearChanged from 'components/Header/action/NearLocation';
import HomeAction from 'components/Home/action/HomeAction';
import LocationChnageStore from 'components/Header/store/LocationChangeStore';
import localAction from 'components/Home/action/fetchLocal';



var Header = React.createClass({
	  getInitialState:function(){
      return {
      	 bodyClass:"",
      	 headerDisplay:false,
      	 userLoggedIn:false,
      	 userDetails:{},
      	 locationList:[],
         locationHeader:"Delhi",
				 areaCkecked:localStorage.area_unit=="s_yd"?true:false
      }
	  },
    componentWillMount:function(){
    	 this.headerToggle();
       FetchLocationAction.locality();
       LocationStore.bind(this.locationResponseReceived);
    	 LoginStatusStore.bind(this.loginStatusResponseReceived);
    	 UserDetailsStore.bind(this.userDetailsResponseReceived);
    	 LogOutStore.bind(this.logOutResponseReceived);
			 AreaChangeStore.bind(this.areaResponse)
			 NearChangeStore.bind(this.nearLocationResponse)
			 LocationChnageStore.bind(this.locChnage)
    },
		locChnage:function(){
				this.state.locationHeader = localStorage.tempData
				this.setState({
					locationHeader:this.state.locationHeader
				})
		},
    componentDidMount:function(){
			if(localStorage.loacalLocation){
				HomeAction.homeList({type:"residential",location:localStorage.loacalLocation})
			}
			else {
				getLocation()
			}

       function getLocation() {
            if (navigator.geolocation) {
							  console.log("Capturing geo loaction");
                navigator.geolocation.getCurrentPosition(showPosition,errorCallback,{timeout: 3000})
            }
						else
						{
                alert("Location not supported by browser")
								localStorage.loacalLocation = localStorage.loacalLocation?localStorage.loacalLocation:1;
								HomeAction.homeList({type:"residential",location:localStorage.loacalLocation})
            }
        }
        function showPosition(position){
					console.log("Error capturing geo loaction");
						if(!localStorage.loacalLocation){
							nearChanged.loactionStatus({lat:position.coords.latitude,long:position.coords.longitude})
						}
						else
						{
							HomeAction.homeList({type:"residential",location:localStorage.loacalLocation})
						}
        }
				 function errorCallback() {
					localStorage.loacalLocation = localStorage.loacalLocation?localStorage.loacalLocation:1;
					HomeAction.homeList({type:"residential",location:localStorage.loacalLocation})
				}
    },
    locationResponseReceived:function(){
       var locationResponse = LocationStore.getResponse().data;
			 for(let  k =0;k<locationResponse.length;k++){
				 if(locationResponse[k].id == localStorage.loacalLocation){
					 this.state.locationHeader = locationResponse[k].name
					 this.setState({
						 locationHeader:this.state.locationHeader
					 })
				 }
			 }
			 localStorage.loacalLocation
       this.setState({
       	 locationList:locationResponse
       })
    },
    logOutResponseReceived:function(){
       var logOutResponse = LogOutStore.getResponse();
       if(logOutResponse.status == 200){
       	  localStorage.setItem('isLoggedIn',false);
       	   this.setState({
       	   	  userLoggedIn:false
       	   })
					 hashHistory.push('/login');
       }
    },
    userDetailsResponseReceived:function(){
       var userResponse = UserDetailsStore.getResponse();
       localStorage.setItem('userDetails',JSON.stringify(userResponse.data));
       this.setState({
       	   userDetails:userResponse
       })
    },
    loginStatusResponseReceived:function(){
      var loginStatusResponse = LoginStatusStore.getResponse();
      localStorage.setItem('isLoggedIn',loginStatusResponse.data['logged-in']);
      if(loginStatusResponse.data['logged-in']){
      	 this.setState({
      	  	userLoggedIn:true
      	 })
      }else{
      	 this.setState({
      	  	userLoggedIn:false
      	 })
      }

    },
    componentWillUnmount:function(){
        LoginStatusStore.unbind(this.loginStatusResponseReceived);
        UserDetailsStore.unbind(this.userDetailsResponseReceived);
				AreaChangeStore.unbind(this.areaResponse)
				NearChangeStore.unbind(this.nearLocationResponse)
				LocationChnageStore.unbind(this.locChnage)
    },
		nearLocationResponse:function(){
			console.log("Near location response recieved");
			var data = NearChangeStore.getResponse().data[0]
			this.state.locationHeader = data.name
			localStorage.loacalLocation = data.id
			this.setState({
				locationHeader:this.state.locationHeader
			})
			setTimeout(function(){ HomeAction.homeList({type:"residential",location:localStorage.loacalLocation});},500);
		},
    componentWillReceiveProps:function(){
    	this.headerToggle();
    },
    logout:function(){
       LogoutAction.logout();
    },
    headerToggle:function(){
        if(this.props.tabKey == 3 || this.props.tabKey == 4 || this.props.tabKey == 5  || this.props.tabKey == 7 || this.props.tabKey == 8 || this.props.tabKey == 10 || this.props.tabKey == 11 || this.props.tabKey == 6 || this.props.tabKey == 12){
            $('#wrapper').addClass('navsearch-bd');
            this.state.headerDisplay = false;
    	}
    	else{
            $('#wrapper').removeClass('navsearch-bd');
            this.state.headerDisplay = true;
    	}
    	this.setState({
    		headerDisplay:this.state.headerDisplay
    	})
    },
    showLogin:function(){
      var userData = JSON.parse(localStorage.getItem('userDetails'))?JSON.parse(localStorage.getItem('userDetails')):{};
      if(this.state.userLoggedIn){
      	return(
  	      		  <li className="logedin-li dropdown">
  		              <a  className="dropdown-toggle" id="logeddrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><span className="lnr lnr-user"></span> Welcome! {userData.fname?userData.fname:""}</a>
  		              <ul className="dropdown-menu" aria-labelledby="logeddrop">
  		                <li><Link to="profile">Profile</Link></li>
  		                <li><a onClick={this.logout}>Logout</a></li>
  		              </ul>
  	            </li>
	      )
      }else{
      	return(
  	      		  <li className="logedin-li dropdown">
  	  					  <Link to="/login"><span className="lnr lnr-user"></span> Login/Signup</Link>
  	            </li>
              )
      }
    },
    changeLocation:function(value,name){
			localStorage.tempData = name
      this.state.locationHeader = name;
      this.setState({
         locationHeader:this.state.locationHeader
      })
			LocationChange.loactionStatus(value)
    },
    showHeader:function(){
      if(this.state.headerDisplay){
      	return(<SubHeader1 />)
      }
      if(!this.state.headerDisplay){
      	return(<SubHeader2 />)
      }
    },
    showPopularCities:function(){
      var locationRespList = this.state.locationList;
      var locationRespArr = [];
      for (var i = 0, len = locationRespList.length; i < len; i++) {
      	locationRespArr.push(<li className="col-sm-3" key={i} onClick={this.changeLocation.bind(this,locationRespList[i].id,locationRespList[i].name)}><a href="javascript:;">{locationRespList[i].name}</a></li>)
      }
      return(locationRespArr)
    },
		getAreaList:function(event,data){
				onAreaChange.AreaStatus($('#areaSelect').prop("checked"))
		},
		areaResponse:function() {

		},

  	render:function(){
  		return(
  			  <header className="navbar-fixed-top no-print">
  				  <div className="top-head">
  				    <div className="container">
  				      <div className="row">
  				        <div className="col-sm-4 measrue-check-wrap">
  				          <span className="pull-left">M<sup>2</sup></span>
  				          <label className="pull-left switch-light" >
  				            <input type="checkbox" id="areaSelect" onChange={this.getAreaList.bind(this,"ddd")} defaultChecked={this.state.areaCkecked}/>
  				            <span className="well">
  				              <a className="btn btn-primary"></a>
  				            </span>
  				          </label>
  				          <span className="pull-left">SQ FT.</span>
  				          <div className="pull-left top-location-sel dropdown">
  				            <span className="lnr lnr-map-marker"></span>
  				            <a href="javascript:;"  className="dropdown-toggle" id="locndrop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">{this.state.locationHeader}<span className="bs-caret"><span className="caret"></span></span></a>
  				              <div className="dropdown-menu" aria-labelledby="locndrop">
  				                <div className="row">
  				                  <div className="col-md-12">
  				                    <h4>Popular Cities</h4>
  				                    <ul className="row">
  				                       {this.showPopularCities()}
  				                    </ul>
  				                  </div>
  				                </div>
  				              </div>
  				          </div>
  				        </div>
  				        <div className="col-sm-4 col-xs-6 topmidbox">
  				          <a href="javascript:;"><span className="lnr lnr-phone-handset"></span> +91 9876543210</a>
  				          <span className="topsocial">
  				            <a href="https://www.facebook.com/Wynvent-1449220591787881/" target="_blank"><i className="fa fa-facebook" aria-hidden="true"></i></a>
  				            <a href="https://plus.google.com/communities/110538009573599069288" target="_blank"><i className="fa fa-google-plus" aria-hidden="true"></i></a>
											<a href="https://twitter.com" target="_blank"><i className="fa fa-twitter" aria-hidden="true"></i></a>
  				          </span>
  				        </div>
  				        <div className="col-sm-4 col-xs-6 logtopbox">
  				          <ul className="pull-right">
  				            {this.showLogin()}
  				          </ul>
  				        </div>
  				      </div>
  				    </div>
  				  </div>
              { this.showHeader()}
              </header>
              )
	}

 })
module.exports = Header;
