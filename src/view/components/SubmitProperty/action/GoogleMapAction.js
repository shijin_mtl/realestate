import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/SubmitProperty/constants/SubmitPropertyConstant';
import config from 'utils/config';
import axios from 'axios';

var fetchCityName = function(){

}


fetchCityName.prototype = {
	 mapLocation:function(parameters){
		axios.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + parameters.lat + ',' + parameters.lng + '&sensor=true')
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.GOOGLE_MAP_RESPONSE_RECEIVED,
				        data: response
				});
		    })
		    .catch(function (error) {
		      
            });

	 }
}
module.exports = new fetchCityName();
