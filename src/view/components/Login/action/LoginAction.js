import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Login/constants/LoginConstant';
import config from 'utils/config';
import axios from 'axios';
import AuthService from 'services/AuthService';
var onLogin = function(){

}

const loginApi = axios.create({
  timeout: 10000,
  withCredentials: true,
  headers:{
  	crossDomain:true
  }
});

onLogin.prototype = {
	 login:function(parameters){
		loginApi.post(config.server + 'api/v1/login/', parameters)
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.LOGIN_RESPONSE_RECIEVED,
				        data: response
				});
		    })
		   .catch(function (error) {
		      AppDispatcher.dispatch({
				        actionType: Constants.LOGIN_ERROR_RESPONSE_RECEIVED,
				        data: error
				});
            });

	 }

}


module.exports = new onLogin();
