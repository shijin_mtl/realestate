import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/AddPremium/constant/PayConstant';
import config from 'utils/config';
import axios from 'axios';
import AuthService from 'services/AuthService';
var PaymentAction = function(){

}

const addressApi = axios.create({
  withCredentials: true,
  headers:{
  	crossDomain:true
  }
});

PaymentAction.prototype = {
	 getList:function(parameters){
		addressApi.post(config.server + 'api/v1/payment/',parameters)
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.PAY_RESPONSE_RECIEVED,
				        data: response
				});
		    })
		   .catch(function (error) {
                console.log(error);
            });
	 },

   getListPost:function(parameters){
		addressApi.post(config.server + 'api/v1/payment/go/?Amount=15000&billing_address='+parameters)
		    .then(function (response) {
			    AppDispatcher.dispatch({
				        actionType: Constants.PAYMENT_RESPONSE_RECIEVED,
				        data: response
				});
		    })
		   .catch(function (error) {
         AppDispatcher.dispatch({
               actionType: Constants.OTP_RESPONSE_RECIEVED,
               data: error
       });
            });
	 }


}

module.exports = new PaymentAction();
