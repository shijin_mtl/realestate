import React from 'react';
import BackGroundImage from 'public/images/no-photo.jpg';
import {Gmaps,Marker} from 'react-gmaps';
import {hashHistory} from 'react-router';
var getPriceFormat = require('utils/ChangePrice.js').getPriceFormat;



var NewInCitySingle = React.createClass({
  showPropertyDetail:function(){
      hashHistory.push({
        pathname:"/propertyDetails",
        query:{id:this.props.data.id}
      })
  },
    render:function(){
    	return(
             <div className="col-sm-6 col-md-4 pplrlistsep">
                 <div className="proplistbox ih-item square effect10 bottom_to_top">
                   <a onClick={this.showPropertyDetail} className="propanchr">
                     <div className="propbandred">{this.props.data.property_for}</div>
                     <div className="prop-thumb" style={{'backgroundImage': this.props.data.thumbnail_url?`url(${this.props.data.thumbnail_url})`:`url(${BackGroundImage})`}}> </div>
                     <div className="prop-infobox">
                       <h3>{this.props.data.name}</h3>
                       <h4>{this.props.data.locality}</h4>
                       <h5><span className="pull-left">{this.props.data.owner_name}</span><br className="clearfix"/></h5>
                     </div>
                     <div className="prop-price">
                       <h2><i className="fa fa-inr" aria-hidden="true"></i>{this.props.data.expected_price?getPriceFormat(this.props.data.expected_price)+"/-":"NOT MENTIONED"}</h2>
                     </div>
                     <div className="info">
                     <Gmaps
                         width={'100%'}
                         height={'100%'}
                         lat={this.props.data.coordinates.lat}
                         lng={this.props.data.coordinates.lng}
                         zoom={12}
                         scrollwheel={false}
                         >
                           <Marker
                             lat={this.props.data.coordinates.lat}
                             lng={this.props.data.coordinates.lng}
                             />
                     </Gmaps>
                       <div className="hvr-btn-map"><button className="btn btn-red" onClick={this.showPropertyDetail}>Read more</button></div>
                     </div>
                   </a>
                 </div>
             </div>
    	)
    }
});
module.exports = NewInCitySingle;
