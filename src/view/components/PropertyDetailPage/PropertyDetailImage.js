import React from 'react';
import images1 from  'public/images/noimage.png';
var Carousel = require('react-responsive-carousel').Carousel;
import 'react-responsive-carousel/lib/styles/carousel.css';
var PropertyDetailImage = React.createClass({
  onChange:function(){

  },
  onClickItem:function(){

  },
  onClickItem:function(){

  },
  componentDidMount:function(){

  },
  showImageCarousel:function(){
     if(this.props.imageList.images && this.props.imageList.images.length >= 0){
          var imgRespList = this.props.imageList.images;
          var imageDisplayList = [];
          for (var i = 0, len = imgRespList.length; i < len; i++) {
             imageDisplayList.push(
                <div key={i}>
                    <img src={imgRespList[i].image} />
                </div>)
          }
          return(<Carousel showArrows={true} onChange={this.onChange} onClickItem={this.onClickItem} onClickThumb={this.onClickThumb} showIndicators={false}>
                     {imageDisplayList}
                 </Carousel>
               )
     }else {
       return(<span/>)
     }

  },
  render:function(){

    return(
           <span className="no-print">
             {this.showImageCarousel()}
           </span>

    )
  }
});
module.exports = PropertyDetailImage;
