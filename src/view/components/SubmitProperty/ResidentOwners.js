import React from 'react';
import { Radio } from 'antd';
const RadioGroup = Radio.Group; 
var ResidentOwners = React.createClass({
   onChange:function(key,event){
    this.props.changeResidentOwner(key,event.target.value)
   },
   render:function(){
   	 return(
   	 	    <div className="property-basic-form">
                    <h4 className="form-title-left-border">Owners Residence</h4>
                    <div className="personal-info-form">
                        <div className="row">
                            <div className="col-md-9 col-sm-12">
                                <div className="form-group">
                                    <label  className="col-md-2 control-label link-text">Resides in</label>
                                    <div className="col-md-6 col-sm-12">
                                          <RadioGroup onChange={this.onChange.bind(this,"owners_residence")} value={this.props.residentialOwners.owners_residence}>
                                            <Radio value={"same_premise"}>Same premise</Radio>
                                            <Radio value={"away"}>Away</Radio>
                                        </RadioGroup>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
   	 )
   }
});
module.exports = ResidentOwners;