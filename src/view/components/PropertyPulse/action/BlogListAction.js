import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/PropertyPulse/constants/PropertyPulseConstants';
import config from 'utils/config';
import axios from 'axios';
var BlogAction = function(){

}

const blogyApi = axios.create({
  withCredentials: true,
});

BlogAction.prototype = {
	getList:function(params){
		blogyApi.get(config.server + 'api/v1/blog-by-category/'+params+'/')
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.BLOG_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}


module.exports = new BlogAction();
