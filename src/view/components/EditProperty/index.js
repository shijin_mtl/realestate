import React from 'react';
import BreadCrumb from './BreadCrumb';
import PropertyDetailAction from 'components/PropertyDetailPage/action/PropertyDetailAction';
import PropertyDetailStore from 'components/PropertyDetailPage/store/PropertyDetailStore';
import PropertyBasic from './PropertyBasic';
import ResidentialConfiguration from './ResidentialConfiguration';
import ResidentialFacilities from './ResidentialFacilities';
import ResidentialAvailability from './ResidentialAvailability';
import ResidentialFinancialInfo from './ResidentialFinancialInfo';
import ResidentialTenants from './ResidentialTenants';
import ResidentialOwners  from './ResidentOwners';
import GoodToKnow from './GoodToKnow';
import Gallery from './Gallery';
import CommericialConfiguration from './CommercialConfiguration';
import CommercialArea from './CommercialArea';
import CommercialAvailability from './CommercialAvailability';
import CommercialFinanceInfo from './CommercialFinancialInfo';
import EditPropertyAction from 'components/EditProperty/action/EditPropertyAction';
import moment from 'moment';
import EditPropertyStore from 'components/EditProperty/store/EditPropertyStore';
var EditProperty = React.createClass({
	getInitialState:function(){
       return{
       	        property:{
	          	  property_for:"sale",
	          	  type:"residential_apartment",
	          	  locality:"",
	          	  zipcode:"",
	          	  city:"",
	              address:"",
	              name:"",
								latitude:"",
								longitude:""
                },
                residentialAvailability:{
                 available_from_date:moment().format('YYYY-MM-DD'),
                 available_immediately:true,
                 age_of_construction:""
                },
                residentialConfiguration:{
		          	bedroom_count:"",
		          	bathroom_count:"",
		          	balcony_count:"",
		          	parking_count_four:"",
		          	parking_count_two:"",
		          	furnished_status:"",
		          	floor_number:"",
		          	total_floor_count:"",
		          	covered_area:"",
		          	covered_area_unit:"sq_ft",
		          	no_of_open_sides:1,
		          	width_of_road_facing_plot:""

                },

                resedentialFinancialInfo:{
  		           monthly_rent:"",
  		           maintenance_charges:false,
  		           water_charges:false,
  		           other_charges:false,
  		           electricity_charges:false,
  		           security_deposit:""
                },
                residentialTenants:{
                  rent_to_bachelors:"does_not_matter",
                  rent_to_family:"does_not_matter",
                  rent_to_non_vegetarians:"does_not_matter",
                   rent_to_with_pets:"does_not_matter"

                },
                goodToKnow:{
	               interesting_details:"",
	               landmarks_and_neighbourhood:""
          		  },
                residentialOwners:{
                 owners_residence:'same_premise'
                },
                commercialConfiguration:{
                   washrooms:"",
                   parking_count_four:"",
                   parking_count_two:"",
                   furnished_status:"",
                   floor_number:"",
                   total_floor_count:"",
                   corner_shop:"",
                   pantry:"",
                   personal_washroom:"",
                   main_road_facing:"",
                   covered_area:"",
                   covered_area_unit:"sq_ft"
                },
                gallery:{
                 image_ids:[],
                 videoLink:"",
                 existingImages:[],
								 old_list:[],
								 get_old:[]
                },
                commercialArea:{
                    plot_area:"",
                    plot_area_unit:"sq_ft",
                    plot_length:"",
                    plot_length_unit:"",
                    plot_width_unit:"",
                    plot_width:"",
                    carpet_area:"",
                    carpet_area_unit:"sq_ft",
                    is_corner_plot:false
                },
                commercialAvailability:{
                  available_from_date:"2017-04-24",
                  available_immediately:true,
                  transaction_type:"new_property"
                },
                commercialFinancialInfo:{
                   price_per_sq_yard:"",
                   expected_price:"",
                   other_charges:"",
                   exclude_duty_and_reg_charges:true
                },
                isLoading:true,
                showCommercial:false,
                showResidential:true
       }
	},
	componentWillMount:function(){
	  PropertyDetailAction.propertyDetailAction({id:this.props.params.id})
    PropertyDetailStore.bind(this.propeertyDetailResponseReceived);
    EditPropertyStore.bind(this.editResponseReceived)
	},
	componentWillUnmount:function(){
      PropertyDetailStore.unbind(this.propeertyDetailResponseReceived);
      EditPropertyStore.unbind(this.editResponseReceived);

	},
  editResponseReceived:function(){
    var editResponse = EditPropertyStore.getResponse();
  },
	propeertyDetailResponseReceived:function(){
      var propertyDetailResponse = PropertyDetailStore.getResponse().data;
			if (propertyDetailResponse.coordinates != null)
			{
				var  initList = (propertyDetailResponse.coordinates)

				var lat = initList.latitude
				var long = initList.longitude
			}
      this.state.property = {property_for:propertyDetailResponse.property_for,type:propertyDetailResponse.type,locality:propertyDetailResponse.locality,zipcode:propertyDetailResponse.zipcode,
                             address:propertyDetailResponse.address,name:propertyDetailResponse.name,city:propertyDetailResponse.city,latitude:lat,longitude:long};

      if(propertyDetailResponse.type == "residential_apartment" || propertyDetailResponse.type == "residential_villa"){

        if(propertyDetailResponse.type == "residential_apartment"){
           this.setState({
             showOwnersResidence:true
          })
        }

        if(propertyDetailResponse.type == "residential_villa"){
           this.setState({
             showOwnersResidence:true
          })
        }

      	this.state.showResidential = true;
        this.state.showCommercial = false;
      	this.state.residentialConfiguration = {bedroom_count:propertyDetailResponse.bedroom_count,bathroom_count:propertyDetailResponse.bathroom_count,
      	                                       balcony_count:propertyDetailResponse.balcony_count,parking_count_four:propertyDetailResponse.parking_count_four,
      	                                       parking_count_two:propertyDetailResponse.parking_count_two,floor_number:propertyDetailResponse.floor_number,
      	                                       total_floor_count:propertyDetailResponse.total_floor_count,covered_area:propertyDetailResponse.covered_area,
      	                                       covered_area_unit:propertyDetailResponse.covered_area_unit,no_of_open_sides:propertyDetailResponse.no_of_open_sides,
      	                                       width_of_road_facing_plot:propertyDetailResponse.width_of_road_facing_plot,furnished_status:propertyDetailResponse.furnished_status}


        this.state.residentialAvailability = {available_from_date:propertyDetailResponse.available_from_date,available_immediately:propertyDetailResponse.available_immediately,
                                              age_of_construction:propertyDetailResponse.age_of_construction}

        this.state.resedentialFinancialInfo = {expected_price:propertyDetailResponse.expected_price,maintenance_charges:propertyDetailResponse.maintenance_charges,
                                                water_charges:propertyDetailResponse.water_charges,electricity_charges:propertyDetailResponse.electricity_charges,other_charges:propertyDetailResponse.other_charges,
																							exclude_duty_and_reg_charges:propertyDetailResponse.exclude_duty_and_reg_charges}

        this.state.residentialTenants = {rent_to_bachelors:propertyDetailResponse.rent_to_bachelors,rent_to_family:propertyDetailResponse.propertyDetailResponse,
                                          rent_to_non_vegetarians:propertyDetailResponse.rent_to_non_vegetarians,rent_to_with_pets:propertyDetailResponse.rent_to_with_pets}

        this.state.residentialOwners = {owners_residence:propertyDetailResponse.owners_residence}


        this.state.goodToKnow = {interesting_details:propertyDetailResponse.interesting_details,landmarks_and_neighbourhood:propertyDetailResponse.landmarks_and_neighbourhood}

      }

      if(propertyDetailResponse.type == "commercial_shop" || propertyDetailResponse.type == "commercial_showroom"){
         this.state.showResidential = false;
         this.state.showCommercial = true;
         this.state.commercialConfiguration = {washrooms:propertyDetailResponse.washrooms,parking_count_four:propertyDetailResponse.parking_count_four,
                                               parking_count_two:propertyDetailResponse.parking_count_two,furnished_status:propertyDetailResponse.furnished_status,
                                               covered_area:propertyDetailResponse.covered_area,covered_area_unit:propertyDetailResponse.covered_area_unit,
                                               corner_shop:propertyDetailResponse.corner_shop,main_road_facing:propertyDetailResponse.main_road_facing,
                                               personal_washroom:propertyDetailResponse.personal_washroom,pantry:propertyDetailResponse.pantry,floor_number:propertyDetailResponse.floor_number,
																						   total_floor_count:propertyDetailResponse.total_floor_count}

         this.state.commercialArea = {plot_area:propertyDetailResponse.plot_area,plot_area_unit:propertyDetailResponse.plot_area_unit,
                                      plot_length:propertyDetailResponse.plot_length,plot_width:propertyDetailResponse.plot_width,is_corner_plot:propertyDetailResponse.is_corner_plot,carpet_area:propertyDetailResponse.carpet_area,
																		carpet_area_unit:propertyDetailResponse.carpet_area_unit}


         this.state.commercialAvailability= {available_from_date:propertyDetailResponse.available_from_date,available_immediately:propertyDetailResponse.available_immediately,
                                             transaction_type:propertyDetailResponse.transaction_type}

         this.state.commercialFinancialInfo = {expected_price:propertyDetailResponse.expected_price,price_per_sq_yard:propertyDetailResponse.price_per_sq_yard,
                                               other_charges:propertyDetailResponse.other_charges,exclude_duty_and_reg_charges:propertyDetailResponse.exclude_duty_and_reg_charges}

				 this.state.goodToKnow = {interesting_details:propertyDetailResponse.interesting_details,landmarks_and_neighbourhood:propertyDetailResponse.landmarks_and_neighbourhood}


			}
			for (let k=0;k<propertyDetailResponse.images.length;k++){
			  this.state.gallery.old_list.push(propertyDetailResponse.images[k].id)
				this.state.gallery.get_old.push(propertyDetailResponse.images[k])
			}

		  this.setState({
				gallery:this.state.gallery
			})




      this.setState({
      	  showResidential:this.state.showResidential,
      	  property:this.state.property,
      	  residentialConfiguration:this.state.residentialConfiguration,
      	  residentialAvailability:this.state.residentialAvailability,
      	  isLoading:false,
      	  resedentialFinancialInfo: this.state.resedentialFinancialInfo,
      	  residentialTenants:this.state.residentialTenants,
      	  residentialOwners:this.state.residentialOwners,
      	  goodToKnow:this.state.goodToKnow,
          showCommercial:this.state.showCommercial,
          commercialConfiguration:this.state.commercialConfiguration,
          commercialArea:this.state.commercialArea,
          commercialAvailability:this.state.commercialAvailability,
          commercialFinancialInfo:this.state.commercialFinancialInfo,
					gallery:this.state.gallery
      })
	},

	deleteImage:function(index){
    this.state.gallery.image_ids.splice(index,1);
    this.setState({
         image_ids:this.state.image_ids
    })
  },

	oldEdit:function(index){
    this.state.gallery.get_old.splice(index,1);
		this.state.gallery.old_list.splice(index,1);
    this.setState({
         gallery:this.state.gallery
    })
  },
	pinFromMap:function(cityDetails,mapResponse,postalCode){
     this.state.property.city = cityDetails[0].long_name;
     this.state.property.locality =  mapResponse[0].address_components[1].long_name;
     this.state.property.address = mapResponse[0].formatted_address;
     this.state.property.zipcode = postalCode.address_components[0].long_name;
     this.setState({
          property:this.state.property
     })
  },
	changeResidentialFinancialInfo:function(key,value){
    this.state.resedentialFinancialInfo[key] = value?value:"";
    this.setState({
        resedentialFinancialInfo:this.state.resedentialFinancialInfo
    })
  },
	changePropertyInfo:function(key,value){
		  this.state.property[key] = value;
		  this.setState({
		  	 property:this.state.property
		  })
			if(this.state.property.type == "commercial_showroom" || this.state.property.type == "commercial_shop"){
	       this.setState({
	         showResidential:false,
	         showCommercial:true
	       })

	    }
	    if(this.state.property.type == "residential_apartment" || this.state.property.type == "residential_villa"){
	       if(this.state.property.type == "residential_apartment" ){
	          this.setState({
	             showOwnersResidence:true
	          })
	       }
	       if(this.state.property.type == "residential_villa"){
	           this.setState({
	             showOwnersResidence:false
	          })
	       }
	       this.setState({
	         showResidential:true,
	         showCommercial:false
	       })
	    }

	    if(this.state.property.property_for == "rent"){
	      this.setState({
	          showMonthlyRentInfo:true
	      })
	    }
	    if(this.state.property.property_for == "sale"){
	      this.setState({
	          showMonthlyRentInfo:false
	      })
	    }
	},

	changeMapcordinates:function(coordinates){
		this.state.property.latitude = coordinates.lat
		this.state.property.longitude = coordinates.lng
		this.setState({
			property:this.state.property
		})
	},
	changeResidentialAvailability:function(key,value){
     this.state.residentialAvailability[key] = value;
     this.setState({
        residentialAvailability:this.state.residentialAvailability
     })

    },
	changeResidentialConfigurations:function(key,value){
     this.state.residentialConfiguration[key] = value;
     this.setState({
        residentialConfiguration:this.state.residentialConfiguration
     })
    },
    changeResidentialTenants:function(key,value){
	    this.state.residentialTenants[key] = value;
	    this.setState({
	      residentialTenants:this.state.residentialTenants
	    })

    },
    changeResidentOwner:function(key,value){
	    this.state.residentialOwners[key] = value;
	    this.setState({
	        residentialOwners:this.state.residentialOwners
	    })
    },
    changeGoodToKnow:function(key,value){
     this.state.goodToKnow[key] = value;
     this.setState({
        goodToKnow:this.state.goodToKnow
     })
    },
    changePhoto:function(fileObj){
	    this.state.gallery.image_ids.push(fileObj);
	    this.setState({
	       gallery:this.state.gallery
	    })
    },
    changeCommercialConfiguration:function(key,value){
      this.state.commercialConfiguration[key] = value;
      this.setState({
          commercialConfiguration:this.state.commercialConfiguration
     })
   },
   changeCommercialArea:function(key,value){
    this.state.commercialArea[key] = value;
    this.setState({
       commercialArea:this.state.commercialArea
    })

  },
  changeCommercialFinancialInfo:function(key,value){
    this.state.commercialFinancialInfo[key] = value;
    this.setState({
        commercialFinancialInfo:this.state.commercialFinancialInfo
    })
  },
   changeCommercialAvailability:function(key,value){
    this.state.commercialAvailability[key] = value;
    this.setState({
        commercialAvailability:this.state.commercialAvailability
    })
  },
    submitProperty:function(){
     var propertyValidation = {}
     if(JSON.parse(localStorage.getItem('isLoggedIn'))){

             var parameters = {};
             var cordinates = [];
          /*   cordinates.push(this.state.mapCoordinates.lat);
             cordinates.push(this.state.mapCoordinates.lng);*/
             if(this.state.property.type == "residential_apartment" || this.state.property.type == "residential_villa"){
							 parameters.add_facilities=[]
                     if(this.state.property.property_for == "sale"){
                        delete this.state.resedentialFinancialInfo.monthly_rent;
                     }
                     if(this.state.property.property_for == "rent"){
                         delete this.state.resedentialFinancialInfo.expected_price;
                     }
               parameters =  Object.assign({}, this.state.property, this.state.residentialConfiguration,
                             this.state.residentialAvailability,this.state.resedentialFinancialInfo, this.state.residentialTenants,
                             this.state.goodToKnow,{coordinates:cordinates});
               parameters.user = JSON.parse(localStorage.getItem('userDetails')).id;
               EditPropertyAction.editProfile(parameters,this.state.gallery,this.props.params);
             }

              if(this.state.property.type == "commercial_showroom" || this.state.property.type == "commercial_shop"){
               parameters =  Object.assign({}, this.state.property, this.state.commercialConfiguration,
                             this.state.commercialAvailability,this.state.commercialFinancialInfo, this.state.commercialArea,
                             this.state.goodToKnow,{coordinates:cordinates});
               parameters.user = JSON.parse(localStorage.getItem('userDetails')).id;
               EditPropertyAction.editProfile(parameters,this.state.gallery,this.props.params);
             }

     }
     else{
            this.setState({
               showPleaseLogin:true
            })
     }
  },
	render:function(){
	 	return(
	 		    <div className="submit-property-section">
				    <div className="container">
				        <BreadCrumb />
								<canvas id="c" style={{'display':'none'}}></canvas>
				        <div className="row">
				            <div className="col-sm-12">
				                {!this.state.isLoading && <form className="submit-property-form form-horizontal">
				                    <h2 className="text-center">Showcase your property to<span className="higlight_text">1 Lac+</span> buyers</h2>
				                    <h5 className="text-center">Post now! Post for <span className="higlight_text">Free*..</span></h5>
				                    <div className="property-details-section">
				                        <div className="row">
				                            <div className="col-sm-12">
				                                <h3>PROPERTY DETAILS</h3>
				                            </div>
				                        </div>
                                         <PropertyBasic propertyBasic={this.state.property}
                                          changePropertyInfo={this.changePropertyInfo}
                                          pinFromMap = {this.pinFromMap}
                                     	    changeMapcordinates = {this.changeMapcordinates}
                                         />

                                          <div className="property-basic-form">
                                              <h4 className="form-title-left-border">Property Detail</h4>

	                                        {this.state.showResidential && <ResidentialConfiguration
	                                         configuration= {this.state.residentialConfiguration}
	                                         propertyType={this.state.property}
	                                         changeResidentialConfigurations={this.changeResidentialConfigurations}/>}



	                                           {/*this.state.showResidential && <ResidentialFacilities
	                                         changeResidentialFacility={this.changeResidentialFacility}
	                                         facility={this.state.facilities}  changeFurnitureFacility={this.changeFurnitureFacility}/>*/}


	                                        {this.state.showResidential && <ResidentialAvailability
	                                         residentialAvailability={this.state.residentialAvailability}
	                                         changeResidentialAvailability ={this.changeResidentialAvailability}/>}


                                            {this.state.showCommercial && <CommericialConfiguration
                                             configuration = {this.state.commercialConfiguration}
                                             changeCommercialConfiguration = {this.changeCommercialConfiguration}
                                             propertyType={this.state.property}/>}



                                        {this.state.showCommercial && <CommercialArea
                                        changeCommercialArea={this.changeCommercialArea} commercialArea={this.state.commercialArea}/>}


                                        {this.state.showCommercial && <CommercialAvailability
                                         commercialAvailability={this.state.commercialAvailability}
                                         changeCommercialAvailability={this.changeCommercialAvailability}/>}
                                          </div>

                                           {this.state.showCommercial && <CommercialFinanceInfo
                                          changeCommercialFinancialInfo = {this.changeCommercialFinancialInfo}
                                          commercialFinancialInfo={this.state.commercialFinancialInfo}/>}

                                          {this.state.showResidential && <ResidentialFinancialInfo
                                           resedentialFinancialInfo = {this.state.resedentialFinancialInfo}
                                           changeResidentialFinancialInfo={this.changeResidentialFinancialInfo}
                                           showMonthlyRentInfo={this.state.showMonthlyRentInfo}/>}

                                           {this.state.showResidential && <div className="personal-info-section">
	                                          <h4 className="form-title-left-border">Tenants you would rent out to</h4>
	                                          <ResidentialTenants changeResidentialTenants = {this.changeResidentialTenants}
	                                            residentialTenants={this.state.residentialTenants}/>
	                                          </div>}
	                                        {this.state.showResidential && this.state.showOwnersResidence && <ResidentialOwners residentialOwners={this.state.residentialOwners}
                                               changeResidentOwner = {this.changeResidentOwner}/>}
                                            <GoodToKnow  goodToKnow={this.state.goodToKnow} changeGoodToKnow={this.changeGoodToKnow}/>
                                            <Gallery  changePhoto={this.changePhoto} imgList = {this.state.gallery.image_ids} deleteImage={this.deleteImage} oldEdit={this.oldEdit} old_list={this.state.gallery.get_old}/>
                                            <div className="row">
    			                                <div className="col-sm-12">
    			                                    <div className="form-group cntr-algn-wrp">
    			                                        <button type="button" className="btn btn-red" onClick={this.submitProperty}>POST PROPERTY</button>
    			                                    </div>
    			                                </div>
                                    </div>
				                    </div>
				                </form>}
				            </div>
				        </div>

				    </div>
				</div>
	 	)
	}
});
module.exports = EditProperty;
