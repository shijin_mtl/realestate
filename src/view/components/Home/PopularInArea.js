import React from 'react';
import SinglePropertyList from './SinglepropertyList';
import Loading from 'components/Loading';
import ErrorScenarios from 'components/ErrorScenarios';
import {Link} from 'react-router';
var PopularInArea = React.createClass({
   handleClick:function(key){
     this.props.changeType(key)
   },
   showPopularList:function(){
    var respList = this.props.homeList;
	  var popularList = [];
	  if(respList.length > 5){
	  	respList = respList.slice(0,6);
	  }
	    if(respList.length > 0){
	    	for (var i = 0, len = respList.length; i < len; i++) {
		  		popularList.push(<SinglePropertyList  propertyList={respList[i]} key={i} />)
		    }
		    return(popularList)
	    }
	    else{
	    	   return(<ErrorScenarios message="No properties matching your search"/>)
	    }
   },
   render:function(){
   	 return(
   	 	     <section className="popularlist-wrap">
			    <div className="container">
			        <h2>POPULAR IN YOUR AREA</h2>
			        <div className="hm-prop-listbox">
			            <div className="cntr-algn-wrp">
			                <ul className="nav nav-tabs" role="tablist">
			                    <li role="presentation"  onClick={this.handleClick.bind(this,"residential")} className="active"><a href="#residentail-list" aria-controls="residentail-list" role="tab" data-toggle="tab">Residential</a>
			                    </li>
			                    <li role="presentation" onClick={this.handleClick.bind(this,"commercial")}><a href="#commercial-list" aria-controls="commercial-list" role="tab" data-toggle="tab">Commercial</a>
			                    </li>
			                </ul>
			            </div>
			            {!this.props.isLoading && <div className="tab-content">
			                <div role="tabpanel" className="tab-pane fade in active" id="residentail-list">
			                    <div className="row pplrlistrow">
			                      {this.showPopularList()}
			                    </div>
			                    {this.props.homeList.length >0 && <div className="cntr-algn-wrp"><Link to="property" className="btn btn-viewall">View All</Link>
			                    </div>}
			                </div>
			                <div role="tabpanel" className="tab-pane" id="commercial-list">
			                  <div className="row pplrlistrow">
			                     {this.showPopularList()}
			                  </div>
			                </div>
			            </div>}
			            {this.props.isLoading && <Loading />}
			        </div>
			    </div>
             </section>
   	 )
   }

});
module.exports = PopularInArea;
