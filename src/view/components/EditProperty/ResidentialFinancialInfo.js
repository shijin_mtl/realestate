import React from 'react';
import { Checkbox } from 'antd';

var ResidentialFinancialInfo = React.createClass({
   handleChange:function(key,event){
    this.props.changeResidentialFinancialInfo(key,parseInt(event.target.value));
   },
   onChange:function(key,event){
        this.props.changeResidentialFinancialInfo(key,event.target.checked);
   },
   render:function(){
   	 return(
   	 	      <div className="personal-info-section">
                <h4 className="form-title-left-border">Financial Info</h4>
                <div className="personal-info-form">
                {!this.props.showMonthlyRentInfo && <div className="row">
                    <div className="col-md-6 col-sm-12">
                        <div className="form-group">
                            <label htmlFor="" className="col-md-3 control-label link-text">Expected Price</label>
                            <div className="col-md-9 col-sm-12">
                                <input type="text" className="form-control" value= {this.props.resedentialFinancialInfo.expected_price} onChange={this.handleChange.bind(this,"expected_price")}/>
                            </div>
                        </div>
                    </div>
                </div>}
                {!this.props.showMonthlyRentInfo &&  <div className="row">
                    <div className="col-md-6 col-sm-12">
                        <div className="form-group">
                            <label htmlFor="" className="col-md-3 control-label link-text">Other charges</label>
                            <div className="col-md-9 col-sm-12">
                                <input type="text" className="form-control" value={this.props.resedentialFinancialInfo.other_charges} onChange={this.handleChange.bind(this,"other_charges")}/>
                            </div>
                        </div>
                    </div>
                </div>}
                 {!this.props.showMonthlyRentInfo &&  <div className="form-group">
                        <label  className="col-md-2 control-label hidden-sm hidden-xs">&nbsp;</label>
                        <div className="col-md-6 col-sm-12">
                            <Checkbox onChange={this.onChange.bind(this,"exclude_duty_and_reg_charges")} checked={this.props.resedentialFinancialInfo.exclude_duty_and_reg_charges?this.props.resedentialFinancialInfo.exclude_duty_and_reg_charges:false}>Stamp Duty And Registration Charges Excluded</Checkbox>
                        </div>
                 </div>}
                </div>
              </div>
   	 )
   }
});
module.exports = ResidentialFinancialInfo;
