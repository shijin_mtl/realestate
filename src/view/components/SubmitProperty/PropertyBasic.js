import React from 'react';
import { Radio ,  Select ,notification} from 'antd';
const Option = Select.Option;
import _ from 'lodash';
var RadioGroup = Radio.Group;
import {Gmaps,Marker} from 'react-gmaps';
import GoogleMapAction from 'components/SubmitProperty/action/GoogleMapAction';
import GoogleMapStore from 'components/SubmitProperty/store/GoogleMapStore';
var autocomplete;
var addressAutocomplete;

var PropertyBasic = React.createClass({
    getInitialState:function(){
      return{
              defaultLocation:"",
              mapCoordinates:{lat:"10.0159",lng:"76.3419"},
              notificationStatus:false
      }
    },
    handleChange:function(key,event){
       this.props.changePropertyInfo(key,event.target.value);
    },
    handleTypeChange:function(value){
      this.props.changePropertyInfo('type',value)

    },



    onMapCreated:function(onMapValue){
    },
    handleMapClick:function(value){
       GoogleMapAction.mapLocation({lat:value.latLng.lat(),lng:value.latLng.lng()});
       this.state.mapCoordinates = {lat:value.latLng.lat(),lng:value.latLng.lng()};
       this.props.changeMapcordinates(this.state.mapCoordinates);
       this.setState({
         mapCoordinates:this.state.mapCoordinates
       })
    },
    componentWillMount:function(){
      GoogleMapStore.bind(this.mapResponseReceived);
    },
    componentWillUnmount:function(){
      GoogleMapStore.unbind(this.mapResponseReceived);
    },
    changeLocation:function(key,event){
        this.props.changePropertyInfo(key,event.target.value);
    },
    componentDidMount:function(){
              var self = this;
  					  var options = {
  							types: ['(cities)'],
  							componentRestrictions: {country: ["in"]}
  					 	};

  						function initialize() {
        						autocomplete = new google.maps.places.Autocomplete((document.getElementById('locality')),options);
                    addressAutocomplete = new google.maps.places.Autocomplete((document.getElementById('address')), {types: ['geocode'], componentRestrictions: {country: ["in"]}});

        						google.maps.event.addListener(autocomplete, 'place_changed', function()
                    {
          						var place = autocomplete.getPlace()
          						var lat = place.geometry.location.lat();
          						var lng = place.geometry.location.lng();
                      self.state.mapCoordinates = {lat:lat,lng:lng};
                      self.props.changeMapcordinates(self.state.mapCoordinates);
                      console.log(place)
                      self.props.propertyBasic.locality = place.address_components[0].long_name
                      self.props.propertyBasic.city = place.address_components[1].long_name
                      self.props.propertyBasic.address = place.formatted_address
                      self.setState({
                        mapCoordinates:self.state.mapCoordinates
                      })
        						});



                    google.maps.event.addListener(addressAutocomplete, 'place_changed', function()
                    {
                      var place = addressAutocomplete.getPlace()
                      var lat = place.geometry.location.lat();
                      var lng = place.geometry.location.lng();
                      self.state.mapCoordinates = {lat:lat,lng:lng};
                      self.props.changeMapcordinates(self.state.mapCoordinates);
                      console.log(place)
                      self.props.propertyBasic.locality = place.address_components[0].long_name
                      self.props.propertyBasic.city = place.address_components[1].long_name
                      self.props.propertyBasic.address = place.formatted_address
                      self.setState({
                        mapCoordinates:self.state.mapCoordinates
                      })
                    });
  					}
  				 initialize();
      },

    mapResponseReceived:function(){
      var mapResponse = GoogleMapStore.getResponse();
      var locationResponse = mapResponse.data.results;
      var cityResponse;
      var postalCode;
      for (var i = 0, len = locationResponse.length; i < len; i++) {
          _.map(locationResponse[i].types,function(item){
              if(item == "locality"){
                 cityResponse = locationResponse[i].address_components;
              }
              if(item == "postal_code"){
                postalCode = locationResponse[i]
              }
          })
      }
      if(!cityResponse){
         cityResponse = [mapResponse.data.results[0].address_components[0]];
      }
       this.props.pinFromMap(cityResponse,locationResponse,postalCode);
    },
    handleSearch:function(value){
      this.props.handleSearch(value)
    },
    geolocate:function() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      },
      verethe:function() {
        alert("safdsaf")
      },

    render:function(){
     const coords = {
      lat: 10.0159,
      lng: 76.3419
     };
     document.onmozfullscreenchange = function(event) {
        alert("asdfasd")
    }
     const params = {v: '3.exp', key: 'AIzaSyAlDhorJuU5ClLfI3ut_j_Ot2JqU6qC80A'};
   	 return(
   	 	      <div className="personal-info-section">
                <h4 className="form-title">Property Basic</h4>
                <div className="property-basic-inner-form">
                    <div className="row">
                        <div className="col-md-6 col-sm-12">
                            <div className="form-group first-field clearfix">
                                <label className="col-sm-3 control-label">Property For</label>
                                <div className="col-sm-9">
                                    <RadioGroup onChange={this.handleChange.bind(this,"property_for")} value={this.props.propertyBasic.property_for}>
                                        <Radio value={'sale'}>Sale</Radio>
                                        <Radio value={'rent'}>Rent</Radio>
                                    </RadioGroup>
                                </div>
                            </div>
                            <div className="form-group">
                                <label  className="col-sm-3 control-label">Property Type</label>
                                <div className="col-sm-9">
                                 <Select defaultValue="residential_apartment" style={{ width: 343 }} onChange={this.handleTypeChange}>
                                    <Option value="residential_apartment">Residential/Apartments</Option>
                                    <Option value="residential_villa">Residential/Villas</Option>
                                    <Option value="commercial_showroom">Commercial/Showrooms</Option>
                                    <Option value="commercial_shop">Commercial/Shops</Option>
                                </Select>
                                </div>
                            </div>
                            <div className="form-group">
                              <label  className="col-sm-3 control-label">Location</label>
                              <div className="col-sm-9">
                                <input type="text" className="form-control" id="locality" value={this.props.propertyBasic.locality} onChange={this.changeLocation.bind(this,"locality")} placeholder=""/>
                                {!this.props.propertyBasicValidation.locality && <span className="help-red">Location is Required</span>}
                              </div>
                            </div>
                            <div className="form-group">
                              <label className="col-sm-3 control-label">City</label>
                              <div className="col-sm-9">
                                <input type="text" className="form-control" id="city" value={this.props.propertyBasic.city} onChange={this.changeLocation.bind(this,"city")}/>
                                 {!this.props.propertyBasicValidation.city && <span className="help-red">City is Required</span>}
                              </div>
                            </div>

                             <div className="form-group">
                                  <label className="col-sm-3 control-label">Name</label>
                                  <div className="col-sm-9">
                                      <input type="text" className="form-control" value={this.props.propertyBasic.name} onChange={this.handleChange.bind(this,"name")}/>
                                      {!this.props.propertyBasicValidation.name && <span className="help-red">Name is Required</span>}
                                  </div>
                                 </div>
                             <div className="form-group">
                                <label className="col-sm-3 control-label">Address</label>
                                <div className="col-sm-9">
                                    <input type="text" className="form-control" value={this.props.propertyBasic.address} onChange={this.handleChange.bind(this,"address")} id="address" placeholder="Enter the address"  onFocus={this.geolocate}/>
                                    {!this.props.propertyBasicValidation.address && <span className="help-red">Address is Required</span>}
                                </div>
                            </div>
                            <div className="form-group">
                                <label  className="col-sm-3 control-label">Pincode</label>
                                <div className="col-sm-9">
                                    <input type="text" className="form-control" value={this.props.propertyBasic.zipcode} onChange={this.handleChange.bind(this,"zipcode")}/>
                                    {!this.props.propertyBasicValidation.zipcode && <span className="help-red">Pincode is Required</span>}
                                </div>
                            </div>
                        </div>
                        <div className="col-md-5 col-sm-12 pull-right sm-pull">
                            <div className="property-map">
                                <Gmaps
                                    width={'auto'}
                                    height={'200px'}
                                    lat={this.state.mapCoordinates.lat}
                                    lng={this.state.mapCoordinates.lng}
                                    zoom={12}
                                    params={params}
                                    onMapCreated={this.onMapCreated}
                                    onClick={this.handleMapClick}
                                    >
                                      <Marker
                                        lat={this.state.mapCoordinates.lat}
                                        lng={this.state.mapCoordinates.lng}
                                        draggable={true}
                                        onDragEnd={this.handleMapClick}/>
                                </Gmaps>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
   	 )
    }
});
module.exports = PropertyBasic;
