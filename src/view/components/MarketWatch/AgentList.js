import React from 'react';
import {hashHistory} from 'react-router';
import TextTruncate from 'react-text-truncate';


var AgentList = React.createClass({
  goToDetail:function(id){
    hashHistory.push({
      pathname:"/agent-detail",
      query:{id:id}
    })
  },
    render:function(){
    	return(
        <div className="col-sm-3 padding-just">
          <div className="featured-boxlist match-item" style={{'minHeight': '302'+'px'}}>
            <div className="build-logobox">
              <div className="build-logo-vertic">
                <img src={this.props.data.logo} width="140" alt="" className="img-responsive"/>
              </div>
            </div>
            <div className="build-info">
            <a>  <h3 onClick={this.goToDetail.bind(this,this.props.data.id)}>{this.props.data.title}</h3></a>
              <ul>
                <li> projects
                  <ul>
                    <li><TextTruncate
                              line={1}
                              truncateText=""
                              text={this.props.data.project_handled}
                              textTruncateChild={<a onClick={this.goToDetail.bind(this,this.props.data.id)}>...</a>}
                          /></li>
                  </ul>
                </li>
              </ul>
              <ul>
                <li>1 Completed Project
                </li>
              </ul>
            </div>
          </div>
        </div>
    	)
    }
});
module.exports = AgentList;
