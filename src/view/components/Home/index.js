import React from 'react';
import HomeScreenBanner from './HomeScreenBanner';
import HomeScreenFilter from './HomeScreenFilter';
import PopularInArea from './PopularInArea';
import ThingsThatMakesAwesome from './ThingsThatMakesAwesome';
import PropertyPulse from './PropertyPulse';
import HomeAction from 'components/Home/action/HomeAction';
import HomeStore from 'components/Home/store/HomeStore';
import LoadingStore from 'components/Home/store/LoadingStore';
import {hashHistory} from 'react-router';
import LocationStore from 'components/SubmitProperty/store/LocalityStore';
import FetchLocationAction  from 'components/SubmitProperty/action/GetLocalityAction';
import Testimonials from './Testimonials';
import LocatoinChangeStore from 'components/Header/store/LocationChangeStore';
import config from 'utils/config';
import NearChangeStore from 'components/Header/store/NearChangeResponse';
import localAction from 'components/Home/action/fetchLocal';



var Home  = React.createClass({
   getInitialState:function(){
      return{
      	     type:"residential",
      	     homeList:[],
      	     locationList:[],
      	     isLoading:true,
      	     filterParams:{property_for:"sale",typeListArr:[],location:"",min_budget:"",max_budget:"",page:1,locality:[]},
             locationTop:localStorage.loacalLocation?localStorage.loacalLocation:1
      }
   },
    componentWillMount:function(){
      window.scrollTo(0,0);
   	  FetchLocationAction.locality();
      HomeStore.bind(this.responseReceived);
      LoadingStore.bind(this.loadingResponseReceived);
      LocationStore.bind(this.locationResponseReceived);
      $(".side-collapse").addClass('in');
      LocatoinChangeStore.bind(this.LocationChangeData)
      NearChangeStore.bind(this.nearLocationResponse)
    },
    componentWillUnmount:function(){
      HomeStore.unbind(this.responseReceived);
      LoadingStore.unbind(this.loadingResponseReceived);
      LocationStore.unbind(this.locationResponseReceived);
      LocatoinChangeStore.unbind(this.LocationChangeData)
      NearChangeStore.unbind(this.nearLocationResponse)
    },
    nearLocationResponse:function(){

    },
    locationResponseReceived:function(){
       this.state.locationList = [];
       var locationResponse = LocationStore.getResponse().data;
       for (var i = 0, len = locationResponse.length; i < len; i++){
       	 this.state.locationList.push(locationResponse[i].name)
       }
       this.setState({
       	      locationList:this.state.locationList
       })
    },
    LocationChangeData:function(){
      var location = LocatoinChangeStore.getResponse();
      this.state.locationTop = LocatoinChangeStore.getResponse();
      this.setState({
        locationTop:this.state.locationTop
      })
      var type=this.state.type;
      setTimeout(function(){
         HomeAction.homeList({type:type,location:location});
         localAction.getList()
       }, 500);

    },
   responseReceived:function(){
      var response = HomeStore.getResponse().results;
      this.setState({
      	  isLoading:false,
      	  homeList:response
      })
   },
   loadingResponseReceived:function(){
     this.setState({
     	isLoading:true
     })
   },
   componentDidMount:function(){
     var e =this
     if(localStorage.loacalLocation){
       HomeAction.homeList({type:"residential",location:localStorage.loacalLocation?localStorage.loacalLocation:1})
     }
           var options = {
             types: ['geocode'],
             componentRestrictions: {country: ["in"]}
            };
           var autocomplete;
           function initialize() {
             autocomplete = new google.maps.places.Autocomplete(
                 /** @type {HTMLInputElement} */(document.getElementById('place-pin')),
                 options);
             google.maps.event.addListener(autocomplete, 'place_changed', function() {

           var place = autocomplete.getPlace()

           var lat = place.geometry.location.lat();
           var lng = place.geometry.location.lng();
           localStorage.setItem('latitude', lat);
           localStorage.setItem('longitude', lng);

                   });
                 }

                initialize();
   },
   changeType:function(value){
      this.state.type = value;
      HomeAction.homeList({type:value,location:localStorage.loacalLocation})
   },
   changeFilters:function(key,value){

   	 if(key == "type"){
	      if (this.state.filterParams.typeListArr.indexOf(value) === -1) {
	         this.state.filterParams.typeListArr.push(value);
	      }
	      else{
	        this.state.filterParams.typeListArr.splice(this.state.filterParams.typeListArr.indexOf(value),1)
	      }
   	 }
   	 else{
            this.state.filterParams[key] = value;
   	 }
        this.setState({
		     	filterParams:this.state.filterParams
		     })
   },
   onSearch:function(){
     this.state.filterParams.latitude = localStorage.latitude
     this.state.filterParams.longitude = localStorage.longitude
     this.setState({
       filterParams:this.state.filterParams
     })
     if(localStorage.latitude =="")
     {
          this.state.filterParams.location = localStorage.loacalLocation
     }
     else {
       {
            this.state.filterParams.location = ''
            this.setState({
              filterParams:this.state.filterParams
            })
       }
     }
      hashHistory.push({
          pathname:"/property",
          query:this.state.filterParams
      })
   },
   render:function(){
   	 return(
   	 	      <span>
     	 	        <HomeScreenBanner locationList={this.state.locationList} showPop={this.props.location.query.landing}/>
                <HomeScreenFilter filterParams = {this.state.filterParams} changeFilters={this.changeFilters} onSearch={this.onSearch} locationList={this.state.locationList}/>
  			        <PopularInArea homeList={this.state.homeList} changeType={this.changeType} isLoading={this.state.isLoading}/>
                <ThingsThatMakesAwesome /><br/>
                {/*<Testimonials />*/}
                <section className="quickinfo-wrap">
      					  <div className="container">
      					    <div className="row">
      					      <div className="col-sm-4">
      					        <img src={config.s3static + "images/prop-icon.png"} alt="Properties" width="115" />
      					        <h3>2 LAKH + PROPERTIES<br/>IN YOUR AREA</h3>
      					      </div>
      					      <div className="col-sm-4">
      					        <img src={config.s3static + "images/construct-icon.png"} alt="Construction" width="115" />
      					        <h3>50+ NEW CONSTRUCTIONS<br/>IN YOUR AREA</h3>
      					      </div>
      					      <div className="col-sm-4">
      					        <img src={config.s3static + "images/client-icon.png" } alt="Construction" width="115" />
      					        <h3>30k+ HAPPY CUSTOMERS<br/>IN YOUR AREA</h3>
      					      </div>
      					    </div>
      					  </div>
                </section>
                <PropertyPulse />
   	 	      </span>
   	 )
   }
});
module.exports = Home;
