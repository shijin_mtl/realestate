import React from 'react';
var Loading  = React.createClass({
	render:function(){
		return(  
			     <div className = "loading-bar">
			      <div className='loader'>
				    <div className='loader--dot'></div>
				    <div className='loader--dot'></div>
				    <div className='loader--dot'></div>
				    <div className='loader--dot'></div>
				    <div className='loader--dot'></div>
				    <div className='loader--dot'></div>
				    <div className='loader--text'></div>
				  </div>
				 </div>
	    )
	}
	
});
module.exports = Loading;