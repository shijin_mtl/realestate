import React from 'react';
import { Form, FormGroup, Validator,TextArea, Input,required,minLength } from 'romagny13-react-form-validation';
import ChangePasswordAction from 'components/UserDashboard/action/ChangePasswordAction';
import ChangePasswordStore from 'components/UserDashboard/store/ChangePasswordStore';
import ChangePasswordErrorStore from 'components/UserDashboard/store/ChangePasswordErrorStore';
import EditProfileAction from 'components/UserDashboard/action/EditProfileAction';
import UserDetailsAction from 'components/Header/action/UserDetailsAction';
import { Radio,Modal } from 'antd';
import EditProfileStore from  'components/UserDashboard/store/EditProfileStore';
import Loading from 'components/Loading';
import ReactDOM from "react-dom";
var RadioGroup = Radio.Group;
import NoProfileImage from 'public/images/no-profile.png';
import {hashHistory} from 'react-router';
import PropertyList from './PropertyList';
import LogoutAction from 'components/Header/action/LogoutAction';
import LogOutStore from 'components/Header/store/LogOutStore';
import changeImageAction from 'components/UserDashboard/action/changeImageAction';


const TWO_MiB = 2097152;


var MyFormGroup = function (props) {
    var groupclassName = props.hasError ? 'form-group has-error' : 'form-group';
    return (
            <div className={groupclassName}>
                {props.children}
                {props.hasError && <span className="help-block">{props.error}</span>}
            </div>
    );
};

var ProfileList = React.createClass({
	getInitialState:function(){
    var userData = JSON.parse(localStorage.userDetails)
      return{
      	      model:{
                 old_password:"",
                 new_password:"",
                 confirm_password:""
              },
              showErrorMessage:false,
              changePasswordSucessResponse:false,
              editProfileModel:{
              	name:userData.fname +" "+userData.lname,
              	phone:userData.phone,
              	email:userData.email,
              	type:"buyer_owner",
              	about_me:userData.about_me
              },
              changePasswordErrorResponse:{},
              openModal:{changePassword:false,editProfile:false},
              passwordBtn:false,
              imagePreviewUrl:"public/images/no-profile.jpg"


      }
	},

  componentWillReceiveProps:function(){
      this.state.imagePreviewUrl = this.props.userDetails.image_url
  },

	componentWillMount:function(){
      ChangePasswordStore.bind(this.changePasswordResponseReceived);
      ChangePasswordErrorStore.bind(this.changePasswordErrorResponse);
      EditProfileStore.bind(this.editProfileResponseReceived);
      LogOutStore.bind(this.logOutResponseReceived);

	},

	componentWillUnmount:function(){
	  ChangePasswordStore.unbind(this.changePasswordResponseReceived);
	  ChangePasswordErrorStore.unbind(this.changePasswordErrorResponse);
    LogOutStore.unbind(this.logOutResponseReceived);
	},

  logOutResponseReceived:function(){
     var logOutResponse = LogOutStore.getResponse();
     if(logOutResponse.status == 200){
        localStorage.setItem('isLoggedIn',false);
         hashHistory.push('/login');
     }
  },
	changePasswordErrorResponse:function(){
      var changePasswordErrorResponse = ChangePasswordErrorStore.getResponse();
      this.setState({
      	   changePasswordErrorResponse:changePasswordErrorResponse
      })
      var datamsg = changePasswordErrorResponse.data
      var errorMsg  = ""
      try {
            errorMsg = errorMsg + datamsg.old_password[0]
      } catch (e) {
      }
      try {
          errorMsg = errorMsg + datamsg.new_password[0]
      } catch (e) {
      }
      Modal.error({
        title: errorMsg,
        okText  :'Ok'
     });
     ReactDOM.findDOMNode(this.refs.ModalClose).click()
     this.setState({
       passwordBtn:false
     })
	},
	changePasswordResponseReceived:function(){
      var changepasswordResponse = ChangePasswordStore.getResponse();
      this.setState({
      	  changePasswordSucessResponse:true
      })
      ReactDOM.findDOMNode(this.refs.ModalClose).click()
      Modal.success({
        title: 'Password changed',
        okText  :'Ok'
     });
     this.state.model ={
        old_password:"",
        new_password:"",
        confirm_password:""
     }
     this.setState({
       passwordBtn:false,
       model:this.state.model
     })
     LogoutAction.logout();


	},
	editProfileResponseReceived:function(){
      var editProfileResp = EditProfileStore.getResponse();
      UserDetailsAction.userDetails();
       var self = this
       setTimeout(function(){  ReactDOM.findDOMNode(self.refs.domClose).click()}, 500);

	},
	onSubmit:function(result){
        if(!result.hasError) {
        	if(result.model.new_password == result.model.confirm_password){
               ChangePasswordAction.changePassword(result.model);
               this.setState({
                 passwordBtn:true
               })
        	}
        	else{
                   this.setState({
                   	  showErrorMessage:true
                   })
        	}


        }
	},
	password:function(value){
      var format = /^((?!.*[\s])(?=.*[!@#$%^&*()_+])(?=.*[0-9]).{8,15})/;
      if(!format.test(value)){
        return{
           name:"password",
           error:'password should contain a special character and a number'
        }

      }
    },
    number:function(value){
      if(value.length!=13 || /^\d+$/.test(value)){
            return{
                name:'number',
                error:"Invalid number"
            }
      }
    },
    email:function(value){
        var atpos = value.indexOf("@");
        var dotpos = value.lastIndexOf(".");
        if (atpos<1 || dotpos<atpos+2 || dotpos+2>=value.length) {
            return{
                name: 'email',
                error: 'Invalid email address'
            }
        }
    },
    editProfile:function(result){
       if(!result.hasError){
       	  result.model.type = this.state.editProfileModel.type;
          var parameters = {};
          parameters =  Object.assign({}, this.state.property);
          parameters.user = JSON.parse(localStorage.getItem('userDetails')).id;
          parameters.image_url = this.state.imagePreviewUrl
          EditProfileAction.editProfile(result.model,parameters);
       }


    },
    _handleImageChange:function(e){
      e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    var allowedExtension = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif', 'image/bmp'];
    if(file.size > TWO_MiB && ! allowedExtension.indexOf(file.type) > -1 )
    {
      Modal.error({
       title: 'File upload error',
       content: 'Please upload image file not more than 2MB in size',
       okText :'Ok'
     });
    }
    else {
      reader.onloadend = () => {
        this.setState({
          file: file,
          imagePreviewUrl: reader.result,
        });
      }
      reader.readAsDataURL(file)
       var parameters = {};
       parameters =  Object.assign({}, this.state.property);
       parameters.user = JSON.parse(localStorage.getItem('userDetails')).id;
       changeImageAction.submitImage(parameters,file,this.props.userDetails);
    }
  },
    changeType:function(event){
      this.state.editProfileModel.type = event.target.value;
      this.setState({
      	 editProfileModel:this.state.editProfileModel
      })
    },
    handleChange:function(event){
    	this.state.editProfileModel['about_me'] = event.target.value;
    	this.setState({
    		 editProfileModel:this.state.editProfileModel
    	})
    },
    handleClick:function(key){
       this.state.openModal[key] = true;
       this.setState({
       	  openModal:this.state.openModal
       })
    },
    onCloseModal:function(key){


    },

    onChangeResponsePageNum:function(value){
      this.props.onChangeResponsePageNum(value);
    },
    changeShortListProperty:function(pageNum){
      this.props.changeShortListProperty(pageNum);
    },
    deleteProperty:function(id,index){
       this.props.deleteProperty(id,index);
    },
    EditAssociate:function(type){
      hashHistory.push({
        pathname:"/"+type,
        query:{id:this.props.userDetails.user_type_id}
      })
    },
    render:function(){
    	var validation = {
    		password : this.password,
    		number:this.number,
    		email:this.email
    	}

    	if(this.props.isLoading){
           return(<Loading />)
    	}

    	else{
    		return(
    		   <span>
	    		    <div className="row profile-topwrap">
			            <div className="col-sm-8">
			                <div className="row">
			                    <div className="col-sm-3 col-xs-4 prof-thumb-wrap">
			                        <div className="profile-thumb-box">
			                            <img src={this.state.imagePreviewUrl?this.state.imagePreviewUrl:"images/no-profile.png"} alt="Profile" className="img-responsive" />
			                            <label className="btn-bs-file btn">Add a photo
			                                <input type="file" onChange={(e)=>this._handleImageChange(e)} ref='profileImage'/>
			                            </label>
			                        </div>
			                    </div>
			                    <div className="col-sm-9 col-xs-8 profl-info-wrap">
			                        <h2>{this.props.userDetails.fname}{'\u00A0'}{this.props.userDetails.lname}</h2>
			                        <div className="profile-continf">
			                            <a href={"tel:"+this.props.userDetails.phone}><span className="lnr lnr-phone-handset"></span> {this.props.userDetails.phone}</a>
			                            <a href={"mailto:"+this.props.userDetails.email} className="profl-mail"><span className="lnr lnr-envelope"></span> {this.props.userDetails.email}</a>
			                        </div>
			                       <i> <p className="profl-abt">{this.props.userDetails.about_me}</p></i>
			                    </div>
			                </div>
			            </div>
			            <div className="col-sm-4 profl-cnt-lnks">
			                <p><a href="#" onClick={this.handleClick.bind(this,"editProfile")} className="redlnkuppr" data-toggle="modal" data-target="#edit-profile-pop">Edit Profile</a>
			                </p>
			                <p><a href="#" onClick={this.handleClick.bind(this,"changePassword")} className="redlnkuppr" data-toggle="modal" data-target="#changepass-profile-pop">Change password</a>
			                </p>
                      <p><a onClick={this.EditAssociate.bind(this,this.props.userDetails.type)} className="redlnkuppr">{this.props.userDetails.type=="builder"?"EDIT BUILDER DETAILS":this.props.userDetails.type=="agent"?"EDIT AGENT DETAILS":""}</a>
			                </p>
			            </div>
	                </div>
                    <hr className="sepline" />
                    <PropertyList shortListedList={this.props.shortListedList}
                       myListings={this.props.myListings} myResponseList={this.props.myResponseList}
                       onChangeResponsePageNum={this.onChangeResponsePageNum} changeShortListProperty={this.changeShortListProperty}
                       deleteProperty = {this.deleteProperty}/>
                     {/*<Modal open={this.state.openModal.changePassword} onClose={this.onCloseModal.bind(this,"changePassword")} little>
					                <div className="modal-header">
					                    <h4 className="modal-title" id="myModalLabel">Change Password</h4>
					                </div>
					                <div className="modal-body">
					                    <div className="pop-form-box">
					                        <Form onSubmit={this.onSubmit}  model={this.state.model} >
						                        <Validator validators={[required('Old Password is required'), minLength(8) ,validation['password']]}>
												     <MyFormGroup>
												      <Input type="password" id="old_password" name="old_password" value={this.state.model.old_password} className="form-control"   placeholder="Old Password"/>
												     </MyFormGroup>
												</Validator>


												<Validator validators={[required('New Password is required'), minLength(8) ,validation['password']]}>
												     <MyFormGroup>
												      <Input type="password" id="new_password" name="new_password" value={this.state.model.new_password} className="form-control"  placeholder="New Password"/>
												     </MyFormGroup>
												</Validator>

												<Validator validators={[required('Confirm Password is required'), minLength(8) ,validation['password']]}>
												     <MyFormGroup>
												      <Input type="password" id="confirm_password" name="confirm_password" value={this.state.model.confirm_password} className="form-control"  placeholder="Confirm Password"/>
												     </MyFormGroup>
												</Validator>

						                        <div className="form-group cntr-algn-wrp">
						                            <button className="btn btn-red" type="submit">Change Password</button>
						                        </div>

						                        {this.state.showErrorMessage && <span className="change-password-message">Passwords donot match</span>}
						                        {this.state.changePasswordErrorResponse.old_password && <span className="change-password-message">{this.state.changePasswordErrorResponse.old_password[0]}</span>  }
						                        {this.state.changePasswordErrorResponse.new_password && <span className="change-password-message">{this.state.changePasswordErrorResponse.new_password[0]}</span>  }
						                        {this.state.changePasswordSucessResponse && <span>Sucessfully changed password,Please Login Again to continue</span>}
						                    </Form>
					                    </div>
					                </div>
                      </Modal>*/}
	                <div className="modal fade" id="changepass-profile-pop"  role="dialog">
					    <div className="vertical-alignment">
					        <div className="modal-dialog vertical-align-center" role="document">
					            <div className="modal-content">
					                <div className="modal-header">
					                    <button type="button" className="close" ref='ModalClose' data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span className="lnr lnr-cross"></span></span>
					                    </button>
					                    <h4 className="modal-title" id="myModalLabel">Change Password</h4>
					                </div>
					                <div className="modal-body">
					                    <div className="pop-form-box">
					                        <Form onSubmit={this.onSubmit}  model={this.state.model} >
						                        <Validator validators={[required('Old Password is required')]}>
												     <MyFormGroup>
												      <Input type="password" id="old_password" name="old_password" value={this.state.model.old_password} className="form-control"   placeholder="Old Password"/>
												     </MyFormGroup>
												</Validator>


												<Validator validators={[required('New Password is required'), minLength(8) ,validation['password']]}>
												     <MyFormGroup>
												      <Input type="password" id="new_password" name="new_password" value={this.state.model.new_password} className="form-control"  placeholder="New Password"/>
												     </MyFormGroup>
												</Validator>

												<Validator validators={[required('Confirm Password is required'), minLength(8) ,validation['password']]}>
												     <MyFormGroup>
												      <Input type="password" id="confirm_password" name="confirm_password" value={this.state.model.confirm_password} className="form-control"  placeholder="Confirm Password"/>
												     </MyFormGroup>
												</Validator>

						                        <div className="form-group cntr-algn-wrp">
						                            <button className="btn btn-red" disabled={this.state.passwordBtn} type="submit">Change Password</button>
						                        </div>
						                        {this.state.showErrorMessage && <span className="change-password-message">Password Mismatch</span>}
						                    </Form>
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>


                    <div className="modal fade" id="edit-profile-pop" tabIndex="-1" role="dialog">
					    <div className="vertical-alignment">
					        <div className="modal-dialog vertical-align-center" role="document">
					            <div className="modal-content">
					                <div className="modal-header">
					                    <button type="button" className="close" ref='domClose' data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span className="lnr lnr-cross"></span></span>
					                    </button>
					                    <h4 className="modal-title" id="myModalLabel">Edit Profile</h4>
					                </div>
					                <div className="modal-body">
					                    <div className="pop-form-box">
					                        <Form onSubmit={this.editProfile}  model={this.state.editProfileModel} >

	                                           <div className="form-group usr-typ-sel">
											    <p>I am</p>
											       <RadioGroup value={this.state.editProfileModel.type} onChange={this.changeType}>
											        <Radio value={ 'buyer_owner'}>Buyer/Owner</Radio>
											        <Radio value={ 'agent'}>Agent</Radio>
											        <Radio value={ 'builder'}>Builder</Radio>
											      </RadioGroup>
											   </div>

												<Validator validators={[required( 'Name is required'), minLength(3) ]}>
												    <MyFormGroup>
												        <Input type="text" id="name" name="name" value={this.state.editProfileModel.name} className="form-control"  placeholder="Name" />
												    </MyFormGroup>
												</Validator>


												<Validator validators={[required( 'Mobile Number is required'),validation['number']]}>
												    <MyFormGroup>
												        <Input type="text" id="phone" name="phone" value={this.state.editProfileModel.phone} className="form-control"  placeholder="Mobile Number" />
												    </MyFormGroup>
												</Validator>

												<Validator validators={[required( 'Email is required'),validation['email'] ]}>
												    <MyFormGroup>
												        <Input type="text" id="email" name="email" value={this.state.editProfileModel.email} className="form-control"  placeholder="Email" />
												    </MyFormGroup>
												</Validator>
												<Validator validators={[required( 'This field is required')]}>
                          <MyFormGroup>
												    <TextArea className="form-control" id="about_me" name="about_me" placeholder="About me" value={this.state.editProfileModel.about_me}/>
                         </MyFormGroup>
                        </Validator>
	                          <div className="form-group cntr-algn-wrp">
	    											<button className="btn btn-red" type="submit">Save Profile</button>
												</div>
										    </Form>
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>
                    </div>

               </span>
    	)


    	}
    }
});
module.exports = ProfileList;
