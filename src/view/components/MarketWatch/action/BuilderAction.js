import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/MarketWatch/constants/MarketWatchConstants';
import config from 'utils/config';
import axios from 'axios';
var BuilderAction = function(){

}

const builderApi = axios.create({
  withCredentials: true,
});

BuilderAction.prototype = {
	builderList:function(params){
        AppDispatcher.dispatch({
	        actionType: Constants.LOADING_RESPONSE_RECEIVED,
	        data: "loading"
		});
		builderApi.get(config.server + 'api/v1/builder/?featured=true')
			.then(function (response) {
		        AppDispatcher.dispatch({
			        actionType: Constants.BUILDER_RESPONSE_RECEIVED,
			        data: response
				});
			})
			.catch(function (error) {
			    console.log("error...",error);
            });
	}
}


module.exports = new BuilderAction();
