import React from 'react';
import BackGroundImage from 'public/images/no-photo.jpg';
import {hashHistory} from 'react-router';
import {Gmaps,Marker} from 'react-gmaps';
import moment from 'moment';
import PopupAction from 'components/Property/action/PopupAction';
var getPriceFormat = require('utils/ChangePrice.js').getPriceFormat;




var SingleProperty = React.createClass({
    showImage:function(){
        if(this.props.propertyContents.thumbnail_url ){
            return( <div className="prop-thumb" style={{backgroundImage: `url(${this.props.propertyContents.thumbnail_url})`}} /> )
        }
        else{
            return( <div className="prop-thumb" style={{backgroundImage: `url(${BackGroundImage})`}} />)
        }
    },


    openInNewTab:function(url) {
      var win = window.open(url, '_blank');
    },


    showPropertyDetail:function(){

      this.openInNewTab("/#/propertyDetails?id="+this.props.propertyContents.id)
      PopupAction.getNow()

      {/*PopupAction.getNow()
        hashHistory.push({
          pathname:"/propertyDetails",
          query:{id:this.props.propertyContents.id},
          target:"_blank"
        })*/}
    },
    render:function(){
        var lat = 10.0159;
        var long = 76.3419;
        var createdTime =  moment(this.props.propertyContents.created).format("Do MMM YY");
          if (Object.keys(this.props.propertyContents.coordinates).length >= 0)
             {
                  lat = this.props.propertyContents.coordinates.lat
                  long = this.props.propertyContents.coordinates.lng

             }
             var price = getPriceFormat(this.props.propertyContents.expected_price);
              if(this.props.propertyContents.monthly_rent){
                price = getPriceFormat(this.props.propertyContents.monthly_rent);
              }
    	return(
    		        <div className="col-sm-12 col-md-6">
                        <div className="proplistbox ih-item square effect10 bottom_to_top">
                            <a href="javascript:;" className="propanchr">
                                <div className="propbandred">{this.props.propertyContents.property_for}</div>
                                {this.showImage()}
                                <div className="prop-infobox">
                                    <h3>{this.props.propertyContents.name?this.props.propertyContents.name:"New Builders"}</h3>
                                    <h4>{this.props.propertyContents.locality?this.props.propertyContents.locality:"Not Mentioned"}</h4>
                                    <h5><span className="pull-left">Posted by {this.props.propertyContents.owner_name}</span> <span className="pull-right">{createdTime}</span><br className="clearfix"/></h5>
                                </div>
                                <div className="prop-price">
                                    <h2><i className="fa fa-inr" aria-hidden="true"></i>{price?price:"Not Mentioned"}</h2>
                                </div>
                                <div className="info">
                                <div>
                                <Gmaps onClick={this.showPropertyDetail}
                                    width={'300px'}
                                    height={'450px'}
                                    lat={lat}
                                    lng={long}
                                    zoom={12}
                                    scrollwheel={false}
                                    viewport={true}>
                                      <Marker
                                        lat={lat}
                                        lng={long}/>
                                </Gmaps>
                                </div>
                                    <div className="hvr-btn-map">
                                        <button className="btn btn-red" onClick={this.showPropertyDetail}>Read more</button>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
    	)
    }
});
module.exports = SingleProperty;
