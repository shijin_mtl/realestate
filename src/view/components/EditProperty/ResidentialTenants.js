import React from 'react';
import { Radio } from 'antd';
const RadioGroup = Radio.Group;
var ResidentialTenants = React.createClass({
   onChange:function(key,event){
    this.props.changeResidentialTenants(key,event.target.value)
   },
   render:function(){
   	 return(
              <span>
                 <div className="personal-info-form">
                        <div className="row">
                            <div className="col-md-6 col-sm-12">
                                <div className="form-group">
                                    <label htmlFor="" className="col-md-3 control-label link-text">Bachelors</label>
                                    <div className="col-md-9 col-sm-12">


			                            <RadioGroup onChange={this.onChange.bind(this,"rent_to_bachelors")} value={this.props.residentialTenants.rent_to_bachelors}>
									        <Radio value={"yes"}>Yes</Radio>
									        <Radio value={"no"}>No</Radio>
									        <Radio value={"does_not_matter"}>Doesn’t Matter</Radio>
                                        </RadioGroup>






                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-sm-12">
                                <div className="form-group">
                                    <label htmlFor="" className="col-md-3 control-label link-text">Family</label>
                                    <div className="col-md-9 col-sm-12">


                                        <RadioGroup onChange={this.onChange.bind(this,"rent_to_family")} value={this.props.residentialTenants.rent_to_family}>
									        <Radio value={"yes"}>Yes</Radio>
									        <Radio value={"no"}>No</Radio>
									        <Radio value={"does_not_matter"}>Doesn’t Matter</Radio>
                                        </RadioGroup>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 col-sm-12">
                                <div className="form-group">
                                    <label htmlFor="" className="col-md-3 control-label link-text">Non-vegetarians</label>
                                    <div className="col-md-9 col-sm-12">

                                        <RadioGroup onChange={this.onChange.bind(this,"rent_to_non_vegetarians")} value={this.props.residentialTenants.rent_to_non_vegetarians}>
									        <Radio value={"yes"}>Yes</Radio>
									        <Radio value={"no"}>No</Radio>
									        <Radio value={"does_not_matter"}>Doesn’t Matter</Radio>
                                        </RadioGroup>


                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-6 col-sm-12">
                                <div className="form-group">
                                    <label htmlFor="" className="col-md-3 control-label link-text">With Pets</label>
                                    <div className="col-md-9 col-sm-12">

                                        <RadioGroup onChange={this.onChange.bind(this,"rent_to_with_pets")} value={this.props.residentialTenants.rent_to_with_pets}>
									        <Radio value={"yes"}>Yes</Radio>
									        <Radio value={"no"}>No</Radio>
									        <Radio value={"does_not_matter"}>Doesn’t Matter</Radio>
                                        </RadioGroup>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
              </span>
   	 )
   }
});
module.exports = ResidentialTenants;
