import React from 'react';
import PropertyList from './PropertyList'


var BasicDetails = React.createClass({
  getInitialState:function(){
    window.scroll(0,0)
    return{
      projectList:[],
      responselist:this.props.data.property
    }
  },

  render:function(){
    return(
      <span>
      <div className="col-sm-3 builder-logo">
        <img src={this.props.data.logo} className="img-responsive" alt="Agent Logo" width="240" />
      </div>
      <div className="col-sm-9">
        <h3 className="agt-bld-tottl">
          <span className="pull-left agt-bldtl">{this.props.data.name}</span>
          {this.props.data.verified && <span className="pull-right feat-ver-box verbboxonly">Verified</span>}
          {this.props.data.featured && <span className="pull-right feat-ver-box featboxonly">Featured</span>}
          <div className="clearfix"></div>
        </h3>
        <h4>Property Developed:</h4>
        <p>{this.props.data.project_handled}</p>
        <h4>Locations handled:</h4>
        <p>{this.props.data.city}</p>
        <h4>Agent Info:</h4>
        <p>{this.props.data.description}</p>
        <div className="agent-recent-prowrap">
              <h3>{this.props.list.length>0?"RECENT PROPERTIES":""}</h3>
              <div className="row pplrlistrow">
                {this.props.list}
              </div>
       </div>
      </div>
      </span>
    )
  }
})
module.exports = BasicDetails;
