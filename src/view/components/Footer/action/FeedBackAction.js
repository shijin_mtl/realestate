import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/Footer/constants/FooterConstants';
import config from 'utils/config'
import axios from 'axios';

var onFeedback = function(){

}

const feedBackApi = axios.create({
  withCredentials: true
});

onFeedback.prototype = {
	 feedback:function(parameters){
	 	feedBackApi.post(config.server  + 'api/v1/feedback/',parameters)
		    .then(function (response) {
		    	AppDispatcher.dispatch({
					actionType: Constants.FEEDBACK_RESPONSE_RECIEVED,
					data: response
				});
		   
            })
			.catch(function (error) {
			     console.log(error);
            });
		
				
	 }
}


module.exports = new onFeedback();
