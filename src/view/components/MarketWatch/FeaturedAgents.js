import React from 'react';
import AgentList from './AgentList'
import {Link,hashHistory} from 'react-router';
import AgentStore from 'components/MarketWatch/store/AgentStore';
import AgentAction from 'components/MarketWatch/action/AgentAction';



var FeaturedAgents = React.createClass({
  getInitialState:function(){
    return{
           list:[],
    }
  },
  componentWillMount:function(){
    AgentStore.bind(this.agentListRecieved)
    AgentAction.agentList()
  },
  componentWillUnmount:function(){
    AgentStore.unbind(this.agentListRecieved)
  },
  agentListRecieved:function(){
    var response = AgentStore.getResponse().results
    for(let i=0;i<response.length;i++){
      this.state.list.push(<AgentList key={i} data={response[i]}/>)
    }
    this.setState({
      list:this.state.list
    })
  },
  showMore: function(){
    hashHistory.push('/agent-list');
  },
    render:function(){
    	return(
        <section className="featured-proplst-wrap">
          <div className="container">
            <h2 className="main-ttl">Featured Agents</h2>
            <div className="featured-boxwrp">
                <div className="row">
                  {this.state.list}
                </div>
            </div>
            <div className="feat-btm-wrp"><h4 className="backline"><a onClick = {this.showMore}>Show All</a></h4></div>
          </div>
        </section>
    	)
    }
});
module.exports = FeaturedAgents;
