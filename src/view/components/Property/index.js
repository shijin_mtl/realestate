import React from 'react';
import 'public/css/bootstrap.min.css';
import 'public/css/font-awesome.min.css';
import 'public/css/linearicon.css';
import 'public/css/common.css';
import 'public/css/toggle-switch.css';
import 'public/css/bootstrap-select.min.css';
import PropertyBanner from './PropertyBanner';
import PropertyList from './PropertyList';
import PropertyAction from 'components/Property/action/PropertyAction';
import PropertyStore from 'components/Property/store/PropertyStore';
import LocalityAction from 'components/SubmitProperty/action/GetLocalityAction';
import LocationStore from 'components/SubmitProperty/store/LocalityStore';
import LoadingStore from 'components/Property/store/LoadingStore';
import {hashHistory} from 'react-router';
import LocatoinChangeStore from 'components/Header/store/LocationChangeStore';
import FooterLocationStore from 'components/Footer/store/FooterLocationStore';
import PopupAction from 'components/Property/action/PopupAction';
import PopupStore from 'components/Property/store/PopupStore';
import config from 'utils/config';
import popUpGet from 'components/Property/action/GetPopUp';
import userPopup from 'components/Property/store/UserPopStore';
import fetchLocalities from 'components/Property/action/GetLocalityAction';




var click = true


var Scroll  = require('react-scroll');
var scroll     = Scroll.animateScroll;
var Home  = React.createClass({
  popResponseReceived:function(){
    if(localStorage.viewCount>5 && this.state.data[0]){
      setTimeout(function(){ $('#modelBtnLanding').click() }, 1000);
        localStorage.viewCount = 0
    }
      this.setState({
          isLoading:false
      })
  },
   getInitialState:function(){
     return{
       data:"",
       propertyResp:{},
       propertyList:[],
       localityList:[],
       autoCompeteLocationList:[],
       isLoading:true,
       filterParameters : {pageNum:this.props.location.query.page?this.props.location.query.page:1,
                           property_for:this.props.location.query.property_for?this.props.location.query.property_for:"sale",
                           typeListArr:this.props.location.query.typeListArr?this.props.location.query.typeListArr:[],
                           bedroomCount:this.props.location.query.bedroomCount?[this.props.location.query.bedroomCount]:[],
                           bathroomCount:this.props.location.query.bathroomCount?this.props.location.query.bathroomCount:[],
                           userType:this.props.location.query.userType?this.props.location.query.userType:[],
                           min_budget:this.props.location.query.min_budget?this.props.location.query.min_budget:'',
                           max_budget:this.props.location.query.max_budget?this.props.location.query.max_budget:'',
                           location:localStorage.loacalLocation?localStorage.loacalLocation:1,
                           sort:this.props.location.query.sort?this.props.location.query.sort:"proximity",
                           availableForFamily:this.props.location.query.availableForFamily?this.props.location.query.availableForFamily:false,
                           minDepositAmount:this.props.location.query.minDepositAmount?this.props.location.query.minDepositAmount:"",
                           maxDepositAmount:this.props.location.query.maxDepositAmount?this.props.location.query.maxDepositAmount:"",
                           latitude:this.props.location.query.latitude?this.props.location.query.latitude:"",
                           longitude:this.props.location.query.longitude?this.props.location.query.longitude:"",
                           locality:this.props.location.query.locality?[this.props.location.query.locality]:[]
                         }
     }
   },
   clearClick:function(){
      this.state.filterParameters = {pageNum:this.props.location.query.page?this.props.location.query.page:1,
                          property_for:"sale",
                          typeListArr:[],
                          bedroomCount:[],
                          bathroomCount:[],
                          userType:[],
                          min_budget:"",max_budget:"",
                          location:[localStorage.loacalLocation],
                          sort:"proximity",
                          availableForFamily:false,
                          minDepositAmount:"",
                          maxDepositAmount:"",
                          latitude:"",
                          longitude:"",
                          locality:[]
                        }
      this.setState({
        filterParameters:this.state.filterParameters
      })
      var parms = this.state.filterParameters

      hashHistory.push({
          pathname:"/property",
          query:parms
      })

     setTimeout(function(){
        PropertyAction.propertyList(parms);
      }, 500);


   },
   componentWillMount:function(){

     if(localStorage.getItem('isLoggedIn')=="false")
		 {
			 hashHistory.push({
	 				pathname:"/login",
	 		})
		 }
      window.scrollTo(0,0);
      LocalityAction.locality();
      PropertyAction.propertyList(this.state.filterParameters);
      LocationStore.bind(this.localityResponseReceived);
      PropertyStore.bind(this.propertyResponseRecieved);
      LoadingStore.bind(this.loadingResponseReceived);
      $(".side-collapse").addClass('in');
      LocatoinChangeStore.bind(this.LocationChangeData);
      FooterLocationStore.bind(this.locationChangeResponseReceived);
      PopupStore.bind(this.popResponseReceived);
      userPopup.bind(this.userpopUp)
      popUpGet.getNow()
   },

   componentWillUnmount:function(){
      PropertyStore.unbind(this.propertyResponseRecieved);
      LocationStore.unbind(this.localityResponseReceived);
      LoadingStore.unbind(this.loadingResponseReceived)
      LocatoinChangeStore.unbind(this.LocationChangeData);
      FooterLocationStore.unbind(this.locationChangeResponseReceived);
      PopupStore.unbind(this.popResponseReceived);
      userPopup.unbind(this.userpopUp)
   },
   userpopUp:function(){
     var details = userPopup.getResponse().data
     this.state.data = details
     this.setState({
       data:this.state.data
     })
   },


   locationChangeResponseReceived:function(){
      var locationChangeResp = FooterLocationStore.getResponse();
      scroll.scrollToTop();
      this.state.filterParameters.location = locationChangeResp.location;
      hashHistory.push({
          pathname:"/property",
          query:this.state.filterParameters
      })
      var filterParams = this.state.filterParameters;

      setTimeout(function(){
         PropertyAction.propertyList(filterParams);
       }, 500);

   },
   LocationChangeData:function(){
      var location = LocatoinChangeStore.getResponse();
      this.state.filterParameters.location = [location];
      hashHistory.push({
          pathname:"/property",
          query:this.state.filterParameters
      })
      var filterParams = this.state.filterParameters;

      setTimeout(function(){
         PropertyAction.propertyList(filterParams);
       }, 500);

   },
   loadingResponseReceived:function(){
      this.setState({
          isLoading:true
      })
   },
   onChangePagination:function(pageNum){
      this.state.filterParameters.pageNum = pageNum;
      hashHistory.push({
          pathname:"/property",
          query:this.state.filterParameters
      })
      PropertyAction.propertyList(this.state.filterParameters);
   },

   localityResponseReceived:function(){
      this.state.autoCompeteLocationList = [];
      var localityResponse = LocationStore.getResponse().data;
       for (var i = 0, len = localityResponse.length; i < len; i++){
         this.state.autoCompeteLocationList.push(localityResponse[i].name)
       }
       {/*if (this.props.location.query.)*/}
      this.setState({
         localityList:localityResponse,
         autoCompeteLocationList:this.state.autoCompeteLocationList
      })

   },
   propertyResponseRecieved:function(){
     var propertyListResponse = PropertyStore.getResponse();
     this.setState({
         isLoading:false,
         propertyResp:propertyListResponse.data,
         propertyList:propertyListResponse.data.results
     })
   },
   changeFilters:function(value,key){
      if(key == "bedroom"){
        if (this.state.filterParameters.bedroomCount.indexOf(value) === -1) {
           this.state.filterParameters.bedroomCount.push(value);
        }
        else{
          this.state.filterParameters.bedroomCount.splice(this.state.filterParameters.bedroomCount.indexOf(value),1)
        }
        hashHistory.push({
            pathname:"/property",
            query:this.state.filterParameters
        })
        PropertyAction.propertyList(this.state.filterParameters);
      }
      if(key == "bathroom"){
        if (this.state.filterParameters.bathroomCount.indexOf(value) === -1) {
           this.state.filterParameters.bathroomCount.push(value);
        }
        else{
          this.state.filterParameters.bathroomCount.splice(this.state.filterParameters.bathroomCount.indexOf(value),1)
        }
        hashHistory.push({
            pathname:"/property",
            query:this.state.filterParameters
        })
        PropertyAction.propertyList(this.state.filterParameters);
      }
      if(key == "user_type"){

        if (this.state.filterParameters.userType.indexOf(value) === -1) {
           this.state.filterParameters.userType.push(value);
        }
        else{
          this.state.filterParameters.userType.splice(this.state.filterParameters.userType.indexOf(value),1)
        }
        this.setState({
          filterParameters:this.state.filterParameters
        })
        hashHistory.push({
            pathname:"/property",
            query:this.state.filterParameters
        })
        PropertyAction.propertyList(this.state.filterParameters);
      }
      if(key=="min_budget" || key =="max_budget"){
        this.state.filterParameters[key] = parseInt(value)?parseInt(value):"";
         hashHistory.push({
            pathname:"/property",
            query:this.state.filterParameters
        })
        PropertyAction.propertyList(this.state.filterParameters);
      }
      if(key=="location"){

        if(!Array.isArray(this.state.filterParameters.locality))
        {
          this.state.filterParameters.locality = [this.state.filterParameters.location]
        }

        if (this.state.filterParameters.locality.indexOf(String(value)) === -1)
        {
           this.state.filterParameters.locality.push(String(value));
        }
        else
        {
          this.state.filterParameters.locality.splice(this.state.filterParameters.location.indexOf(String(value)),1)
        }
        hashHistory.push({
            pathname:"/property",
            query:this.state.filterParameters
        })
        PropertyAction.propertyList(this.state.filterParameters);
      }

   },
   changeType:function(value){
      this.state.filterParameters.property_for = value;
      this.setState({
         filterParameters:this.state.filterParameters
      })
   },
   onSearch:function(){
       this.state.filterParameters.latitude = localStorage.latitude
       this.state.filterParameters.longitude = localStorage.longitude
       hashHistory.push({
          pathname:"/property",
          query:this.state.filterParameters
      })
      PropertyAction.propertyList(this.state.filterParameters);
      fetchLocalities.locality(localStorage.loacalLocation);
   },
   onChange:function(value){
      if(Array.isArray(this.state.filterParameters.typeListArr)){
        if (this.state.filterParameters.typeListArr.indexOf(value) === -1) {
           this.state.filterParameters.typeListArr.push(value);
        }
        else{
          this.state.filterParameters.typeListArr.splice(this.state.filterParameters.typeListArr.indexOf(value),1)
        }
      }
      else{
               this.state.filterParameters.typeListArr = [this.state.filterParameters.typeListArr];
                this.state.filterParameters.typeListArr.push(value);
      }
   },
   componentDidMount:function(){
                 var options = {
                   types: ['geocode'],
                   componentRestrictions: {country: ["in"]}
                  };

                 var autocomplete;
                 function initialize() {
                   autocomplete = new google.maps.places.Autocomplete(
                       /** @type {HTMLInputElement} */(document.getElementById('place-pin')),
                       options);
                   google.maps.event.addListener(autocomplete, 'place_changed', function() {

           var place = autocomplete.getPlace()

           var lat = place.geometry.location.lat();
           var lng = place.geometry.location.lng();
           localStorage.setItem('latitude', lat);
           localStorage.setItem('longitude', lng);

                   });
                 }
                initialize();
   },
   changeBudget:function(key,value){
      if(key == "location"){
        this.state.filterParameters[key] = value
      }
      if(key == "localitity"){
        this.state.filterParameters[key] = value
      }
      else{
        this.state.filterParameters[key]  = parseInt(value);
      }

      this.setState({
         filterParameters:this.state.filterParameters
      })
   },
   handleSort:function(key){
     this.state.filterParameters.sort = key;
     this.setState({
        filterParameters:this.state.filterParameters
     })
      hashHistory.push({
          pathname:"/property",
          query:this.state.filterParameters
      })
     PropertyAction.propertyList(this.state.filterParameters);
   },
   availableForFamily:function(value){
     this.state.filterParameters[value] = !this.state.filterParameters.availableForFamily;
      hashHistory.push({
          pathname:"/property",
          query:this.state.filterParameters
      })
     PropertyAction.propertyList(this.state.filterParameters);
   },
   handleDepositAmount:function(value,key){
       this.state.filterParameters[key] = value;
       this.setState({
           filterParameters:this.state.filterParameters
       })
        hashHistory.push({
          pathname:"/property",
          query:this.state.filterParameters
        })
        PropertyAction.propertyList(this.state.filterParameters);
   },
   clickedId:function(){
     if($('#ms-download').is(":checked"))
     {
       var link = document.createElement("a");
       link.download = "asdfdsfdsf";
       link.target = "_blank";
       link.href = this.state.data[0].file
       document.body.appendChild(link);
       link.click();
       document.body.removeChild(link);
       link ="";
     }
   },
   getMyList:function(){
   },
   render:function(){
   	 return(
     	 	    <span>
                 <PropertyBanner filterParams={this.state.filterParameters} changeType={this.changeType}
                   onSearch={this.onSearch} onChange={this.onChange} changeBudget={this.changeBudget}
                   autoCompeteLocationList={this.state.autoCompeteLocationList}
                   checkedFilters = {this.props.location.query} />
                 <PropertyList clearClick = {this.clearClick} getMyList={this.getMyList} propertyListResp = {this.state.propertyList} localityList={this.state.localityList} propertyResp={this.state.propertyResp}
                    onChangePagination={this.onChangePagination} onChangeFilters={this.changeFilters}  isLoading={this.state.isLoading}
                    checkedFilters = {this.props.location.query} filterParams={this.state.filterParameters} handleSort={this.handleSort}
                    availableForFamily={this.availableForFamily} handleDepositAmount={this.handleDepositAmount}/>
                    <button type="button"  className="btn btn-info btn-lg" style={{display:'none'}} id='modelBtnLanding' data-toggle="modal" data-target="#landing-pop">Open Modal</button>
                    <div className="modal fade" id="landing-pop" tabIndex="-1" role="dialog">
                    <div className="vertical-alignment">
                      <div className="modal-dialog modal-lg vertical-align-center" role="document">
                            <div className="modal-content">
                              <div className="tab-content">
                                <div className="tab-pane fade active in " id="landing-main2" style={{backgroundImage: `url(${this.state.data[0] ?this.state.data[0].image:""})`}}>
                                    <div className="modal-header" style={{'borderBottom': 0}}>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" id='closelanding'>close</button>
                                    </div>
                                    <div className="modal-body">
                                      <div className="row">
                                        <div className="col-sm-6 col-md-7">
                                          <div className="hm-bannerinfo">
                                            <h2>{this.state.data[0]?this.state.data[0].title:"Looking for a property?"} </h2>
                                            <p>{this.state.data[0]?this.state.data[0].description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis"}</p>
                                          </div>
                                        </div>
                                        <div className="col-sm-6 col-md-5 optimum-space">
                                                <label htmlFor="ms-download" >
                                                  <div className="ms-content" >
                                                    <div className="ms-content-inside">
                                                      <input type="checkbox" id="ms-download" onClick={this.clickedId} />
                                                      <div className="ms-line-down-container">
                                                        <div className="ms-line-down"></div>
                                                      </div>
                                                      <div className="ms-line-point"></div>
                                                    </div>
                                                  </div>
                                                </label>
                                        </div>
                                      </div>
                                      <a href="#landing-success" data-toggle="tab" aria-expanded="false" id="clickOn"></a>
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="landing-success">
                                  <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">close</button>
                                  </div>
                                  <div className="modal-body">
                                    <div className="succes-msgdiv">
                                      <img src={config.s3static +"images/checkred-icon.png"} width="85" className="icon-succ" alt="Success"/>
                                      <h3>THANK YOU!</h3>
                                      <h5>For sharing your information with us.</h5>
                                      <p className="midtext-suc">Our representatives will get in touch with your shortly.</p>
                                      <p>Meanwhile you can:</p>
                                      <p className="succ-lnkp"><a href="/#/">VISIT OUR HOMEPAGE</a><span className="pipe hidden-xs">|</span><a href="/#/property">SEARCH FOR A PROPERTY</a></p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                      </div>
                    </div>
                    </div>

     	 	    </span>
   	 )
   }
});
module.exports = Home;
